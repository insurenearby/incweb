package com.heartbeat.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.FactoryConfigurationError;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.json.JsonSanitizer;

/**
 * Servlet implementation class DataRequest
 */
@WebServlet(description = "Servelet to create a new proposal", urlPatterns = { "/DataRequest" })
public class DataRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(DataRequest.class.getName());
	
    /**
     * Default constructor. 
     */
    public DataRequest() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)  {
		JsonParser parser = new JsonParser();
		String inputData = "";
		String line = null;
		
		BufferedReader reader;
		
		try {
			reader = request.getReader();
			while ((line = reader.readLine()) != null){
		    	inputData = inputData + line.replaceAll("\r","").replaceAll("\n","");
		    }
			logger.info(inputData);
		    JsonObject input = parser.parse(inputData.trim()).getAsJsonObject();
		    PrintWriter out = response.getWriter();
		    out.write(getData(input).toString());
		    
		} catch (Exception e) {
			logger.error(e);
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		}
	}
	
	
	public String getAllQuotes(String lob, String productType,String input){
		JsonParser parser = new JsonParser();
		JsonObject output = new JsonObject();
		String serviceName = "getAllQuotes";
		logger.info( serviceName + "-Input:" + input+ "\r\n");
		try {
			
			JsonObject obj = parser.parse(input).getAsJsonObject();
			JsonObject auth = new JsonObject();
			String accesskey = System.getProperty("accesskey");
			String secretkey = System.getProperty("secretkey");
			String url = System.getProperty("url") + lob + "/" + productType;
			auth.addProperty("accesskey", accesskey);
			auth.addProperty("secretkey", secretkey);
			obj.add("authentication", auth);
			logger.info(obj.toString());
			//String url = "http://testapi.brokeredge.in/rest/quote/allquotes/" + lob + "/" + productType;
			
			RequestConfig config = RequestConfig.custom()
				    .setSocketTimeout(20000)
				    .setConnectTimeout(30000) // 2Min Timeout
				    .build();
			HttpClient httpclient = HttpClients.custom()
				    .setDefaultRequestConfig(config).build();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type","application/json");
			post.setEntity(new StringEntity(obj.toString()));
			HttpResponse response = httpclient.execute(post);
			String html = EntityUtils.toString(response.getEntity());
			String wellFormedJson = JsonSanitizer.sanitize(html);
			output = parser.parse(wellFormedJson).getAsJsonObject();
			// Group Quotes by Insurer
			
		} catch (Exception  e) {
			output.addProperty("error", e.getMessage());
			e.printStackTrace();			
		}
		
		return output.toString(); 
	}
	
	
	public String getData(JsonObject obj){
		JsonParser parser = new JsonParser();
		JsonObject output = new JsonObject();
		HttpClient httpclient = HttpClients.createDefault();
//		String url = "http://testapi.brokeredge.in" + obj.get("url").getAsString();
		String url = "http://testapi.brokeredge.in/rest" + obj.get("url").getAsString();
//		String url = "http://localhost:8080/BrokerEdgeAWS/rest" + obj.get("url").getAsString();
		try {
			JsonObject  data = obj.get("data").getAsJsonObject();
			JsonObject auth = new JsonObject();
			
			String accesskey = System.getProperty("accesskey");
			String secretkey = System.getProperty("secretkey");
			url = System.getProperty("url") + obj.get("url").getAsString();
			if (accesskey == null || accesskey.equalsIgnoreCase("")){
				accesskey = "wecare";
				secretkey = "wecare";
//				url = "http://localhost:8080/BrokerEdgeAWS/rest" + obj.get("url").getAsString();
//				url = "http://testapi.brokeredge.in" + obj.get("url").getAsString();
				url = "http://testapi.brokeredge.in/rest" + obj.get("url").getAsString();

				System.out.println(url);
			}
			
			auth.addProperty("accesskey", accesskey);
			auth.addProperty("secretkey", secretkey);
			data.add("authentication", auth);
			System.out.println(auth);
			
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type","application/json");
//			post.setHeader("Content-Type","application/json ; charset=UTF-8");

			post.setEntity(new StringEntity(data.toString()));
			HttpResponse response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				String html = EntityUtils.toString(entity);
				//System.out.println((html));
				String wellFormedJson = JsonSanitizer.sanitize(html);
				output = parser.parse(wellFormedJson).getAsJsonObject();
			}
			
			EntityUtils.consume(entity);
			
		} catch (JsonSyntaxException|ExceptionInInitializerError |FactoryConfigurationError | NumberFormatException | IOException  e) {
			output.addProperty("error", e.getLocalizedMessage());
			e.printStackTrace();			
		} finally
		{
			HttpClientUtils.closeQuietly(httpclient);
		}
		
		return output.toString(); 
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
		doGet(request, response);
	}

}
