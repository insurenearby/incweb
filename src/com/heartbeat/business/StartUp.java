package com.heartbeat.business;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.FactoryConfigurationError;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.json.JsonSanitizer;


/**
 * Servlet implementation class DataRequest
 */
public class StartUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(StartUp.class.getName());
	
    /**
     * Default constructor. 
     */
    public StartUp() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)  {
		JsonParser parser = new JsonParser();
		String inputData = "";
		String line = null;
		String serviceName = "Startup";
		
		
		try {
			JsonObject startUpData = startUp ();
		    
		} catch (Exception e) {
			logger.error(e);
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		}
	}
	
	
	
	
	public String getData(JsonObject obj){
		JsonParser parser = new JsonParser();
		JsonObject output = new JsonObject();
				
		try {
			
			JsonObject  data = obj.get("data").getAsJsonObject();
			JsonObject auth = new JsonObject();
			
			String accesskey = System.getProperty("accesskey");
			String secretkey = System.getProperty("secretkey");
			String url = System.getProperty("url") + obj.get("url").getAsString();
			if (accesskey == null || accesskey.equalsIgnoreCase("")){
				accesskey = "wecare";
				secretkey = "wecare";
				url = "http://testapi.brokeredge.in/rest" + obj.get("url").getAsString();
			}
			
			auth.addProperty("accesskey", accesskey);
			auth.addProperty("secretkey", secretkey);
			data.add("authentication", auth);
			System.out.println((data));
			System.out.println("starttt upp");
			//String url = "https://testapi.brokeredge.in/rest" + obj.get("url").getAsString();;
			HttpClient httpclient = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type","application/json");
			post.setEntity(new StringEntity(data.toString()));
			HttpResponse response = httpclient.execute(post);
			String html = EntityUtils.toString(response.getEntity());
			System.out.println((html));
			String wellFormedJson = JsonSanitizer.sanitize(html);
			output = parser.parse(wellFormedJson).getAsJsonObject();
			
			
		} catch (JsonSyntaxException|ExceptionInInitializerError |FactoryConfigurationError | NumberFormatException | IOException  e) {
			output.addProperty("error", e.getLocalizedMessage());
			e.printStackTrace();			
		}
		
		return output.toString(); 
	}
	
	private JsonObject startUp (){
		
		JsonObject output  = new JsonObject();
		String accesskey = System.getProperty("accesskey");
		String secretkey = System.getProperty("secretkey");
		String org = System.getProperty("wecare");
		if (accesskey == null || accesskey.equalsIgnoreCase("")){
			accesskey = "wecare";
			secretkey = "wecare";
		}
		output.addProperty("org", org);
		output.addProperty("accesskey", accesskey);
		output.addProperty("secretkey", secretkey);
		output.addProperty("rmId", "guest");
		return output;
		
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
		doGet(request, response);
	}

}
