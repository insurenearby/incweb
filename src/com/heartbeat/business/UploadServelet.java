package com.heartbeat.business;

import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.util.IOUtils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

/**
 * Servlet implementation class uploadServelet
 */
@WebServlet(name = "UploadServelet", urlPatterns = {"/fileUpload"})
@MultipartConfig
public class UploadServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = 
            Logger.getLogger(UploadServelet.class.getCanonicalName());   
	// location to store file uploaded
    private static final String UPLOAD_DIRECTORY = "upload";
	private boolean isMultipart;
	private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 2;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 2; // 40MB
	private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 10; // 50MB
	   
   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response) {
		  
	   
	      
		try {
			System.out.println("Uploading file to s3");
			HttpSession session=request.getSession(false);  
			//if(session!=null){
				// Check that we have a file upload request
			    isMultipart = ServletFileUpload.isMultipartContent(request);
			    response.setContentType("text/html");
			    PrintWriter out;
				out = response.getWriter( );
			      PrintWriter writer = response.getWriter();
		          
			      if( !isMultipart ) {
			         out.println("No file uploaded"); 
			         return;
			      }
			  
			      DiskFileItemFactory factory = new DiskFileItemFactory();
			   
			      // maximum size that will be stored in memory
			      factory.setSizeThreshold(MEMORY_THRESHOLD);
			   
			      // Location to save data that is larger than maxMemSize.
			      factory.setRepository(new File("c:\\Windows\\temp"));

			      // Create a new file upload handler
			      ServletFileUpload upload = new ServletFileUpload(factory);
			   
			      // sets maximum size of upload file
			        upload.setFileSizeMax(MAX_FILE_SIZE);
			         
			     // sets maximum size of request (include file + form data)
			        upload.setSizeMax(MAX_REQUEST_SIZE);

		         String userId = "";
		         String appId = "";
		         String docId = "";
		         String policyNo = "";
		         String spCode = "";
		         int insurerId = 0;
			         
			      try { 
			         // Parse the request to get file items.
			         List<FileItem> fileItems = upload.parseRequest(request);
				
			         // Process the uploaded file items
			         

			         //String uploadPath = getServletContext().getRealPath("")
			         //        + File.separator + UPLOAD_DIRECTORY;
			          
			         //String uploadPath = FnaConstants.uploadDir + File.separator + UPLOAD_DIRECTORY;
			         // creates the directory if it does not exist
			         //File uploadDir = new File(uploadPath);
			         //if (!uploadDir.exists()) {
			         //    uploadDir.mkdir();
			         //}
			         
			        
			         Iterator<FileItem> i = fileItems.iterator();
			         // Store pointer to current file here
			         FileItem itemFile = null;
			         while ( i.hasNext () ) {
			            FileItem fi = i.next();
			            
			            if ( !fi.isFormField () ) {
			               // Get the uploaded file parameters
			            	itemFile=fi;
			            }else
			            {
			            	System.out.println(fi.getFieldName() + ":" + fi.getString());
			            	if (fi.getFieldName().equalsIgnoreCase("userId")) {
			            		userId = fi.getString();
			            	}
			            	if (fi.getFieldName().equalsIgnoreCase("appId")) {
			            		appId = fi.getString();
			            	}
			            	if (fi.getFieldName().equalsIgnoreCase("policyNo")) {
			            		policyNo = fi.getString();
			            	}
			            	if (fi.getFieldName().equalsIgnoreCase("spCode")) {
			            		spCode = fi.getString();
			            	}
			            	if (fi.getFieldName().equalsIgnoreCase("docId")) {
			            		docId = fi.getString();
			            	}
			            	
			            	if (fi.getFieldName().equalsIgnoreCase("insurerId")) {
			            		insurerId = Integer.parseInt(fi.getString().trim());
			            	}
			            	
			            	if (fi.getFieldName().equalsIgnoreCase("fileData")) {
			            		JsonParser parser = new JsonParser();
			            		JsonObject fileData = parser.parse(fi.getString()).getAsJsonObject();
			            		System.out.println(fileData.toString());
			            	}
			            	
			            	
			            }
			         }
			         // Name format: U2132132_4606639_1111111111_1026010179_Form
			         Calendar c = Calendar.getInstance();
			         SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
			 		 dateFormat.setLenient(false);
			 		 String currdate = dateFormat.format(c.getTime());
			         if (itemFile != null) {
			        	 String fileDir = userId + "/"+ insurerId + "/" + appId;
			        	 String docName = "";
			        	 switch (insurerId) {
			        	 case 110:
			        		 docName = policyNo + "_" + spCode+ "_" + appId + "_" + docId + "_Form";
			        		 break;
			        	 case 109 :
			        		 docName = policyNo + "_000000000_" + docId + "_" + currdate;
			        		 break;
			        	 case 102 :			        		 
			        		 docName = docId + "_" + currdate;
			        		 break;
			        	 case 131: 			        		 
			        		 docName = docId;
			        		 break;
			        		 // Upload document to Apollo 
			        	 }
			        	 
			        	 writeToS3(itemFile, fileDir, docName);
			        	 response.setStatus(HttpServletResponse.SC_OK);
			         } else
			         {
			        	 out.println("File is empty"); 
			        	 logger.error("File is empty"); 
			        	 System.out.println("File is empty"); 
			        	 response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
			         }
		         } catch(Exception ex) {
		        	 logger.error(ex);
		        	 writer.println(ex.getMessage());
		        	 response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		         }
			      logger.info(docId + " :Upload done");

		          writer.println(docId + " :Upload done");
		          writer.flush();
				
		  /*} else
			{
				System.out.println("Session is null");
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			}*/

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
			      
   	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
     throws ServletException, java.io.IOException {

   		throw new ServletException("GET method used with " +
   				getClass( ).getName()+": POST method required.");
   	}
   	
   	
   	void writeToS3(FileItem itemFile, String fileDir, String fileName) throws Exception {
   		
   		//BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY);
   		String S3_BUCKET_NAME = "lastdecimal.brokeredge.applications";
   		String keyName = fileDir + "/";
   		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
				 .withCredentials(new DefaultAWSCredentialsProviderChain())
				 .withRegion(Regions.US_EAST_1)
				 .build();
   		
        ObjectMetadata om = new ObjectMetadata();
        om.setContentLength(itemFile.getSize());
        String ext = FilenameUtils.getExtension(itemFile.getName());
        keyName = keyName + fileName + '.' + ext;
        System.out.println(keyName);
        InputStream is = itemFile.getInputStream();
        String content = IOUtils.toString(is).replaceAll("\\\\\"", "\"");
        s3Client.putObject(S3_BUCKET_NAME, keyName, content);
   	}
   	
   	
   	
			   
	


}
