package com.heartbeat.business;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Servlet implementation class PostPayment
 */
@WebServlet(description = "Redirect to PG", urlPatterns = { "/RedirectToPG" })
public class RedirectToPG extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(RedirectToPG.class.getName());
	
    /**
     * Default constructor. 
     */
    public RedirectToPG() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)  {
		JsonParser parser = new JsonParser();
		String inputData = "";
		String line = null;
		
		BufferedReader reader;	
		try {
			reader = request.getReader();
			while ((line = reader.readLine()) != null){
		    	inputData = inputData + line.replaceAll("\r","").replaceAll("\n","");
		    }
			logger.info(inputData);
			 JsonObject input = parser.parse(inputData.trim()).getAsJsonObject();
			String url = input.get("url").getAsString();
			System.out.println("Redirecting to Url: " + url);
			response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", url);
		} catch (Exception e) {
			logger.error(e);
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		}
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
		JsonParser parser = new JsonParser();
		String inputData = "";
		String line = null;
		
		BufferedReader reader;	
		try {
			reader = request.getReader();
			while ((line = reader.readLine()) != null){
		    	inputData = inputData + line.replaceAll("\r","").replaceAll("\n","");
		    }
			logger.info(inputData);
			 JsonObject input = parser.parse(inputData.trim()).getAsJsonObject();
			String url = input.get("url").getAsString();
			System.out.println("Redirecting to Url: " + url);
			//RequestDispatcher view = request.getRequestDispatcher(url);
			//view.forward(request, response);
			response.sendRedirect(url);
			return;
		} catch (Exception e) {
			logger.error(e);
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		}
	}

}
