package com.heartbeat.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class CreateProposal
 */
@WebServlet(description = "Servlet to create a new proposal", urlPatterns = { "/CreateProposal" })
public class CreateProposal extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	AmazonDynamoDB dbClient = AmazonDynamoDBClientBuilder.standard()
			.withCredentials(new DefaultAWSCredentialsProviderChain())
			 .withRegion(Regions.US_EAST_1).build();
							
	protected DynamoDB dynamoDB = new DynamoDB(dbClient);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateProposal() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JsonParser parser = new JsonParser();
		String inputData = "";
		String line = null;
		
		BufferedReader reader;
		// Get Quote template
		try {
			JsonObject outObj = new JsonObject();
			reader = request.getReader();
			while ((line = reader.readLine()) != null){
		    	inputData = inputData + line.replaceAll("\r","").replaceAll("\n","");
		    }
			System.out.println(inputData);
			
			// Authenticate the Call and then create a session
			HttpSession session=request.getSession(); 
			// Call Create proposal to 
			JsonObject input = parser.parse(inputData).getAsJsonObject();
		    String redirectUrl =  createProposal(input);
			if (!redirectUrl.contentEquals("")) {
				System.out.println(redirectUrl);
				//response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
				//response.setHeader("Location", redirectUrl);
				//response.setHeader("Location", "http://www.google.com");
				outObj.addProperty("result", redirectUrl);
			} else
			{
				outObj.addProperty("error", "Error Creating Application");
				
			}
			
			PrintWriter out = response.getWriter();
			response.setStatus(HttpServletResponse.SC_OK);
		    out.write(outObj.toString());
		    
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		}
		
		// write to DB
		// Create Url and return
			
	}
	
	String createProposal(JsonObject input) throws Exception {
//		String url = "http://testapi.brokeredge.in/submitProposal/createProposal/";
		
		String accesskey = System.getProperty("accesskey");
		String secretkey = System.getProperty("secretkey");
		String url = System.getProperty("url");
		if (accesskey == null || accesskey.equalsIgnoreCase("")){
			accesskey = "wecare";
			secretkey = "wecare";
//			url = "http://localhost:8080/BrokerEdgeAWS/rest/submitProposal/createProposal/";
//		   url = "http://testapi.brokeredge.in/submitProposal/createProposal/";
		   url="http://testapi.brokeredge.in/rest/submitProposal/createProposal/";
		}
		String returnUrl = "";
	    String productId = input.get("productId").getAsString();
	    url = url + productId;
	    System.out.println(url);
	    HttpClient httpClient = HttpClients.createDefault();
	    HttpPost post = new HttpPost(url);
	    post.setHeader("Content-Type", "application/json");
	    post.setEntity(new StringEntity(input.toString()));
		HttpResponse httpresponse = httpClient.execute(post);
		HttpEntity entity = httpresponse.getEntity();
		if (entity != null) {
			returnUrl = EntityUtils.toString(entity);
			EntityUtils.consume(entity);
		}
		HttpClientUtils.closeQuietly(httpClient);
		return returnUrl;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	
	

}
