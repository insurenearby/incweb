package com.heartbeat.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.FactoryConfigurationError;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.json.JsonSanitizer;


/**
 * Servlet implementation class PostPayment
 */
@WebServlet(description = "Servelet to create a new proposal", urlPatterns = { "/PostPayment/*" })
public class PostPayment extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(PostPayment.class.getName());
	
    /**
     * Default constructor. 
     */
    public PostPayment() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)  {
		JsonParser parser = new JsonParser();
		String inputData = "";
		String line = null;
		String serviceName = "PostPayment";
		BufferedReader reader;
		
		try {
			reader = request.getReader();
			String requestUrl = request.getRequestURI().toString();
			System.out.println(requestUrl);
			String insurerId = request.getPathInfo().replaceAll("\\/", "");
			Map<String, String[]> params = request.getParameterMap(); 
			JsonObject obj = new JsonObject();
			for (Entry<String, String[]> param : params.entrySet()) {
				String key = param.getKey();
				String value = param.getValue()[0];
				obj.addProperty(key, value);
			}
			System.out.println(obj.toString());
			String url = requestUrl.replaceAll("PostPayment", "#!/PaymentConfirmation") + "/" + parsePostData(insurerId, obj);
			response.sendRedirect(url);
		    
		} catch (Exception e) {
			logger.error(e);
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
		}
	}
	
	
	private String parsePostData(String insurerId, JsonObject input) {
		
		JsonParser parser = new JsonParser();
		JsonObject output = new JsonObject();
		JsonObject data = input;
		String referenceId = "";
		HttpClient httpclient = HttpClients.createDefault();
		String url = "http://testapi.brokeredge.in/rest/payment/confirmation/" + insurerId; 
		
		try {
			
			JsonObject auth = new JsonObject();
			
			String accesskey = System.getProperty("accesskey");
			String secretkey = System.getProperty("secretkey");
			
			if (accesskey == null || accesskey.equalsIgnoreCase("")){
				accesskey = "wecare";
				secretkey = "wecare";
			/**url = "http://localhost:8080/BrokerEdge/rest/payment/confirmation/" + insurerId;*/
				url = "https://insurenearbyuinew.ap-south-1.elasticbeanstalk.com/rest/payment/confirmation/" + insurerId;
				
			}
			System.out.println(url);
			auth.addProperty("accesskey", accesskey);
			auth.addProperty("secretkey", secretkey);
			data.add("authentication", auth);
			System.out.println((data));
			
			
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type","application/json");
			post.setEntity(new StringEntity(data.toString()));
			HttpResponse response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			String html = EntityUtils.toString(entity);
			EntityUtils.consume(entity);
			System.out.println((html));
			String wellFormedJson = JsonSanitizer.sanitize(html);
			output = parser.parse(wellFormedJson).getAsJsonObject();
			if (output.has("referenceId")) {
				referenceId = output.get("referenceId").getAsString();
			}
			
		} catch (JsonSyntaxException|ExceptionInInitializerError |FactoryConfigurationError | NumberFormatException | IOException  e) {
			output.addProperty("error", e.getLocalizedMessage());
			e.printStackTrace();			
		} finally
		{
			HttpClientUtils.closeQuietly(httpclient);
		}
		
		return referenceId; 
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
		doGet(request, response);
	}

}
