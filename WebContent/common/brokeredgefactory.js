"use strict";
brokeredgeApp.factory('brokeredgefactory',['$resource','$window','$http', '$rootScope', '$sce',function($resource,$http,$window,$rootScope, $sce) {
	// body...
		
		var perimum={};
		var annualIncome=[{name:'1 Lakh',val:'100000'},{name:'2 Lakh',val:'200000'},{name:'3 Lakh',val:'300000'},{name:'4 Lakh',val:'400000'},{name:'5 Lakh',val:'500000'},{name:'6 Lakh',val:'600000'},{name:'7 Lakh',val:'700000'},{name:'8 Lakh',val:'800000'},{name:'9 Lakh',val:'900000'},{name:'10 Lakh',val:'1000000'},{name:'11 Lakh',val:'1100000'},{name:'12 Lakh',val:'1200000'},{name:'13 Lakh',val:'1300000'},{name:'14 Lakh',val:'1400000'},{name:'15 Lakh',val:'1500000'},{name:'16 Lakh',val:'1600000'},{name:'17 Lakh',val:'1700000'},{name:'18 Lakh',val:'1800000'},{name:'19 Lakh',val:'1900000'},{name:'20 Lakh',val:'2000000'},{name:'21 Lakh',val:'2100000'},{name:'22 Lakh',val:'2200000'},{name:'23 Lakh',val:'2300000'},{name:'24 Lakh',val:'2400000'},{name:'25 Lakh',val:'2500000'},{name:'26 Lakh',val:'2600000'},{name:'27 Lakh',val:'2700000'},{name:'28 Lakh',val:'2800000'},{name:'29 Lakh',val:'2900000'},{name:'30 Lakh',val:'3000000'},{name:'31 Lakh',val:'3100000'},{name:'32 Lakh',val:'3200000'},{name:'33 Lakh',val:'3300000'},{name:'34 Lakh',val:'3400000'},{name:'35 Lakh',val:'3500000'},{name:'36 Lakh',val:'3600000'},{name:'37 Lakh',val:'3700000'},{name:'38 Lakh',val:'3800000'},{name:'39 Lakh',val:'3900000'},{name:'40 Lakh',val:'4000000'},{name:'41 Lakh',val:'4100000'},{name:'42 Lakh',val:'4200000'},{name:'43 Lakh',val:'4300000'},{name:'44 Lakh',val:'4400000'},{name:'45 Lakh',val:'4500000'},{name:'46 Lakh',val:'4600000'},{name:'47 Lakh',val:'4700000'},{name:'48 Lakh',val:'4800000'},{name:'49 Lakh',val:'4900000'},{name:'50 Lakh',val:'5000000'},{name:'60 Lakh',val:'6000000'},{name:'70 Lakh',val:'7000000'},{name:'80 Lakh',val:'8000000'},{name:'90 Lakh',val:'9000000'},{name:'1 Crore',val:'10000000'}];
		var QuotesData,city;
		var state=["RAJASTHAN","PUNJAB","JHARKHAND","JAMMU AND KASHMIR","DAMAN AND DIU","DADRA AND NAGAR HAVELI","CHATTISGARH","MAHARASHTRA","MADHYA PRADESH","KERALA","KARNATAKA","ASSAM","HIMACHAL PRADESH","HARYANA","GUJARAT","DELHI","CHANDIGARH","BIHAR","ARUNACHAL PRADESH","ANDAMAN & NICOBAR ISLANDS","ANDHRA PRADESH","TELANGANA","TRIPURA","SIKKIM","PONDICHERRY","ORISSA","NAGALAND","MIZORAM","MEGHALAYA","MANIPUR","UTTARANCHAL","GOA","WEST BENGAL","UTTAR PRADESH","LAKSHDWEEP","TAMIL NADU"];
		var currentInsurer=["UNITED INDIA INSURANCE CO.LTD.","THE ORIENTAL INSURANCE CO. LTD.","THE NEW INDIA ASSURANCE CO. LTD.","TATA AIG GENERAL INSURANCE CO.LTD.","SHRIRAM GENERAL INSURANCE COMPANY LIMITED","SBI GENERAL INSURANCE COMPANY LTD","ROYAL SUNDARAM ALLIANCE INSURANCE CO.LTD.","RELIANCE GENERAL INSURANCE CO.LTD.","RAHEJA QBE GENERAL INSURANCE COMPANY LIMITED","NATIONAL INSURANCE CO.LTD.","MAGMA HDI GENERAL INSURANCE CO LTD","LIBERTY VIDEOCON GENERAL INSURANCE COMPANY LIMITED","L&T GENERAL INSURANCE COMPANY LIMITED","IFFCO TOKIO GENERAL INSURANCE CO. LTD.","ICICI LOMBARD GENERAL INSURANCE CO. LTD.","FUTURE GENERALI INDIA INSURANCE COMPANY LIMITED","CHOLAMANDALAM MS GENERAL INSURANCE CO.LTD.","BHARTI AXA GENERAL INSURANCE COMPANY LIMITED","BAJAJ ALLIANZ GENERAL INSURANCE CO.LTD"];
		var buyQuotes;
		var relation=['Spouse','Son','Sister','Self','Mother','Mother in Law','Grand Son','Grand Mother','Grand Father','Grand Daughter','Father in Law','Father','Daughter in Law','Brother'];
		var relationship=[{"key":"1","value":"Self"},{"key":"2","value":"Child"},{"key":"4","value":"Father"},{"key":"5","value":"Mother"},{"key":"6","value":"Brother"},{"key":"7","value":"Sister"},{"key":"11","value":"Partner"},{"key":"12","value":"Special Concession Adult"},{"key":"13","value":"Special Concession Child"},{"key":"14","value":"Wife"},{"key":"15","value":"Husband"},{"key":"17","value":"Daughter"},{"key":"18","value":"Father-in-law"},{"key":"19","value":"Mother-in-law"},{"key":"20","value":"Son"}]
		var maritalStatus=[{"key":"4","value":"Divorced"},{"key":"1","value":"Married"},{"key":"5","value":"Separated"},{"key":"2","value":"Single"},{"key":"3","value":"Widowed"}]
		var ocupation=[{"id":"303601","name":"ACCOUNTANT"}, {"id":"303603","name":"ACROBAT"},{"id":"303605","name":"ACTUARY"}, {"id":"303607","name":"ACUPUNCTURIST"},{"id":"303609","name":"ADMINISTRATIVE WORKER"}];
		var arrivalAtCountry={WorldWideExcludingUSandCanada:["Afganistan","Albania","Algeria","Andorra","Angola","Argentina","Armenia","Aruba","Ascension","Australia","Austria","Azerbaijan Republic","Azores","Bahrain","Bangladesh","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Boputatswana","Bosnia and Herzegovina","Botswana","Brazil","Brunei Darussalam","Bulgaria","Burkina Fasso","Burundi","Cambodia","Cameroon","Canary Island","Cape Verde","Central African Republic","Chad","Chile","China","Ciskei","Cocos Keeling Island","Colombia","Comoros","Congo","Cook Island","Costa Rica","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Diego Garcia","Djibouti","Ecuador","Egypt","El Salvador Rep.","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Island","Faroe Island","Fiji Republic","Finland","Fr.Guinea","Fr.Polynesia","France","France","Gabonese Republic","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Guadeloupe","Guam","Guatemala","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hongkong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Japan","Japan","Jordan","Kazakhstan","Kenya","Kirghistan","Kiribati","Korea (North)","Korea (South)","Kuwait","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Madeira Island","Malawi","Malaysia","Maldives","Mali","Malta","Mariana Island","Marshal Island","Martinque","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Anthilles","New Caledonia","New Zealand","Niger","Nigeria","Niue Island","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papau New Guinea","Paraguay","Peru","Phillipines","Poland","Portugal","Qatar","Reunion","Rodrigues Island","Romania","Russian Federation","Rwandese Republic","Samoa American","Samoa Western","San Marino","Sao Tome and Principe","Saudi Arabia","Schengen Countires","Senegal","Seychelles","Sierra Leone","Singapore","Slovak Republic","Slovenia","Soloman Island","Somalia Democratic Republic","South Africa","Spain","Srilanka","St.Helena","St.Pierre and Miquelon","Sudan","Suriname","Swaziland","Sweeden","Switzerland","Syria","Tadjikistan","Taiwan","Tanzania","Thailand","Togolese Republic","Tonga","Transkei","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","UGANDA","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Vanuatu","Vatican City State","Venda","Venezuela","Vietnam","Wallis and Futina Island","Yemen (Saana)","Yogoslavia","Zaire","Zambia","Zimbabwe"],Asia:["Afganistan","Armenia","Azerbaijan Republic","Bahrain","Bangladesh","Bhutan","Brunei Darussalam","Cambodia","China","Hongkong","India","Indonesia","Iraq","Israel","Jordan","Kazakhstan","Kirghistan","Korea (North)","Korea (South)","Kuwait","Laos","Lebanon","Macao","Malaysia","Maldives","Mongolia","Nepal","Oman","Pakistan","Papau New Guinea","Phillipines","Qatar","Russian Federation","Saudi Arabia","Singapore","Srilanka","Syria","Tadjikistan","Taiwan","Thailand","United Arab Emirates","Uzbekistan","Vietnam","Yemen (Saana)"],WorldWide:["Afganistan","Albania","Algeria","Andorra","Angola","Argentina","Armenia","Aruba","Ascension","Australia","Austria","Azerbaijan Republic","Azores","Bahrain","Bangladesh","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Boputatswana","Bosnia and Herzegovina","Botswana","Brazil","Brunei Darussalam","Bulgaria","Burkina Fasso","Burundi","Cambodia","Cameroon","Canada","Canary Island","Cape Verde","Central African Republic","Chad","Chile","China","Ciskei","Cocos Keeling Island","Colombia","Comoros","Congo","Cook Island","Costa Rica","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Diego Garcia","Djibouti","Ecuador","Egypt","El Salvador Rep.","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Island","Faroe Island","Fiji Republic","Finland","Fr.Guinea","Fr.Polynesia","France","France","Gabonese Republic","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Guadeloupe","Guam","Guatemala","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hongkong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Japan","Japan","Jordan","Kazakhstan","Kenya","Kirghistan","Kiribati","Korea (North)","Korea (South)","Kuwait","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Madeira Island","Malawi","Malaysia","Maldives","Mali","Malta","Mariana Island","Marshal Island","Martinque","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Anthilles","New Caledonia","New Zealand","Niger","Nigeria","Niue Island","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papau New Guinea","Paraguay","Peru","Phillipines","Poland","Portugal","Qatar","Reunion","Rodrigues Island","Romania","Russian Federation","Rwandese Republic","Samoa American","Samoa Western","San Marino","Sao Tome and Principe","Saudi Arabia","Schengen Countires","Senegal","Seychelles","Sierra Leone","Singapore","Slovak Republic","Slovenia","Soloman Island","Somalia Democratic Republic","South Africa","Spain","Srilanka","St.Helena","St.Pierre and Miquelon","Sudan","Suriname","Swaziland","Sweeden","Switzerland","Syria","Tadjikistan","Taiwan","Tanzania","Thailand","Togolese Republic","Tonga","Transkei","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","UGANDA","USA","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Vanuatu","Vatican City State","Venda","Venezuela","Vietnam","Wallis and Futina Island","Yemen (Saana)","Yogoslavia","Zaire","Zambia","Zimbabwe"],Europe:[' Russia','Germany','Turkey','France','United Kingdom','Italy','Spain','Ukraine','Poland','Romania','Kazakhstan','Netherlands','Belgium','Greece','Czech Republic','Portugal','Sweden','Hungary','Azerbaijan','Belarus','Austria','Switzerland','Bulgaria','Serbia','Denmark','Finland','Slovakia','Norway','Ireland','Croatia','Bosnia and Herzegovina','Georgia','Moldova','Armenia','Lithuania','Albania','Macedonia','Slovenia','Latvia','Kosovo','Estonia','Cyprus','Montenegro','Luxembourg','Malta',' Iceland','Andorra','Liechtenstein','Monaco','San Marino','Vatican City']};
		let depRate =1;
	
		return{
		 
	    setQuotesData:function(data)
	    {
	    	QuotesData=data;
	    },
	    getQuotesData:function()
	    {
	    	return QuotesData
	    },
	    setData:function(b)
	    {
	    	perimum=b;
	    },
	    getData:function()
	    {
	    	return perimum;
	    },
	    getAnualIncome:function()
	    {
	    	return annualIncome;
	    },
	    getOccupation:function()
	    {
	    	return ocupation;
	    },
	   getRelationship:function()
	   {
		   return relationship;
	   },
	    getCurrentInsurer:function()
	    {
	    	return currentInsurer;
	    },
	    setBuyQuotes:function(obj)
	    {
	    	buyQuotes=obj;
	    },
	    getBuyQuotes:function()
	    {
	    	return buyQuotes;
	    },
	    getRelation:function()
	    {
	    	return relation;
	    },
	    getMaritalStatus:function()
	    {
	    	return maritalStatus;
	    },
	    getAmhi:function(productType)
	    {
	    	var url='';
	    	if(productType=="royal"){
	    		 url="common/royal.json";
	    	}
	    	else if(productType=='religare')
	    		{
	    		 url="common/religare.json";
	    		}
	    	else if(productType=="axa")
	    		{
	    		url="common/axa.json";
	    		}
	    	else if(productType=="star")
    		{
    		  url="common/star.json";
    		}
	    	else if(productType=='hdfc')
	    		{
	    		url="common/hdfc.json";
	    		}
	    	else if(productType=="universal"){
	    		  url="common/universal.json";
	    	    }
	    	else
	    		{
	    		 url="common/AMHI.json";
	    		}
	    	
	    	console.log(url)
	    	var res=$resource(url,{},{
	    		'get':{method:'GET'},
	    		'save':{method:'POST'},
	    		'query':{method:'POST',isArray:false},
	    		'remove':{method:'DELETE'},
	    		'delete':{method:'DELETE'}
	    	});
	    	return res.query();
	    },
	    getQuoteProposal:function()
	    {
	    	var response = $resource('./DataRequest', [], {
		          processRequest: {
		             method: 'POST'}
		    });
			return response;
	    },
	    getRelation:function()
	    {
	    	var response = $resource('./DataRequest', [], {
		          processRequest: {
		             method: 'POST'}
		    });
			return response;
	    },
	    getMaster:function(productId, masterId)
	    {
	    	var input={url:'',data:{}}
	    	input.url='/master/codes/'+productId+'/'+ masterId;
			
	    	var res = $resource('./DataRequest', [], {
		          save: {
		             method: 'POST'}
		    });
			
			var tmp = res.save(input);
			return tmp;
	    },
		
		getMasterValue : function(masterName, masterId){
			// 
			var masterValue = masterId;
			var values = insurerMaster[masterName];
			
			if (typeof(values) != 'undefined'){
				for (var i = 0; i<values.length; i++){
					if (values[i].id == masterId){
						masterValue = values[i].value; 
						break;
					}
				}
			}
			return masterValue; 
		},
	    getMedicalQui:function()
	    {
	    	var response = $resource('./DataRequest', [], {
		          processRequest: {
		             method: 'POST'}
		    });
			return response;
	    },
	    getPincode:function()
	    {
	    	var response = $resource('./DataRequest', [], {
		          processRequest: {
		             method: 'POST'}
		    });
			return response;
	    },
	    getAskBirbal:function()
	    {
	    	var url="templates/askBirbal/askbirbal.json";
	    	var res=$resource(url,{},{
	    		'get':{method:'GET'},
	    		'save':{method:'POST'},
	    		'query':{method:'POST',isArray:false},
	    		'remove':{method:'DELETE'},
	    		'delete':{method:'DELETE'}
	    	});
	    	return res.query();
	    },
	    getInsurerList:function(){
	    	
	    	var url="js/Insurerlist.json";
	    	var res=$resource(url,{},{
	    		'get':{method:'GET'},
	    		'save':{method:'POST'},
	    		'query':{method:'POST',isArray:false},
	    		'remove':{method:'DELETE'},
	    		'delete':{method:'DELETE'}
	    	});
	    	return res.query();
	    	
	    },
	    getarrivalAtCountry:function()
	    {
	    	
	    	return arrivalAtCountry
	    },
	    getInsurerRatings:function()
	    {
	    	var url="./common/Insurer_Ratings.json";
	    	var res=$resource(url,[],{
	    		'get':{method:'GET'},
	    		'save':{method:'POST'},
	    		'query':{method:'POST',isArray:false},
	    		'remove': {method: 'DELETE'},
	            'delete': {method: 'DELETE'}
	    	});
	    	return res.query();
	    },
	    getState:function()
	    {
	    	var response = $resource('./DataRequest', [], {
		           processRequest: {
		              method: 'POST'}
		     });
			 return response
	    },
	    getCity:function()
	    {
	    	var response = $resource('./DataRequest', [], {
		           processRequest: {
		              method: 'POST'}
		     });
			 return response;
	    },
	    getInsurerCode:function()
	    {
	    	var response = $resource('./DataRequest', [], {
		           processRequest: {
		              method: 'POST'}
		     });
			 return response
	    },
	    getRolloverNewBussiness:function(type,name)
	    {
	    	var url;
	    	if(name=="rsa")
	    	{
	    		console.log(name);
	    		if(type=='New Business')
	    		{
		    		 url="./common/car/rsa/rsaBussiness.json";
	    		}
		    	else
	    		{
	    		    url='./common/car/rsa/rsarollover.json';
	    		}
	    	}
	    	else if(name=='hdfc')
    		{
	    		if(type=='New Business')
	    		{
		    		 url="./common/car/hdfc/hdfcnewBussiness.json";
	    		}
		    	else
	    		{
	    		    url='./common/car/hdfc/hdfcrollover.json';
	    		}
    		}
	    		
	    	
	    	
	    	var res=$resource(url,[],{
	    		'get':{method:'GET'},
	    		'save':{method:'POST'},
	    		'query':{method:'POST',isArray:false},
	    		'remove': {method: 'DELETE'},
	            'delete': {method: 'DELETE'}
	    	});
	    	return res.query();
	    },
	    calcPercentage:function(rank,count){
	    	return (count-rank)/(count-1);
	    },
	    getServiceRequest:function()
	    {
	    	
	    },
	    makeIndicator:function(val)
	    {
	    	var r=255-((255-0)*val);
	    	var g=68+((112-68)*val);
	    	var b=68+((192-68)*val);
	    	var rgb=r.toFixed(0)+","+g.toFixed(0)+","+b.toFixed(0);
	    	var ma=val*90;
	    	var animate={
		    		'background-color':'rgb('+rgb+')', 'background-color' : 'rgba('+rgb+',1.0)','margin-left':''+ma.toFixed(1)+'%'," transition-delay":" 2s;"
	    	  }
	    	/*$('#'+id).css('background-color','rgb('+rgb+')');
	    	$('#'+id).css('background-color','rgba('+rgb+',0.5)');
	    	$('#'+id).css('margin-left',''+ma.toFixed(1)+'%');*/
//	    	$('#'+id).animate({
//	    		'background-color':'rgb('+rgb+')', 'background-color' : 'rgba('+rgb+',1.0)','margin-left':''+ma.toFixed(1)+'%'
//	    	  }, 1500);
	    	return animate;
	    },
	    fmtSummaryVal:function(rating,rank,count,thumbs)
	    {
	    	var percent=null;
	    	var html=rating+"&nbsp;&nbsp;&nbsp;&nbsp;( ";
	    	var thumbsico=null;
	    	if(rank)
	    	{
	    		html+="#"+Number(rank).toFixed(0)+" of "+Number(count).toFixed(0) +"&nbsp;&nbsp;";
	    		percent=this.calcPercentage(rank,count);
	    	}
	    	if(percent!=null && percent<=.25)
	    	{
	    		html+=" ,<span style=\"color:#ff4444\"> Bottom 25% </span>";
	    		thumbsico=" <img src=\""+"./image/thumbsdown.png"+"\" style=\"width:1em;vertical-align:text-top;\"/>";
	    	}
	    	if(percent!=null && percent>=.75)
	    	{
	    		html+=" ,<span style=\"color:#0070c0\"> Top 25% </span>";
	    		thumbsico=" <img src=\""+"./image/thumbsup.png"+"\" style=\"width:1em;vertical-align:text-bottom;\"/>";
	    	}		
	    	html+=" )";
	    	
	    	if(thumbs && thumbs==true && thumbsico!=null)
	    		html+=thumbsico;
	    	return html;
	    	
	    },
	    formatRs:function(number)
	    {

	    	if(typeof number ==='string')
	    		number=parseFloat(number);

	    	var v=number;
	    	var s=""+Math.floor(v);
	    	if(v<0)
	    		s=""+Math.ceil(v);
	    	var r="";
	    	//alert(number+"|"+v+"|"+s);
	    	for(var i=s.length-1;i>-1;i--)
	    	{
	    		var p=s.length-i;
	    		if(p==4 || p==6 ||p==8)
	    			r=s.charAt(i)+','+r;
	    		else
	    			r=s.charAt(i)+r;
	    	}
	    	
	    	if(v.toFixed(2).indexOf('.')!=-1 && v.toFixed(2).substring(v.toFixed(2).indexOf('.'))!=".00")
	    		r+=v.toFixed(2).substring(v.toFixed(2).indexOf('.'));
	    	
	    	return r;
	    },
	    
	    getCalculateNcbRate:function(ncb)
	    {
	    	var ncbRate='';
	    	
	    	switch(ncb)
	    	{
	    		case '0':
	    			    ncbRate=0.20;
	    				break;
	    		case "20":
	    			console.log(ncb)
    			    ncbRate=0.25;
    				break;
	    		case "25":
    			    ncbRate=0.35;
    				break;
	    		case "35":
    			    ncbRate=0.45;
    				break;
	    		case "45":
    			    ncbRate=0.50;
    				break;
	    	}
	    	return ncbRate
	    },
	    getDepreciationRate:function(age)
	    {
	    	if (age <= 0.5) {
				depRate = 0.95;
			} else if (age <= 1) {
				depRate = 0.85;

			} else if (age <= 2) {
				depRate = 0.80;

			} else if (age <= 3) {
				depRate = 0.70;

			} else if (age <= 4) {
				depRate = 0.60;

			} else if (age <= 5) {
				depRate = 0.50;

			} else if (age <= 6) {
				depRate = 0.475;

			} else if (age <= 7) {
				depRate = 0.4512;

			} else if (age <= 8) {
				depRate = 0.4073;

			} else {
				depRate = 0.3869;

			}

			return depRate;

	    },
	    base64Encode : function(input){
	    	return btoa(JSON.stringify(input));
	    },
	    
	    base64Decode : function(data){
	    	return JSON.parse(atob(data));
	    },
	    calulateVehicleAge:function(dob)
	    {
	    	var dt = new Date(dob);
			var dateToday = new Date();
			var currYear = dateToday.getFullYear();
			var currMonth = dateToday.getMonth();
		    var currDay = dateToday.getDate();
			var dobYear = dt.getFullYear();
			var dobMonth = dt.getMonth();
			var dobDay = dt.getDate();
			var age = currYear - dobYear+1;
			if ((currMonth < dobMonth) || (currMonth == dobMonth && currDay<dobDay)){
				age--;
			}
			return age;
	    },
	    calculateAge:function(dob)
	    {
	    	var dt = new Date(dob);
			var dateToday = new Date();
			var currYear = dateToday.getFullYear();
			var currMonth = dateToday.getMonth();
		    var currDay = dateToday.getDate();
			
			var dobYear = dt.getFullYear();
			var dobMonth = dt.getMonth();
			var dobDay = dt.getDate();
			var age = currYear - dobYear;
			if ((currMonth < dobMonth) || (currMonth == dobMonth && currDay<dobDay)){
				age--;
			}
			return age;
	    },
	    convertNumberToWords:function(num){
			var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
			var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
			if ((num = num.toString()).length > 9) return 'overflow';
		   
			var n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
		    if (!n) return; var str = '';
		    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
		    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
		    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
		    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
		    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]])  : '';
		    str= str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		    return str;
		},
		addDate: function(currDate, addYear, addMonth, addDate){
			
			var date = new Date(currDate);
			var currYear=date.getFullYear();
			var currMonth=date.getMonth();
			var currDate=date.getDate();
			var newDate = new Date(currYear+addYear, currMonth+addMonth, currDate+addDate);
			return newDate;
		},
//	    getWecare:function()
//	    {
//	    	var url="http://testapi.brokeredge.in/rest/master/makes/";
//	    		 var url = "./common/wecare.json";
//	    		 console.log("getWecare1_url",url);
//				var res = $resource(url, [], {
//	            'get': {method: 'GET'},
//	            'save': {method: 'POST'},
//	            'query': {method: 'POST', isArray: false},
//	            'remove': {method: 'DELETE'},
//	            'delete': {method: 'DELETE'}
//	        		});
//				console.log("getWecare3_res",res);
//	        return res.query();
//	    },
	    getWecare:function()
	    {
	    		//var url="https://api.brokeredge.in/rest/master/makes/";
	    		var url = "./common/wecare.json";
	    		//var url="https://test.instainsure.com/weba/getvehiclemake"
				var res = $resource(url, [], {
	            'get': {method: 'GET'},
	            'save': {method: 'POST'},
	            'query': {method: 'POST', isArray: false},
	            'remove': {method: 'DELETE'},
	            'delete': {method: 'DELETE'}
	        		});
	        return res.query();
	    },
		strToDate: function(date){
			var newDate = "";
			if (date && date != "" && typeof(date) != "undefined") {
				
				var parts = date.substring(0,10).split("-");
				var newDate = new Date(date.substring(0,10));
				
			}
			return newDate;
		}
	}
	//validate of moter
	
	
	
}])