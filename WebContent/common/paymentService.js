/**
 * 
 */
brokeredgeApp.factory('paymentService', ['$resource','$http',
function($http, conFig) {

    var dataFactory = {};

    dataFactory.postdata = function (data) {
        return $http.post('https://www.royalsundaram.net/web/dtctest/paymentgateway', data);
    };
    return dataFactory;
}]);