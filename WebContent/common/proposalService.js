/**
 * 
 */
brokeredgeApp.factory('proposalService', ['$http', function($http, conFig) {
	
	var hostUrl = "http://Heartbeatuat.us-east-1.elasticbeanstalk.com";
	var interations = 0;
	
    return {
        getApplicationData: function(productId, rmId, customerId, appNo) { 
        	
           var appUrl = "/submitProposal/getProposal/" + productId
	           + "/" + rmId
 		   + "/" + customerId
 		   + "/" + appNo;
           console.log(appUrl);
	
           var input = {
					url: '',
					data : {}
			};
			
			input.url = appUrl;
			var res = $http('./DataRequest', [], {
			       process: {
			          method: 'POST'}
			 });
	
            return $http.post(input)
            .then(function(response) {
                console.log("coming from servicejs", response.data);
                //return data when promise resolved
                //that would help you to continue promise chain.
                return response.data;
            },
   		  	function(error) {
            	var errorObj = {};
            	errorObj.error = "Unexpected error from Insurer while processing the policy";
     			return errorObj;
     		});
        },
        
        saveApplicationData : function(productId, rmId, customerId, appNo, proposalData) { 
        	var appUrl = "/submitProposal/getProposal/" + productId
	           + "/" + rmId
	           + "/" + customerId
	           + "/" + appNo;
        	console.log(appUrl);
	
	         var input = {
						url: appUrl,
						data : proposalData
			 };
			
	         return $http.post('./DataRequest', input);
			
			
        },
        getMasterData : function(productId, masterId) { 
        	
        	var input={url:'',data:{}}
        	input.url='/master/codes/'+ productId + "/" + masterId;
        	
        	return $http.post('./DataRequest', input)
            .then(function(response) {
            	var output = [];
            	if (data.hasOwnProperty("result")) {
            		output = response.data;
            	}	
                return output;
            },
   		  	function(error) {
     			return [];
     		});
        },
        
        submitProposalAsync : function(lob, insurerId, productId, proposalData) {
        	
        	var appUrl='/submitProposal/'+ lob +'/'+ insurerId+'/'+ productId;
        	console.log(appUrl);
	
	         var input = {
						url: appUrl,
						data : angular.copy(proposalData)
			 };
	         
	         return $http.post("./DataRequest",JSON.stringify(input))
	        	.then(function(response) {
	                return response;
	            },function(error) {
	            	var errorObj = {};
	            	errorObj.error = "Unexpected error from Insurer while processing the policy";
	     			return errorObj;
	     		});
        },
        
   	 	getAreasFromPincode: function  ( insurerId, pincode){
    			
			var input={url:'',data:{}}
	    	
			input.url="/master/getDetailsForPincode/" + insurerId + "/" + pincode;
			return $http.post('./DataRequest', input)
				.then(function(data){
					var output = [];
	            	if (data.hasOwnProperty("result")) {
	            		output = response.data;
	            	}	
	                return output;
    			},
       		  	function(error) {
         			return [];
         		})
    	},
    	
    	validatePrevPolicyType: function(type){
    		
    		var policyPremiumError="";
    		if(val != "C"){
    			
    			policyTypeError="Renewal of Liability only policy online is not allowed"
    		}
    		return policyPremiumError;
    	},
    	getCoversToSelect : function(lob, productType) { 
        	
        	var input={url:'',data:{}}
        	input.url = "/master/getCoversToSelect/" + lob + "/" + productType;
        	return $http.post(hostUrl + "/DataRequest", input)
            .then(function(response) {
            	var output = [];
            	if (data.hasOwnProperty("result")) {
            		output = response.data;
            		var displayOrder = 99;
        			for (var i=0; i<$scope.output.length; i++){
        				if (output[i].coverid == coverId){
        					displayOrder = output[i].displayOrder;
        					break
        				}
        				
        			}
            	}	
                return output;
            },
   		  	function(error) {
     			return [];
     		});
        },
        getDisplayFilters : function(lob, productType) { 
        	
        	var input={url:'',data:{}}
        	input.url = "/master/getFilters/" + lob + "/" + productType;
        	return $http.post('./DataRequest', input)
            .then(function(response) {
            	var filters = [];
            	if (data.hasOwnProperty("result")) {
	    			angular.forEach(data.result, function(value, key) {
	    				var filter = angular.copy(value);
	    				filter.options = [];
	    				filter.selectedId = "A";
	    				var options  = JSON.parse(filter.displayText);
	    				
	    				angular.forEach(options, function(value, key1) {
	    					var option = {};
	    					for(var k in value){
	    						option.id = k;
		    					option.value = value[k];
	    					}
	    					filter.options.push(option);
	    				});
	    				filter.options.push({"id" : "A", "value": "All"});
	    				
	    				filters.push(filter);
	    			});
            	}	
                return filters;
            },
   		  	function(error) {
     			return [];
     		});
        },
        getAllQuotes : function(lob, productType,params) { 
        	
        	var input={url:'',data:{}}
        	input.url = "/quote/allquotes/" + lob + "/" + productType;
        	input.data = params;
        	var result = {};
        	return $http.post('./DataRequest', input)
            .then(function(response) {
            	result = angular.copy(response);
                return result;
            },
   		  	function(error) {
            	var errorObj = {};
            	errorObj.error = "Unexpected error from Insurer while processing the policy";
     			return errorObj;
     		});
        },
        submitQuoteRequest : function(lob, productType, params) { 
        	
        	var input={url:'',data:{}}
        	input.url = "/quote/submitAllQuotesRequest/" + lob + "/" + productType;
        	input.data = params;
        	var output = {};
        	return $http.post('./DataRequest', input)
            .then(function(response) {
            	var output = {};
	    		if (!data.hasOwnProperty("error")){
	    			output.quoteResuestId =data.requestId;
	    			output.numResultsExpected =data.numResultsExpected;
	    		} else
	    		{
		    	    console.error(data);
		    	    output.error=data.error;
	    		}	
                return output;
            },
   		  	function(error) {
            	var errorObj = {};
            	errorObj.error = "Unexpected error from Insurer while processing the policy";
     			return errorObj;
     		});
        },
        getQuoteResults : function(requestId) { 
        	
        	var input={url:'',data:{}}
        	input.url = "/quote/getResults/" + resuestId;
        	
        	var result = {};
        	return $http.post('./DataRequest', input)
            .then(function(response) {
            	result = angular.copy(response);
                return result;
            },
   		  	function(error) {
            	var errorObj = {};
            	errorObj.error = "Unexpected error from Insurer while processing the policy";
     			return errorObj;
     		});
        },
        
        getProposalSubmissionResults : function (requestId) {
        	var input = {
        			url: "",
        			data:{}
        			
        	};
    		iterations++;
    		
    		if (iterations >24) {
    			$interval.cancel(resultPromise);
    			$scope.loading = false;
    		}
    		
    		input.url = "/submitProposal/result/" + $scope.proposalResuestId;
    		var result = {};
        	return $http.post('./DataRequest', input)
            .then(function(data) {
            	result = angular.copy(data);
                return result;
            },
   		  	function(error) {
            	var errorObj = {};
            	result.key="Proposal Submission couldn't be completed due to the following errors";
            	var str = "Unexpected error from Insurer while processing the policy";
            	result.errors=cleanArray(str.split(";"))
     			return result;
     		});
        }
    	
    };
}]);