brokeredgeApp.service('modalProvider',['$uibModal','$log',function($uibModal,$log){
	this.openPopupModal= function() {
		 var modalInstance = $uibModal.open({
											 	  animation:true,
											      ariaLabelledBy: 'modal-title',
											      ariaDescribedBy: 'modal-body',
											      templateUrl: './templates/modal/commonModal.html',
      											  size: 'xl',
			 									});
			 modalInstance.result.then(function (selectedProduct) {
			      //$scope.selected = selectedItem;
			    }, function () {
			      $log.info('Modal dismissed at: ' + new Date());
			});
	}
}])