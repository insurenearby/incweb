/**
 * 
 */
"use strict";
brokeredgeApp.factory('servicefactory', ['$resource', '$parse', function($resource, $parse){
		var tabZone=[{id:'radio1',value:1,text:'Health',checked:'checked'},{id:'radio2',value:2,text:'Motor',checked:''},{id:'radio3',value:3,text:'Term',checked:''},{id:'radio4',value:4,text:'Travel',checked:''}]	
		var healthvals = [{"id":16,"name":"Apollo Munich Health Insurance Company Ltd."},{"id":12,"name":"Bajaj Allianz General Insurance Co. Ltd."},{"id":15,"name":"Bharti AXA General Insurance Company Limited"},{"id":46,"name":"CignaTTK"},{"id":2,"name":"HDFC ERGO General Insurance Co. Ltd."},{"id":5,"name":"Iffcos"},{"id":41,"name":"L&T Insurance"},{"id":18,"name":"Max Bupa"},{"id":1,"name":"Reliance"},{"id":17,"name":"Star Health &amp; Allied Insurance Company Ltd."},{"id":8,"name":"Tata AIG General Insurance Company Ltd."}];
		var motorvals = [{"id":13,"name":"Bajaj Allianz General Insurance Co. Ltd."},{"id":16,"name":"Bharti Axa General Insurance Company Limited"},{"id":14,"name":"Future Generali"},{"id":2,"name":"Hdfc Ergo General Insurance Co. Ltd."},{"id":5,"name":"Iffco Tokio General Insurance Co. Ltd."},{"id":21,"name":"L&t Insurance"},{"id":28,"name":"Liberty Videocon General Insurance Co. Ltd."},{"id":1,"name":"Reliance General Insurance Co. Ltd."},{"id":10,"name":"Royal Sundaram General Insurance"},{"id":8,"name":"Tata Aig General Insurance Company Ltd."},{"id":17,"name":"Universal Sompo"}];
		var termvals = {"0":{"id":"8","name":"AVIVA","Supplier_FullName":"Aviva Life Insurance Ltd."},"2":{"id":"20","name":"Bharti Axa","Supplier_FullName":"Bharti Axa"},"5":{"id":"9","name":"Exide Life Insurance","Supplier_FullName":"Exide Life Insurance Company Limited"},"6":{"id":"1","name":"Future Generali","Supplier_FullName":"Future Generali India Life"},"7":{"id":"16","name":"HDFC Standard","Supplier_FullName":"HDFC Standard"},"8":{"id":"5","name":"ICICI","Supplier_FullName":"ICICI Prudential Life Insurance Co Ltd"},"14":{"id":"12","name":"RELIANCE","Supplier_FullName":"Reliance Life Insurance Company Ltd."},"16":{"id":"7","name":"SBI","Supplier_FullName":"SBI Life Insurance Co. Ltd."},"18":{"id":10,"name":"Max","Supplier_FullName":"Max Life Insurance Co. Ltd."},"19":{"id":17,"name":"Aegon Religare","Supplier_FullName":"Aegon Religare"}};
		var travelvals = {"0":{"id":"1","name":"Apollo","AboutInsurer":"We have known Apollo Hospital as provider of life. They are a brand name of chain of specialty hospitals in India. Apollo hospitals were one of it first kind to provide an international medical exposure to Indian patients. Now this health care giant has ventured in to health insurance. With their quality health care and a brand name in field of health, they are surely at a good advantage point than others in Health insurance sector. Apollo Hospitals have got in to joint venture with world leaders in health care, Munich. This is a European Company, among the top five in world private health insurance company. This partnership is a hugely successful one, seeing the rapid growth rate of Apollo insurance. Ann Indian giant joins hands with a global health care giant; the result seems to be pre declared. Apollo Insurance is sure to make deep and long lasting penetration in health care insurance."},"2":{"id":"11","name":"Bharti Axa","AboutInsurer":"Bharti AXA General Insurance Company Ltd is a joint venture between Bharti Enterprises, a leading Indian business group and AXA, a world leader in financial protection. The joint venture company has a 74% stake from Bharti and 26% stake of the AXA Group. The organization commenced national operations in August 2008 and currently has 59 branch offices across the country. It is the first organization in the general insurance industry to receive dual certifications of ISO 9001:2008 and ISO 27001:2005 within the first year of operations in 2009 and subsequently the certification has been renewed in 2012 for further 3 years."},"3":{"id":"19","name":"Blue Ribbons","AboutInsurer":null},"4":{"id":"13","name":"Future Generali","AboutInsurer":null},"5":{"id":"4","name":"HDFC","AboutInsurer":"Headquartered in Mumbai, HDFC ERGO general insurance is joint venture between HDFC Limited- one of India's leading housing finance company and ERGO International AG- Germany's insurance provider. HDFC holds 76% equity stake while the rest is owned by ERGO. HDFC ERGO is the recipient of iAAA rating (highest rating for claim paying ability) from ICRA, well known rating agency."},"6":{"id":"5","name":"Iffco Tokio","AboutInsurer":"IFFCO-TOKIO General Insurance is committed to innovation and customer service. And with this commitment Iffco Tokio has always succeeded in creating bench marks in insurance sector. They have shown remarkable performance in a short period and their prospects of future growth is very bright. The upper selling point of IFFCO- TOKIO Insurance Services is that make tailor made products to suit one and every one. Their presence is now felt in almost 68 countries of India and the number is increasing with every passing day."},"7":{"id":"12","name":"New India assurance","AboutInsurer":null},"8":{"id":"6","name":"Reliance","AboutInsurer":"Reliance is a name to reckon with in today's time. Reliance General Insurance is a company that has exhibited tremendous growth potential in the recent years. Reliance General Insurance is an auxiliary branch of the main group of Reliance, known as the Reliance Capital. Reliance General Insurance Company proudly claims to be the first non life insurance group in India to work with a license from IRDA. Reliance General Insurance is highly known in the market of insurance for providing quality customer services and reliable insurance products. Reliance General Insurance is accredited in insurance sector for \"best insurance practices\" world wide."},"9":{"id":"10","name":"Religare","AboutInsurer":"Religare Health Insurance Company Limited is a specialist health insurer engaged in the distribution & servicing of health insurance products. Its shareholders comprise three strong entities: Religare Enterprises Limited, a leading diversified financial services group based out of India, Union Bank of India & Corporation Bank. Religare is promoted by the founders of Fortis Hospitals, which owns or manages 68 hospitals in India;Religare brings an extensive travel insurance plan that curbs out the worries of uninvited troubles on your dream family holiday. Avail remarkable benefits like automatic policy extension and upgradation to business class during medical emergencies and much more at no sub-limits and fair premium charges on a per-day basis! "},"10":{"id":"7","name":"Royal Sundaram","AboutInsurer":" Royal Sundaram General Insurance was among the first lot of private insurance companies that plunged in to business post privatization in the year 2001. This was the time, when insurance sector, a much protected one by government, was opened to the private companies also. Till then it was only government agencies that provided insurance. The spate of trading freedom provided to these private insurance companies like Royal Sundaram General Insurance unleashed a new era, where customer was the king. Insurance policy buyer was spoiled for choices and world class services that were for obvious reasons difficult to expect from government insurance providing agencies."},"11":{"id":"9","name":"Tata AIG","AboutInsurer":"Joint venture of Tata and AIG has strengthened the company. Tata Company has immense stronghold and base in Indian market across industries while AIG is a company with world base. Thus Tata AIG General Insurance Company, a joint venture of an Indian giant and a world giant has surely taken insurance sector in India to higher levels. They are providing Indian customers with world class products and policies. AIG is a world renowned US based Company with distribution channels in more than 130 countries, quite a global presence. AIG has global business in financial services, consumer finance, retail and direct investment, asset management, trading and market making, real estate investment management, aircraft leasing, retirement saving products and so on and many more, the list is unending. All this points towards one thing strongly that Tata AIG General Insurance is a dependable company that can be trusted upon having such reputed credentials. More over Tata holds 74 % of the Company stakes while AIG hold the remaining 26%, so Indian consumers need not worry at all investing in Tata AIG."},"12":{"id":"15","name":"Universal Sompo","AboutInsurer":"The joint venture has been capitalized with shareholders funds of over 230 cr. including share premium. The Company received the Licence and Certificate of Registration from Insurance Regulatory and Development Authority in November 2007. Three of the Indian partners are leading banks with a combined asset base of Rs. 3,14,071 crores and over 4000 branches and distribution centers. Plus the 4th largest FMCG company in India with over 1.5 million retail outlets. Sompo Japan Nipponkoa Insurance Inc. is a Japanese insurance company. It is the largest property and casualty insurance company in Japan and holding a stake of 26% in USGI."}};
		var claimProcesstravel=[{name:'Dental treatment',id:'2'},{name:'Personal accident',id:'3'},{name:'Baggage delay',id:'7'},{name:'Missed connection',id:'8'},{name:'Passport loss',id:'9'},{name:'Hijack distress',id:'11'}]
		var moter;
		var country=[{name:'India',stdcode:'91'},{name:'USA/CANADA',stdcode:'1'},{name:'UNITED KINGDOM',stdcode:'44'},{name:'SPAIN',stdcode:'34'},{name:'SAUDI ARABIA',stdcode:'966'},{name:'SINGAPORE',stdcode:'65'},{name:'UAE',stdcode:'971'},{name:'Abu Dabhi',stdcode:'9712'},{name:'Dubai',stdcode:'9714'},{name:'AUSTRALIA',stdcode:'61'},{name:'BRAZIL',stdcode:'55'},{name:'OMAN',stdcode:'968'},{name:'BOLIVIA',stdcode:'591'},{name:'AFGHANISTAN',stdcode:'93'},{name:'AFRICAN REP-SPECIAL',stdcode:'61'},{name:'ALBANIA',stdcode:'355'},{name:'ALGERIA',stdcode:'213'},{name:'AMERICAN SAMOA',stdcode:'1684'},{name:'ANDORRA',stdcode:'376'},{name:'ANGOLA',stdcode:'244'},{name:'ANGUILLA',stdcode:'1264'},{name:'ANTARCTICA',stdcode:'6721'},{name:'ANTIGUA &amp; BARBUDA',stdcode:'1268'}, {name:'ANTILLES NETHERLANDS', stdcode:'599'},{name:'ARGENTINA',stdcode:'54'},{name:'ARMENIA',stdcode:'374'} ,{name:'SCENSION ISLAND',stdcode:'247'},{name:'ATLANTIC WEST (SAT)',stdcode:'874'},{name:'AUSTRIA',stdcode:'43'}, {name:'AZERBAIJAN',stdcode:'994'}, {name:'BAHAMAS',stdcode:'1242'}, {name:'BAHRAIN',stdcode:'973'}, {name:'BANGLADESH',stdcode:'880'}, {name:'BARBADOS',stdcode:'1246'}, {name:'BELARUS',stdcode:'375'}, {name:'BELGIUM',stdcode:'32'}, {name:'BELIZE',stdcode:'501'}, {name:'BENIN',stdcode:'229'}, {name:'BERMUDA',stdcode:'1441'}, {name:'BHUTAN',stdcode:'975'}, {name:'BOSNIA',stdcode:'387'}, {name:'BOTSWANA',stdcode:'267'}, {name:'BRITISH VIRGIN ISLAND',stdcode:'1284'}, {name:'BRUNEI',stdcode:'673'}, {name:'BULGARIA',stdcode:'359'}, {name:'BURKINA FASSO',stdcode:'226'}, {name:'BURUNDI',stdcode:'257'}, {name:'CAMBODIA',stdcode:'855'}, {name:'CAMEROON',stdcode:'237'}, {name:'CANADA',stdcode:'1867'}, {name:'CAPE VERDE ISLAND',stdcode:'238'}, {name:'CAYMAN ISLAND',stdcode:'1345'}, {name:'CENTRAL AFRICAN REP',stdcode:'236'}, {name:'CHAD(REPUBLIC)',stdcode:'235'}, {name:'CHILE',stdcode:'56'}, {name:'CHINA',stdcode:'86'}, {name:'COLOMBIA',stdcode:'57'}, {name:'COMOROS',stdcode:'269'}, {name:'CONGO PEOPLES REPUBL',stdcode:'242'}, {name:'COOK ISLAND',stdcode:'682'}, {name:'COST RICA',stdcode:'506'}, {name:'CROATIA',stdcode:'385'}, {name:'CUBA',stdcode:'53'}, {name:'CYPRUS',stdcode:'357'}, {name:'CZECH',stdcode:'420'}, {name:'DEM. REP. OF CONGO',stdcode:'243'}, {name:'DENMARK',stdcode:'45'}, {name:'DIEGO GARCIA',stdcode:'246'}, {name:'DJIBOUTI',stdcode:'253'}, {name:'DOMINICA REP',stdcode:'1829'}, {name:'EGYPT',stdcode:'20'}, {name:'EL SALVADOR REPUBLIC',stdcode:'503'}, {name:'DOMINICA',stdcode:'1767'}, {name:'EQUADOR',stdcode:'593'}, {name:'EQUATORIAL GUINEA',stdcode:'240'}, {name:'ERITREA',stdcode:'291'}, {name:'ESTONIA',stdcode:'372'}, {name:'ETHIOPIA',stdcode:'251'}, {name:'FAEROE',stdcode:'298'}, {name:'FALKLAND ISLAND',stdcode:'500'}, {name:'FIJI',stdcode:'679'}, {name:'FINLAND',stdcode:'358'}, {name:'FRANCE',stdcode:'33'}, {name:'FRENCH GUYANA',stdcode:'594'}, {name:'FRENCH POLYNESIA',stdcode:'689'}, {name:'GABON',stdcode:'241'}, {name:'GAMBIA',stdcode:'220'}, {name:'GEORGIA',stdcode:'995'}, {name:'GERMANY',stdcode:'49'}, {name:'GHANA',stdcode:'233'}, {name:'GIBRALTER',stdcode:'350'}, {name:'GREECE',stdcode:'30'}, {name:'GREENLAND',stdcode:'299'}, {name:'GRENADA',stdcode:'1473'}, {name:'GUADELOUPE',stdcode:'590'}, {name:'GUAM',stdcode:'671'}, {name:'GUATEMALA',stdcode:'502'}, {name:'GUINEA BISSAU REPUBL',stdcode:'245'}, {name:'GUINEA REPUBLIC',stdcode:'224'}, {name:'GUYANA REPUBLIC',stdcode:'592'},{name:'HAITI REPUBLIC',stdcode:'509'}, {name:'HONDURAS REPUBLIC',stdcode:'504'}, {name:'HONGKONG',stdcode:'852'}, {name:'HUNGARY',stdcode:'36'}, {name:'ICELAND',stdcode:'354'}, {name:'IMMERSAT',stdcode:'882'}, {name:'INDONESIA',stdcode:'62'}, {name:'IRAN',stdcode:'98'}, {name:'IRAQ',stdcode:'964'}, {name:'IRELAND',stdcode:'353'}, {name:'ISRAEL',stdcode:'972'}, {name:'ITALY',stdcode:'39'}, {name:'IVORY COAST',stdcode:'225'}, {name:'JAMAICA',stdcode:'1876'}, {name:'JAPAN',stdcode:'81'}, {name:'JORDAN',stdcode:'962'}, {name:'KAZAKHSTAN',stdcode:'77'}, {name:'KENYA REPUBLIC',stdcode:'254'}, {name:'KIRIBATI',stdcode:'686'}, {name:'KUWAIT',stdcode:'965'}, {name:'KYRGYZSTAN',stdcode:'996'}, {name:'LAOS',stdcode:'856'}, {name:'LATVIA',stdcode:'371'}, {name:'LEBANON',stdcode:'961'}, {name:'LESOTHO',stdcode:'266'}, {name:'LIBERIA REPUBLIC',stdcode:'231'}, {name:'LIBYA',stdcode:'218'}, {name:'LIECHTENSTEIN',stdcode:'423'}, {name:'LITHUANIA',stdcode:'370'}, {name:'LUXEMBOURG',stdcode:'352'}, {name:'MACAU',stdcode:'853'}, {name:'MACEDONIA',stdcode:'389'}, {name:'MADAGASCAR',stdcode:'261'}, {name:'MALAWI',stdcode:'265'}, {name:'MALAYSIA',stdcode:'60'}, {name:'MALDIVES',stdcode:'960'}, {name:'MALI REPUBLIC',stdcode:'223'}, {name:'MALTA',stdcode:'356'}, {name:'MARIANA ISLAND',stdcode:'670'}, {name:'MARISAT',stdcode:'873'}, {name:'MARISAT ATLANTIC',stdcode:'870'}, {name:'MARISAT PACIFIC',stdcode:'872'}, {name:'MARSHAL ISLAND',stdcode:'692'}, {name:'MARTINIQUE',stdcode:'596'}, {name:'MAURITANIA',stdcode:'222'}, {name:'MAURITIUS',stdcode:'230'}, {name:'MEXICO',stdcode:'52'}, {name:'MICRONESIA (FEDERAL)',stdcode:'691'}, {name:'MOLDOVA',stdcode:'373'}, {name:'MONACO',stdcode:'377'}, {name:'MONGOLIA',stdcode:'976'}, {name:'MONTSERRAT',stdcode:'1664'}, {name:'MOROCCO',stdcode:'212'}, {name:'MOZAMBIQUE',stdcode:'258'}, {name:'MYANMAR',stdcode:'95'}, {name:'NAMIBIA',stdcode:'264'}, {name:'NAURU',stdcode:'674'}, {name:'NEPAL',stdcode:'977'}, {name:'NETHERLANDS',stdcode:'31'}, {name:'NEW ZEALAND',stdcode:'64'}, {name:'NICARAGUA',stdcode:'505'}, {name:'NIGER REPUBLIC ',stdcode:'227'}, {name:'NIGERIA',stdcode:'234'}, {name:'NIUE ISLAND ',stdcode:'683'}, {name:'NORFOLK ISLAND',stdcode:'672'}, {name:'NORTH KOREA',stdcode:'850'}, {name:'NORWAY',stdcode:'47'}, {name:'PAKISTAN',stdcode:'92'}, {name:'PALAU',stdcode:'680'}, {name:'PALESTINE',stdcode:'970'}, {name:'PANAMA',stdcode:'507'}, {name:'PAPUA- NEW-GUINEA',stdcode:'675'}, {name:'PARAGUAY',stdcode:'595'}, {name:'PERU',stdcode:'51'}, {name:'PHILIPPINES',stdcode:'63'}, {name:'POLAND',stdcode:'48'}, {name:'PORTUGAL',stdcode:'351'}, {name:'PUERTO RICO',stdcode:'1787'}, {name:'QATAR',stdcode:'974'}, {name:'ROMANIA',stdcode:'40'}, {name:'RUNION ISLAND ',stdcode:'262'}, {name:'RUSSIA',stdcode:'7'}, {name:'RWANDA REPUBLIC',stdcode:'250'}, {name:'SAIPAN',stdcode:'1670'}, {name:'SAMOA AMERICAN',stdcode:'684'}, {name:'SAMOA WESTERN',stdcode:'685'}, {name:'SAN MARINO',stdcode:'378'}, {name:'SAOTOME PRINCIPE ISL',stdcode:'239'}, {name:'SATELLITE',stdcode:'883'}, {name:'SENEGAL',stdcode:'221'}, {name:'SEYCHELLES',stdcode:'248'}, {name:'SIERRA LEONE',stdcode:'232'}, {name:'SLOVAKIA',stdcode:'421'}, {name:'SLOVENIA',stdcode:'386'}, {name:'SOLOMON ISLAND',stdcode:'677'}, {name:'SOMALIA',stdcode:'252'}, {name:'SOUTH AFRICA',stdcode:'27'}, {name:'SOUTH KOREA',stdcode:'82'}, {name:'SOUTH SUDAN ',stdcode:'211'}, {name:'SRI LANKA',stdcode:'94'}, {name:'ST MAARTEN-ROC',stdcode:'1721'}, {name:'ST PIERRE &amp; MIQUELON',stdcode:'508'}, {name:'ST. DENIS',stdcode:'282'}, {name:'ST. HELENA',stdcode:'290'}, {name:'ST. KITTS &amp; NEVIS',stdcode:'1869'}, {name:'ST. LUCIA',stdcode:'1758'}, {name:'ST.VINCENT &amp; GRENADI',stdcode:'1784'}, {name:'SUDAN DEMO. REP',stdcode:'249'}, {name:'SURINAM REPUBLIC',stdcode:'597'}, {name:'SWAZILAND',stdcode:'268'}, {name:'SWEDEN',stdcode:'46'}, {name:'SWITZERLAND',stdcode:'41'}, {name:'SYRIA',stdcode:'963'}, {name:'TAIWAN',stdcode:'886'}, {name:'TAJIKISTAN',stdcode:'992'}, {name:'TANZANIA',stdcode:'255'}, {name:'THAILAND',stdcode:'66'}, {name:'TOGO LESE REP',stdcode:'228'}, {name:'TONGA',stdcode:'676'}, {name:'TRINIDAD &amp; TOBAGO',stdcode:'1868'}, {name:'TUNISIA',stdcode:'216'}, {name:'TURKEY',stdcode:'90'}, {name:'TURKMENISTAN',stdcode:'993'}, {name:'TURKS &amp; CAUCUS',stdcode:'1649'}, {name:'TUVALU',stdcode:'688'}, {name:'UGANDA',stdcode:'256'}, {name:'UKRAINE',stdcode:'380'}, {name:'UNITED ARAB EMIRATES',stdcode:'971'}, {name:'URUGUAY',stdcode:'598'}, {name:'UZBEKISTAN',stdcode:'998'}, {name:'VANUATU NEW HEBRIDES',stdcode:'678'}, {name:'VENEZUELA',stdcode:'58'}, {name:'VIETNAM',stdcode:'84'}, {name:'WALLIS &amp; FUTUNA',stdcode:'681'}, {name:'YEMEN',stdcode:'967'}, {name:'YUGOSLAVIA',stdcode:'381'}, {name:'ZAMBIA',stdcode:'260'}, {name:'ZIMBABWE',stdcode:'263'}]
	return{
			getTabZone:function()
			{
				return tabZone;
			},
			getHealth:function()
			{
				   var url='./common/wecare.json'
					var res = $resource(url, [], {
			            'get': {method: 'GET'},
			            'save': {method: 'POST'},
			            'query': {method: 'GET', isArray: false},
			            'remove': {method: 'DELETE'},
			            'delete': {method: 'DELETE'}
			        });
	        
	        	return res.query();
			},
			
			getInsurerFormDownload:function(result,i)
			{
				
				switch(Number(i))
				{

					 case 16:
								return result.appoloMunich;
								break;
					case 12:
								return result.bajajAllianz;
								break;
					case 15:
								return result.bhartiAxa;
								break;
					case 46:
							return result.cignaTTk;
							break;
					case 2:
							return result.hdfcErgo;
							break;
					case 5:
							return result.Iffcos;
							break;
					case 41:
							return result.landT;
							break;
					case 18:
							return result.maxBuppa;
							break;
					case 1:
							return result.relaince;
							break;
					case 17:
							return result.startHealth;
							break;
					case 8:
							console.log(result)
							return result.tataAig;
							break;
					default:''
				}
				//return k;
			},
			getCountryMobile:function()
			{
				return country;
			},
			getInsurer:function(inssureVal)
			{
				
				switch(inssureVal)
				{
					case 'Health':
									return healthvals;
									break;
					case 'Motor':
									return motorvals;
									break;
					case 'Travel':
									return travelvals;
									break;

					case 'Term':
									return termvals;
									break;
					default:'';

				}
			},
			
			
			getMoterDownload:function(result,i)
			{
				switch(Number(i))
				{
					case 13:
							return result.bajajAllianz;
							break;
					case 16:
							return result.bhartiAxa;
							break;
					case 14:
							return result.FutureGenerali;
							break;
					case 2:
							return result.hdfcErgo;
							break;
					case 5:
							return result.Iffcos;
							break;
					case 21:
							return result.landT;
							break;
					case 28:
							return result.libVideoconInsu;
							break;
					case 1:
							return result.reliance;
							break;
					case 10:
							return result.royalGenInsu;
							break;
					case 8:
							return result.tataAig;
							break;
					case 17:
							return result.UniversalSampo;
							break;
				}
			},
			getTermDownload:function(result,i)
			{
				switch(Number(i))
				{
					case 8:
							return result.aviva;
							break;
					case 20:
							return result. bhartiAxa ;
							break;
					case 9:
							return result.exideLife ;
							break;
					case 1:
							return result.futureGenerali ;
							break;
					case 16:
							return result.hdfcStandard  ;
							break;
					case 5:
							return result.icciPrudential ;
							break;
					case 12:
							return result.relianceLife ;
							break;
					case 7:
							return result.sbiLife ;
							break;
					case 10:
							return result.maxLife ;
							break;
					case 17:
							return result.agionLife;
							break;
				}
			},
			
			getOtherProcessTravel:function(result,i)
			{
				switch(Number(i))
				{
					case 2:
							return result.dentalTreatment;
							break;
					case 3:
							return result.personalAccident;
							break;
					case 7:
							return result.baggageDelay;
							break;
					case 8:
							return result.missedConnection;
							break;
					case 9:
							return result.passportLoss;
							break;
					case 11:
							return result.hijackDistress;
							break;
				}
			},
			getTravelDownload:function(result,i)
			{
				switch(Number(i))
				{
					case 1:
							return result.apollo;
							break;
					case 11:
							return result.bhartiAxa;
							break;
					case 19:
							return result.blueRibbons ;
							break;
					case 13:
							return result.futureGenerali ;
							break;
					case 4:
							return result.hdfc  ;
							break;
					case 5:
							return result.Iffcos ;
							break;
					case 12:
							return result.newIndia ;
							break;
					case 6:
							return result.reliance ;
							break;
					case 10:
							return result.religare ;
							break;
					case 7:
							return result.royalSundaram;
							break;
					case 9:
							return result.tataAig;
							break;
					case 15:
							return result.universalSampo;
							break;
					default:''
				}
			},
	}

}])