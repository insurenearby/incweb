
"use strict";
brokeredgeApp.factory('lifefactory',['$resource','$http',function($resource,$http) {
	
	var insurerMaster = {};
	var allDataLoaded = false;
	var city=["Abohar","Agartala","Agra","Ahmedabad","Ahmednagar","Ajmer","Alapuzha","Allahabad","Aluva","Alwar","Ambala","Amravati","Amritsar","Anand","Anantapur","Angul","Ankleshwar","Asansol","Aurangabad","Balhotra (Rajasthan)","Bangalore","Bardoli","Bareilly","Baroda","Bayad, Gujrat","Beawar","Bhagalpur","Bharatpur","Bharuch","Bhilai","Bhilwara","Bhopal","Bhubaneshwar","Bhuj","Bikaner","Bilaspur","Bokaro","Bulandshashr","Burdwan","Calicut","Chandigarh","Chennai","Chhindwara","Cochin","Coimbatore","Contai","Cuttak","Davangere","Dehradun","Delhi","Dhanbad","Dibrugarh","Duliajan","Durgapur","Erode","Faridabad","Faridkot","Gandhinagar","Gandidham","Gangtok","Ghaziabad","Goa","Gorakhpur","Guntur","Gurdaspur","Gurgaon","Guwahati","Gwalior","Haldwani","Hapur","Hoshangabad","Hosur","Howrah","Hubli","Hyderabad","Ichalkaranji","Indore","Jabalpur","Jaipur","Jalandhar","Jammu","Jamnagar","Jamshedpur","Jaunpur","Jhansi","Jodhpur","Kakinada","Kalyan","Kannur","Kanpur","Kapurthala","Karad","Karimnagar","Karnal","Karur","Kashipur","Khanna","Kishangarh, Rajasthan","Kolhapur","Kolkata","Kollam","Kota","Kurnool","Kurukshetra","Lucknow","Ludhiana","Madurai","Malappuram","Malegaon","Malkapur","Malout","Mandi H.P.","Mandigobindgarh","Mandsaur","Mangalore","Meerut","Mirayalguda","Mohali","Moradabad","Morbi","Mumbai","Murshidabad","Mysore","Nadia","Nagpur","Namakkal","Nanded","Nashik","Navsari","Neemuch","Nellore","Noida","Ongole","Pala","Panipat","Pathankot","Patiala","Patna","Phagwara","Pondicherry","Porbandar","Pune","Raipur","Rajamundry","Rajkot","Ratlam","Rewari","Rohtak","Ropar","Saharanpur","Salem","Sambalpur","Secunderabad","Shimoga","Silliguri","Sirsa","Solapur","Sonepat","Sriganganagar","Surat","Tezpur","Thane","Thiruvalla","Tinsukia","Tirunelveli","Tirupur","Trichy","Trivandrum","Udaipur","Ujjain","Valsad","Vapi","Varanasi","Vellore","Vijayawada","Yamuna Nagar"];
    var bankname=['ABHYUDAYA COOP BANK','ABU DHABI COMMERCIAL BANK','AHMEDABAD MERC COOP BANK','ALLAHABAD BANK','ANDHRA BANK','AXIS BANK','B N PARIBAS BANK','BANK OF AMERICA','BANK OF BAHRAIN & KUWAIT','BANK OF BARODA','BANK OF CEYLON','BANK OF INDIA','BANK OF MAHARASHTRA','BANK OF NOVA SCOTIA','BANK OF TOKYO-MITSUBISHI','BARCLAYS BANK','BASSEIN CATHOLIC CO-OP BANK','BHARAT COOPERATIVE BANK','CANARA BANK','CATHOLIC SYRIAN BANK','CENTRAL BANK OF INDIA','CHINATRUST COMMERCIAL BANK','CITI BANK','CITIZEN CREDIT COOP BANK','CITY UNION BANK','CORPORATION BANK','COSMOS CO-OP. BANK','CREDIT AGRICOLE CORP N INVSMNT BANK','DENA BANK','DEUTSCHE BANK','DEVELOPMENT BANK OF SINGAPORE','DEVELOPMENT CREDIT BANK','DHANALAXMI BANK','DICGC','DOMBIVLI NAGARI SAHAKARI BANK LTD','FEDERAL BANK','FIRSTRAND BANK','GREATER BOMBAY CO-OP. BANK LTD','HDFC BANK LTD.','HONG KONG & SHANGHAI BANK','ICICI BANK LTD.','IDBI BANK','INDIAN BANK','INDIAN OVERSEAS BANK','INDUS-IND BANK','ING VYSYA BANK','JAMMU & KASHMIR BANK','JANATA SAHAKARI BANK LTD (PUNE)','JANKALYAN SHAKARI BANK','JP MORGAN CHASE BANK','KALUPUR COMM COOP BANK','KALYAN JANATA SAHAKARI BANK','KAPOLE BANK','KARAD URBAN COOP BANK LTD','KARNATAKA BANK','KARNATAKA STATE COOP APEX BANK','KARUR VYSYA BANK','KOTAK MAHINDRA BANK','LAKSHMI VILAS BANK','MAHANAGAR COOP BANK','MASHREQ BANK','MEHSANA URBAN COOPERATIVE BANK LTD','MIZUHO CORPORATE BANK LTD','MSCB','NAINITAL BANK LTD','NEW INDIA CO-OPERATIVE BANK','NKGSB BANK','NUTAN NAGARIK SAHAKARI BANK','OMAN INTERNATIONAL BANK','ORIENTAL BANK OF COMMERCE','PARSIK JANATA SAHAKARI BANK','PUNJAB AND MAHARASHTRA BANK COOP LTD','PUNJAB AND SIND BANK','PUNJAB NATIONAL BANK','RAJKOT NAGARIK SAHAKARI BANK LTD','RATNAKAR BANK','RESERVE BANK OF INDIA','SARASWAT CO-OPERATIVE BANK','SHAMRAO VITHAL COOP BANK','SHINHAN BANK','SOCIETE GENERALE','SOUTH INDIAN BANK','STANDARD CHARTERED BANK','STATE BANK OF BIKANER AND JAIPUR','STATE BANK OF HYDERABAD','STATE BANK OF INDIA','STATE BANK OF MAURITIUS','STATE BANK OF MYSORE','STATE BANK OF PATIALA','STATE BANK OF TRAVANCORE','SURAT PEOPLE?S CO-OP BANK','SYNDICATE BANK','TAMILDADU STATE APEX COOP BANK','TAMILNADU MERC. BANK','THANE JANATA SAHAKARI BANK','THE NASIK MERCHANTS CO-OP BANK LTD.','THE ROYAL BANK OF SCOTLAND','UBS AG','UCO BANK','UNION BANK OF INDIA','UNITED BANK OF INDIA','VIJAYA BANK','WEST BENGAL STATE COOPERATIVE BANK LTD','YES BANK'];
	var exactNatureofDuty=['Accountant / Accounts Staff','Actor/Actress - No Stunt Work','Actor/Actress -With Stunt Work','Actuary','Acupuncturist','Administration/Clerical Worker','Advertising Staff/Professional','Air Con Worker - Hgts <15 mtrs','Air Con Worker - Hgts >15 mtrs','Air Traffic Controller','Aircraft - Fitter/Technician','Aircraft - Loader/Bag Handler','Aircraft - Maintenance','Ambulance Officer','Animal Welfare Officer','Architect / Architectural Work','Armed Forces - Bomb Disposal','Army','Art Dealer / Antique Dealer','Artist - Non-Salaried','Artist - Salaried','Assembler - Aircraft / Other','Attendant/Orderly - Medical','Baker / Bakery Worker','Banking Professional / Staff','Bartender / Bar Worker','Beautician','Blind / Curtain Fitter','Body Guard','Boiler Maker / Boilerman','Book Binder','Border Security Force','Broadcasting Technician','Builder','Building Maintenance Worker','Cabin Crew / Flight Attendant','Cafeteria / Canteen Assistant','Call Centre / Telesales Staff','Cameraman - No Dangerous Work','Cameraman -Disaster/War Report','Car Dealer / Salesman','Carpenter / Joiner','Cashier','Catering Staff','Charter Accountant','Chemical factory','Choreographer','Civil Defence Emergency Worker','Civil Servant/Government Work','Coast Guard','Composer','Computer Repairer/Technician','Conductor','Construction','Consultancy','Container Ship / Tanker - Crew/Officer','Cook / Chef','Counsellor','Crane Driver - Construction/Docks/Shipyard','CRPF','Cruise Liner - Crew / Officer','Customs / Immigration Staff','Cutter - Block / Flame / Glass','Dancer - Professional','Demolition Work - with/without Explosives','Dentist / Dental Surgeon/Dental Assistant','Dietician / Nutritionist','Director / Producer','Diver','Doctor - Medical / Specialist/Surgeon','Driver','Office work','Driving Instructor / Examiner','Editor','Electrician/Linesman','Engineer/Techn - Construction','Engineer/Techn- Admin/Research','Event Manager','Explosives/Bombs','Fashion Designer / Stylist','Fireman / Fire Fighter','Fishing','Fitness - Instructor/Trainer','Florist','Foreman/Supervisor','Forestry','Forklift Driver','Furniture Mover / Packer','Glass Worker','Grocery Shop','Hairstylist / Hairdresser','Heavy Vessel - Crew / Officer','Human Resouces','Indian Navy','Information Technology','Insurance Professional / Staff','Interior Decorator / Designer','Jeweller','Journalist','Lawyer/Judge','Lecturer / Professor','Librarian','Life Guard','Lift / Escalator Fitter','Manager','Managing Director / CEO','Marketing/Advertising','Mechanic','Mechanic - Aircraft Engine','Merchant Marine','Musician','Nuclear Power plant','Nurse / Nurse Assistant','Oil & Gas Refinery/ONGC','Optician / Ophthalmologist','Physiotherapist','Pilot','Plant Operator','Police Officer / Police Work','Politician','Printing','Property / Real Estate Agent','Public Relations Officer','Railways - Driver, Guard, maintanence','Sailor / Seaman','Scientist','Security Guard','Ship Passenger Vessel - Crew/Officer','Student','Surveyor','Tailor / Dressmaker','Teacher','Technicians / Technologists','Trader','Tutor - Private','Underground Mining','Veterinarian / Surgeon','Welder']
	var timePick=['00:00','00:05','00:10','00:15','00:20','00:25','00:30','00:35','00:40','00:45','00:50','00:55','01:00','01:05','01:10','01:15','01:20','01:25','01:30','01:35','01:40','01:45','01:50','01:55','02:00','02:05','02:10','02:15','02:20','02:25','02:30','02:35','02:40','02:45','02:50','02:55','03:00','03:05','03:10','03:15','03:20','03:25','03:30','03:35','03:40','03:45','03:50','03:55','04:00','04:05','04:10','04:15','04:20','04:25','04:30','04:35','04:40','04:45','04:50','04:55','05:00','05:05','05:10','05:15','05:20','05:25','05:30','05:35','05:40','05:45','05:50','05:55','06:00','06:05','06:10','06:15','06:20','06:25','06:30','06:35','06:40','06:45','06:50','06:55','07:00','07:05','07:10','07:15','07:20','07:25','07:30','07:35','07:40','07:45','07:50','07:55','08:00','08:05','08:10','08:15','08:20','08:25','08:30','08:35','08:40','08:45','08:50','08:55','09:00','09:05','09:10','09:15','09:20','09:25','09:30','09:35','09:40','09:45','09:50','09:55','10:00','10:05','10:10','10:15','10:20','10:25','10:30','10:35','10:40','10:45','10:50','10:55','11:00','11:05','11:10','11:15','11:20','11:25','11:30','11:35','11:40','11:45','11:50','11:55','12:00','12:05','12:10','12:15','12:20','12:25','12:30','12:35','12:40','12:45','12:50','12:55','13:00','13:05','13:10','13:15','13:20','13:25','13:30','13:35','13:40','13:45','13:50','13:55','14:00','14:05','14:10','14:15','14:20','14:25','14:30','14:35','14:40','14:45','14:50','14:55','15:00','15:05','15:10','15:15','15:20','15:25','15:30','15:35','15:40','15:45','15:50','15:55','16:00','16:05','16:10','16:15','16:20','16:25','16:30','16:35','16:40','16:45','16:50','16:55','17:00','17:05','17:10','17:15','17:20','17:25','17:30','17:35','17:40','17:45','17:50','17:55','18:00','18:05','18:10','18:15','18:20','18:25','18:30','18:35','18:40','18:45','18:50','18:55','19:00','19:05','19:10','19:15','19:20','19:25','19:30','19:35','19:40','19:45','19:50','19:55','20:00','20:05','20:10','20:15','20:20','20:25','20:30','20:35','20:40','20:45','20:50','20:55','21:00','21:05','21:10','21:15','21:20','21:25','21:30','21:35','21:40','21:45','21:50','21:55','22:00','22:05','22:10','22:15','22:20','22:25','22:30','22:35','22:40','22:45','22:50','22:55','23:00','23:05','23:10','23:15','23:20','23:25','23:30','23:35','23:40','23:45','23:50','23:55'];
	var deathCause=[{name:'Normal',code:'N'},{name:'Natural Death',code:'ND'},{name:'Accidental',code:'A'},{name:'Other',code:'U'}]
	var PremiumPayMethod=[{name:'Direct bill',code:'DB'},{name:'ECS / Direct Debit / ACH',code:'PP'},{name:'Credit Card',code:'CC'},{name:'Standing instruction',code:'SI'},{name:'Salary Deduction',code:'SD'},{name:'Single Premium',code:'SP'},{name:'Unanswered',code:'U'}]
		
	var addFamily={"Father":'',"Mother":'','Brother':[],"Sister":[]}
		
	
	return{
			getproposalFormTemplate:function()
		    {
		    		var url = "./proposal/life/BrokerEdgeInput.json";
					var res = $resource(url, [], {
		            'get': {method: 'GET'},
		            'save': {method: 'POST'},
		            'query': {method: 'POST', isArray: false},
		            'remove': {method: 'DELETE'},
		            'delete': {method: 'DELETE'}
		        		});
		        return res.query();
		    },
		    getBI(insurerId, input){
		    	
		    },
		    setMaster: function (data){
		    	insurerMaster = data;
		    },
		    getCity:function()
		    {
		    	return city;
		    },
		    getState:function()
		    {
		    	return state;
		    },
		    getBankName:function()
		    {
		    	return bankname;
		    },
		    getTimePick:function()
		    {
		    	return timePick;
		    },
		    calculateAge : function (dob){
			var dt = new Date(dob);
			var dateToday = new Date();
			var currYear = dateToday.getFullYear();
			var currMonth = dateToday.getMonth();
		    var currDay = dateToday.getDate();
			
			var dobYear = dt.getFullYear();
			var dobMonth = dt.getMonth();
			var dobDay = dt.getDate();
			var age = currYear - dobYear;
			if ((currMonth < dobMonth) || (currMonth == dobMonth && currDay<dobDay)){
				age--;
			}
			return age;
		},
		
		loadMasterData : function (insId){
			// Loads all the Master Data for The Insurers in Insurer Specific Files
			var fl = insId + ".json";
			var url = "./masters/" + fl;
		    var res = $resource(url, [], {
	            'get': {method: 'GET'},
	            'save': {method: 'POST'},
	            'query': {method: 'GET', isArray: false},
	            'remove': {method: 'DELETE'},
	            'delete': {method: 'DELETE'}
	        });
		    
		    var tmp = res.query();
		    tmp.$promise.then(function(data) {
		    	insurerMaster[insId] = tmp;
		    	console.log(insurerMaster);
		    	allDataLoaded = true;
		    });
		},
		
		getMasterData : function (insId, productId, masterName){
			console.log("get Masters for :" + masterName);
			var data = [];
			if (insurerMaster.hasOwnProperty(insId)){
				if (insurerMaster[insId].hasOwnProperty(productId) && insurerMaster[insId][productId].hasOwnProperty(masterName)){
					data = insurerMaster[insId][productId][masterName];
				} else if (insurerMaster[insId].hasOwnProperty(masterName))
				{
					data = insurerMaster[insId][masterName];
				}
			}
			var output = [];
			angular.forEach(data, function(value, key) {
				var item = {};
				item.id = key;
				item.value = value;
				output.push(item);
			});
			console.log(output);
			return output;
			
		},
		
		getMasters : function(masterName){
			// 
			//console.log(insurerMaster[masterName])
			var values = insurerMaster[masterName];
			
			return values;
			
		},
		
		getMasterValue : function(masterName, masterId){
			// 
			var masterValue = masterId;
			var values = insurerMaster[masterName];
			
			if (typeof(values) != 'undefined'){
				for (var i = 0; i<values.length; i++){
					if (values[i].id == masterId){
						masterValue = values[i].value; 
						break;
					}
				}
			}
			return masterValue; 
		},

		getNewApplicationNumber : function(){
			// Code here for application number generation 
			
			
			//
		},
		
		getApplicationData : function(appNumber){
			// return saved Application numbers from the DB
			
			
			//
		},
		getPremiumPayMethod:function()
		    {
		    	return PremiumPayMethod;
		    },
		isLoading : function(){
			return !allDataLoaded;
		},
		calculateAge : function (dob){
			var dt = new Date(dob);
			var dateToday = new Date();
			var currYear = dateToday.getFullYear();
			var currMonth = dateToday.getMonth();
		    var currDay = dateToday.getDate();
			
			var dobYear = dt.getFullYear();
			var dobMonth = dt.getMonth();
			var dobDay = dt.getDate();
			var age = currYear - dobYear;
			if ((currMonth < dobMonth) || (currMonth == dobMonth && currDay<dobDay)){
				age--;
			}
			return age;
		},
		numToWords: function(num){
			var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
			var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
			if (Number(num)>=1000000000) return 'overflow';
		   
			var n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
		    if (!n) return; var str = '';
		    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
		    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
		    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
		    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
		    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]])  : '';
		    str= str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		    return str;
		}
	}
	
	
}])