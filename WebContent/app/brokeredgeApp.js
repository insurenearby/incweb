var brokeredgeApp=angular.module('brokeredgeApp', ['ngAnimate','ngStorage','ngSanitize', 'ngRoute', 'ngCookies', 'ngResource', 'ui.bootstrap','ui.select','btorfs.multiselect']);
brokeredgeApp.config(['$locationProvider','$routeProvider', '$provide',function($locationProvider,$routeProvider, $provide){
	
	$routeProvider.
	when('/index', {
	         template: '<brokeredge-home></brokeredge-home>'
	       }).
	when('/index/:section',{
		template: '<brokeredge-home></brokeredge-home>'
	}).
	when('/quiries',{
		template:'<wecare-quiries></wecare-quiries>'
	}).
	when('/service-request',{
		template:'<wecare-service-request></wecare-service-request>'
	}).
	when('/complaint',{
		template:'<wecare-complaint></wecare-complaint>'
	}).
	when('/underconstruction',{
		template: '<under-construction></under-construction>'
	}).
	when('/insurer-rating',{
		template: '<brokeredge-insurer-rating></brokeredge-insurer-rating>'
	}).
	 when('/bestpolicy',{
		 template: '<brokeredge-bpfm></brokeredge-bpfm>'
	 }).
	 when('/critical',{
			template: '<brokeredge-critical></brokeredge-critical>'
		}).
	when('/retail',{
		template:'<brokeredge-retail></brokeredge-retail>'
	}).
	when('/corporate',{
		template:'<brokeredge-corporate></brokeredge-corporate>'
	}).
	when('/management',{
		template:'<wecare-management></wecare-management>'
	}).
	when('/approach',{
		template:'<wecare-approach></wecare-approach>'
	}).
	when('/contact',{
		template:'<wecare-contact></wecare-contact>'
	}).
	when('/career',{
		template:'<wecare-career></wecare-career>'
	}).
	when('/privacy',{
		template:'<brokeredge-privacy></brokeredge-privacy>'
	}).
	when('/termOfUse',{
		template:'<brokeredge-term-Of-Use></brokeredge-term-Of-Use>'
	}).
	when('/quotes/:lob/:productType/:base64Input',{
		template:'<brokeredge-quotes></brokeredge-quotes>'
	}).
	when('/quotes/:lob/:productType/:requestId/:numResultsExpected',{
			template:'<brokeredge-quotes></brokeredge-quotes>'
	 }).
	 when('/myplan/:base64Input',{
			template:'<brokeredge-myplan><brokeredge-myplan>'
	 }).
	 when('/proposal/:productId/:rmId/:customerId/:appNo', {
		 controller: 'proposalController',
         templateUrl: 'templates/proposal/proposalTemplate.html',
        	 
     }).
	 when('/carProposal/:productType/:productId/:base64Input', {
    	 template:'<brokeredge-proposal></brokeredge-proposal>'
        	 
     }).
	 when('/homeProposal/:productType/:productId/:base64Input', {
    	 template:'<brokeredge-home-proposal></brokeredge-home-proposal>'
        	 
     }).
     when('/proposal/102MO01PC1/:rmId/:customerId/:appNo',{
    	 template:'<brokeredge-car-proposal></brokeredge-car-proposal>'
     }).
     when('/proposal/102HL03F01/:rmId/:customerId/:appNo',{
    	 template:'<brokeredge-health-proposal></brokeredge-health-proposal>'
     }).
     when('/proposal/102HL03F02/:rmId/:customerId/:appNo',{
    	 template:'<brokeredge-health-proposal></brokeredge-health-proposal>'
     }).
     when('/proposal/102HL03F03/:rmId/:customerId/:appNo',{
    	 template:'<brokeredge-health-proposal></brokeredge-health-proposal>'
     }).
     when('/PaymentConfirmation/:insurerId/:referenceId',{
    	 template:'<brokeredge-post-payment></brokeredge-post-payment>'
     }).
	when('/signin',{
		template:'<ins-signin></ins-singin>'
	}).
	otherwise({ redirectTo: '/index' });
	$locationProvider.html5Mode(false);
	$locationProvider.hashPrefix('!');
}]);



//Directive for File Upload
brokeredgeApp.directive('fileModel', ['$parse', function ($parse) {
	return {
	    require: "ngModel",
	    link: function postLink(scope,elem,attrs,ngModel) {
	      elem.on("change", function(e) {
	        var file = elem[0].files[0];
	        ngModel.$setViewValue(file);
	      })
	    }
	  };
}]);


brokeredgeApp.run(['$rootScope', '$location', '$route',  function($rootScope, $location,$route) {
	 	
			
	    $rootScope.$on('$locationChangeStart', function (event, next, current) {
	    	
	         console.log("brokeredgeApp_run_path",$location.path());
		});
}]);

//brokeredgeApp.config(['$qProvider', function ($qProvider) {
//    $qProvider.errorOnUnhandledRejections(false);
//}]);