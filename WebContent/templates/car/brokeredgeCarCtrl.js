"use strict";
brokeredgeApp.controller('brokeredgeCarCtrl',['$http','$scope','brokeredgefactory','carService','$location','$resource','$filter',function($http,$scope,brokeredgefactory,carService,$location,$resource,$filter){
	$scope.carInput={idv:'',claimsCurrYr:'N',policyType:'Rollover','productType':'PC', 'lob':'Motor'};
	$scope.makes='';
	$scope.models='';
	$scope.variants='';
	$scope.idvLow = 0;
	$scope.idvHigh = 0;
	$scope.carErrorText = "";
	$scope.idvValid = false;
	$scope.Rto='';
	$scope.yoms=[];
	$scope.loading = false;
	var age ,DepreciationRate;
	var carFormObj={
				 "vehicleId":"",
			     "vehicleRegistrationDate":"",
			     "idv":"",
			     "vehicleAge":"",
			     "price":"",
			     "policyStartDate":"",
			     "claim": "",
         	     "prevncb":"" ,
                 "rto": "",
                 "yom": "",
                 "typeOfBusiness": "",
                  "pinCode": ""
	           }
	
	
	// Date picker
	var dateToday=new Date();
	var currYear = dateToday.getFullYear();
	var currMonth = dateToday.getMonth();
	var currDay = dateToday.getDate();
	console.log(currYear);
	console.log(currMonth);
	console.log(currDay);
	
	// Define date Ranges

    $scope.startPolicyOptions={
	    	maxDate: new Date(currYear, currMonth, currDay+45),
	    	minDate: new Date(currYear, currMonth, currDay+1)
	}
    
    
    
    $scope.regdateOptions = {
    		maxDate: new Date(currYear, currMonth, currDay+15),
    		minDate: new Date(currYear, currMonth, currDay)
    };
    

    $scope.prevdateOptions = {
    		minDate: new Date(currYear, currMonth, currDay),
    		maxDate: new Date(currYear, currMonth, currDay+45)
    };
	
    console.log($scope.prevdateOptions);    
    $scope.open2 = function() {
		$scope.popup2.opened = true;
	};
    $scope.open1 = function() {
    	
		$scope.popup1.opened = true;
	};
	$scope.open3=function()
	{
		$scope.popup3.opened = true;
	}
	
	$scope.formats = ['yyyy-MM-dd','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];
	$scope.altInputFormats = ['M!/d!/yyyy'];
	$scope.popup2 = {
		opened: false
	};
	$scope.popup1 = {
		opened: false
	};
	$scope.popup3 = {
			opened: false
	};
	
	
	$scope.init=function()
	{
		$scope.carInput.lob = "Motor";
		$scope.getMake();
		$scope.getRto();
		getYoms();
//		$scope.getCar();
	}
	
//	$scope.getCar = function(){
//		$http({
//			method: 'POST',
//			url : 'http://testapi.brokeredge.in/rest/master/makes',
//			data : {
//				"authentication": {
//					"accesskey": "wecare",
//					"secretkey": "wecare"
//				  }
//			}
//			}).
//			then(function(data){
//			console.log(data)
//			$scope.makes= data.data['result'];
//		}).catch(error =>{
//			console.log(error)
//		})
//	}
	
//	$scope.getMake=function()
//	{
//		var input={
//				data:{
//					"authentication": {
//						"accesskey": "wecare",
//						"secretkey": "wecare"
//					  }
//				}
//				}
//		//input.url='/master/makes/';
//		
//		var tmp = carService.getMake().processRequest({} , JSON.stringify(input));
//		console.log(tmp)
//		tmp.$promise.then(function(data){
//			$scope.makes=data['result'];
//			
//		}).catch(error => {
//			console.log(error)
//		})


	// get car make
	$scope.getMake=function()
	{
		var input={
				url:'',
				data:{}
		}
		input.url='/master/makes/'
		var tmp=	carService.getMake().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			$scope.makes=data['result'];
			console.log("dataa",data)
			
//		})
//	}
			}).catch(error => {
			console.log(error)
		})
	}
	
	// get car Model
	$scope.getModel=function()
	{
		var input={
				url:'',
				data:{}
		}
		input.url="/master/models/"+$scope.carInput.make.id;
		var tmp=carService.getModel().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			$scope.models=data['result'];
		})
	}
	
	// get car Variant
	$scope.getVariant=function(carmodals)
	{
		
		var input={
				url:'',
				data:{}
		}
		
		input.url="/master/variants/"+$scope.carInput.make.id+'/'+$scope.carInput.model.id;
		var tmp=carService.getVariant().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			$scope.variants=data['result'];
			console.log(data);
			
		});
	
	}
	
	
	
	//get rto
	$scope.getRto=function()
	{
		var input={
				url:'',
				data:{}
		}
		input.url='/master/rto'
		//var tmp=carService.getRto();
		var tmp=carService.getRto().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			$scope.Rto=data['result'];
		});
	}
	
	//get Idv
	$scope.getIdv=function()
	{
		if($scope.carInput.variant && $scope.carInput.vehicledoReg){
			var age = 0;
			if ($scope.carInput.policyType =='Rollover') {
				age = currYear - Number($scope.carInput.yrOfManu) + 1;
			}			
			DepreciationRate=brokeredgefactory.getDepreciationRate(age);
			var idv = parseInt(DepreciationRate*$scope.carInput.variant.price);
			$scope.idvLow = 0.9*idv;
			$scope.idvHigh = 1.1*idv;
		    $scope.carInput.idv=parseInt(DepreciationRate*$scope.carInput.variant.price);
		    $scope.validateIDV();
		}
	 
	}
	// Date of registration Year
	function getYoms()
	{
		
		var endYear=currYear-1;
		var startYear = currYear -10;
		$scope.yoms = [];
		if($scope.carInput.policyType=='New Business'){
			var endYear=currYear;
			var startYear = currYear -1;
			
		}
		
		for(var i=endYear;i>=startYear;i--)
		{
			$scope.yoms.push(i);
		}
	}
	$scope.init();
	
	//change policy type
	$scope.policyTypeChange=function(bussinessType)
	{
		
		
		if($scope.carInput.policyType=='New Business') {
			$scope.carInput.prevPolicy='';
			$scope.carInput.currNcb='';
			$scope.carInput.vehicledoReg='';
			$scope.carInput.claimsCurrYr='N';
			$scope.startPolicyOptions.minDate = new Date();
			$scope.regdateOptions = {
		    		maxDate: new Date(currYear, currMonth, currDay+15),
		    		minDate: new Date(currYear, currMonth, currDay)
			};
			$scope.carInput.yrOfManu = currYear;
		} else
		{
			$scope.carInput.yrOfManu = Number(currYear) -1;
		}
		$scope.carInput.vehicledoReg = null;
		getYoms();
	}
	

	$scope.yomChange=function(yom) {
		
		$scope.carInput.vehicledoReg = null
		if ($scope.carInput.policyType == "Rollover") {

			var startYear = Number(yom);
			var endYear = startYear + 1;
			var endMonth = 11
			var endDay = 31;
			
			if (startYear == currYear || endYear == currYear){
				endYear = currYear;
				var endMonth = Number(currMonth)-10;
				var endDay = currDay;
			}
		    $scope.regdateOptions = {
		    		minDate: new Date(startYear, 0, 1),
		    		maxDate: new Date(endYear, endMonth, endDay)
		    };
		} else
		{
			$scope.regdateOptions = {
		    		maxDate: new Date(currYear, currMonth, currDay+15),
		    		minDate: new Date(currYear, currMonth, currDay)
		    };
		}
				
		console.log($scope.regdateOptions);
	}
	
	$scope.validateIDV = function(){
		
		if ($scope.carInput.idv < $scope.idvLow || $scope.carInput.idv > $scope.idvHigh ){
			
			$scope.carErrorText = "IDV out of Allowed Range";
			$scope.idvValid = false;
		} else
		{
			$scope.carErrorText = "";
			$scope.idvValid = true;
		}
		
	}
	
	
	//submit car form 
	
	$scope.submitForm=function() {
		
		if($scope.carInput.currNcb=="") {
			$scope.carInput.currNcb=0;
		}
		carFormObj.vehicleId=$scope.carInput.variant.makeId+'^'+$scope.carInput.variant.modelId+'^'+$scope.carInput.variant.variantId
		carFormObj.vehicleRegistrationDate=$filter('date')(new Date($scope.carInput.vehicledoReg),"yyyy-MM-dd");
		carFormObj.idv=$scope.carInput.idv;
		console.log($scope.carInput.vehicledoReg)
		carFormObj.vehicleAge=brokeredgefactory.calculateAge($scope.carInput.yrOfManu);
		console.log(carFormObj.vehicleAge)
		carFormObj.price=parseInt($scope.carInput.variant.price);
		//carFormObj.policyStartDate=$filter('date')(new Date($scope.carInput.polStartDate),'yyyy-MM-dd');
		carFormObj.policyCurrentExpDate=$filter('date')(new Date($scope.carInput.prevPolicy),'yyyy-MM-dd');
		if ($scope.carInput.policyType=='Rollover'){
			carFormObj.policyStartDate=$filter('date')(brokeredgefactory.addDate(carFormObj.policyCurrentExpDate, 0, 0, 1),'yyyy-MM-dd');
		}else
		{
			carFormObj.policyStartDate=carFormObj.vehicleRegistrationDate;
		}
		carFormObj.claim=$scope.carInput.claimsCurrYr;
		carFormObj.prevncb=$scope.carInput.currNcb;
		carFormObj.rto=$scope.carInput.RTO.rto;
		carFormObj.pincode=$scope.carInput.pincode;
		carFormObj.yom=$scope.carInput.yrOfManu;
		carFormObj.typeOfBusiness=$scope.carInput.policyType;
		carFormObj.vehicleName=$scope.carInput.variant.makeName+' '+$scope.carInput.variant.modelName+' '+$scope.carInput.variant.variantName;
		console.log(carFormObj)
		// Submit Quote Request
		submitQuoteRequest(carFormObj);

	}
	

	
	// SubmitQuoteRequest 
	
	function submitQuoteRequest(quoteInput){
		var input = {
    			url: "",
    			data:quoteInput
    			
    	};
		
		var quotes = $resource('./DataRequest', {}, {
	          processRequest: {
	             method: 'POST'}
	    });
		
		input.url = "/quote/submitAllQuotesRequest/" + $scope.carInput.lob + "/" + $scope.carInput.productType;
		//console.log(JSON.stringify($scope.quoteInput))
		//console.log(JSON.stringify(input));
		$scope.loading = true;
    	var tmp = quotes.processRequest({}, JSON.stringify(input));
    	tmp.$promise.then(function(data){
    		//console.log(data)
    		$scope.loading = false;
    		if (!data.hasOwnProperty("error")){
    			if (data.numResultsExpected == 0){
    				$scope.error = "No Quotes available for this selection";
    			} else
    			{
    				// Transfer Control to Quotes page
    				var url = "/quotes/" + $scope.carInput.lob +'/' + $scope.carInput.productType +'/' + data.requestId +'/'+ data.numResultsExpected ;
    				console.log(url);
			    	$location.path(url);
    			}
    		} else
    		{
	    	    console.error(data);
	    	    var str=data.error;
				$scope.carErrorText=str.split(";");
			}
    		
    	}, function (error) {
    		$scope.loading = false;
    	    console.error(data);
    	    $scope.carErrorText ="Unexpected error processing quote Request";
			
    	});
	}
	
	
	  
	$scope.setDate = function(year, month, day) {
		$scope.selectedOption.targetym = new Date(year, month, day);
	};
	//watch get the idv
	$scope.$watch('carInput.vehicledoReg', function(newVal, oldVal){
		  //var date = $scope.carInput.vehicledoReg
		  $scope.getIdv()	
	});
	$scope.$watch('carInput.polStartDate',function(newVal,oldVal){
		if (typeof(newVal) != "undefined") {
			$scope.prevdateOptions.maxDate=new Date(newVal);
			console.log($scope.prevdateOptions);
		}
		
	})
		//watch of choosing new Bussiness of registration Date
		
		

}]);
