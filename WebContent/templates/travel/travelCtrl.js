"use strict";
brokeredgeApp.controller('travelCtrl',['$scope','brokeredgefactory','$location','$resource','$filter',function($scope,brokeredgefactory,$location,$resource,$filter){
$scope.travelInput={member:[{dob:'',gender:'',adultChild:''}],productType:'LTS',adultAge:[],childAge:[]}
$scope.travelDuration=[];
$scope.ariaTravel=[{"name":'Worldwide',"val":"1"},{"name":'Worldwide excluding US and Canada',"val":"2"},{"name":'Asia Excluding Japan',"val":"3"},{"name":'Europe',"val":'4'}];
$scope.sumAssu=[{name:'Fifteen Thousand Only',val:'15000'},{name:'Twenty Thousand Only',val:'20000'},{name:'Twenty Five Thousand Only',val:'25000'},{name:'Thirty Thousand Only',val:'30000'},{name:'Fifty Thousand Only',val:'50000'},{name:'One Hundred Thousand Only',val:'100000'},{name:'Two Hundred Thousand Only',val:'200000'},{name:'Two Hundred Fifty Thousand Only',val:'250000'},{name:'Three Hundred  Thousand Only',val:'300000'},{name:'Five Hundred  Thousand Only',val:'500000'}];
$scope.travelFormObj={"doblist":'',"agelist":"","saType":"","days":"","adult":"","children":"","genderlist":"","sa":""}
$scope.dobList=[];
$scope.genderList=[];
$scope.travelInput.adultAge=[];
$scope.ageList='';
$scope.countries = [];
$scope.loading = false;
$scope.idvValid = false;

	
$scope.productType=function() {
	$scope.travelDuration=[];

	if($scope.travelInput.productType=='LTS')
	{
		$scope.travelInput.saType='INDV';
		
		for(var i=1;i<=364;i++)
		{
			$scope.travelDuration.push(i);
		}
	}
	else
	{
		$scope.travelInput.member=[];
		$scope.travelInput.member.push({dob:'',gender:'',adultChild:''});
		$scope.travelInput.saType='INDV';
		$scope.travelDuration=['30','45','60'];
	}

}

//add memeber
$scope.addMember=function()
{
	$scope.travelInput.member.push({dob:'',gender:'',adultChild:''});
}
//reset member
$scope.reset=function()
{
	$scope.travelInput.member=[];
	$scope.travelInput.member.push({dob:'',gender:'',adultChild:''});
}

	//submit form
	$scope.travelSubmit=function()
	{
		$scope.travelInput.lob ="Travel";
		
		if($scope.travelValidation()) {
			$scope.genderList = [];
			$scope.dobList = [];
			$scope.ageList = [];
			
			angular.forEach($scope.travelInput.member,function(m,n){
						$scope.t_member=m;
					if($scope.t_member.adultChild=='Adult')
					{
						$scope.travelInput.adultAge.push(brokeredgefactory.calculateAge($scope.t_member.dob));
						$scope.dobList.push($filter('date')($scope.t_member.dob, "yyyy-MM-dd"))
						$scope.genderList.push($scope.t_member.gender)
					}
					else if($scope.t_member.adultChild=='Child')
					{
						$scope.travelInput.childAge.push(brokeredgefactory.calculateAge($scope.t_member.dob));
						$scope.dobList.push($filter('date')($scope.t_member.dob, "yyyy-MM-dd"));
						$scope.genderList.push($scope.t_member.gender)
					}
			});
			
			
			
			// Update list
			
			$scope.ageList=_.concat($scope.travelInput.adultAge,$scope.travelInput.childAge);
			
			$scope.travelFormObj.adult=String($scope.travelInput.adultAge.length);
			$scope.travelFormObj.children=String($scope.travelInput.childAge.length);
			$scope.travelFormObj.doblist=$scope.dobList.join();
			$scope.travelFormObj.agelist=$scope.ageList.join()
			$scope.travelFormObj.saType=$scope.travelInput.saType		
			$scope.travelFormObj.days=$scope.travelInput.durationOfTravel
			$scope.travelFormObj.genderlist=$scope.genderList.join();
			$scope.travelFormObj.sa=$scope.travelInput.sumAssus.val;
			var countryCodes = [];
			var countryNames = [];
			angular.forEach($scope.travelInput.selectedCountries,function(value,key){
				countryCodes.push(value.countryCode);
				countryNames.push(value.countryName);
			});
			$scope.travelFormObj.countrylist=countryCodes.join();
			$scope.travelFormObj.countryNames=countryNames.join();
			
			console.log($scope.travelFormObj);
			//Travel/LTS
			submitQuoteRequest($scope.travelFormObj);
		}
		
	}


	// SubmitQuoteRequest 
	
	function submitQuoteRequest(quoteInput){
		var input = {
				url: "",
				data:quoteInput
				
		};
		
		var quotes = $resource('./DataRequest', {}, {
	          processRequest: {
	             method: 'POST'}
	    });
		
		input.url = "/quote/submitAllQuotesRequest/" + $scope.travelInput.lob + "/" + $scope.travelInput.productType;
		//console.log(JSON.stringify($scope.quoteInput))
		//console.log(JSON.stringify(input));
		var tmp = quotes.processRequest({}, JSON.stringify(input));
		$scope.loading = true;
		tmp.$promise.then(function(data){
			//console.log(data)
			$scope.loading = false;
			if (!data.hasOwnProperty("error")){
				if (data.numResultsExpected == 0){
					$scope.error = "No Quotes available for this selection";
				} else
				{
					// Transfer Control to Quotes page
					var url = "/quotes/" + $scope.travelInput.lob +"/" +$scope.travelInput.productType +'/' + data.requestId +'/'+ data.numResultsExpected ;
					console.log(url);
			    	$location.path(url);
				}
			} else
			{
	    	    console.error(data);
	    	    var str=data.error;
				$scope.quoteError.errors=str.split(";");
			}
			
		}, function (error) {
			$scope.loading = false;
		    console.error(data);
		    $scope.error ="Unexpected error processing quote Request";
			
		});
	}

$scope.checkAdult=function()
{
	var ok,dob;
	angular.forEach($scope.travelInput.member,function(i,j){
		$scope.t_member=i;
		if($scope.t_member.adultChild=='Adult')
			{
				dob=brokeredgefactory.calculateAge($scope.t_member.dob);
				if(dob<18)
				{
					//travelForm.adultchild.$setValidity("required", false);
					ok=false;
					$scope.errMessage='Adults must be older than 18';
				}
				else
				{
					//$scope.adult.push('Adult');
					ok=true;
					$scope.errMessage='';
				}
			}
	});
	
	return ok;			
}
	$scope.travelValidation=function(travelForm) {
		var ok = true;
		$scope.adult=[];
		$scope.child=[];
		var dob;	
		$scope.errMessage='';
		if ($scope.travelInput.saType == "FF" && $scope.travelInput.member.length <2){
			$scope.errMessage='Need atleast 2 members for Family Floater';
			ok = false;
		}

		angular.forEach($scope.travelInput.member,function(member,key){
			
			if(member.adultChild=='Adult') {
				dob=brokeredgefactory.calculateAge(member.dob);
				if(dob<18)
				{
					//travelForm.adultchild.$setValidity("required", false);
					$scope.errMessage='Adults must be older than 18';
					ok = false;
				}
				else
				{
					$scope.adult.push('Adult');
				}
				
			} else 
			
			{
				
				dob=brokeredgefactory.calculateAge(member.dob);
				if(dob<25){
					$scope.child.push('Child');
					
				} else
				{
					$scope.errMessage='Child Age must be less than 25';
					ok = false;
				}
			}
			
			
		});
		
		/*if ($scope.adult.length == 0 && $scope.travelInput.saType == "FF"){
			$scope.errMessage='Must have atleast 1 Adult';
			ok = false;
		}*/
		
		return ok;
	}
	
	//get Countries
	function getCountries()
	{
		var input={
				url:'/master/countries',
				data:{}
		}
				
		var res = $resource('./DataRequest', [], {
	          save: { method: 'POST' }
	    });
		
		var tmp = res.save(input);
		tmp.$promise.then(function(data){
			$scope.countries =$filter('orderBy')(data['result'], 'countryName');
			console.log($scope.countries);
		});
	}
	
	$scope.productType();
	getCountries();
	$scope.travelInput.saType='INDV';
	
	$scope.countryChange = function(){
		var countryNames = [];
		angular.forEach($scope.travelInput.selectedCountries,function(value,key){
			countryNames.push(value.countryName);
		});
		
		$scope.countryNames=countryNames.join();
	}
	
	//Date picker
    var dateToday = new Date();
    var yearMin = dateToday.getFullYear() - 100;
    var yearMax = dateToday.getFullYear();
    var monthToday = dateToday.getMonth();
    var dayToday = dateToday.getDate()-1;
    $scope.dateOptions = {
        maxDate: new Date(yearMax, monthToday, dayToday),
        minDate: new Date(yearMin, monthToday, dayToday)
      };
	   $scope.opened = [];
	   $scope.open = function($event,$index) {
	            $scope.opened[$index] = true;
	        };
	 $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
}]);