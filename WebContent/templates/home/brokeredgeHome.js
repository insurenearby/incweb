"use strict";
brokeredgeApp.component('brokeredgeHome',{
	templateUrl: 'templates/home/home.html',
	controller:['$scope','$location','$routeParams',function($scope,$location,$routeParams){
		// caraousel
		$scope.tab = 1;
		$scope.active = 0;
	    $scope.setTab = function(newTab){
	      $scope.tab = newTab;
	    };
	    $scope.isSet = function(tabNum){
	      return $scope.tab === tabNum;
	    };
	    // car deatails
//	    $scope.carSet=1;
//	    $scope.setCarTab=function(newTab){
//	    	$scope.carSet = newTab;
//	    }
//	    $scope.isSetCar = function(tabNum){
//	      return $scope.carSet === tabNum;
//	    };
	    function init()
	    {
	    	if ($routeParams.hasOwnProperty("section")){
    			switch($routeParams.section){
    			case "Car":
    				$scope.active = 0;
    				break;
    			case "Health":
    				console.log($routeParams.section)
    				$scope.active = 1;
    				break;
    			case "Travel":
    				$scope.active = 2;
    				break;
    			case "Home":
    				$scope.active = 3;
    				break;
    			case "Term Life":
    				$scope.active = 5;
    				break;
    			}
			}
			
			setInterval(function() {
			$('.blink_me').fadeOut(500);
			$('.blink_me').fadeIn(500);
		},1000);
	    }
	    
	    //checkforRedirect();
	    init();
	    
	    
	    //Check query Parameters to see if we need to go to another url
	    
	    function checkforRedirect(){
	    	
	    	var searchObject = $location.search(); //= $location.search('redirectUrl');
	    	if (searchObject.hasOwnProperty("redirectUrl")){
	    		$location.path(searchObject.redirectUrl);
	    	}
	    }

		
	   
			
	}]
})