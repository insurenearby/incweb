"use strict";
brokeredgeApp.controller('brokeredgeHeaderCtrl',['$scope','$location','$log',function($scope, $location, $log){
	
	$scope.showheader = true;
	var path = $location.path();
	console.log("path",path);
	if (path == "/underconstruction"){
		$scope.showheader = false;
	}
	
	$scope.menuTop=[
					{
						title:'About Us',
						action:'',
						class:'hdeeplink',
						menuTop:[
							{
								action:'#!/management',
								title:'Management'
							},
							{
								action:'#!/approach',
								title:'Approach'
							},
							{
								action:'#!/contact',
								title:' Contact us'
							},
							{
								action:'#!/career',
								title:'Careers'
							}
						  ]
					},
					{
						title:'Downloads',
						action:'',
						class:'fdeeplink'
					},
					{
						title:"FAQ's",
						action:'',
						class:'fdeeplink'
					},
					{
						title:"Knowledge Bank",
						action:'',
						class:'hidden'
					},
					{
						title:"Blog",
						action:'',
						class:''
					}
				 ]
	
	$scope.menus=[
					{
						title:'HOME',
						action:'#',
						class:'hdeeplink'
					},
					{
						title:'SUPPORT',
						action:'',
						class:'hdeeplink',
						menus:[
								 {
									title:'COMPLAINTS',
									action:'#!/complaint'
								 },
								{
									title:'SERVICE REQUEST',
									action:'#!/service-request',
								},
								{
									title:'QUERIES',
									action:'#!/quiries',
								}
							 ]
					},
					{
						title:'SIGN IN/REGISTER',
						action:'#',
						class:''
					}
				]

	$scope.status = {
    isopen: false
  };

  $scope.toggled = function(open) {
    $log.log('Dropdown is now: ', open);
  };

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };
}])