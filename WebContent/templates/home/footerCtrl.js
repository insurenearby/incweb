"use strict";
brokeredgeApp.controller('footerCtrl',['$scope',function($scope){
	$scope.menus=[
					{siteMap:[{
								title:'HOME',
								action:'',
								class:'fdeeplink'
							},
							{
								title:'BEST POLICY FOR ME ',
								action:'',
								class:''
							},
							{
								title:'INSURER RATING',
								action:'',
								class:''
							},
							]},{
				siteMap:[
							{
								title:'OUR SOLUTIONS ',
								action:'',
								class:''
							},
							{
								title:'SUPPORT',
								action:'',
								class:''
							},
							{
								title:'FAQS',
								action:'',
								class:''
							},
						 ]
						 },
						 {
				siteMap:[
							{
								title:'ABOUT US',
								action:'',
								class:''
							},
							{
								title:'DOWNLOADS',
								action:'',
								class:''
							},
							{
								title:'BLOG',
								action:'',
								class:''
							},
						 ]
						}
				]
	$scope.ulft=[
				{
					title:'Insurance Regulatory and Development Authority',
					action:'http://www.irda.gov.in',
					class:''	
				},
				{
					title:' Insurance Institute of India',
					action:'https://www.insuranceinstituteofindia.com/',
					class:''	
				},
				{
					title:'National Insurance Academy',
					action:'http://www.niapune.com/',
					class:''	
				},
				{
					title:'IRDA Customer Education',
					action:'http://www.policyholder.gov.in/',
					class:''	
				},
				{
					title:'Grievance',
					action:'http://www.policyholder.gov.in/IGMS_Complaint_Logging.aspx',
					class:''
				},
				{
					title:'Life Insurance Council ',
					action:'http://www.lifeinscouncil.org/',
					class:''	
				}
				];
		$scope.icowrap=[
						{
							text:'Value for money',
							imgUrl:'value'
						},
						{
							text:'Trusted by buyers',
							imgUrl:'trusted'
						},
						{
							text:'No Spamming',
							imgUrl:'no-spams'
						},
						{
							text:'Best Rates',
							imgUrl:'rates'
						},
						{
							text:'Quotes Compared',
							imgUrl:'quotes'
						},
						{
							text:'Happy Customers',
							imgUrl:'customers'
						},
						{
							text:'Customer Centric',
							imgUrl:'centric'
						},
						{
							text:'Insta Assistance',
							imgUrl:'assistance'
						}
					   ];
	$scope.footerList=[{
						name:'Terms of Use',
						action:'#!/termOfUse'
					   },
					   {
					   	name:'Privacy Policy',
					   	action:'#!/privacy'
					   },
					    {
					   	name:'Contact us',
					   	action:'#!/contact'
					   },
					   ]
}])