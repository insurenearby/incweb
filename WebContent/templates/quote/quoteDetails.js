"use strict";
brokeredgeApp.controller('quoteDetailCtrl',['$scope', '$sce', '$uibModalInstance','selectedProduct', function($scope,$sce,$uibModalInstance, selectedProduct){
	
	$scope.selectedProduct = selectedProduct;
	//console.log($scope.selectedProduct);
	var attributes = [];
	angular.forEach($scope.selectedProduct.attributes,function(attribute, key){
		var item = {};
		angular.forEach(attribute,function(value, name){
			item.name = name;
			item.value = value;
			attributes.push(item);
		})
	});
	$scope.selectedProduct.attributes =attributes;
	angular.forEach($scope.selectedProduct.covers,function(cover, key){
		// refcator cover limits
		var coverLimit = {};
		angular.forEach(cover.coverLimits,function(limit, key1){
			var limitText = "";
			if (limit.hasOwnProperty("period")){
				var periodText = limit.period;
				switch(limit.period){
				case "AOY":
					periodText = "In a Policy Year";
					break;
				case "AOH":
					periodText = "Per Hospitalization";
					break;
				case "AOH":
					periodText = "Per Claim";
					break;
				}
				limitText = periodText + ": ";								
			}
			limitText = limitText + limit.limitValue;
			

			
			// get Type of Value;
			
			if (limit.subLimit == "Maximum Claims Amount" 
			&& (limit.limitType == "Value" || limit.SubLimit =="Formula")
			){
				limit.inrSymbol = "Y";
			}
			
			
			
			if (limit.hasOwnProperty("cond")){
				limitText = limitText + "( " + limit.cond + " ) ";								
			}
			limit.value = limitText + "<Br>";
			
			if (!coverLimit.hasOwnProperty(limit.sublimit)){
				coverLimit[limit.sublimit] = [];
			}
			coverLimit[limit.sublimit].push(limit);			
		});
		cover.coverLimits = coverLimit;
	});
	console.log($scope.selectedProduct);
	
	$scope.ok = function () {
		$uibModalInstance.close(''); // Put the result here
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	$scope.getHtml = function(html){
        return $sce.trustAsHtml(html);
    };
}]);
brokeredgeApp.filter('html', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});