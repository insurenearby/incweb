﻿/**
 * This controller is common quotes display controller for all Quotes
 */
"use strict";
brokeredgeApp.component('brokeredgeQuotes',{
	templateUrl:'templates/quote/quotes.html',
	controller:['$scope','$window','brokeredgefactory','$uibModal','$routeParams','$location','$filter','$resource', '$interval', function($scope, $window, brokeredgefactory, $uibModal, $routeParams, $location, $filter, $resource,$interval){
		$scope.compareViewQuotes=[];
		$scope.coverOptions=[];
		$scope.Quotesheader=true;
		$scope.hasAddons = false;
		$scope.toggle = false;
		$scope.searchCoversRefine={};
		var toggle_button=false;
		$scope.allQuotes = [];
		$scope.numQuotesReturnedLast = 0;
		$scope.displayQuotes = [];
		$scope.email='';
		$scope.quoteError = {};
		$scope.quoteInput = {};
		$scope.oneAtATime=true;
		$scope.input = {
			lob : "",
			productType : ""
		}
		$scope.quoteResuestId = "";
		$scope.numResultsExpected = 0;
		$scope.numResultsDisplayed = 0;
		$scope.availableAddons = [];
		var GSTRate = 1.18;
		$scope.filters = [];
		$scope.quoteFetchComplete = false;
		$scope.quoteFetchSuccess = false;
		$scope.coverFetchComplete = false;
		$scope.quotefetchError = false;
		$scope.addSub={};
		var resultStopTime = 0;
		$scope.recomendations = [];
		
		$scope.portfolioheaders = [];
		$scope.health=['Sum Assured','']
		$scope.portfolioheaderLabels = [
		   {
	          "label": "Sum Assured",
	          "value": "sa"
	        },
	        {
	          "label": "Covered upto Age",
	          "value": "mma"
	        },
	        {
	          "label": "Years to pay",
	          "value": "ppt"
	        },
	        {
	          "label": "Payment Frequency",
	          "value": "mode"
	        }
	        
	      ];
		
		var resultPromise = null;
		function init(){
			
			$scope.input.lob = $routeParams.lob;
			$scope.input.productType = $routeParams.productType;
			$scope.input.requestId = $routeParams.requestId;
			$scope.numResultsExpected = $routeParams.numResultsExpected;
			
			if($scope.input.productType=='CIL'){$scope.subTitle='Critical Illness'}
			else if($scope.input.productType=='PC'){$scope.subTitle='Motor'}
			else if($scope.input.productType=='Term'){$scope.subTitle='Life';}
			else if($scope.input.lob=='Travel'){$scope.subTitle='Travel'}
			else if($scope.input.lob=='HOME'){$scope.subTitle='Home'}
			else if($scope.input.productType=='MI'){$scope.subTitle='Investments'}
			else if($scope.input.productType=='FF'||$scope.input.productType=='INDV'||$scope.input.productType=='TOPUP'){
				$scope.subTitle='Health'
			}
			//console.log($scope.quoteInput);
			
			$scope.getCoversToSelect($scope.input.lob,$scope.input.productType);
			getDisplayFilters($scope.input.lob,$scope.input.productType);
			
			// Get Results

			$scope.quoteFetchSuccess = false;
			
			resultStopTime = new Date().getTime() + 50000;
			var resultPromise = $interval(getResults,5000, 10, false);
		}
		
		function populateHeader(){
    	
			console.log($scope.quoteInput);
			
			if($scope.input.productType=='Term'||$scope.input.productType=='CIL')
			{
				for (var i = 0; i<$scope.portfolioheaderLabels.length; i++){
	    			var head = {};
	    			head.label = $scope.portfolioheaderLabels[i].label;
	    			var value = $scope.portfolioheaderLabels[i].value;
	    			switch (value){
	    			case "sa":
	    				head.value = $filter('currency')($scope.quoteInput.sa, "₹", 0)
	    				break;
	    			case "mma":
	    				head.value =$scope.quoteInput.mma
	    				break;
	    			case "ppt":
	    				head.value = $scope.quoteInput.ppt
	    				break;
	    			case "mode":
	    				if ($scope.quoteInput.mode == "1"){
	    					head.value = "Yearly"
	    				}else
	    				{
	    					head.value = "One Time"
	    				}	    				
	    				break;
	    			}
	    			$scope.portfolioheaders.push(head);
	    			
	    		}
			}
			else if($scope.input.productType=='FF'||$scope.input.productType=='INDV')
			{
				$scope.portfolioheaders=[{"label":'Policy',"value":''},{"label":'Members ages',"value":''},{"label":'Sum Insured',"value":''},{"label":'Tenure',"value":''}]
				
				if($scope.quoteInput.children!=0){
					$scope.portfolioheaders[0].value=$scope.quoteInput.adult+ ' adult(s) , '+$scope.quoteInput.children+' children(s)';	
				}
				else{
					$scope.portfolioheaders[0].value=$scope.quoteInput.adult+ 'adult(s)'
				}
				
				//console.log($scope.quoteInput)
				var ages = "";
				for (var i =0; i<$scope.quoteInput.members.length; i++ ){
					var age = brokeredgefactory.calculateAge($scope.quoteInput.members[i].dob);
					if (ages.length>0){
						ages = ages + ", ";
					}
					ages = ages + age;
				}
				$scope.portfolioheaders[1].value= ages +' Yrs';
				
				//var s=$filter('currency')($scope.quoteInput.sa, "&#8377;", 1);
				$scope.portfolioheaders[2].value=$filter('currency')($scope.quoteInput.sa, "₹", 0);
				$scope.portfolioheaders[3].value=$scope.quoteInput.tenure+ 'Yr';
				
			}
			else if($scope.input.productType=='TOPUP')
			{
				$scope.portfolioheaders=[{"label":'Policy',"value":''},{"label":'Oldest members age',"value":''},{"label":'Sum Insured',"value":''},{"label":'Deductible',"value":''},{"label":'Tenure',"value":''}]
				
				if($scope.quoteInput.children!=0){
					$scope.portfolioheaders[0].value=$scope.quoteInput.adult+ ' adult(s) , '+$scope.quoteInput.children+' children(s)';	
				}
				else{
					$scope.portfolioheaders[0].value=$scope.quoteInput.adult+ 'adult(s)'
				}
				
				//console.log($scope.quoteInput)
				$scope.arr=$scope.quoteInput.agelist.split(',').map(parseFloat);
				$scope.portfolioheaders[1].value=_.max($scope.arr)+' Yrs';
//				
				//var s=$filter('currency')($scope.quoteInput.sa, "&#8377;", 1);
				$scope.portfolioheaders[2].value=$filter('currency')($scope.quoteInput.sa, "₹", 0);
				$scope.portfolioheaders[3].value=$filter('currency')($scope.quoteInput.deductible, "₹", 0);
				$scope.portfolioheaders[4].value=$scope.quoteInput.tenure+ 'Yr';
				
			}
			else if($scope.input.lob=='Travel') {

				 $scope.portfolioheaders=[{"label":'Members',"value":''},{"label":'Sum Insured',"value":''},{"label":'Countries Visiting',"value":''},{"label":'Travel Days',"value":''}]
				 var hdr1Value = "";	
				 if($scope.quoteInput.children!=0){
					 hdr1Value=$scope.quoteInput.adult+ ' adult(s) , '+$scope.quoteInput.children+' children(s)';	
				 }
				 else{
					hdr1Value=$scope.quoteInput.adult+ ' adult(s)'
				 }
			 	
				hdr1Value = hdr1Value + "/ ages: " + $scope.quoteInput.agelist;
			 	$scope.portfolioheaders[0].value= hdr1Value;			 	
				$scope.portfolioheaders[1].value=$filter('currency')($scope.quoteInput.sa, "USD$", 0);
				if ($scope.quoteInput.isSchengen){
					$scope.portfolioheaders[1].value=$filter('currency')($scope.quoteInput.sa, "EUR", 0);
				}
				$scope.portfolioheaders[2].value = $scope.quoteInput.countryNames;
				$scope.portfolioheaders[3].value=$scope.quoteInput.days;
			}
			else if($scope.input.productType=='PC'){
				$scope.portfolioheaders=[{"label":'MOTOR',"value":''},{"label":'RTO',"value":''},{"label":'Applicable NCB',"value":''}]
				$scope.portfolioheaders[0].value=$scope.quoteInput.vehicleName;
				$scope.portfolioheaders[1].value=$scope.quoteInput.rto;
				$scope.portfolioheaders[2].value = "0%";
				if ($scope.quoteInput.typeOfBusiness == "Rollover") {
					$scope.portfolioheaders[2].value=$scope.getApplicableNCB($scope.quoteInput.prevncb, $scope.quoteInput.claim) + "%";
				}
				
			}
			else if($scope.input.productType=='Home'){
				$scope.portfolioheaders=[{"label":'Building SI',"value":''},{"label":'Total Content SI',"value":''},{"label":'Policy Type',"value":''},{"label":'Tenure',"value":''}]
				$scope.portfolioheaders[0].value=$filter('currency')($scope.quoteInput.sastruct, "₹", 0);
				$scope.portfolioheaders[1].value=$filter('currency')($scope.quoteInput.contentTotal, "₹", 0);
				var typeText = "";
				switch ($scope.quoteInput.policyType){
				case 1:
					 typeText = "Building and Content";
					 break;
				case 2:
					 typeText = "Building Only";
					 break;
				case 3:
					 typeText = "Content Only";
					 break;
				}
				$scope.portfolioheaders[2].value=typeText;
				var pt = "";
				if ($scope.quoteInput.structpt > 0) {
					pt = "Building :" + $scope.quoteInput.structpt; 
				}
				if ($scope.quoteInput.contentpt > 0) {
					if (pt != ""){
						pt = pt + " / "
					}
					pt = pt + "Content :" + $scope.quoteInput.contentpt; 
				}
				$scope.portfolioheaders[3].value = pt + " Years";
			}
    		
    	}
		
		$scope.getCoversToSelect = function(lob, productType){
			var input = {
	    			url: "",
	    			data : {}
	    	};
			
			var response = $resource('./DataRequest', [], {
		          processRequest: {
		             method: 'POST'}
		    });
			
			
	    	input.url = "/master/getCoversToSelect/" + $scope.input.lob + "/" + $scope.input.productType;
	    	
	    	var tmp = response.processRequest({}, JSON.stringify(input));
	    	
			tmp.$promise.then(function(data){
				
				var filterExp = {display: "Y", mandatory : "N" };
	    		if (data.hasOwnProperty("result")){
	    			$scope.allCovers = data.result;
	    			var covers =$filter('filter')(data.result, filterExp);
	    			
	    			angular.forEach(covers, function(value, key) {
	    				value.selected = false;
	    				value.display = false;
	    				$scope.coverOptions.push(value);
	    			});
	    			//console.log($scope.coverOptions);
	    			$scope.coverFetchComplete = true;
	    		}
	    	}, function (error) {
	    	    console.error(data);
	    	    /*$scope.quoteError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str="Unexpected error from Insurer while processing the policy";
				$scope.proposalError.errors=str.split(";");
				$scope.quoteFetchComplete = true;
	    	    $scope.quoteFetchError = true;*/
	    	});
		}
		
		function getDisplayOrder(coverId){
			
			var displayOrder = 99;
			for (var i=0; i<$scope.allCovers.length; i++){
				if ($scope.allCovers[i].coverid == coverId){
					displayOrder = $scope.allCovers[i].displayOrder;
					break
				}
				
			}
			
			return displayOrder;
		}
		
		// Adds or deletes covers from selected covers array.
		$scope.processCoverOptions = function(){
			
			if ($scope.coverFetchComplete && $scope.quoteFetchSuccess){
				$scope.displayQuotes = [];
				$scope.recomendations = [];
				$scope.availableAddons = []; // new Selection may change addon covers;
				var selectedCovers = getSelectedCovers();
				console.log(selectedCovers);
				// Get Selected Refine search Options
				//console.log($scope.allQuotes);
				
				$scope.allQuotes = $filter('filter')($scope.allQuotes, {online:true} );
				angular.forEach($scope.allQuotes, function(value, key) {
					// Check each
					var quote = value;
					var displayPremium = quote.totalPremium;
					var stdCovers = getStandardQuoteCovers(quote);
					var addonCovers = getAddonQuoteCovers(quote);
					
					quote.display = true;
					for (var i = 0; i<selectedCovers.length; i++){
						quote.display = false;
						for (var j = 0; j<stdCovers.length; j++){
							if (selectedCovers[i] == stdCovers[j]){
								quote.display = true;
								break;
							}
						}
																		
						// If found in std cover then no need to add cover Premium
						if (!quote.display){ // Check in addon  covers
							for (var j = 0; j<addonCovers.length; j++){
								if (selectedCovers[i] == addonCovers[j]){
									quote.display = true;
									displayPremium = Number(displayPremium) + getAddonPremium(quote, selectedCovers[i]);
									break;
								}
							}
						}
						
						if (!quote.display){ // exit if even one selected cover is absent
							break;
						} 
					}
					
											
					if (quote.display){
						// Get new list of Addon Covers
						for (var j=0; j< addonCovers.length; j++) {
							if($scope.availableAddons.indexOf(addonCovers[j])<0){
								$scope.availableAddons.push(addonCovers[j]);
							} 
						}
						
						for (var j=0; j< stdCovers.length; j++) {
							if($scope.availableAddons.indexOf(stdCovers[j])<0){
								$scope.availableAddons.push(stdCovers[j]);
							} 
						}
						quote.displayPremium = displayPremium;
						quote.key = quote.productId;
						if (quote.hasOwnProperty("planId")){
							quote.key = quote.key + "^" + quote.planId;
						}
						quote.selectedForCompare = false;
						$scope.displayQuotes.push(quote);
					}
					
				});
				
				
				// Apply Refine filters
				
				angular.forEach($scope.filters, function(value, key) {
					var filter = value;
					if (filter.selectedId !="A"){ // "A" means select all
						var filterExp = {};
						filterExp[filter.key] = filter.selectedId;
						$scope.displayQuotes = angular.copy($filter('filter')($scope.displayQuotes, filterExp));
					}
				});
				
				// Check if coveroption is to be displayed
				for (var i=0; i<$scope.coverOptions.length; i++){
					var coverId = $scope.coverOptions[i].coverid;
					//$scope.coverOptions[i].display = false;
					if (isCoverAvailable(coverId)){
						$scope.coverOptions[i].display = true;
					}
				};
				//console.log($scope.coverOptions);
				$scope.coverOptions = angular.copy($filter('filter')($scope.coverOptions, {"display":true}));
				if ($scope.coverOptions.length>0){
					$scope.hasAddons = true;
				}else
				{
					$scope.hasAddons = false;
				}
				// Sort the display quotes 
				$scope.displayQuotes = angular.copy(angular.copy($filter('orderBy')($scope.displayQuotes, 'displayPremium')));
				//if ($scope.displayQuotes.length != $scope.numResultsDisplayed) {
					$scope.numResultsDisplayed = $scope.displayQuotes.length;
					createRecommendations();
				//}
				
			}

		};
		
		function isCoverAvailable(coverId) {
			
			var available = false;
			
			for (var i=0; i<$scope.availableAddons.length; i++){
				if ($scope.availableAddons[i] == coverId){
					available = true;
					break;
				}
			}
			
			return available;
			
		}
		$scope.getApplicableNCB = function (currNCB, claim){
			var ncb = 0;
			if (claim == "N"){
				var numNcb = Number(currNCB);
				switch (numNcb){
				case 0:
					ncb = 20;
					break;
				case 20:
					ncb = 25;
					break;
				case 25:
					ncb = 35;
					break;
				case 35:
					ncb = 45;
					break;
				case 45:
				case 50:
					ncb = 50;
					break;
				default:
					ncb = 0;
					break;
				}
			}
			return ncb;	
		}
		
		
		function getDisplayFilters(lob, productType){
			var input = {
	    			url: "",
	    			data : {}
	    	};
			
			var response = $resource('./DataRequest', [], {
		          processRequest: {
		             method: 'POST'}
		    });
			
			var filters = [];
	    	input.url = "/master/getFilters/" + $scope.input.lob + "/" + $scope.input.productType;
	    	var tmp = response.processRequest({}, JSON.stringify(input));
			tmp.$promise.then(function(data){
				
	    		if (data.hasOwnProperty("result")){
	    			
	    			angular.forEach(data.result, function(value, key) {
	    				var filter = value;
	    				filter.options = [];
	    				filter.selectedId = "A";
	    				var options  = JSON.parse(filter.displayText);
	    				
	    				angular.forEach(options, function(value, key1) {
	    					var option = {};
	    					for(var k in value){
	    						option.id = k;
		    					option.value = value[k];
	    					}
	    					filter.options.push(option);
	    				});
	    				filter.options.push({"id" : "A", "value": "All"});
	    				
	    				filters.push(filter);
	    			});
	    			$scope.filters = filters;
	    			
	    			
	    		}
	    	}, function (error) {
	    	    console.error(data);
	    	    /*$scope.quoteError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str="Unexpected error from Insurer while processing the policy";
				$scope.proposalError.errors=str.split(";");
				$scope.quoteFetchComplete = true;
	    	    $scope.quoteFetchError = true;*/
	    	});
			
		}
		
		// Bunch up quotes by an insurer together
		function createRecommendations(){
			
			var insurers = [];
			var reco = {};
			//console.log($scope.displayQuotes)
			angular.forEach($scope.displayQuotes, function(value, key) {
				var quote = value;
				var id = quote.insurerId;
				quote.selectedForCompare = false;
				$scope.compareViewQuotes=[];
				//quote.summaryDisplays = $scope.createSummaryDisplay(quote.productId);
				//console.log(insurers)
				if (!itemExists(insurers, id)){
					
					insurers.push(id);
					var item = {};
					item.insurerId = quote.insurerId;
					item.currentIndex = 0;
					item.index = 0;
					item.productCount = 0;
					
					reco[id] = item;
					reco[id].quotes = [];
					
				}
				reco[id].productCount = reco[id].productCount + 1;
				// display name should be product name plus plan name
				if (quote.hasOwnProperty("planName")){
					quote.displayName = quote.productName + " - " + quote.planName;
				}else
				{
					quote.displayName = quote.productName;
				}
				reco[id].quotes.push(quote);
			});
			
			//console.log(insurers);
			angular.forEach(insurers, function(value, key1) {
				var id = value;
				//console.log(value)
				$scope.recomendations.push(reco[id]);
			});
			//console.log($scope.recomendations);
		}
		
		function itemExists(array, item){
			var exists= false;
			for (var i = 0; i< array.length; i++){
				if (array[i] == item){
					exists= true;
					break;
				}
			}
			
			return exists;
		}
		
		$scope.createSummaryDisplay = function (productId){
			var sumDisplays = [];
			var displays = $scope.createDisplayAttributes(productId);
			
			for (var j=0; j<displays.length; j++){
				var display = displays[j];
				if (display.summary == "Y"){
					sumDisplays.push(display.text);
				}
			}
			
			return sumDisplays;
		}
		
		
		function getAddonPremium(quote, coverid){
			var premium = 0;
			if (quote.hasOwnProperty("premiumbreakup")){
				for (var j =0; j <quote.premiumbreakup.addon.length; j++){
					var cover = quote.premiumbreakup.addon[j];
					if (cover.coverId == coverid){
						if (cover.netPremium){
							premium =  Math.round(Number(cover.premium));
						} else
						{
							premium =  Math.round(GSTRate*Number(cover.premium));
						}
						break;
						break;
					}
				}
				
			}
			
			if (quote.hasOwnProperty("premiumBreakup")){
				for (var j =0; j <quote.premiumBreakup.addon.length; j++){
					var cover = quote.premiumBreakup.addon[j];
					if (cover.coverId == coverid){
						if (cover.netPremium){
							premium =  Math.round(Number(cover.premium));
						} else
						{
							premium =  Math.round(GSTRate*Number(cover.premium));
						}
						break;
					}
				}
			}
			return premium;
		}
		
		// Get Quote Information
		$scope.getAllQuotes = function(quotesInput){
			
			var input = {
	    			url: "",
	    			data:quotesInput
	    			
	    	};
			
			var quotes = $resource('./DataRequest', {}, {
		          processRequest: {
		             method: 'POST'}
		    });
			
			input.url = "/quote/allquotes/" + $scope.input.lob + "/" + $scope.input.productType;
			//console.log(JSON.stringify($scope.quoteInput))
			//console.log(JSON.stringify(input))
	    	var tmp = quotes.processRequest({}, JSON.stringify(input));
	    	tmp.$promise.then(function(data){
				
				$scope.quoteFetchSuccess = true;
				$scope.quoteFetchComplete = true;
	    		if (data.hasOwnProperty("result")){
	    			$scope.allQuotes=angular.copy(data.result);
	    			
	    			$scope.processCoverOptions();
	    			
	    		} else
	    			
	    		{
	    			console.error(data);
		    	    $scope.quoteError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data.error;
					$scope.proposalError.errors=str.split(";");
					$scope.quoteFetchComplete = true;
		    	    $scope.quoteFetchError = true;
	    		}
	    		
	    	}, function (error) {

	    	    console.error(data);
	    	    $scope.quoteError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str="Unexpected error from Insurer while processing the policy";
				$scope.proposalError.errors=str.split(";");
				$scope.quoteFetchComplete = true;
	    	    $scope.quoteFetchError = true;
	    	});
			
		}
		//
		

		
		// Get Quote Information
		function submitQuoteRequest(quotesInput){
			
			var input = {
	    			url: "",
	    			data:quotesInput
	    			
	    	};
			
			var quotes = $resource('./DataRequest', {}, {
		          processRequest: {
		             method: 'POST'}
		    });
			
			input.url = "/quote/submitAllQuotesRequest/" + $scope.input.lob + "/" + $scope.input.productType;
			//console.log(JSON.stringify($scope.quoteInput))
			//console.log(JSON.stringify(input));
	    	var tmp = quotes.processRequest({}, JSON.stringify(input));
	    	tmp.$promise.then(function(data){
	    		//console.log(data)
				
	    		if (!data.hasOwnProperty("error")){
	    			$scope.quoteResuestId =data.requestId;
	    			$scope.numResultsExpected =data.numResultsExpected;
	    			//console.log("Number of results Expected: " + $scope.numResultsExpected);
	    			if ($scope.numResultsExpected == 0){
	    				$scope.quoteFetchSuccess = true;
	    				$scope.quoteFetchComplete = true;
	    			}
	    		} else
	    		{
		    	    console.error(data);
		    	    var str=data.error;
					$scope.quoteError.errors=str.split(";");
					$scope.quoteFetchComplete = true;
		    	    $scope.quoteFetchError = true;
	    		}
	    		
	    	}, function (error) {
	    	    console.error(data);
	    	    $scope.quoteError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str="Unexpected error from Insurer while processing the policy";
				$scope.proposalError.errors=str.split(";");
				$scope.quoteFetchComplete = true;
	    	    $scope.quoteFetchError = true;
	    	});
		}
		//
		
		// Get Quote Information
		 function getResults (){
			
			//console.log("Getting results");
			var input = {
	    			url: "",
	    			data:{}
	    			
	    	};
			
			var quotes = $resource('./DataRequest', {}, {
		          processRequest: {
		             method: 'POST'}
		    });
			
			input.url = "/quote/getResults/" + $scope.input.requestId;
			//console.log(JSON.stringify($scope.quoteInput))
			//console.log(JSON.stringify(input))
	    	var tmp = quotes.processRequest({}, JSON.stringify(input));
	    	tmp.$promise.then(function(data){
	    		//console.log(data);
				
	    		if (data.hasOwnProperty("result")){
	    			if (data.requestParams){
	    				// For first Time retrun populate header
	    				$scope.quoteInput = data.requestParams;	
	    				if (!$scope.quoteFetchSuccess) {
	    					populateHeader();
	    				}
	    				$scope.quoteFetchSuccess = true;
	    			}
	    			
	    			if (data.result.length > $scope.allQuotes.length) {
	    				//$scope.allQuotes=data.result;
	    				$scope.allQuotes = $filter('filter')(data.result, {online:true} );
	    				//console.log("Expected Results: " + $scope.numResultsExpected);
		    			//console.log("Returned Results: " + $scope.allQuotes.length);
	    				$scope.quoteFetchComplete = true;
		    			if ($scope.allQuotes.length>$scope.displayQuotes.length) {
		    				$scope.processCoverOptions();
		    			}
	    			}
	    			
	    			var time = new Date().getTime()
	    			if (($scope.allQuotes.length >= $scope.numResultsExpected)
	    				|| (resultStopTime < time)){
	    				$interval.cancel(resultPromise);
	    				$scope.quoteFetchSuccess = true;
	    				$scope.quoteFetchComplete = true;
	    			}
	    			
	    		} else
	    		{
	    			console.error(data);
		    	    var str=data['error'];
					$scope.proposalError.errors=str.split(";");
					$scope.quoteFetchComplete = true;
		    	    $scope.quoteFetchError = true;
	    		}
	    	}, function (error) {
	    	    console.error(data);
	    	    $scope.quoteFetchSuccess = true;
	    	});
			

			
		}
		//
		
		function getSelectedCovers(){
			
			var filterExp = {selected : true};
			var data = $filter('filter')($scope.coverOptions, filterExp);
			var selectedCovers = [];
			angular.forEach(data,function(value,key){
				selectedCovers.push(value.coverid);
			});
			
			return selectedCovers;
		}
		
		function getStandardQuoteCovers(quote){
				
			var stdFilter = {coverType : "S"};
			var data = $filter('filter')(quote.covers, stdFilter);
			var stdCovers = [];
			angular.forEach(data,function(value,key){
				stdCovers.push(value.coverId);
			});
			return stdCovers;
		}
		
		function getAddonQuoteCovers(quote){
			// Only those addon covers where premium is present should be counted as add-on.
			var addCovers = [];
			if (quote.hasOwnProperty("premiumBreakup")){
				angular.forEach(quote.premiumBreakup.addon,function(value,key){
					addCovers.push(value.coverId);
				});
			}
			return addCovers;
		}
		
		$scope.currentIndex=0
		$scope.nextProduct = function(i){
			
			
			var index = $scope.recomendations[i].currentIndex + 1;
			if ($scope.recomendations[i].quotes.length == index){
				$scope.recomendations[i].currentIndex = 0;
			}else
			{
				$scope.recomendations[i].currentIndex = index;
			}
			$scope.currentIndex=$scope.recomendations[i].currentIndex
			
			
		}
		
		
		$scope.fillProposal=function(quote)
		{
			
			//console.log(quote);
			//console.log($scope.input.productType);
			var params = angular.copy(quote);
			delete params.attributes;
			delete params.covers;
			delete params.summaryDisplays;
			//params.netPremium = Math.round(quote.totalPremium/1.18);
			params.productType = $scope.input.productType;
		    params.lob = $scope.input.lob;
			params.productId = quote.productId;
			if (quote.hasOwnProperty("planId")){
				params.planId = quote.planId;
			}
			params.addc = getSelectedCovers();
			
			
//			params.totalPremium = quote.displayPremium;
//			params.sa = $scope.quoteInput.sa;
			var url = "";
			params.requestParams=$scope.quoteInput;
			if ($scope.input.productType == "PC"){
				params.requestParams.currNcb=$scope.getApplicableNCB($scope.quoteInput.prevncb, $scope.quoteInput.claim);
			}
			
			var input = JSON.stringify(params);
			console.log(input);
			var res = $resource('./CreateProposal', {}, {
	          create: {
	             method: 'POST',
		    	    isArray: false
			  }
		    });
			var tmp=res.create(input);
			$scope.quoteFetchSuccess = false;
			tmp.$promise.then(function(data){
				$scope.quoteFetchSuccess =  true;
				if (data.hasOwnProperty("result")){
					var redirectUrl = data.result;
					$location.path(redirectUrl);
					
				}
			})
			
	       
		}
		
		
		
		$scope.getAttributeValues = function (productId){
			var obj = {};
			for (var j=0; j<$scope.allQuotes.length; j++){
				var product = $scope.allQuotes[j];
				obj.lob = product.lob;
				obj.productType = product.productType;
				if (product.productId == productId){
					var attributes = product.attributes;
					for (var k=0; k<attributes.length; k++){
						var attribute = attributes[k];
						for (var key in attribute){
							var value = attribute[key];
							obj[key] = value;
						}
					}
				}
			}
			
			return obj;
		}
		
		$scope.createDisplayAttributes = function(productId){
			var displays = [];
			var attributes = $scope.getAttributeValues(productId);
			var lob = attributes.lob;
			var productType = attributes.productType;
			switch (lob){
			case "Health":
				switch(productType){
				case "CI":
					displays = $scope.createCIDisplayAttributes(attributes);
					break;
				case "FF":
				case "INDV":
					displays = $scope.createHospDisplayAttributes(attributes);
					break;
				case "TOPUP":
					displays = $scope.createTopUpDisplayAttributes(attributes);
					break;
				}
				break;
			case "Motor":
				displays = $scope.createMotorDisplayAttributes(attributes);
				break;
			case "Life":
				switch(productType){
				case "CIL":
					displays = $scope.createCILDisplayAttributes(attributes);
					break;
				case "Term":
					displays = $scope.createTermDisplayAttributes(attributes);
					break;
					
				}
				break;
			}
			return displays
		}
		
		$scope.createMotorDisplayAttributes = function(attributes){
			var displays = [];
			
			var display = {};
			display.key = "Entry Age Criteria";
			display.text = (typeof attributes.Age_Discount != 'undefined' ?  'Additional age discount may apply' : '');
			display.summary = "N";
			displays.push(display);
			
			var display = {};
			display.key = "Occupation Discount";
			display.text = (typeof attributes.Occupation_Discount != 'undefined' ?  'Occupation discount may apply' : '');
			display.summary = "N";
			displays.push(display);
			
			return displays;
		}
		
		$scope.createCIDisplayAttributes = function(attributes){
			var displays = [];
			
			var display = {};
			display.key = "Entry Age Criteria";
			display.text = "Adult Between ages: " + attributes.Min_Entry_Age + "-" + attributes.Max_Entry_Age + ". " + (typeof attributes.Min_Child_Age!= 'undefined' ? 'Dependent Child between '  + attributes.Min_Child_Age + '-' + attributes.Max_Child_Age + '. ' :'');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Allowed Sum Insured";
			display.text = "Choose from sum insured of " + attributes.SA_Values;
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Members";
			display.text = 'Maximum Members allowed: ' + attributes.Max_Members +  ', can include up to ' + attributes.Max_Adult + ' adults and ' + attributes.Max_Child + ' Children. '  +  (attributes.Parents_Allowed== 'Y'? ' Parents_Allowed': '');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Tenures";
			display.text = 'Tenures:  ' + attributes.Tenure + ' Year(s)';
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Survival Period";
			display.text = 'To claim, need to survive for ' + Math.ceil(attributes.SurvivalPeriod)  + ' days after diagonosis';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Other Info";
			display.text = (typeof OtherInfo != 'undefined' ? attributes.OtherInfo : '');
			display.summary = "Y";
			displays.push(display);
			
			return displays;
		}
		
		$scope.createCILDisplayAttributes = function(attributes){
			var displays = [];
			
			var display = {};
			display.key = "Entry Age Criteria";
			display.text = "Adult Between ages: " + attributes.Min_Entry_Age + "-" + attributes.Max_Entry_Age + ". " + (typeof attributes.Min_Child_Age!= 'undefined' ? 'Dependent Child between '  + attributes.Min_Child_Age + '-' + attributes.Max_Child_Age + '. ' :'');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Key Information";
			display.text = (typeof attributes.OtherInfo != 'undefined' ? attributes.OtherInfo : '');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Waiting Period";
			display.text = 'Waiting Period of ' + Math.ceil(attributes.WaitingPeriod)  + ' days';
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Survival Period";
			display.text = 'To claim, need to survive for ' + Math.ceil(attributes.SurvivalPeriod)  + ' days after diagonosis';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Illness Covered";
			display.text = attributes.Illness_Covered;
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Know Your Policy";
			display.text = attributes.KYP;
			display.summary = "N";
			displays.push(display);
			
			return displays;
		}
		
		$scope.createTermDisplayAttributes = function(attributes){
			var displays = [];
			
			var display = {};
			display.key = "Entry Age Criteria";
			display.text = "Adult Between ages: " + attributes.Min_Entry_Age + "-" + attributes.Max_Entry_Age + ". " + (typeof attributes.Min_Child_Age!= 'undefined' ? 'Dependent Child between '  + attributes.Min_Child_Age + '-' + attributes.Max_Child_Age + '. ' :'');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Key Information";
			display.text = (typeof attributes.OtherInfo != 'undefined' ? attributes.OtherInfo : '');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Know Your Policy";
			display.text = attributes.KYP;
			display.summary = "N";
			displays.push(display);
			
			return displays;
		}
		
		$scope.createHospDisplayAttributes = function(attributes){
			var displays = [];
			
			var display = {};
			display.key = "Entry Age Criteria";
			display.text = "Adult Between ages: " + attributes.Min_Entry_Age + "-" + attributes.Max_Entry_Age + ". " + (typeof attributes.Min_Child_Age!= 'undefined' ? 'Dependent Child between '  + attributes.Min_Child_Age + '-' + attributes.Max_Child_Age + '. ' :'');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Allowed Sum Insured";
			display.text = "Choose from sum insured of " + attributes.SA_Values;
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Members";
			display.text = 'Maximum Members allowed: ' + attributes.Max_Members +  ', can include up to ' + attributes.Max_Adult + ' adults and ' + attributes.Max_Child + ' Children. '  +  (attributes.Parents_Allowed== 'Y'? ' Parents_Allowed': '');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Tenure";
			display.text = 'Tenures:  ' + attributes.Tenure + ' Year(s)';
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Sublimits";
			display.text = ((typeof attributes.SUBL!= 'undefined' || attributes.SUBL=='N' )? 'No' : 'Has') +  ' capping of Room Rent';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Claims Loading";
			display.text = 'Premium will ' +  (attributes.ClaimsLoading=='N' ? 'not' : '') +  ' increase after claims';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Freelook";
			display.text = (typeof attributes.freelook != 'undefined' ? attributes.freelook  + ' return guarantee' : '');
			display.summary = "N";
			displays.push(display);
			
			//display = {};
			//display.key = "Medical Test";
			//display.text = (typeof  (attributes.Med_Test_Age) == 'undefined' ||  userdata.currentflow.age<=attributes.Med_Test_Age)&& (typeof  (attributes.Med_Test_SI) == 'undefined' ||  $scope.quoteInput.sa<=attributes.Med_Test_SI) ? 'Medical test not required ' : 'Medical test required';
			//display.summary = "Y";
			//displays.push(display);
			
			display = {};
			display.key = "Renewal Benefit";
			display.text = typeof  (attributes.Renewal_Benefit_Type) != 'undefined'? attributes.CB_per_Year  + ' ' +  attributes.Renewal_Benefit_Type  + ' every claim free year subject to a maximum of '  + attributes.Max_CB :'';
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Pre Existing Condition";
			display.text = 'Existing illness covered after ' +  attributes.Pre_Con_Waiting_Period + ' months';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Max Renewal Age";
			display.text = (attributes.Max_Renewal_Age=='Lifelong' ? 'LifeLong Renewal' : 'Renewal up to age ' + attributes.Max_Renewal_Age);
			display.summary = "N";
			displays.push(display);
			
			return displays;
		}

		
		$scope.createTopUpDisplayAttributes = function(attributes){
			var displays = [];
			
			var display = {};
			display.key = "Entry Age Criteria";
			display.text = "Adult Between ages: " + attributes.Min_Entry_Age + "-" + attributes.Max_Entry_Age + ". " + (typeof attributes.Min_Child_Age!= 'undefined' ? 'Dependent Child between '  + attributes.Min_Child_Age + '-' + attributes.Max_Child_Age + '. ' :'');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Allowed Sum Insured";
			display.text = "Choose from sum insured of " + attributes.SA_Values;
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Members";
			display.text = 'Maximum Members allowed: ' + attributes.Max_Members +  ', can include up to ' + attributes.Max_Adult + ' adults and ' + attributes.Max_Child + ' Children. '  +  (attributes.Parents_Allowed== 'Y'? ' Parents_Allowed': '');
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Tenure";
			display.text = 'Tenures:  ' + attributes.Tenure + ' Year(s)';
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Type";
			display.text = 'Claims will be paid after expenses for ' + (attributes.SUPER=='N' ? 'each hospitalization' : ' the year') +  ' exceed the deductible';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Base Policy Needed";
			display.text = (attributes.BASE=='N' ? 'No' : '') +  ' Base policy needed to buy this top up ';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Medical Test";
			display.text = (typeof  (attributes.Med_Test_Age) == 'undefined' ||  userdata.currentflow.age<=attributes.Med_Test_Age)&& (typeof  (attributes.Med_Test_SI) == 'undefined' ||  userdata.currentflow.sa<=attributes.Med_Test_SI) ? 'Medical test not required ' : 'Medical test required';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Renewal Benefit";
			display.text = typeof  (attributes.Renewal_Benefit_Type) != 'undefined'? attributes.CB_per_Year  + ' ' +  attributes.Renewal_Benefit_Type  + ' every claim free year subject to a maximum of '  + attributes.Max_CB :'';
			display.summary = "N";
			displays.push(display);
			
			display = {};
			display.key = "Pre Existing Condition";
			display.text = 'Existing illness covered after ' +  attributes.Pre_Con_Waiting_Period + ' months';
			display.summary = "Y";
			displays.push(display);
			
			display = {};
			display.key = "Max Renewal Age";
			display.text = (attributes.Max_Renewal_Age=='Lifelong' ? 'LifeLong Renewal' : 'Renewal up to age ' + attributes.Max_Renewal_Age);
			display.summary = "N";
			displays.push(display);
			
			return displays;
			
		}
		
		init();
		
		//  Modal form below
		var $ctrl=this;
		$scope.emailQuotes=function()
		{
			
			var modalInstance=$uibModal.open({
				animation: $ctrl.animationsEnabled,
				ariaLabelledBy:'modal-title',
				ariaDescribedBy:'modal-body',
				templateUrl:'templates/common/emailQuotes.html',
				controller:'emailModal',
				controllerAs:'$ctrl',
				size:'md',
				scope:$scope,
				resolve:{
					'compareItems':function(){
						return $scope.email;
					}
				}
			});
			modalInstance.result.then(function (response) {
            $scope.selected = response;
            
	        }, function () {
	            //console.log('Modal dismissed at: ' + new Date());
	        });
		}
		$scope.emailSuccess=function()
		{
			 var modalInstance=$uibModal.open({
    			animation: $ctrl.animationsEnabled,
				ariaLabelledBy:'modal-title',
				ariaDescribedBy:'modal-body',
				templateUrl:'templates/common/emailQuotesSucess.html',
				controller:'emailSuccess',
				controllerAs:'$ctrl',
				size:'md',
				scope:$scope,
				resolve:{
					'compareItems':function(){
						return $scope.email;
					}
				}
			});
    		modalInstance.result.then(function (response) {
            $scope.selected = response;
           
	        }, function () {
	            //console.log('Modal dismissed at: ' + new Date());
	        });
		}
		
		$scope.animationsEnabled = true;
		
		// Function to Handle Detailed View of Product
		$scope.premiumBreakUp=function(key) {
			console.log(key);
			var selectedProduct= getQuotesForCompare(key);
			// Delete AddOns that are not selected
			var selectedCovers = getSelectedCovers();
			var addons = [];
			if (!selectedProduct.premiumBreakup.base){
				selectedProduct.premiumBreakup.base = selectedProduct.premiumBreakup.Basic
				selectedProduct.premiumBreakup.Basic = null;
			}
			angular.forEach(selectedProduct.premiumBreakup.base,function(item, key){
				item.displayOrder = Number(getDisplayOrder(item.coverId));
			});
			
			angular.forEach(selectedProduct.premiumBreakup.addon,function(item, key){
				var selected = false;
				item.displayOrder = getDisplayOrder(item.coverId);
				for (var i = 0; i<selectedCovers.length; i++){
				
					if (item.coverId == selectedCovers[i]) {
						selected = true;
						break;
					}
				}
				
				if (selected) {
					addons.push(item)
				}
			});
			selectedProduct.premiumBreakup.addon = addons;
			//
			var modalInstance = $uibModal.open({
			      animation: $scope.animationsEnabled,
			      ariaLabelledBy: 'modal-title',
			      ariaDescribedBy: 'modal-body',
			      templateUrl: 'templates/quote/pb.html',
			      controller: 'quotePBCtrl',
			      size: 'md',
			      resolve: {
			    	  selectedProduct: function () {
			          return selectedProduct;
			        }
			      }
			    });
			
			
			modalInstance.result.then(function (selectedProduct) {
			      //$scope.selected = selectedProduct;
			    }, function () {
			      $log.info('Modal dismissed at: ' + new Date());
			});
		}
		
		
		// Function to Handle Detailed View of Product
		$scope.productDetail=function(key) {
			
			var selectedProduct= getQuotesForCompare(key);

			var modalInstance = $uibModal.open({
			      animation: $scope.animationsEnabled,
			      ariaLabelledBy: 'modal-title',
			      ariaDescribedBy: 'modal-body',
			      templateUrl: 'templates/quote/quoteDetails.html',
			      controller: 'quoteDetailCtrl',
			      size: 'lg',
			      resolve: {
			    	  selectedProduct: function () {
			          return selectedProduct;
			        }
			      }
			    });
			
			
			modalInstance.result.then(function (selectedProduct) {
			      //$scope.selected = selectedProduct;
			    }, function () {
			      $log.info('Modal dismissed at: ' + new Date());
			});
		}
		
		function getQuotesForCompare(productKey){
			
			var filterExp = {selected : true, coverid : ""};
			console.log($scope.coverOptions);
			var selectedProduct ={}
			for (var i= 0; i<$scope.displayQuotes.length; i++) {
				if ($scope.displayQuotes[i].key ==productKey){
					selectedProduct = angular.copy($scope.displayQuotes[i]);
					
					var covers = [];
					angular.forEach(selectedProduct.covers, function(cover, key1) {
						
						if (cover.coverType == "S"){
							covers.push(cover);
														
						}else
						{
							// Check if the addon cover is selected.
							filterExp.coverid = cover.coverId;
							
							var data = $filter('filter')($scope.coverOptions, filterExp, true);
							
							if (data.length>0){
								console.log(data);
								covers.push(cover);
							}
						}
					});
					selectedProduct.covers = covers;
					break;
				}
			}
			//console.log($scope.allQuotes);
			return selectedProduct;
		}
		
		
		// Add or subtract from Compare
		$scope.addCompare=function(key, selected) {
			//console.log($scope.allQuotes);
			if(selected==true) {
				if ( $scope.compareViewQuotes.length < 4) {
					for (var i = 0; i < $scope.displayQuotes.length ; i++) {
						if ($scope.displayQuotes[i].key== key) {
							//console.log($scope.displayQuotes[i]);
							$scope.compareViewQuotes.push(angular.copy($scope.displayQuotes[i]));
				        }
					}
			   		
				} else
				{
					for (var i = 0; i < $scope.displayQuotes.length; i++) {
				        if ($scope.displayQuotes[i].key== key) 
				        {
				        	$scope.displayQuotes[i].selectedForCompare =false;
				        }
					}
				}
			}
			else if(selected==false)
			{
				for (var i = 0; i < $scope.compareViewQuotes.length ; i++) {
					
			        if ($scope.compareViewQuotes[i].key== key) 
			        {
			        	$scope.compareViewQuotes.splice(i, 1);
			        }
				}
				
			}
			
			
			//console.log($scope.allQuotes);
		}
		
		// Function to compare 
		$scope.comparePolicy=function(compareViewQuote) {
			// $scope.compareViewQuotes
			var selectedProducts=[];
			
			// Key Helps Identify the quote Selected. Retrieve and send to Modal window
			for (var j= 0; j<compareViewQuote.length; j++){
				var item = angular.copy(compareViewQuote[j]);
				selectedProducts.push(getQuotesForCompare(item.key));
			}
			
			var modalInstance = $uibModal.open({
			      animation: $scope.animationsEnabled,
			      ariaLabelledBy: 'modal-title',
			      ariaDescribedBy: 'modal-body',
			      templateUrl: 'templates/quote/compareQuotes.html',
			      controller: 'quoteCompareCtrl',
			      size: 'lg',
			      resolve: {
			    	  selectedProducts: function () {
			          return selectedProducts;
			        }
			      }
			});
			
			
			modalInstance.result.then(function (output) {
				console.log(output);
				if (output.action == "buy"){
					$scope.fillProposal(output.data)
				}
			     
			    }, function () {
			      console.log('Modal dismissed at: ' + new Date());
			      
			});
		}	
		
		$scope.buyPremium=function(obj)
		{
			fnafactory.setData(obj);
			$location.path('perimiumBuy');
		}
		
		
		
	}]
});

//Email Modal
brokeredgeApp.controller('emailModal',['$scope','$uibModalInstance','$uibModal',function($scope,$uibModalInstance,$uibModal){
var $ctrl = this;

$ctrl.ok = function () {
    //
   			$scope.emailSuccess()
	        $uibModalInstance.close($scope.email);
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);
// Email Success
brokeredgeApp.controller('emailSuccess',['$scope','$uibModalInstance',function($scope,$uibModalInstance){
var $ctrl=this;

$ctrl.cancel = function () {
	
    $uibModalInstance.dismiss('cancel');
  };
}]) 
