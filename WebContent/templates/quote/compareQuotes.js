/**
 * 
 */
"use strict";
// compare policy model
brokeredgeApp.controller('quoteCompareCtrl',['$scope','$uibModalInstance','selectedProducts','$sce', function($scope,$uibModalInstance, selectedProducts ,$sce){
	
	$scope.selectedProducts = selectedProducts;
	console.log(JSON.stringify($scope.selectedProducts));
	$scope.compare_policy=$scope.selectedProducts;
	$scope.compare_Covers=[];
	$scope.compareAttributes=[];
	$scope.isFeature = false;
	$scope.isopen = true;
	$scope.colWidth = 100/Number($scope.selectedProducts.length + 1) + "%";
	$scope.compareData = {
			coverDisplay : [],
			attributeDisplay : [],
			premiums : [],
	};
	
		
	function createCompareData(){
		
		// get cover and attributekeys to compare on.
		angular.forEach($scope.selectedProducts,function(product, key){
			// get a list of covers
			$scope.compareData.premiums.push(product.displayPremium);  
			angular.forEach(product.covers,function(cover, key1){
				if (!isCoverAvailable(cover.coverId)){
					var item = {};
					item.name = cover.coverName;
					item.id = cover.coverId;
					$scope.compare_Covers.push(item)
				}
			});
			//console.log($scope.compare_Covers);
			//attribute loop
			angular.forEach(product.attributes,function(attribute, key){
				angular.forEach(attribute, function(value, id) {
					if (!isAttributeAvailable(id)){
						var item = {};
						item.id = id;
						item.name = angular.copy(id).replace(/_/g, " ");
						$scope.compareAttributes.push(angular.copy(item));
					}
				});
			})
			
		});
		
		// create the compare data arrays
		for (var i=0; i<$scope.compare_Covers.length; i++){
			var display = {};
			display.cover = angular.copy($scope.compare_Covers[i]);
			display.coverData = [];
			for (var j = 0; j<$scope.selectedProducts.length; j++){
				var productKey = $scope.selectedProducts[j].key;
				display.coverData.push(angular.copy(getCover(productKey, $scope.compare_Covers[i].id)));
			}
			$scope.compareData.coverDisplay.push(angular.copy(display));
		}
		
		//console.log($scope.compareAttributes);
				
		for (var i=0; i<$scope.compareAttributes.length; i++){
			var display = {};
			display.attribute = $scope.compareAttributes[i];
			display.attributeData = [];
			for (var j = 0; j<$scope.selectedProducts.length; j++){
				var productKey = $scope.selectedProducts[j].key;
				display.attributeData.push(angular.copy(getAttribute(productKey, $scope.compareAttributes[i].id)));
			}
			$scope.compareData.attributeDisplay.push(angular.copy(display));
		}
		console.log($scope.compareData);
	}

	function isCoverAvailable(coverId) {
		
		var available = false;
		
		for (var i=0; i<$scope.compare_Covers.length; i++){
			if ($scope.compare_Covers[i].id == coverId){
				available = true;
				break;
			}
		}
		return available;
	}

	function isAttributeAvailable(id) {
		
		var available = false;
		
		for (var i=0; i<$scope.compareAttributes.length; i++){
			if ($scope.compareAttributes[i].id == id){
				available = true;
				break;
			}
		}
		return available;
	}
	
	function getCover(productKey, coverId){
		var cover = {"exists" : false};
		for (var i = 0; i<$scope.selectedProducts.length; i++){
			var product = $scope.selectedProducts[i];
			if (product.key == productKey){
				for (var j = 0; j<product.covers.length; j++){
					if (product.covers[j].coverId == coverId){
						cover = product.covers[j];
						// refactor limits
						var coverLimit = {};
						angular.forEach(cover.coverLimits,function(limit, key){
							var limitText = "";
							if (limit.hasOwnProperty("period")){
								var periodText = limit.period;
								switch(limit.period){
								case "AOY":
									periodText = "In a Policy Year";
									break;
								case "AOH":
									periodText = "Per Hospitalization";
									break;
								case "AOH":
									periodText = "Per Claim";
									break;
								}
								limitText = periodText + ": ";								
							}
							
							limitText = limitText + limit.limitValue;
							
							if (limit.hasOwnProperty("cond")){
								limitText = limitText + "( " + limit.cond + " ) ";								
							}
							limit.value = limitText;
							
							if (!coverLimit.hasOwnProperty(limit.sublimit)){
								coverLimit[limit.sublimit] = [];
							}
							coverLimit[limit.sublimit].push(limit);
						});					
						cover.coverLimits = coverLimit;
						cover.exists = true;
					}
				}
			}
		}
		
		return cover;
	}
	
	function getAttribute(productKey, attrId){
		var attribute = {"exists" : false};
		for (var i = 0; i<$scope.selectedProducts.length; i++){
			var product = $scope.selectedProducts[i];
			if (product.key == productKey){
				for (var j = 0; j<product.attributes.length; j++){
					if (product.attributes[j][attrId]){
						attribute.name = attrId;
						attribute.value = product.attributes[j][attrId];
						attribute.exists = true;
					}
				}
			}
		}
		
		return attribute;
	}
	
	createCompareData();
	
	$scope.printDiv = function(divName) {
		  var printContents = document.getElementById(divName).innerHTML;
		  var popupWin = window.open('', '_blank', 'width=800,height=600');
		  popupWin.document.open();
		  popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="./css/addon.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
		  popupWin.document.close();
	} 
	
	$scope.buyNow = function(quote){
		var output = {};
		output.action = "buy";
		output.data = quote
		$uibModalInstance.close(output);
	}
	
	$scope.ok = function () {
		$uibModalInstance.close('ok'); // Put the result here
	};

	$scope.close = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	$scope.getHtml = function(html){
        return $sce.trustAsHtml(html);
    };
}]);
brokeredgeApp.filter('html', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});