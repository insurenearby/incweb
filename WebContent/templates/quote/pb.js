"use strict";
brokeredgeApp.controller('quotePBCtrl',['$scope', '$sce', '$uibModalInstance','selectedProduct', function($scope,$sce,$uibModalInstance, selectedProduct){
	
	$scope.premiums = {};
	console.log(selectedProduct);
	if (selectedProduct.premiumBreakup.Basic) {
		$scope.premiums.base = selectedProduct.premiumBreakup.Basic;
	} else if (selectedProduct.premiumBreakup.base) {
		$scope.premiums.base = selectedProduct.premiumBreakup.base;
	}
	
	$scope.premiums.addon = selectedProduct.premiumBreakup.addon;
	$scope.premiums.other = selectedProduct.premiumBreakup.other;
	$scope.premiums.netPremium = selectedProduct.netPremium;
	$scope.premiums.serviceTax = selectedProduct.serviceTax;
	$scope.premiums.totalPremium = selectedProduct.totalPremium;
	$scope.premiums.idv = selectedProduct.idv;
	
	var items = [];
	angular.forEach(selectedProduct.premiumBreakup.addon,function(item, key){
		if (isCoverSelected(item.coverId)) {
			$scope.premiums.netPremium = Number($scope.premiums.netPremium) + Number(item.netPremium);
			var gst = Math.round(item.premium - item.netPremium);
			$scope.premiums.serviceTax = Number($scope.premiums.serviceTax) + Number(gst);
			$scope.premiums.totalPremium = Number($scope.premiums.totalPremium) + Number(item.premium);
		}
	});	
	
	function isCoverSelected(coverId) {
		var selected = false;
		angular.forEach(selectedProduct.covers,function(cover, key){
			if (cover.coverId = coverId){
				selected = true;
			}
		});
		return selected;
	}
	
	
		
	$scope.ok = function () {
		$uibModalInstance.close(''); // Put the result here
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	$scope.getHtml = function(html){
        return $sce.trustAsHtml(html);
    };
}]);
brokeredgeApp.filter('html', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});