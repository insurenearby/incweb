"use strict";
brokeredgeApp.controller('brokeredgeHealthCtrl',['$scope','$filter','brokeredgefactory','$location','$resource',function($scope,$filter,brokeredgefactory,$location,$resource){
	$scope.healthInput={'lob': 'Health', deductible: 0, members:[{dob:'',gender:'',adultChild:''}]};
	$scope.annualIncome=[{name:'One Lakh',val:'100000'},{name:'Two Lakh',val:'200000'},{name:'Two Lakh and Half Lakh',val:'250000'}, {name:'Three Lakh',val:'300000'}, {name:'Three Lakh and Half Lakh',val:'350000'}, {name:'Four Lakh',val:'400000'},{name:'Four and Half Lakh',val:'450000'},{name:'Five Lakh',val:'500000'},{name:'Five and Half Lakh',val:'550000'},{name:'Six Lakh',val:'600000'},{name:'Seven Lakh',val:'700000'},{name:'Seven and Half Lakh',val:'750000'},{name:'Eight Lakh',val:'800000'},{name:'Nine Lakh',val:'900000'},{name:'Ten Lakh',val:'1000000'},{name:'Fifteen Lakh',val:'1500000'},{name:'Nineteen Lakh',val:'1900000'},{name:'Tweenty Lakh',val:'2000000'},{name:'Tweenty Five Lakh',val:'2500000'},{name:'Thirty Lakh',val:'3000000'},{name:'Forty Lakh',val:'4000000'},{name:'Fifty Lakh',val:'5000000'},{name:'Sixty Lakh',val:'6000000'},{name:'Seventy Five',val:'7500000'},{name:'1 Crore',val:'10000000'},{name:'1 Crore Fifty Lakhs',val:'15000000'},{name:'2 Crore',val:'20000000'},{name:'3 Crore',val:'30000000'},{name:'6 Crore',val:'60000000'}];
	$scope.relationships = [];
	$scope.dobList=[];
	$scope.genderList=[];
	$scope.ageLists=[];
	$scope.healthObj={};
	$scope.policyType='';
	$scope.dobOptions=[];
	$scope.loading = false;
	$scope.selfInsured = false;
	$scope.idvValid = false;

	$scope.quoteError = {};
	// Date picker
	var dateToday = new Date();
    var yearMin = dateToday.getFullYear() - 125;
    var yearMax = dateToday.getFullYear();
    var monthToday = dateToday.getMonth();
    var dayToday = dateToday.getDate()-1;
    $scope.dateOptions = {
        maxDate: new Date(yearMax, monthToday, dayToday-90),
        minDate: new Date(yearMin, monthToday, dayToday)
      };


	$scope.adultDobOptions={
	    	maxDate: new Date(yearMax-18, monthToday, dayToday),
	    	minDate: new Date(yearMin, monthToday, dayToday)
	}
	
	$scope.childDobOptions={
	    	maxDate: new Date(yearMax-0, monthToday-3, dayToday),
	    	minDate: new Date(yearMax-25, monthToday, dayToday)
	}
   $scope.opened = [];
   $scope.open = function($event,$index) {
            $scope.opened[$index] = true;
        };
	$scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    
    
	function init() {
		getInsuredRelationship();
		$scope.reset();
		console.log("healthh");
	}
	//add member
	$scope.addMember=function()
	{
		$scope.healthInput.members.push({dob:''});
		$scope.dobOptions.push($scope.adultDobOptions);
	}
	// reset the member
	$scope.reset=function()
	{
		$scope.healthInput.members=[];
		$scope.healthInput.members.push({dob:''});
		$scope.dobOptions = [];
		$scope.dobOptions.push($scope.adultDobOptions);
		$scope.selfInsured = false;
	}
	
	//submit health form
	$scope.submitForm=function() {
				
		var healthInput = {};
		healthInput.lob = "Health";
		healthInput.adult = 0;
		healthInput.children = 0;
		
		if($scope.checkError()) {
			var input={
					url:'',
					data:{},
				}
			   input.url="/master/getStatePinStd/"+$scope.healthInput.pincode;
			   var tmp=brokeredgefactory.getPincode().processRequest({}, JSON.stringify(input));
			   tmp.$promise.then(function(data){
				   if(data.pincode==undefined){
						$scope.error="Invalid Pin code";
					
					}
					else
					{
						$scope.error="";
												
						healthInput.pincode = $scope.healthInput.pincode;
						healthInput.tenure = $scope.healthInput.tenure;
						healthInput.sa = $scope.healthInput.sumInsured.val;
						healthInput.deductible = 0;
						healthInput.members = [];
						for (var i =0; i< $scope.healthInput.members.length; i++){
							var member = {}; 
							member.dob = angular.copy($filter('date')($scope.healthInput.members[i].dob, "yyyy-MM-dd"))
							member.type = angular.copy($scope.healthInput.members[i].type)
							member.gender = angular.copy($scope.healthInput.members[i].gender)
							member.relationship = angular.copy($scope.healthInput.members[i].relationship.id);
							healthInput.members.push(member);
						}
						if($scope.policyType =='enhanceExCover')
						{
							healthInput.deductible=$scope.healthInput.deductibe;
							healthInput.productType='TOPUP'
						}
						console.log(healthInput);
					
						// Submit Quote Request
						submitQuoteRequest(healthInput);
					}
			   });
		}
	}
	
	// SubmitQuoteRequest
	function submitQuoteRequest(quoteInput){
		var input = {
    			url: "",
    			data:quoteInput
    			
    	};
		
		var quotes = $resource('./DataRequest', {}, {
	          processRequest: {
	             method: 'POST'}
	    });
		
		input.url = "/quote/submitAllQuotesRequest/" + $scope.healthInput.lob + "/" + $scope.healthInput.productType;
		//console.log(JSON.stringify($scope.quoteInput))
		//console.log(JSON.stringify(input));
    	var tmp = quotes.processRequest({}, JSON.stringify(input));
    	$scope.loading = true;
    	tmp.$promise.then(function(data){
    		//console.log(data)
    		$scope.loading = false;
    		if (!data.hasOwnProperty("error")){
    			if (data.numResultsExpected == 0){
    				$scope.error = "No Quotes available for this selection";
    			} else
    			{
    				// Transfer Control to Quotes page
    				var url = "/quotes/Health/"+$scope.healthInput.productType +'/' + data.requestId +'/'+ data.numResultsExpected ;
    				console.log(url);
			    	$location.path(url);
    			}
    		} else
    		{
    			console.error(data);
	    	    var str=data.error;
				$scope.quoteError.errors=str.split(";");
				
			}
    		
    	}, function (error) {
    	    console.error(data);
    	    $scope.quoteError.errors ="Unexpected error processing quote Request".split(";");
			
    	});
	}
	
	$scope.checkMember=function(member, idx){
		
		
		var relId = angular.copy(member.relationship.id);
		if (relId == '4' || relId == '5' || relId == '9' || relId == '12'){
			member.type = "C";
			$scope.dobOptions[idx] = $scope.childDobOptions;
		} else
		{
			member.type = "A";
			$scope.dobOptions[idx] = $scope.adultDobOptions;
		}
		if (relId == '1'){
			$scope.selfInsured = true;
		}
		
		// Assign Gender
		if (relId == '3' || relId == '5' || relId == '9'  || relId == '11' || relId == '13' 
			|| relId == '14' || relId == '15'){
			member.gender = "M";
		}
		
		if (relId == '2' || relId == '4' || relId == '6' || relId == '7' || relId == '8' 
			|| relId == '10' || relId == '12'){
			member.gender = "F";
		}
		
	}
	


	$scope.filterValue = function($event){
		$scope.error="";
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
            
        }
    };

		
	$scope.checkError=function()
	{
		//$scope.checkAdultAge();
		var ok = true;
		if ($scope.healthInput.productType == 'FF'&& $scope.healthInput.members.length<=1) {
			ok=false;
			$scope.quoteError.errors='Minimum 2  family members required in floater family.'.split(";");
		}
		
		var numAdults = 0;
		var adultsRels = [];
		// Check Adult Relationships for FF
		if ($scope.healthInput.productType == 'FF'){
			for (var i=0; i< $scope.healthInput.members.length; i++){
				if ($scope.healthInput.members[i].type == 'A'){
					numAdults++;
					adultsRels.push($scope.healthInput.members[i].relationship.id);
				}
			}
		}
		
		if (numAdults==2){
			ok=false;
			$scope.quoteError.errors='Adults must be Husband and wife'.split(";");
			adultsRels.sort();
			if (adultsRels[0]==1 && adultsRels[1] == 2){
				ok=true;
			}
			if (adultsRels[0]==7 && adultsRels[1] == 14){
				ok=true;
			}
			if (adultsRels[0]==8 && adultsRels[1] == 13){
				ok=true;
			}
			if (ok){
				$scope.quoteError.errors = [];
			}
		}		
		return ok;
	}
	

	
	//get InsuredRelationship
	function getInsuredRelationship()
	{
		console.log("Entered getInsu")
		var input={
				url:'/master/insuredRelationship',
				data:{}
		}
				
		var res = $resource('./DataRequest', [], {
	          save: { method: 'POST' }
	    });
		
		var tmp = res.save(input);
		
		console.log("InsuredRelationship",tmp)
		tmp.$promise.then(function(data){
			console.log("tmp",tmp);
			$scope.relationships =data['result'];
			console.log("data_health",data['result']);
			
		});
	}
	
	init();

}]);