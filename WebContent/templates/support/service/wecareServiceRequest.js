"use strict";
brokeredgeApp.component('wecareServiceRequest',{
	templateUrl:'templates/support/service/service.html',
	controller:['$scope','servicefactory',function($scope,servicefactory){
		$scope.productType='health';
    	$scope.tabZones=servicefactory.getTabZone();
    	$scope.IntimateclaimDetails={};
    	$scope.productclaim=['Health','Motor','Travel','Term'];
    	$scope.issue=['Pre-Authorization','Partially approved','Not approved','Payment issue','No response from insurer','Others']
    	$scope.countryMobile=servicefactory.getCountryMobile();
    	$scope.selTravelProcess=[{type:'medicalExpenses',text:'Medical Expenses'},{type:'tripCancellation',text:'Trip cancellation'},{type:'lossOfBaggage',text:'Loss of Baggage'},{type:'financialEmergency',text:'Financial Emergency'},{type:'tripDelay',text:'Trip Delay'}]
    	$scope.claimProcessTravel=[{name:'Dental treatment',id:'2'},{name:'Personal accident',id:'3'},{name:'Baggage delay',id:'7'},{name:'Missed connection',id:'8'},{name:'Passport loss',id:'9'},{name:'Hijack distress',id:'11'}];
    	$scope.current="tab1";
    	$scope.current='';
    	$scope.insureDownload='';
        $scope.setTab=function(product)
    	{
            var tmp=servicefactory.getHealth()
                tmp.$promise.then(function(data) {
                    $scope.healthTab=data['service'].health;
                    $scope.result=data['service'];
                   
                });
    		
    		$scope.productType=product;
            $scope.innerTabProducts='knowprocess';
            $scope.current="tab1"
             $scope.insureDownload=""
    	}
    	$scope.setTab('Health');
    	
    	$scope.innerTab=function(innerTabProduct,id)
    	{
    		$scope.innerTabProducts=innerTabProduct;
    		$scope.current=id;
    		
    	}
    	$scope.innerTab('knowprocess','tab1');
    	$scope.processActive='Cashless';
    	$scope.processTab=function(process)
    	{
           $scope.processActive=process;
    	}
    	$scope.inurerFormDownlod=function(name,downloadForm,insurerVal)
    	{

    		if(insurerVal=="")
    		{

    		}
    		else
    		{
                if(name=="Health")
                {
                  $scope.insureDownload=servicefactory.getInsurerFormDownload(downloadForm,insurerVal)  
                }
    			else if(name=='Motor')
                {
                    $scope.insureDownload=servicefactory.getMoterDownload(downloadForm,insurerVal)
                }
                else if(name=='Term')
                {
                    $scope.insureDownload=servicefactory.getTermDownload(downloadForm,insurerVal)
                }
                else
                {
                     $scope.insureDownload=servicefactory.getTravelDownload(downloadForm,insurerVal)
                }
    			
    		}
    		
    	}
    	$scope.selectProduct=function(selProductClaim)
    	{

    		$scope.insurer=servicefactory.getInsurer(selProductClaim);
    		
    	}
       
        // term
        $scope.selOtherProcess=function(result,otherPro)
        {
            $scope.otherProcesTravel=servicefactory.getOtherProcessTravel(result,otherPro)
        }
	}]
})