/**
 * 
 */
"use strict";
brokeredgeApp.controller('otpCtrl',['$scope','$uibModalInstance', '$resource', 'input', function($scope,$uibModalInstance,$resource, input){
	
	$scope.otpInput = input;
	console.log(input)
	function checkOTP(){
		
				
		var input = {
    			url: '/master/checkOtp',
    			data : {}
    	};
		
        input.data = $scope.otpInput;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		
		tmp.$promise.then(function(data){
			var output = angular.copy(data);
			if (output.result == "true") {
				$uibModalInstance.close(output);
			} else
			{
				$scope.otpError = true;
			}
			
		})
	}
	
	
    $scope.ok = function () {
    	checkOTP();
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	

}]);