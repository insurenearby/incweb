/**
 * 
 */
"use strict";
brokeredgeApp.component('brokeredgeCritical',{
	templateUrl:'templates/criticalIllness/criticalillness.html',
	controller:['$scope','brokeredgefactory','$filter','$location',function($scope,brokeredgefactory,$filter,$location){
		$scope.sumInsured=[{name:'1 Lakh',val:'100000'},{name:'2 Lakh',val:'200000'},{name:'3 Lakh',val:'300000'},{name:'4 Lakh',val:'400000'},{name:'5 Lakh',val:'500000'},{name:'6 Lakh',val:'600000'},{name:'7 Lakh',val:'700000'},{name:'8 Lakh',val:'800000'},{name:'9 Lakh',val:'900000'},{name:'10 Lakh',val:'1000000'},{name:'11 Lakh',val:'1100000'},{name:'12 Lakh',val:'1200000'},{name:'13 Lakh',val:'1300000'},{name:'14 Lakh',val:'1400000'},{name:'15 Lakh',val:'1500000'},{name:'16 Lakh',val:'1600000'},{name:'17 Lakh',val:'1700000'},{name:'18 Lakh',val:'1800000'},{name:'19 Lakh',val:'1900000'},{name:'20 Lakh',val:'2000000'},{name:'21 Lakh',val:'2100000'},{name:'22 Lakh',val:'2200000'},{name:'23 Lakh',val:'2300000'},{name:'24 Lakh',val:'2400000'},{name:'25 Lakh',val:'2500000'},{name:'26 Lakh',val:'2600000'},{name:'27 Lakh',val:'2700000'},{name:'28 Lakh',val:'2800000'},{name:'29 Lakh',val:'2900000'},{name:'30 Lakh',val:'3000000'},{name:'31 Lakh',val:'3100000'},{name:'32 Lakh',val:'3200000'},{name:'33 Lakh',val:'3300000'},{name:'34 Lakh',val:'3400000'},{name:'35 Lakh',val:'3500000'},{name:'36 Lakh',val:'3600000'},{name:'37 Lakh',val:'3700000'},{name:'38 Lakh',val:'3800000'},{name:'39 Lakh',val:'3900000'},{name:'40 Lakh',val:'4000000'},{name:'41 Lakh',val:'4100000'},{name:'42 Lakh',val:'4200000'},{name:'43 Lakh',val:'4300000'},{name:'44 Lakh',val:'4400000'},{name:'45 Lakh',val:'4500000'},{name:'46 Lakh',val:'4600000'},{name:'47 Lakh',val:'4700000'},{name:'48 Lakh',val:'4800000'},{name:'49 Lakh',val:'4900000'},{name:'50 Lakh',val:'5000000'},{name:'60 Lakh',val:'6000000'},{name:'70 Lakh',val:'7000000'},{name:'80 Lakh',val:'8000000'},{name:'90 Lakh',val:'9000000'},{name:'1 Crore',val:'10000000'}];
		$scope.polTerm=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"];
		var ciInput = {};
		$scope.ci= {
		    		age : "18",
		    		mma : "60",
		    		pt : "0",
		    		ppt : "0",
		    		sa : "10000000"
		    		
		    };
//			
			function PopulatePassedData(){
				$scope.ci.sa  = $scope.critical.goalTarget;
				$scope.ci.termDob = new Date($scope.critical.customerDob);
				$scope.ci.pt = $scope.ci.mma- Number($scope.critical.age);
				$scope.ci.ppt = $scope.ci.mma- Number($scope.critical.age);
			}
			//Submit form
			$scope.submitCritical=function()
			{
				$scope.ci.age=brokeredgefactory.calculateAge($scope.ci.termDob);
				$scope.ci.sa=Number($scope.ci.salIncome);
		    	$scope.ci.dob=$filter('date')($scope.ci.termDob, "yyyy-MM-dd");
		    	ciInput.dob=$scope.ci.dob;
		    	ciInput.pincode =$scope.ci.pincode;
		    	ciInput.gender=$scope.ci.gender;
		    	ciInput.sa =''+ $scope.ci.sa;
		    	ciInput.pt=$scope.ci.policyTerm;
		    	ciInput.ppt=$scope.ci.NoOfYrsPay;
		    	var url = "/quotes/Life/CIL/" + brokeredgefactory.base64Encode(ciInput);
				 $location.path(url);
			}
		// Date picker
		    var dateToday = new Date();
			var yearMin = dateToday.getFullYear() - 60;
			var yearMax = dateToday.getFullYear() - 18;
		    var monthToday = dateToday.getMonth();
		    var dayToday = dateToday.getDate();
		    $scope.dateOptions = {
		    		maxDate: new Date(yearMax, monthToday, dayToday),
		    		minDate: new Date(yearMin, monthToday, dayToday)
		    };
		    
		    $scope.open1 = function() {
				$scope.popup1.opened = true;
			};

		  
			$scope.setDate = function(year, month, day) {
				$scope.selectedOption.targetym = new Date(year, month, day);
			};

			$scope.formats = ['yyyy-MM-dd','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
			$scope.altInputFormats = ['M!/d!/yyyy'];

			$scope.popup1 = {
				opened: false
			};
	}]
})