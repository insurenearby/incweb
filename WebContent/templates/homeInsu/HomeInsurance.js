/**
 * 
 */
"use strict";
brokeredgeApp.controller('HomeInsurance',['$scope','brokeredgefactory','$location','$resource',function($scope,brokeredgefactory,$location,$resource){
	
	$scope.loading = false;
	$scope.idvValid = false;

	$scope.error='';
	$scope.pts = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	$scope.homeInput = {
		lob : "Home",
		productType : "Home",
		policyType : 1,
		pt : 1,
		hasStructure : false,
		hascontent : false,
		structure : {
			area : 0,
			costSqft : 2500
			
		},
		contentTypes : [
			{ contentType : "CONT",
				  displayName : "Content",
				  isInsured : 0,
				  valueNeeded : "Y",
				  contentValue : 0
				},
			{ contentType : "DOMAPL",
			  displayName : "Appliances",
			  isInsured : 0,
			  valueNeeded : "Y",
			  contentValue : 0
			},
			{ contentType : "JEWEL",
			  displayName : "Jewelery and Valuables",
			  isInsured : 0,
			  valueNeeded : "Y",
			  contentValue : 0
			},
			{ contentType : "ELECEQP",
			  displayName : "Mobile Eqipment",
			  isInsured : 0,
			  valueNeeded : "Y",
			  contentValue : 0
			},
			{ contentType : "TP",
				  displayName : "Third Party Liability",
				  isInsured : 0,
				  valueNeeded : "Y",
				  contentValue : 0
			},
			{ contentType : "BAGG",
			  displayName : "Baggage",
			  isInsured : 0,
			  valueNeeded : "Y",
			  contentValue : 0
			},
			{ contentType : "CASH",
			  displayName : "Cash",
			  valueNeeded : "N",
			  isInsured : 0,
			  contentValue : 0
			},
			{ contentType : "EXTEQP",
				  displayName : "External Equipment",
				  isInsured : 0,
				  valueNeeded : "Y",
				  contentValue : 0
			}
		]
			
	}
	
	$scope.estimateReConstHtml=function(num) {
		var word=brokeredgefactory.convertNumberToWords(num);
		
		return word;
	}
	
	$scope.homeForm=function() 	{
		var input = {
				structure : {},
				
		};
		input.policyType = $scope.homeInput.policyType;
		input.contentpt = 0;
		input.structpt = 0;
		input.pincode = $scope.homeInput.pincode;
		input.productType = "Home";		
		if ($scope.homeInput.policyType !=3){
			input.structpt = $scope.homeInput.structpt;
			$scope.homeInput.hasStructure = true;
			input.structure.area = $scope.homeInput.structure.area;
			input.structure.costSqft = $scope.homeInput.structure.costSqft;
			input.sastruct = $scope.homeInput.structure.area *$scope.homeInput.structure.costSqft;
		}
		input.contentTotal = 0;
		if ($scope.homeInput.policyType !=2){
			input.contentpt = $scope.homeInput.contentpt;
			$scope.homeInput.hasContent = true;
			angular.forEach($scope.homeInput.contentTypes, function(value, key){
			      var type= value.contentType;
			      input.contentTotal = Number(input.contentTotal) + Number(value.contentValue);
			      if (value.isInsured == "0") {
			    	  value.contentValue = 0;
			      }
			      switch (type){
			      case "CONT": 
			    	  input.sacont = value.contentValue;
			    	  break;
			      case "JEWEL": 
			    	  input.sajewel = value.contentValue;
			    	  break;
			      case "DOMAPL": 
			    	  input.sadomapl = value.contentValue;
			    	  break;
			      case "ELECEQP": 
			    	  input.saeleceqp = value.contentValue;
			    	  break;
			      case "BAGG": 
			    	  input.sabagg = value.contentValue;
			    	  break;
			      case "CASH":
			    	  if (value.isInsured == "1") {
			    		  input.sacash = 5000;
				      }
			    	  break;
			      case "EXTEQP": 
			    	  input.saexteqp = value.contentValue;
			    	  break;;
			      case "TP": 
			    	  input.satp = value.contentValue;
			    	  break;
			      }
			      
			});
		}
		console.log(JSON.stringify(input));
		submitQuoteRequest(input);
	}

	
	// SubmitQuoteRequest
	function submitQuoteRequest(quoteInput){
		var input = {
    			url: "",
    			data:quoteInput
    			
    	};
		
		var quotes = $resource('./DataRequest', {}, {
	          processRequest: {
	             method: 'POST'}
	    });
		
		input.url = "/quote/submitAllQuotesRequest/" + $scope.homeInput.lob + "/" + $scope.homeInput.productType;
		//console.log(JSON.stringify($scope.quoteInput))
		//console.log(JSON.stringify(input));
    	var tmp = quotes.processRequest({}, JSON.stringify(input));
    	$scope.loading = true;
    	tmp.$promise.then(function(data){
    		//console.log(data)
    		$scope.loading = false;
    		if (!data.hasOwnProperty("error")){
    			if (data.numResultsExpected == 0){
    				$scope.error = "No Quotes available for this selection";
    			} else
    			{
    				// Transfer Control to Quotes page
    				var url = "/quotes/"+$scope.homeInput.productType + "/" + $scope.homeInput.productType +'/' + data.requestId +'/'+ data.numResultsExpected ;
    				console.log(url);
			    	$location.path(url);
    			}
    		} else
    		{
	    	    console.error(data);
	    	    var str=data.error;
				$scope.quoteError.errors=str.split(";");
			}
    		
    	}, function (error) {
    		$scope.loading = false;
    	    console.error(data);
    	    $scope.error ="Unexpected error processing quote Request";
			
    	});
	}
	
	
		
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
    }
};


// Change Title
$scope.$watch('homeInput.policyType', function(newValue, oldValue) {
    //console.log('insurerId changed to: ' + newValue);
	if (newValue == "3") {
		$scope.pts = [1,2,3];
	} else
	{
		$scope.pts = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	}
	
});
}])