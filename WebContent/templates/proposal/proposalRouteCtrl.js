/**
 * This is the main controller that is called from the quote pages. This controller then
 *  invokes the appropriate proposal form pages for fulfillment
 */
"use strict";
brokeredgeApp.controller('proposalController',['$rootScope', '$scope', '$routeParams', function($rootScope, $scope, $routeParams){
		
		$scope.appInput = {};
		
				
		function init(){
			
			var formUrl = "";
			// get RouteParams
			$scope.appInput.productId = $routeParams.productId;
			$scope.appInput.rmId = $routeParams.rmId;
			$scope.appInput.customerId = $routeParams.customerId;
			$scope.appInput.appNo = $routeParams.appNo;
			switch ($scope.appInput.productId){
			case "109N091V02":
			case "109N091V01":
				formUrl = "templates/proposal/life/bsli/bsliLifeProposal.html";
				break;
			case "110N104V01":
			case "110N104V02":
				formUrl = "";
				break;
			case "110N106V01":
			case "110N106V02":
				formUrl = "";
				break;
			case "110N129V01":
			case "110N129V02":
			case "110N130V01":
			case "110N130V02":
				formUrl = "templates/proposal/life/aia/aiaLifeProposal.html";
				break;
			case "102HL03F01":
			case "102HL03F02":
			case "102HL03F03":
				formUrl = "templates/proposal/health/royalSundaram/royalSundaram.html";
				break;
			case "102MO01PC1":
				formUrl = "templates/proposal/car/royalSundaram/royalSundaram.html";
				break;
			case "103MO01PC1":
			case "103MO01PC2":
			case "103MO01PC3":
				formUrl = "templates/proposal/car/rgi/rgi.html";
				break;
			case "103HL02F01":
				formUrl = "templates/proposal/health/rgi/rgi.html";
				break;
			case "103TL01V01":
				formUrl = "templates/proposal/travel/rgi/rgi.html";
				break;
			case "106MO01PC1":
				formUrl = "templates/proposal/car/itgi/itgi.html";
				break;
			case "106HL03F01":
			case "106HL03I01":
				formUrl = "templates/proposal/health/itgi/itgi.html";
				break;
			case "108MO01PC1":
			case "108MO01PC2":
			case "108MO01PC3":
			case "108MO01PC4":
			case "108MO01PC5":
			case "108MO01PC6":
			case "108MO01PC7":
				formUrl = "templates/proposal/car/tata/tata.html";
				break;
			case "108TL01V01":
			case "108TL02V01":
			case "108TL03V01":
			case "108TL01V02":
			case "108TL02V02":
			case "108TL03V02":
				formUrl = "templates/proposal/travel/aig/aig.html";
				break;
			case "113MO01PC1":
				formUrl = "templates/proposal/car/bagic/bagic.html";
				break;
			case "125HL01F01":
			case "125HL01F02":
			case "125HL06F01":
			case "125HL06F02":
			case "125HL06I02":
				formUrl = "templates/proposal/health/hdfc/hdfc.html";
				break;
			case "125MO01PC1":
			case "125MO01PC2":
			case "125MO01PC3":
			case "125MO01PC4":
			case "125MO01PC5":
				formUrl = "templates/proposal/car/hdfc/hdfc.html";
				break;
			case "125TL01V01":
			case "125TL02V01":
			case "125TL03V01":
			case "125TL04V01":
				formUrl = "templates/proposal/travel/hdfc/hdfc.html";
				break;
			case "129HL01F01":
			case "129HL05F01":
			case "129HL05I01":
			case "129HL07F01":
			case "129HL08I01":
			case "129HL08F01":
				formUrl = "templates/proposal/health/star/star.html";
				break;
			case "129TL01V01":
				formUrl = "templates/proposal/travel/star/star.html";
				break;
			case "131HL08F01":
			case "131HL08F02":
			case "131HL08F03":
			case "131HL01I01":
			case "131HL01I02":
			case "131HL01I03":
			case "131HL06F01":
			case "131HL06I01":
				formUrl = "templates/proposal/health/apollo/apollo.html";
				break;
			case "132MO01PC1":
			case "132MO01PC2":
			case "132MO01PC3":
				formUrl = "templates/proposal/car/fg/fg.html";
				break;
			case "132HL04F01":
				formUrl = "templates/proposal/health/fg/fg.html";
				break;
			case "132TL01V01":
			case "132TL01V02":
			case "132TL01V03":
			case "132TL02V01":
				formUrl = "templates/proposal/travel/fg/fg.html";
				break;
			case "134MO01PC1":
				formUrl = "templates/proposal/car/sompo/sompo.html";
				break;
			case "134HL03F01":
			case "134HL04I01":
				formUrl = "templates/proposal/health/sompo/sompo.html";
				break;
			case "134TL01V01":
			case "134TL02V01":
			case "134TL03V01":
				formUrl = "templates/proposal/travel/sompo/sompo.html";
				break;
			case "139MO01PC1":
				formUrl = "templates/proposal/car/axa/axa.html";
				break;
			case "139HL01F01":
			case "139HL01F02":
			case "139HL01F03":
				formUrl = "templates/proposal/health/axa/axa.html";
				break;
			case "139TL01V01":
			case "139TL02V01":
			case "139TL03V01":
				formUrl = "templates/proposal/travel/axa/axa.html";
				break;
			case "102FI03H01":
			case "102FI03H02":
			case "102FI03H03":
			case "102FI03H04":
			case "102FI03H05":
				formUrl = "templates/proposal/home/royalSundaram/royalSundaram.html";
				break;
			case "148HL01F01":
			case "148HL01F02":
			case "148HL01F03":
			case "148HL03T01":
				formUrl = "templates/proposal/health/religare/religare.html";
				break;
			case "148TL01V01":
			case "148TL01V02":
			case "148TL01V03":
			case "148TL01V04":
			case "148TL01V05":
			case "148TL01V06":
			case "148TL02V01":
			case "148TL02V02":
				formUrl = "templates/proposal/travel/religare/religare.html";
				break;
			case "151HL02F01":
			case "151HL02F02":
			case "151HL02F03":
			case "151HL02F04":
			case "151HL04F01":
			case "151HL04F02":
			case "151HL04F03":
			case "151HL04F04":
				formUrl = "templates/proposal/health/cigna/cigna.html";
				break;
			case "158MO01PC1":
				formUrl = "templates/proposal/car/godigit/goDigit.html";
				break;
			}
			console.log("Redirecting To:" + formUrl);
			$scope.templateUrl = formUrl;
			
		}
		init();
	
	}
]);