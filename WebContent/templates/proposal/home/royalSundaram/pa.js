/**
 * 
 */
"use strict";
brokeredgeApp.controller('paCtrlRSA',['$scope','$uibModalInstance', '$filter', 'input', function($scope, $uibModalInstance, $filter,  input){
	
	console.log(input);
	
	
	$scope.employeeData = input;
	$scope.occupationList = ["Bearer","Cook", "Sweeper"];
	$scope.relList = ["Wife", "Daughter", "Father", "Husband", "Mother", "Son", "Grand Son", "Mother in Law", "Nephew", "Niece", "Sister", "Aunt", "Brother", "Brother in Law", "Daughter in Law", "Father in Law", "Grand Daughter", "Grand Father", "Grand Mother", "Sister in Law", "cousin"];
	// Date stuff
	// Date picker
    var dateToday = new Date();
    var yearMin = dateToday.getFullYear()-80;
    var yearMax = dateToday.getFullYear();
    var yearAdult = dateToday.getFullYear() - 18;
    var monthToday = dateToday.getMonth();
    var dayToday = dateToday.getDate();
    $scope.dobemployee ={};
    
    $scope.dobAdult={
    	 maxDate: new Date(yearAdult, monthToday, dayToday),
    	 minDate: new Date(yearMin, monthToday, dayToday)
    }
    
    $scope.opened=function($event,$index)
	{

	    $scope.opened[$index] = true;
	    
	}
    
    $scope.open = function() {
		$scope.popup1.opened = true;
	};
	$scope.popup1 = {
		opened: false
	};
	
		
	$scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    
    	
	$scope.ok = function () {
		console.log($scope.employeeData);
		$scope.employeeData.dob = $filter('date')($scope.employeeData.dob, "yyyy-MM-dd")
		$uibModalInstance.close($scope.employeeData); // Put the result here
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	// validate 
	$scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	$scope.alphaChar=function($event)
	   {
		   	var regex = new RegExp("^[a-zA-Z ]+$");
		    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (regex.test(str)) {
		        return true;
		    }

		    event.preventDefault();
		    return false;
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
}]);