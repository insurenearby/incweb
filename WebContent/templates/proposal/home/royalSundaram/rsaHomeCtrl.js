/**
 * 
 */
"use-strict"
brokeredgeApp.controller('rsaHomeCntrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','FormSubmitter',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,FormSubmitter){
	//$scope.proposalList=['policy','Proposer','Vehicle','Previous policy','Contact Info','Cover','T & C']
	$scope.proposalInput={};
	$scope.drivingExpr=""
	$scope.buildingAgeList=[{"id" :"0 to 5", "value" : "Up to 5 Years"},{"id" :"5 to 10", "value" : "5 to 10 Years"},{"id" :"10 to 15", "value" : "10 to 15 Years"},{"id" :"15 to 20", "value" : "15 to 20 Years"},{"id" :"20 to 25", "value" : "20 to 25 Years"},{"id" :"25 to 30", "value" : "25 to 30 Years"}];
	$scope.showError='';
	$scope.covers=[];
	$scope.min=1000;
	$scope.max=100000;
	$scope.insurerMaster = {};
	$scope.coversData=[];
	//$scope.proposalInput.policy.policyStartdate='';
	$scope.active=0;
	$scope.tabStatus={
			firstDisabled : false,
			firstComplete : 'indone',
			secondDisabled : false,
			secondComplete : 'indone',
			thirdDisabled : true,
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthDisabled:true,
			sixthComplete:'indone',
	}
	
	// Date Setup
	var dateToday=new Date();
	
	$scope.dateOptionsFuture = {
    		minDate: dateToday
    }
	
	var adultMin = dateToday.getFullYear()-100;
	var adultMax = dateToday.getFullYear()-18;
	var currmonth = dateToday.getMonth();
	var currdate = dateToday.getDate();
	$scope.adultDob={
			minDate: new Date(adultMin, currmonth, currdate),
	    	maxDate: new Date(adultMax, currmonth, currdate)
	}
	console.log($scope.adultDob);
	$scope.opened = [];
	$scope.open1 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open = function($event,$index) {
            $scope.opened[$index] = true;
    };
	$scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    

	$scope.polArea = {};
	$scope.corArea = {};
	$scope.polState={};
	$scope.polCity={};
	$scope.corState={};
	$scope.corCity={};
	
	$scope.areasPolicy = []; 
	$scope.areasCorr = []; 
	//
	$scope.loading = true;
	
	function init() {
		
		$scope.proposalInput.policy.displayPremium = $scope.proposalInput.policy.premiumPayable;
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		$scope.getMaster("State");
		$scope.getMaster("Occupation");
		$scope.getMaster("NomineeRel");
		$scope.getMaster("Relationship");
		console.log($scope.insurerMaster);
		getAreasFromPincode('P', $scope.proposalInput.proposer.policyPincode);
		$scope.setCityState('P',$scope.polArea);
		getAreasFromPincode('C', $scope.proposalInput.proposer.corrPincode);
		$scope.setCityState('C',$scope.corArea);
		
		
		// set sacash
		angular.forEach($scope.proposalInput.contents,function(value,key){
			if(value.contentType == "CASH"){
				switch ($scope.proposalInput.productId){
				case "102FI03H01":
					value.contentValue = 5000;
					break;
				case "102FI03H02":
				case "102FI03H03":
					value.contentValue = 15000;
					break;
				case "102FI03H04":
				case "102FI03H05":
					value.contentValue = 25000;
					break;
				}
			}		
		})
	}
	
	$scope.pageErrors = {
			buildingError: false,
			buildingErrorText: ""
			
	}
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				formatApplication();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
		
		
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		 formatDateToStr();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatApplication();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
		
		
	}
	

	function formatApplication(){
		formatStrToDate();
		setSelectBoxes();
	}
	
	
	// Comvert all strings in the sabed applications to Date format.
	function formatStrToDate (){
		
		$scope.proposalInput.proposer.proposerDob = brokeredgefactory.strToDate($scope.proposalInput.proposer.proposerDob);	
	}
	
	// Convert all Date in the model to String.
	function formatDateToStr (){
		
		$scope.proposalInput.proposer.proposerDob = $filter('date')($scope.proposalInput.proposer.proposerDob,'yyyy-MM-dd');	
	}
	
	function setSelectBoxes(){
		
		$scope.polState.id = $scope.proposalInput.proposer.policyStateCode;
		$scope.polState.value = $scope.proposalInput.proposer.policyState;
		$scope.polCity.id = $scope.proposalInput.proposer.policyCityCode;
		$scope.polCity.value = $scope.proposalInput.proposer.policyCity;
		$scope.corState.id = $scope.proposalInput.proposer.corrStateCode;
		$scope.corState.value = $scope.proposalInput.proposer.corrState;
		$scope.corCity.id = $scope.proposalInput.proposer.corrCityCode;
		$scope.corCity.value = $scope.proposalInput.proposer.corrCity;
	}
	
	$scope.getMaster = function(masterId){
		var input={url:'',data:{}}
		
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url='/master/codes/'+$scope.proposalInput.productId+'/'+ masterId;
		
		
		var tmp=res.save(JSON.stringify(input));
		
		tmp.$promise.then(function(data){
			var masterName = masterId + "List";
			$scope.insurerMaster[masterName] =  data.result;
		
		})
		
	}
	$scope.title=function(val)
	{
		if(val=='M')
		{
			$scope.proposalInput.proposer.title='';
		}
		else
		{
			$scope.proposalInput.proposer.title='';
		}
	}
	
	
	$scope.policy=function(data)
	{
		var dateToday=new Date();
		var yearMax=dateToday.getFullYear();
		var monthToday=dateToday.getMonth();
		var dayToday=dateToday.getDate()+1;
		$scope.proposalInput.policy.policyStartdate=new Date(yearMax,monthToday,dayToday);
		
		$scope.proposalInput.policy.basePremium=$scope.quoteInput.quoteData.netPremium;
		$scope.proposalInput.policy.serviceTax=$scope.quoteInput.quoteData.serviceTax;
		$scope.proposalInput.policy.premiumPayable=$scope.quoteInput.quoteData.totalPremium;
		/*$scope.proposalInput.vehicle.price=$scope.quoteInput.CarInput.price;
		$scope.proposalInput.vehicle.rto=$scope.quoteInput.CarInput.rto;*/
		$scope.policyStart();
		//console.log($scope.proposalInput.policy)
		//$scope.validatePolicyStartDate($scope.proposalInput.policy.policyStartdate)
	}
	
	$scope.getProposerAge=function(dob) {
		$scope.proposalInput.proposer.custage=brokeredgefactory.calculateAge(dob);
	}
	
	$scope.getAge=function(dob) {
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob);
	}
	
	
	$scope.policyPremiumError="";
	$scope.premiumMilageError="";
	$scope.checkAnualMilage=function(form,val)
	{
		
		$scope.premiumMilageError="";
		if(val>=1000 && val<=50000)
		{
			form.annualMilleage.$setValidity('required',true);
			$scope.premiumMilageError=""
		}
		else
		{
			form.annualMilleage.$setValidity('required',false);
			$scope.premiumMilageError="Minimum value 1000 and Max value 50000"
		}
	}
	$scope.checkPolicyPremium=function(form,val)
	{
		
		$scope.policyPremiumError="";
		if(val>=2000 && val<=100000)
		{
			form.policyPremium.$setValidity('required',true);
			$scope.policyPremiumError=""
		}
		else
		{
			form.policyPremium.$setValidity('required',false);
			$scope.policyPremiumError="Minimum value 2000 and Max value 100000"
		}
	}
	
	
	 function getAreasFromPincode (type, pincode){
			
			var re = new RegExp("^[0-9]{6}$");
			if (re.test(pincode)){
				
				var input={url:'',data:{}}
		    	
				input.url="/master/getDetailsForPincode/" + $scope.proposalInput.insurerId + "/" + pincode;
				
		    	var res = $resource('./DataRequest', [], {
			          save: {
			             method: 'POST'}
			    });
				
				var tmp = res.save(input);
				tmp.$promise.then(function(data){
					if (data.hasOwnProperty("result")){
						 if (type == "P"){
							 $scope.areasPolicy = data.result;
							 //$scope.getCity(data.result[0].stateId);
							 $scope.setCityState("P", $scope.polArea)
						 } else
						 {
							 $scope.areasCorr = data.result; 
							 //$scope.getCity(data.result[0].stateId);
							 $scope.setCityState("C", $scope.corArea)
						 }
					}
					
					
				})		
			}
			
			
			
		}
	
	$scope.setCityState = function(type, area){
		
		var areas = []
		if (typeof(area.area) != "undefined"){
			 if (type == "P"){
				 $scope.proposalInput.proposer.policyDistrict = area.district;
				 $scope.proposalInput.proposer.policyArea = area.area;
				 $scope.proposalInput.proposer.policyAreaCode = area.area;
				 //$scope.getCity(area.stateId);
				 
			 } else
			 {
				 $scope.proposalInput.proposer.corrDistrict = area.district;
				 $scope.proposalInput.proposer.corrArea = area.areaName;
				 $scope.proposalInput.proposer.corrAreaCode = area.areaId;
				 //$scope.getCity(area.stateId);
			 }
		}
	}

	
	$scope.contact=function(data) {
		$scope.policyState=data.policyState;
		$scope.policyCity=data.policyCity;
	}
	
	$scope.correspondAddress=function() {
		$scope.proposalInput.proposer.corrAddress1=$scope.proposalInput.proposer.policyAddress1;
		$scope.proposalInput.proposer.corrAddress2=$scope.proposalInput.proposer.policyAddress2;
		$scope.proposalInput.proposer.corrAddress3=$scope.proposalInput.proposer.policyAddress3;
		$scope.proposalInput.proposer.corrArea =$scope.proposalInput.proposer.policyArea;
		$scope.proposalInput.proposer.corrDistrict =$scope.proposalInput.proposer.policyDistrict;
		$scope.proposalInput.proposer.corrStateCode=$scope.proposalInput.proposer.policyStateCode;
		$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
		$scope.proposalInput.proposer.corrCityCode =$scope.proposalInput.proposer.policyCityCode;
		$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
		$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
	 	 
	}
	
	function populateAddonCovers() {
		$scope.covers = [];
		angular.forEach($scope.quoteInput.quoteData.premiumBreakup.addon,function(val,key){
				var cover = {};
				console.log(val);
				Object.keys(val).forEach(function(key1){
					cover.coverId = key1;
					cover.premium = Math.round(Number(val[key1])*1.18);
					cover.coverName = getCoverName(cover.coverId);
					cover.isSelected = false;
					$scope.covers.push(cover);
				});
		});
		
		console.log($scope.covers)
		
	}
	
	function getCoverName(coverId) {
		
		var name = "";
		switch (coverId){
		case "TERROR":
			name = "Terrorism Risk";
			break;
		case "WSLI":
			name = "Waiver of sublimit for content";
			break;
		case "ALTACCO":
			name = "Alternate Accomodation";
			break;
		case "LOSSRENT":
			name = "Loss of Rent";
			break;
		}
		
		return name;
		
	}
	
	$scope.premiumAdd=function(premium) {
		
		$scope.proposalInput.policy.premiumPayable= $scope.proposalInput.policy.premiumPayable+premium;
		updateWCPremium()
	}

	
	$scope.premiumSubtract=function(premium) {
		$scope.proposalInput.policy.premiumPayable= $scope.proposalInput.policy.premiumPayable-premium;
		updateWCPremium();
	}
	
	
	$scope.checkData=function()
	{
		if($scope.policyState!=$scope.proposalInput.proposer.policyState)
		{
			$scope.polCity='';
		}
	}
	
	
	$scope.changeState=function(type, state)
	{
		if (type == 'P'){
			$scope.polCity='';
			$scope.proposalInput.proposer.policyStateCode=state.id;
		 	$scope.proposalInput.proposer.policyState=state.value;	
		} else if (type == 'C'){
			$scope.corCity='';
			$scope.proposalInput.proposer.corrStateCode=state.id;
		 	$scope.proposalInput.proposer.corrState=state.value;	
		}
	 	
	 	$scope.getCity(type, state.id);
	}
	
	
	$scope.getCity=function(type, id)
	{
		var input={
				url:'',
				data:{}
		}
		input.url='/master/city/'+$scope.proposalInput.insurerId+'/'+id;
		var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			$scope.insurerMaster.CityList=data['result'];
			angular.forEach(data['result'],function(val,key){
				
					if(val.value==$scope.policyCity) {
						if (type == 'P') {

							 $scope.polCity=val;
							 $scope.proposalInput.proposer.policyCityCode=val.id;
						 	 $scope.proposalInput.proposer.policyCity=val.value;
						} else if (type == 'C'){

							 $scope.corrCity=val;
							 $scope.proposalInput.proposer.corrCityCode=val.id;
						 	 $scope.proposalInput.proposer.corrCity=val.value;
						}
					}
			});
		})
	}
	
	$scope.changeCity=function(type, city)
	{
		
		if (type == 'P') {
			 $scope.proposalInput.proposer.policyCityCode=city.id;
		 	 $scope.proposalInput.proposer.policyCity=city.value;
		} else if (type == 'C'){
			 $scope.proposalInput.proposer.corrCityCode=city.id;
		 	 $scope.proposalInput.proposer.corrCity=city.value;
		}
	 	
	}
	
	$scope.back=function(index) {
		$scope.active=index -1;
	}
	//next section
	$scope.nextSection = function(index) {
		
		switch (index){
		case "2":
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			$scope.tabStatus.secondDisabled = false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "3":
			$scope.active =2;
			$scope.tabStatus.secondComplete = 'done';
			$scope.tabStatus.thirdDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			console.log($scope.proposalInput)
			break;
		case "4":
							
				$scope.active =3;
				$scope.tabStatus.thirdComplete='done';
				$scope.tabStatus.fourthDisabled= false;
				$scope.proposalError={key:"",errorPirnt:[]}
			
			break;
		
		case "5":
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';
			$scope.tabStatus.fifthDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "6":
			$scope.showError='true';
	   		$scope.active =5;
			$scope.tabStatus.fifthOpen = false;
			$scope.tabStatus.fifthComplete = 'done';
			$scope.tabStatus.sixthOpen = true;
			$scope.tabStatus.sixthDisabled = false;
			$scope.correspondAddress();
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		}
		
		saveApplicationData ();
	}

	
		
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };


     function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
	}
     

  	
  	function setpolicyAddress(){
  		
  		if ($scope.proposalInput.proposer.policyRegAddressSame =='Y'){
  			$scope.proposalInput.proposer.corrAddress1 = $scope.proposalInput.proposer.policyAddress1;
  			$scope.proposalInput.proposer.corrAddress2 = $scope.proposalInput.proposer.policyAddress2;
  			$scope.proposalInput.proposer.corrAddress3 = $scope.proposalInput.proposer.policyAddress3;
  			$scope.proposalInput.proposer.corrArea = $scope.proposalInput.proposer.policyArea;
  			$scope.proposalInput.proposer.corrCity = $scope.proposalInput.proposer.policyCity;
  			$scope.proposalInput.proposer.corrCityCode = $scope.proposalInput.proposer.policyCityCode;
  			$scope.proposalInput.proposer.corrState = $scope.proposalInput.proposer.policyState;
  			$scope.proposalInput.proposer.corrStateCode = $scope.proposalInput.proposer.policyStateCode;
  			$scope.proposalInput.proposer.corrPincode = $scope.proposalInput.proposer.policyPincode;
  		}
  		

		var addresses = [];
		var address = {}
		address.addressType = "C";
 		address.addressLine1 = $scope.proposalInput.proposer.corrAddress1;
		address.addressLine2 = $scope.proposalInput.proposer.corrAddress2;
		address.addressLine3 = $scope.proposalInput.proposer.corrAddress3;
		address.area = $scope.proposalInput.proposer.corrArea
		address.district = $scope.proposalInput.proposer.corrDistrict
		address.city = $scope.proposalInput.proposer.corrCity;
		address.cityCode = $scope.proposalInput.proposer.corrCityCode;
		address.state = $scope.proposalInput.proposer.corrState;
		address.stateCode = $scope.proposalInput.proposer.corrStateCode;
		address.pincode = $scope.proposalInput.proposer.corrPincode;
		addresses.push(address);
		
		var address = {}
		address.addressType = "P";
 		address.addressLine1 = $scope.proposalInput.proposer.policyAddress1;
		address.addressLine2 = $scope.proposalInput.proposer.policyAddress2;
		address.addressLine3 = $scope.proposalInput.proposer.policyAddress3;
		address.area = $scope.proposalInput.proposer.policyArea
		address.district = $scope.proposalInput.proposer.policyDistrict
		address.city = $scope.proposalInput.proposer.policyCity;
		address.cityCode = $scope.proposalInput.proposer.policyCityCode;
		address.state = $scope.proposalInput.proposer.policyState;
		address.stateCode = $scope.proposalInput.proposer.policyStateCode;
		address.pincode = $scope.proposalInput.proposer.policyPincode;
		addresses.push(address);
		$scope.proposalInput.proposer.address = angular.copy(addresses);
		
		var contacts = []
		var contact = {};
		contact.contactType = "mobile";
		contact.contactText = $scope.proposalInput.proposer.mobile;
		contacts.push(contact);
		var contact = {};
		contact.contactType = "email";
		contact.contactText = $scope.proposalInput.proposer.email;
		contacts.push(contact);
		$scope.proposalInput.proposer.contacts = angular.copy(contacts);
  	}
  	
	function populateCoverData(){
		
		$scope.proposalInput.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			
			if(value.isSelected == "Y")
				{
				 var cover = {};
				 cover.name = value.coverId;
				 $scope.proposalInput.covers.push(cover);
				}
			
		})
		
	}
  	
    $scope.makePayment=function()
 	{
 		//$scope.healthProposalInput.proposer.proposerDob=$(filter)
    	populateCoverData();
    	setpolicyAddress();
    	formatDateToStr();
 		var input={url:'',data:{}}
 		input.url='/submitProposal/home/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
 		
 		$scope.proposalInput.policy.basePremium = Math.round($scope.proposalInput.policy.premiumPayable/1.18);
 		$scope.proposalInput.policy.serviceTax = $scope.proposalInput.policy.premiumPayable - $scope.proposalInput.policy.basePremium;

 		var homeProposal=$scope.proposalInput;
 		console.log(JSON.stringify(homeProposal));
 		
		
		
		var date=new Date($scope.proposalInput.policy.policyEnddate);
 		homeProposal.policy.policyStartdate=$filter('date')(new Date(date.getFullYear()-1,date.getMonth(),date.getDate()+1),'yyyy-MM-dd')
 		var startDate=new Date(homeProposal.policy.policyStartdate)
 		homeProposal.policy.policyEnddate=$filter('date')(new Date(startDate.getFullYear()-1,startDate.getMonth(),startDate.getDate()+1),'yyyy-MM-dd')
 		input.data=homeProposal;
 		console.log(JSON.stringify(homeProposal))
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
 		$scope.loading = true;
 		tmp.$promise.then(function(data){
			if(data.hasOwnProperty("error"))
			{
				$scope.loading = false;
				$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str=data['error'];
				$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
				console.log($scope.proposalError)
				//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
				if(data['error']=="Premium Mismatch")
				{
				 	
				 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]})
				} 
				
			}
			else
			{
				$scope.loading = false;
				$scope.proposalError={key:"",errorPirnt:[]}
				var method = 'POST';
				var url = data.payUrl;
				console.log(url);
				var paymentInput = angular.copy(data.paymentInput);
				console.log(paymentInput);
				var quoteId = paymentInput.quoteId;
				alert("Quote Id: " + quoteId);
				FormSubmitter.submit(url, method, paymentInput);
			}
 		})
 	} 
    $scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined) {
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.proposalInput.policy.displayPremium = $scope.proposalInput.policy.premiumPayable;
        	//$scope.proposalInput.policy.serviceTax=$scope.selected.ServiceTax;
        	//$scope.proposalInput.policy.basePremium=$scope.selected.basePremium;
        	  $scope.makePayment();
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
    
    function otp(otpInput) {
		var pgUrl = otpInput.pgUrl;
		
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/modal/otp.html',
		      controller: 'otpCtrl',
		      size: 'md',
		      resolve: {
		    	  'input': function () {
		          return otpInput;
		        }
		      }
		});
		modalInstance.result.then(function (output) {
        if(output.result == "true")
        	{
        	//basePremium.
        	$window.location.href = pgUrl;
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
    // $scope.validatePolicyStartDate()
 // date picker
    $scope.backPreviousPage=function()
	{
		window.history.back();
	}

    $scope.deleteWC = function(i){
    	$scope.proposalInput.wcList.splice(i,1);
    	updateWCPremium();
    }
    


    $scope.deletePA = function(i){
    	$scope.proposalInput.paList.splice(i,1);
    	updatePAPremium();
    }
    
    
    function updatePAPremium() {
    	
    	$scope.proposalInput.policy.displayPremium = $scope.proposalInput.policy.premiumPayable;
    	angular.forEach($scope.proposalInput.paList,function(val,key){
    		
    		var premium = Math.round(1.18*0.0049*val.annualWages) ;
    		$scope.proposalInput.policy.displayPremium = Number($scope.proposalInput.policy.displayPremium) + Number(premium);
    		
    	});
    	
    }
    
    function validateOtherSI(){
    	
    	$scope.pageErrors.buildingError = false;
		$scope.pageErrors.buildingErrorText = "";
		 
		if ( $scope.proposalInput.policy.hasStructure == 'Y' && $scope.proposalInput.structure.builtByConcrete != "Y") {
			$scope.pageErrors.buildingError = true;
			 $scope.pageErrors.buildingErrorText = "Only Building made out of concrete can be insured";
		}
		
    	if (typeof($scope.proposalInput.structure.compoundwallSI)!= "undefined" && $scope.proposalInput.structure.compoundwallSI >0) {

			var highVal = 0.2*$scope.proposalInput.structure.buildingSI;
			var lowVal = 0.1*$scope.proposalInput.structure.buildingSI;
			 if (Number($scope.proposalInput.structure.compoundwallSI) >highVal || Number($scope.proposalInput.structure.compoundwallSI) <lowVal ){
				 $scope.pageErrors.buildingError = true;
				 $scope.pageErrors.buildingErrorText = "Please enter the valid compound wall Sum Insured. The minimum should be 10% and the maximum should be 20% of the building sum insured amount.";
			 }
		}
    	
    	if (typeof($scope.proposalInput.structure.landscapeSI)!= "undefined" && $scope.proposalInput.structure.landscapeSI >0) {

			var highVal = 0.2*$scope.proposalInput.structure.buildingSI;
			var lowVal = 0.1*$scope.proposalInput.structure.buildingSI;
			 if (Number($scope.proposalInput.structure.landscapeSI) >highVal || Number($scope.proposalInput.structure.landscapeSI) <lowVal ){
				 $scope.pageErrors.buildingError = true;
				 $scope.pageErrors.buildingErrorText = "Please enter the valid Landscape Sum Insured. The minimum should be 10% and the maximum should be 20% of the building sum insured amount.";
			 }
		}
    }
    
    function updateWCPremium() {
    	
    	$scope.proposalInput.policy.displayPremium = $scope.proposalInput.policy.premiumPayable;
    	angular.forEach($scope.proposalInput.wcList,function(val,key){
    		
    		var premium = Math.round(1.18*0.0049*val.annualWages) ;
    		$scope.proposalInput.policy.displayPremium = Number($scope.proposalInput.policy.displayPremium) + Number(premium);
    		
    	});
    	
    }
	
	getApplicationData();
    
    //Modal Windows
    $scope.addWC=function() {
		
		//console.log(employee);
    	var employee = {};    	
		var modalInstance=$uibModal.open({
			ariaLabelledBy:'modal-title',
			ariaDescribedBy:'modal-body',
			templateUrl:'./templates/proposal/home/royalSundaram/wcdetail.html',
			controller:'wcCtrlRSA',
			size:'lg',
			resolve:{
				input:function()
				{
					return employee;
				}
			}
		});
		modalInstance.result.then(function(response){
			var output = response;
			if(response!=undefined) {
				$scope.proposalInput.wcList.push(output);
				updateWCPremium();
			}

			console.log($scope.proposalInput.wcList);
		})
	}
    
    
    $scope.addPA=function() {
		
		//console.log(employee);
    	var employee = {};    	
		var modalInstance=$uibModal.open({
			ariaLabelledBy:'modal-title',
			ariaDescribedBy:'modal-body',
			templateUrl:'./templates/proposal/home/royalSundaram/padetail.html',
			controller:'paCtrlRSA',
			size:'lg',
			resolve:{
				input:function()
				{
					return employee;
				}
			}
		});
		modalInstance.result.then(function(response){
			var output = response;
			if(response!=undefined) {
				$scope.proposalInput.paList.push(output);
				updateWCPremium();
			}

			console.log($scope.proposalInput.paList);
		})
	}
    
  
  // Add Functions for Watch Variables  
    $scope.$watch('proposalInput.proposer.policyPincode', function(newValue, oldValue) {
        
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		getAreasFromPincode("P", newValue);
    	}
    	
    });
    
    $scope.$watch('proposalInput.proposer.corrPincode', function(newValue, oldValue) {
        
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		getAreasFromPincode("C", newValue);
    	}
    	
    });
    
	$scope.$watch('proposalInput.structure.builtByConcrete', function(newVal, oldVal){
		if (typeof(newVal) != "undefined") {
			validateOtherSI();
		}
		
	});
    
	$scope.$watch('proposalInput.structure.compoundwallSI', function(newVal, oldVal){
		if (typeof(newVal) != "undefined") {
			validateOtherSI();
		}
		
	});
	
	$scope.$watch('proposalInput.structure.landscapeSI', function(newVal, oldVal){
		if (typeof(newVal) != "undefined") {
			validateOtherSI();
		}
	});
    
}])