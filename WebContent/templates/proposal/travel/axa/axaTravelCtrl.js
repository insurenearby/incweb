/**
 * 
 */
"use-strict"
brokeredgeApp.controller('axaTravelCtrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','FormSubmitter',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,FormSubmitter){
	
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polCity = {};
	$scope.corCity = {}
	$scope.polCities = [];
	$scope.corCities = []
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}
	

	$scope.pageErrors = {
			insuredError: false,
			medicalError: false,
			policyError : false,
			insuredErrorText : "",
			medicalErrorText : "",
			policyErrorText : ""
	}
	
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
    
    $scope.expiryDateOptions={
			maxDate: new Date(newYearMax+10, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
    // Methods for opening of Calendar Control  
	$scope.opened= {
    	nomineeDob : [],
    	expiryDate : [],
    	startDate : []
    };
	$scope.open1 = function() {
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
	
   $scope.open = function(name,$index) {
	   		if (name == "nomineeDob"){
	   			$scope.opened.nomineeDob[$index] = true;
	   		}
	   		if (name == "expiryDate"){
	   			$scope.opened.expiryDate[$index] = true;
	   		}
	   		if (name == "startDate"){
	   			$scope.opened.startDate[$index] = true;
	   		}
            
   };
   
   
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
    

	
	function init() {
		$scope.input.lob = "Travel";
		$scope.input.productType = $scope.proposalInput.productType;
		initializeMedicalQuestions();
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			switch (addressType){
			case "C":
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCitycode = value.cityCode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStatecode = value.stateCode;
				break;
			}
		});		

		
		if ((Number($scope.proposalInput.policy.numAdults) + Number($scope.proposalInput.policy.numChildren)) == 1){
			$scope.proposalInput.policy.floater = "N";

		} else
		{
			$scope.proposalInput.policy.floater = "Y";
		}
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		getMaster($scope.proposalInput.productId, "Occupation");
		getMaster($scope.proposalInput.productId, "Realtionship");
		getMaster($scope.proposalInput.productId, "NomineeRel");
		getMaster($scope.proposalInput.productId, "Title");
		getMaster($scope.proposalInput.productId, "MaritalStatus");
		getMaster($scope.proposalInput.productId, "State");
		getQuestions($scope.proposalInput.productId, "PedList");


		$scope.proposalInput.policy.tcAgree = false;
		
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalQuestions();
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;
		
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates ();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates (){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob.substr(0, 10));	
		}
		
		if (typeof($scope.proposalInput.proposer.nomineeDob) != "undefined" && $scope.proposalInput.proposer.nomineeDob != ""){
			$scope.proposalInput.proposer.nomineeDob = new Date($scope.proposalInput.proposer.nomineeDob.substr(0, 10));	
		}
		
		angular.forEach($scope.proposalInput.insured,function(value,key){
			if (typeof(value.passportExpiryDate) != "undefined" && value.passportExpiryDate != ""){
				value.passportExpiryDate = new Date(value.passportExpiryDate.substr(0, 10));
			}
		})
		
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeDob = new Date($scope.proposalInput.proposer.appointeeDob.substr(0, 10));
		}
		
		
		
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
		}
		
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			angular.forEach(insured.pedList,function(item,key){
				
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						$scope.medicalQuestions[i].exists = item.exists;
						angular.forEach(item.pedText,function(illness,key){
							var detail = angular.copy(illness);
							detail.insuredId = insuredId;
							detail.insuredName = insuredName;
							detail.questionId = code;
							$scope.medicalQuestions[i].details.push(detail);
						})
					}
				}
				
			})
		})
		//console.log($scope.medicalQuestions);
	}

	
	function ProcessMedicalQuestions(){
		
		// For AXA medical and Lifestyle answers are stored at Proposer Level
		angular.forEach($scope.medicalQuestions,function(question,key){
				$scope.proposalInput.proposer[question.id] = question.exists;
		})
		
	}
	
	function getMaster(productId, masterId){
		$scope.insurerMaster[masterId] = [];
		var tmp=brokeredgefactory.getMaster(productId, masterId);
		tmp.$promise.then(function(data){
			//console.log(data);
			$scope.insurerMaster[masterId]= data['result'];
			if (masterId == "Realtionship"){
				addGendertoRel();
			}
			//console.log($scope.insurerMaster);
		})
	}	

	
	function getQuestions(productId, type){
		var questions = [];
		 var input = {
	    			url: '/master/getQuestions/' + productId + '/' + type,
	    			data : {}
	    	};
	        
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
		var tmp=res.process(input);
		tmp.$promise.then(function(data){
			//console.log(data);
			if (data.hasOwnProperty("result")){
				if (type == "PedList"){
					$scope.medicalQuestions = data.result;
					initializeMedicalQuestions();
				}
				
			}
			//console.log($scope.insurerMaster);
		})
		
	}
	
	$scope.getMasterValue = function(masterName, masterId){
		// 
		var masterValue = masterId;
		var values = $scope.insurerMaster[masterName];
		
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value; 
					break;
				}
			}
		}
		return masterValue; 
	}
	
	$scope.getQuestionText = function(type, id){
		// 
		var questionText = "";
		var values = [];
		if (type == "PedList"){
			values = $scope.medicalQuestions;
		}

				
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == id){
					questionText = values[i].value; 
					break;
				}
			}
		}
		return questionText; 
	}
	
	function addGendertoRel (){
		var records = [];
		angular.forEach($scope.insurerMaster.Realtionship,function(val,key){
			var record = val;
			record.gender = getRelGender(val.value);
			records.push(record);
		});
		$scope.insurerMaster.Relationship = records;
	}
	
	
	
	$scope.checkRelationShip=function(form)
     {
    	
		$scope.pageErrors.policyErrorText = "";
		$scope.pageErrors.policyError = false;
		$scope.pageErrors.insuredErrorText = "";
		$scope.pageErrors.insuredError = false;
		$scope.pageErrors.medicalErrorText = "";
		$scope.pageErrors.medicalError = false;
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.maritalStatus= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
    	var selfCount = 0;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "Self"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if (val.type == "C"){ // Proposer cannot be a child.
    				$scope.pageErrors.insuredError= true;
    				$scope.pageErrors.insuredErrorText = "Child cannot be a proposer";
    			}
    			selfCount++;
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
    	if (selfCount>1) {
			$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
    	}    	
     }
	
	function setRelationship() {
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "Self"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
		
	}	
	
	$scope.checkMedicalStatus = function(){
		
		$scope.pageErrors.medicalError = false;
		$scope.pageErrors.medicalErrorText = "";
		
		for (var i =0; i<$scope.medicalQuestions.length; i++){
			if ($scope.medicalQuestions[i].exists == "Y"){
				$scope.pageErrors.medicalError = true;
				$scope.pageErrors.medicalErrorText = "Cannot buy this policy online";
				break;
			}
			
		};
		
	}
	
	
	
	$scope.checkPolicyStatus = function(){
		
		$scope.pageErrors.policyError = false;
		$scope.pageErrors.policyErrorText = "";
		
		if ($scope.proposalInput.policy.currentLocation == 'N') {
			$scope.pageErrors.policyError = true;
			$scope.pageErrors.policyErrorText = "Insured Need to be in India at the time of buying policy";
		}
		
		if ($scope.proposalInput.policy.validPassport == 'N') {
			$scope.pageErrors.policyError = true;
			$scope.pageErrors.policyErrorText = "Insured need to have an Indian Passport for this policy";
		}
		
	}
		
	$scope.setGender=function(val)
	{
		if($scope.proposalInput.proposer.title=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	function getRelGender(rel){
		var gender = "MF";
		if(rel=='Wife'  || rel=='Aunt'  || rel=='Daughter'  || rel=='Daughter in law'  || rel=='Grand Daughter'  || rel=='Grand Mother'  || rel=='Mother in law'  || rel=='Mother'   || rel=='Niece'  || rel=='Sister in law'  || rel=='Sister'){
			gender = "F";
				
		} else if (rel=='Brother in law'  || rel=='Brother'  || rel=='Father in law'  || rel=='Father'  || rel=='Grand Father'  || rel=='Grand Son'  || rel=='Husband'  || rel=='Nephew'   || rel=='Son in law'  || rel=='Son'  || rel=='Uncle'){
			gender = "M";
		}
		return gender;
	}

	//Nominee
	$scope.calculateAge=function(dob)
	{
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	
	
	
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "1":
			
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			break;
		case "2":			
			setRelationship();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=2;
			$scope.tabStatus.secondComplete = 'done';
			break;
		case "3":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =3;
			$scope.tabStatus.thirdComplete = 'done';
			break;
		case "4":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';			
			break;
		case "5":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =5;
			$scope.tabStatus.fifthComplete='done';
			break;
		}
		
		saveApplicationData ();
	}
	
	function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
		}
	function setpolicyAddress(){
 		
		var addresses = [];
		var address = {}
		address.addressType = "C";
 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
		address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
		address.city = $scope.proposalInput.proposer.corrCity;
		address.cityCode = $scope.proposalInput.proposer.corrCity;
		address.state = $scope.proposalInput.proposer.corrStateCode;
		address.stateCode = $scope.proposalInput.proposer.corrStateCode;
		address.pincode = $scope.proposalInput.proposer.corrPincode;
		addresses.push(address);
		$scope.proposalInput.proposer.address = angular.copy(addresses);
		
		//
		var contacts = []
		var contact = {};
		contact.contactType = "mobile";
		contact.contactText = $scope.proposalInput.proposer.mobile;
		contacts.push(contact);
		var contact = {};
		contact.contactType = "email";
		contact.contactText = $scope.proposalInput.proposer.email;
		contacts.push(contact);
		$scope.proposalInput.proposer.contacts = angular.copy(contacts);
		
 	}
	
	$scope.makePayment=function()
	{
		//$scope.proposalInput.proposer.proposerDob=$(filter);
		
		// Populate Medical and LifeStyle data
		$scope.proposalError={key:"",errorPirnt:[]}
		
		if ($scope.proposalInput.policy.floater == 'N'){
			$scope.proposalInput.policy.floaterSi = $scope.proposalInput.policy.sumInsured;
		}
		$scope.proposalInput.proposer.nomineeName = $scope.proposalInput.proposer.nomineeFirstName + " " + $scope.proposalInput.proposer.nomineeLastName;
		
		var travelProposal = angular.copy($scope.proposalInput);
		travelProposal.policy.policyStartdate = $filter('date')(new Date(travelProposal.policy.policyStartdate),'yyyy-MM-dd');
		travelProposal.proposer.proposerDob = $filter('date')(new Date(travelProposal.proposer.proposerDob),'yyyy-MM-dd');
		angular.forEach(travelProposal.insured,function(val,key){
			val.nomineeDob = $filter('date')(new Date(val.nomineeDob),'yyyy-MM-dd');
		});
		
		
		console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
		
		var input={url:'',data:{}}
		input.url='/submitProposal/travel/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
		input.data=angular.copy(travelProposal);
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		$scope.loading = true;
		tmp.$promise.then(function(data){
			$scope.loading = false;
				if(data.error)
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
					//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
					if(data['error']=="Premium Mismatch")
					{
					 	
					 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]})
					}
				}
				else if (data.hasOwnProperty("payUrl")) 
				{
					$scope.proposalError={key:"",errorPirnt:[]}
					console.log(data);
					var paymentInput = angular.copy(data);	
					var url = data.payUrl;
					var method = "POST";
					var method = "GET";
					// Added for UAT, Please remove in production
					//paymentInput.Amount = 1;
					FormSubmitter.submit(url, method, paymentInput);
					
				} else
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
				}
			
		
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
	}
	
	getApplicationData();
	
	
	$scope.showTC=function(){
		
		var modalInstance=$uibModal.open({
		  animation: $scope.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      controller : 'tcCtrl',
	      templateUrl: 'templates/proposal/health/apollo/tc.html',
	      size: 'lg'
		});
		modalInstance.result.then(function (agree) {
	      console.log(agree);
	      if( typeof(agree) != 'undefined')
	      	{
	    	  $scope.proposalInput.policy.tcAgree = agree;
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	
	
	$scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
	
	
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
     
     $scope.appointeeAge=function(dob) {
 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
 	}
     
     // Watch function // Watch function
     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {
         	
         	var date=new Date(newValue);
         	if ($scope.proposalInput.policy.productType == "AMT") {
         		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+ 1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
         	} else
         	{
         		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear(),date.getMonth(),date.getDate()+ Number($scope.proposalInput.policy.travelDays)-1),'yyyy-MM-dd');
         	}
     		
         }
     	
     });
     
     $scope.$watch('proposalInput.policy.currentLocation', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {
         	
        	 angular.forEach($scope.proposalInput.insured,function(val,key){
        		 val.currentLocation = newValue;
        	 });
         }
     	
     });
     
     
}])