/**
 * 
 */
"use-strict"
brokeredgeApp.controller('hdfcTravelCtrl',['$scope','brokeredgefactory','FormSubmitter','$routeParams','$filter','$resource','$uibModal','$http','$window','paymentService',function($scope,brokeredgefactory,FormSubmitter,$routeParams,$filter,$resource,$uibModal,$http,$window,paymentService){
			
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polState={};
	$scope.polCity={};
	$scope.nomState={};
	$scope.nomCity={};
	$scope.polCities = [];
	$scope.nomCities = [];
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	
	$scope.pageErrors = {
			
			insuredError: false,
			medicalError: false,
			insuredErrorText : "",
			medicalErrorText : ""
	}
	
	
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}
	
	$scope.pedList = [
	    {
	        "id": "1",
	        "name": "Cancer / Carcinoma / Malignancy",
	        "valid": "0"
	    },
	    {
	        "id": "2",
	        "name": "Diabetes",
	        "valid": "1"
	    },
	    {
	        "id": "3",
	        "name": "Thyroid",
	        "valid": "1"
	    },
	    {
	        "id": "4",
	        "name": "Arthritis",
	        "valid": "1"
	    },
	    {
	        "id": "5",
	        "name": "Bypass / Angioplasty",
	        "valid": "1"
	    },
	    {
	        "id": "6",
	        "name": "Kidney disease",
	        "valid": "1"
	    },
	    {
	        "id": "7",
	        "name": "Gall bladder",
	        "valid": "1"
	    },
	    {
	        "id": "8",
	        "name": "Fracture",
	        "valid": "1"
	    },
	    {
	        "id": "9",
	        "name": "Joint Replacement Surgery",
	        "valid": "1"
	    },
	    {
	        "id": "10",
	        "name": "Disc Prolapse",
	        "valid": "1"
	    },
	    {
	        "id": "11",
	        "name": "Gastric Ulcer",
	        "valid": "1"
	    },
	    {
	        "id": "12",
	        "name": "Asthma / Bronchitis",
	        "valid": "1"
	    },
	    {
	        "id": "13",
	        "name": "Cataract",
	        "valid": "1"
	    },
	    {
	        "id": "14",
	        "name": "Paralysis",
	        "valid": "0"
	    },
	    {
	        "id": "15",
	        "name": "Multiple sclerosis",
	        "valid": "0"
	    },
	    {
	        "id": "16",
	        "name": "Parkinson's / Parkinsonism ",
	        "valid": "0"
	    },
	    {
	        "id": "17",
	        "name": "Seizure disorder",
	        "valid": "0"
	    },
	    {
	        "id": "18",
	        "name": "Epilepsy",
	        "valid": "0"
	    },
	    {
	        "id": "19",
	        "name": "Muscular atrophy",
	        "valid": "0"
	    },
	    {
	        "id": "20",
	        "name": "lymphoma",
	        "valid": "0"
	    }
	]
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
    $scope.expiryDateOptions={
			maxDate: new Date(newYearMax+10, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
       
    $scope.opened= {
        	nomineeDob : [],
        	expiryDate : [],
        	startDate : [],
        	proposerDob : []
        };
	$scope.open1 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open = function(name,$index) {
   		if (name == "nomineeDob"){
   			$scope.opened.nomineeDob[$index] = true;
   		}
   		if (name == "expiryDate"){
   			$scope.opened.expiryDate[$index] = true;
   		}
   		if (name == "startDate"){
   			$scope.opened.startDate[$index] = true;
   		}

   		if (name == "proposerDob"){
   			$scope.opened.proposerDob[$index] = true;
   		}
           
  };
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
    

	
	function init() {
		$scope.input.lob = "Health";
		$scope.input.productType = $scope.proposalInput.productType;
		initializeMedicalQuestions();
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		});
		
		if ((Number($scope.proposalInput.policy.numAdults) + Number($scope.proposalInput.policy.numChildren)) == 1){
			$scope.proposalInput.policy.floater = "N";

		}
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			switch (addressType){
			case "C":
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				value.corrAddressLine3 = $scope.proposalInput.proposer.corrArea = value.addressLine3;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCityCode = value.cityCode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStateCode = value.stateCode;
				$scope.proposalInput.proposer.corrPincode = value.pincode;
				break;
			}
		});
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		getMaster($scope.proposalInput.productId, "State");
		getMaster($scope.proposalInput.productId, "Relationship"); 
		getMaster($scope.proposalInput.productId, "TravelPurpose");
		getMaster($scope.proposalInput.productId, "NomineeRel");
		getQuestions($scope.proposalInput.productId, "PedList")	;
		
		$scope.polState.id = $scope.proposalInput.proposer.corrStateCode;
		$scope.polState.value = $scope.proposalInput.proposer.corrState;
		$scope.getCity($scope.polState,'policy');
		$scope.polCity.id = $scope.proposalInput.proposer.corrCityCode;
		$scope.polCity.value = $scope.proposalInput.proposer.corrCity;
		$scope.proposalInput.policy.tcAgree = "N";
		
	}
	

	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalQuestions();
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;
		
		/* Don't wait for Return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates (){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob);	
		}
		
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			if (typeof(insured.nomineeDob) != "undefined" && insured.nomineeDob != ""){
				insured.nomineeDob = new Date(insured.nomineeDob);
			}

			if (typeof(insured.passportExpiryDate) != "undefined" && insured.passportExpiryDate != ""){
				insured.passportExpiryDate = new Date(insured.passportExpiryDate);
			}
			
		});
		
		
		
		
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
		}
		
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			angular.forEach(insured.pedList,function(item,key){
				
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						$scope.medicalQuestions[i].exists = item.exists;
						angular.forEach(item.pedText,function(illness,key){
							var detail = angular.copy(illness);
							detail.insuredId = insuredId;
							detail.insuredName = insuredName;
							detail.questionId = code;
							$scope.medicalQuestions[i].details.push(detail);
						})
					}
				}
				
			})
		})
		//console.log($scope.medicalQuestions);
	}

	
	function ProcessMedicalQuestions(){
		
		// initialize the medical detail Array
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			
			angular.forEach(insured.pedList,function(item,key2){
				item.pedText = [];
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.medicalQuestions,function(question,key){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(medDetail,key){
					 var questionId = medDetail.questionId;
					 var insuredId = medDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key1){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.pedList,function(item,key2){
								 if (item.pedCode == questionId){
									 item.exists = "Y";
									 var pedDetails = {}
						    	  	 pedDetails.monthOfDiagnosis = medDetail.monthOfDiagnosis;
									 pedDetails.yearOfDiagnosis = medDetail.yearOfDiagnosis;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 pedDetails.medicationDetail = medDetail.medicationDetail;
									 pedDetails.treatmentOutCome = medDetail.treatmentOutCome;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 item.pedText.push(pedDetails);
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	
	$scope.getCity = function(state,name) {
		
		if(name=='nomadd')
		{
			$scope.proposalInput.proposer.nomineeStateCode=state.id;
			$scope.proposalInput.proposer.nomineeState=state.value;  
		}else if(name=="policy") {
			$scope.proposalInput.proposer.corrStateCode = state.id;
			$scope.proposalInput.proposer.corrState = state.value;
		}
		
		var cities = [];
		var input={
				url:'',
				data:{}
		}
		
	
		if (typeof(state) != "undefined"){
			input.url='/master/city/'+$scope.proposalInput.insurerId+'/'+state.id;
			var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
			tmp.$promise.then(function(data){
				cities=data['result'];
				if(name=='nomadd')
				{
					$scope.nomCities = cities
				}else if(name=="policy") { 
					$scope.polCities = cities
				}
				
			})
		}

	}
	
	$scope.changeCity = function(city,name) {
		if(name=="policy") {
			$scope.proposalInput.proposer.corrCityCode = city.id;
			$scope.proposalInput.proposer.corrCity = city.value;
			
		}else if(name=="nomadd") {
			$scope.proposalInput.proposer.nomineeCityCode = city.id;
			$scope.proposalInput.proposer.nomineeCity =  city.value;
		} 
	}
	
	function getMaster(productId, masterId){
		$scope.insurerMaster[masterId] = [];
		var tmp=brokeredgefactory.getMaster(productId, masterId);
		tmp.$promise.then(function(data){
			//console.log(data);
			$scope.insurerMaster[masterId]= data['result'];
			if (masterId == "Relationship"){
				addGendertoRel();
			}
			//console.log($scope.insurerMaster);
		})
	}
	

	
	function getQuestions(productId, type){
		var questions = [];
		 var input = {
	    			url: '/master/getQuestions/' + productId + '/' + type,
	    			data : {}
	    	};
	        
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
		var tmp=res.process(input);
		tmp.$promise.then(function(data){
			//console.log(data);
			if (data.hasOwnProperty("result")){
				if (type == "PedList"){
					$scope.medicalQuestions = data.result;
					initializeMedicalQuestions();
				}
				
			}
			//console.log($scope.insurerMaster);
		})
		
	}
	
	$scope.getMasterValue = function(masterName, masterId){
		// 
		var masterValue = masterId;
		var values = $scope.insurerMaster[masterName];
		
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value; 
					break;
				}
			}
		}
		return masterValue; 
	}
	
	$scope.getQuestionText = function(type, id){
		// 
		var questionText = "";
		var values = [];
		if (type == "PedList"){
			values = $scope.medicalQuestions;
		}

				
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == id){
					questionText = values[i].value; 
					break;
				}
			}
		}
		return questionText; 
	}
	
	function addGendertoRel (){
		var records = [];
		angular.forEach($scope.insurerMaster.Relationship,function(val,key){
			var record = val;
			record.gender = getRelGender(val.value);
			record.type = getRelType(val.value);
			records.push(record);
		});
		$scope.insurerMaster.Relationship = records;
	}
	
	
	//validate of 
	$scope.validateOcupation=function()
	{
		if($scope.proposalInput.proposer.occupationType=='POLITICIAN')
		{
			$scope.ocupationError="Please contact our nearest branch or kindly chat with our online representative or call us at 1860 425 0000 for more information.";
		}
		else
			{
			$scope.ocupationError="";
			}
	}
	
	$scope.checkInsuredError=function()
     {
    	

		$scope.pageErrors.insuredErrorText = "";
		$scope.pageErrors.insuredError = false;
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.maritalStatus= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
    	var selfCount = 0;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "E"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if (val.type == "C"){ // Proposer cannot be a child.
    				$scope.pageErrors.insuredError= true;
    				$scope.pageErrors.insuredErrorText = "Proposer Cannot be child";
    			}
    			selfCount++;
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    		
    		// Check pedList to see if there is an invalid PED
    		val.illness = [];
    		if (val.isPED == "Y") {
        		angular.forEach(val.ped, function(illness,key){
        			val.illness.push(illness.name);
        			if (illness.valid == "0"){
        				$scope.pageErrors.insuredError= true;
        				$scope.pageErrors.insuredErrorText = "Cannot buy the policy online";
        			}
        		})
    		}
    		
    	});
    	if (selfCount>1) {
    		$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
    	}
    	
    	
    	
     }
	
	function setRelationship() {
		
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "E"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
	}
	
	
	$scope.checkMedicalStatus = function(){
		
		$scope.pageErrors.medicalError = false;
		$scope.pageErrors.medicalErrorText = "";
		
		for (var i =0; i<$scope.medicalQuestions.length; i++){
			if ($scope.medicalQuestions[i].exists == "Y"){
				$scope.pageErrors.medicalError = true;
				$scope.pageErrors.medicalErrorText = "Cannot buy this policy online";
				break;
			}
			
		};
		
	}
	
	$scope.setGender=function(val)
	{
		if($scope.proposalInput.proposer.title=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	$scope.calulateBmi=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			
			if(key.type=='A')
				{
				var heighttm=$scope.proposalInput.insured[val].height*0.01;
				var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
				$scope.proposalInput.insured[val].bmi=parseInt(bmi)
				}
			
		})
	}

	
	function getRelGender(rel){
		var gender = "MF";
		if(rel=='Wife'  || rel=='Aunt'  || rel=='Daughter'  || rel=='Daughter in law'  || rel=='Grand Daughter'  || rel=='Grand Mother'  || rel=='Mother in law'  || rel=='Mother'   || rel=='Niece'  || rel=='Sister in law'  || rel=='Sister'){
			gender = "F";
				
		} else if (rel=='Brother in law'  || rel=='Brother'  || rel=='Father in law'  || rel=='Father'  || rel=='Grand Father'  || rel=='Grand Son'  || rel=='Husband'  || rel=='Nephew'   || rel=='Son in law'  || rel=='Son'  || rel=='Uncle'){
			gender = "M";
		}
		return gender;
	}
	
	function getRelType(rel){
		var type = "A";
		if(rel=='Son'  || rel=='Daughter' ){
			type = "C";				
		} 
		
		return type;
	}

	//Nominee
	$scope.calculateAge=function(dob)
	{
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	
	
	$scope.checkMedicalQuition=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			angular.forEach($scope.proposalInput.insured[val].pedList,function(k,l){
				if(k.exists=='Y')
				{
					$scope.errorMessageMedical="You can't buy policy online"+$scope.proposalInput.insured[val].firstName+" has medical condition";
				}
			})
		})
	}
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "1":
			$scope.calulateBmi();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			break;
		case "2":
			setRelationship();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=2;
			$scope.tabStatus.secondComplete = 'done';
			break;
		case "3":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =3;
			$scope.tabStatus.thirdComplete = 'done';
			break;
		case "4":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';			
			break;
		case "5":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =5;
			$scope.tabStatus.fifthComplete='done';
			break;
		}
		
		saveApplicationData ();
	}
	
	function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
	}
	

	
	function setpolicyAddress(){
 		
		var addresses = [];
		var address = {}
		address.addressType = "C";
 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
		address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
		address.addressLine3 = $scope.proposalInput.proposer.corrArea;
		address.city = $scope.proposalInput.proposer.corrCity;
		address.cityCode = $scope.proposalInput.proposer.corrCityCode;
		address.state = $scope.proposalInput.proposer.corrState;
		address.stateCode = $scope.proposalInput.proposer.corrStateCode;
		address.pincode = $scope.proposalInput.proposer.corrPincode;
		addresses.push(address);
		$scope.proposalInput.proposer.address = angular.copy(addresses);
		
		var contacts = []
		var contact = {};
		contact.contactType = "mobile";
		contact.contactText = $scope.proposalInput.proposer.mobile;
		contacts.push(contact);
		var contact = {};
		contact.contactType = "email";
		contact.contactText = $scope.proposalInput.proposer.email;
		contacts.push(contact);
		$scope.proposalInput.proposer.contacts = angular.copy(contacts);
		
 	}
	
	$scope.makePayment=function()
	{
		//$scope.proposalInput.proposer.proposerDob=$(filter);
		$scope.proposalInput.proposer.nomineeFirstName = $scope.proposalInput.insured[0].nomineeName;
		$scope.proposalInput.proposer.nomineeLastName = $scope.proposalInput.insured[0].nomineeName;
		// Populate Medical and LifeStyle data
		setpolicyAddress();
		
		var travelProposal = angular.copy($scope.proposalInput);
		travelProposal.proposer.proposerDob = $filter('date')(new Date(travelProposal.proposer.proposerDob),'yyyy-MM-dd');
		travelProposal.proposer.nomineeDob = $filter('date')(new Date(travelProposal.proposer.nomineeDob),'yyyy-MM-dd');
		console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
		var input={url:'',data:{}}
		input.url='/submitProposal/travel/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
		input.data=angular.copy(travelProposal);
		$scope.loading = true;
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
				if(data.hasOwnProperty("error"))
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
					//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
					if(data['error']=="Premium Mismatch")
					{
					 	
					 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]})
					}
				}
				else
				{
					$scope.proposalError={key:"",errorPirnt:[]}

					var url = data.payUrl;
					var paymentInput = {};
					returnUrl = "http://Heartbeatuat.us-east-1.elasticbeanstalk.com/Postpayment/125"
					var method = 'POST';	
					
					paymentInput.CustomerID = data.CustomerID
					paymentInput.TxnAmount= data.TxnAmount;
					paymentInput.AdditionalInfo1= data.AdditionalInfo1;
					paymentInput.AdditionalInfo2= data.AdditionalInfo2;
					paymentInput.AdditionalInfo3= data.AdditionalInfo3;
					paymentInput.hdnPayMode= data.hdnPayMode;
					paymentInput.UserName= data.UserName;
					paymentInput.UserMailId= data.UserMailId;
					paymentInput.ProductCd= data.ProductCd;
					paymentInput.ProducerCd= data.ProducerCd;
					FormSubmitter.submit(url, method, paymentInput);
					
				}
			
				$scope.loading = false;
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
	}
	
	getApplicationData();
	
	$scope.AddMedicalRow = function(){
		var questionDetail = {
			"insuredId" : "",
			"details" : ""
		}
		$scope.proposalInput.medicalDetails.push(questionDetail);
	}
	

	$scope.DeleteMedicalRow = function(i){
		$scope.proposalInput.medicalDetails.splice(i);
	}
	
	$scope.AddMedicalDetails = function(questionId){
		
		var input = {};
		input.questionId = questionId;
		var insureds = [];
		
		angular.forEach($scope.proposalInput.insured,function(val,key){
			var insured = {};
			insured.id = key;
			insured.name = val.firstName + " " + val.lastName;
			insureds.push(insured);
		});
		
		input.insureds = insureds;
	
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/health/royalSundaram/medicalQuestionDetail.html',
		      controller: 'rsaMedicalQuestionDetailCtrl',
		      size: 'md',
		      resolve: {
		    	  'input': function () {
		          return input;
		        }
		      }
		});
		modalInstance.result.then(function (questionDetail) {
	      console.log(questionDetail);
	      if( typeof(questionDetail) != 'undefined' && questionDetail.action == "add")
	      	{
	    	  //Add Question details
	      	  $scope.proposalInput.medicalDetails.push(questionDetail);
	      	  console.log($scope.proposalInput.medicalDetails);
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	$scope.showTC=function(){
		
		var modalInstance=$uibModal.open({
		  animation: $scope.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      controller : 'tcCtrl',
	      templateUrl: 'templates/proposal/health/hdfc/tc.html',
	      size: 'lg'
		});
		modalInstance.result.then(function (agree) {
	      console.log(agree);
	      if( typeof(agree) != 'undefined')
	      	{
	    	  $scope.proposalInput.policy.tcAgree = agree;
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	
	
	$scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
	
	
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
     
     $scope.appointeeAge=function(dob) {
 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
 	}
     
     // Watch function // Watch function
     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {

         	var date=new Date(newValue);
           	if ($scope.proposalInput.policy.productType == "AMT") {
           		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+ 1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
           	} else
           	{
           		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear(),date.getMonth(),date.getDate()+ Number($scope.proposalInput.policy.travelDays)-1),'yyyy-MM-dd');
           	}
         }
     	
     });
     
}])