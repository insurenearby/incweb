/**
 * 
 */
"use-strict"
brokeredgeApp.controller('rgiTravelCtrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','FormSubmitter',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,FormSubmitter){
		
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polArea = {};
	$scope.corArea = {}
	$scope.nomArea={};
	$scope.areasPolicy = []; 
	$scope.areasCorr = []; 
	$scope.areasNom = [];
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}
	

	$scope.pageErrors = {
			insuredError: false,
			medicalError: false,
			insuredErrorText : "",
			medicalErrorText : "",
			policyError: false,
			policyErrorText : ""
	}
	
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
    
    $scope.expiryDateOptions={
			maxDate: new Date(newYearMax+10, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
    // Methods for opening of Calendar Control  
    $scope.opened= {
        	proposerDob : false,
        	nomineeDob : false,
        	appointeeDob : false,
        	policyStartDate : false
    };
	$scope.open1 = function() {
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
	
	$scope.open = function(name) {
        $scope.opened[name] = true;
	};
   
   
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
	
	function init() {
		$scope.input.lob = "Travel";
		$scope.input.productType = $scope.proposalInput.productType;
		initializeMedicalQuestions();
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			
			switch (addressType){
			case "C":
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCityCode = value.cityCode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStateCode = value.stateCode;
				$scope.proposalInput.proposer.corrDistrict = value.district;
				$scope.proposalInput.proposer.corrDistrictId = value.districtId;
				$scope.proposalInput.proposer.corrPincode = value.pincode;
				$scope.corArea.areaId = value.areaCode;
				$scope.corArea.areaName = value.addressLine3;
				$scope.corArea.stateId = value.stateCode;
				$scope.corArea.stateName = value.state;
				$scope.corArea.cityId = value.cityCode;
				$scope.corArea.cityName = value.city;
				$scope.corArea.districtId = value.districtId;
				$scope.corArea.districtName = value.district;
				
				break;
			case "P":
				$scope.proposalInput.proposer.policyAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.policyAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.policyCity = value.city;
				$scope.proposalInput.proposer.policyCityCode = value.cityCode;
				$scope.proposalInput.proposer.policyState = value.state;
				$scope.proposalInput.proposer.policyStateCode = value.stateCode;
				$scope.proposalInput.proposer.policyDistrict = value.district;
				$scope.proposalInput.proposer.policyDistrictId = value.districtId;
				$scope.proposalInput.proposer.policyPincode = value.pincode;
				$scope.polArea.areaId = value.areaCode;
				$scope.polArea.areaName = value.addressLine3;
				$scope.polArea.stateId = value.stateCode;
				$scope.polArea.stateName = value.state;
				$scope.polArea.cityId = value.cityCode;
				$scope.polArea.cityName = value.city;
				$scope.polArea.districtId = value.districtId;
				$scope.polArea.districtName = value.district;
				break;
			}
		});
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		formatDates ();
		getMaster($scope.proposalInput.productId, "Occupation");
		getMaster($scope.proposalInput.productId, "Relationship");
		getMaster($scope.proposalInput.productId, "MaritalStatus");
		getMaster($scope.proposalInput.productId, "Nationality");
		getMaster($scope.proposalInput.productId, "NomineeRel");
		getQuestions($scope.proposalInput.productId, "PedList")	;	
		//getMaster($scope.proposalInput.productId, "Disease")	;

		getAreasFromPincode('P', $scope.proposalInput.proposer.policyPincode);
		getAreasFromPincode('C', $scope.proposalInput.proposer.corrPincode);
		$scope.proposalInput.policy.tcAgree = "N";
		
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalQuestions();
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;
		
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates ();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates (){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob);	
		}
		angular.forEach($scope.proposalInput.insured,function(value,key){
			if (typeof(value.nomineeDob) != "undefined" && value.nomineeDob != ""){
				value.nomineeDob = new Date(value.nomineeDob);
			}
			if (typeof(value.passportExpiryDate) != "undefined" && value.passportExpiryDate != ""){
				value.passportExpiryDate = new Date(value.passportExpiryDate);
			}
		})
		
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeDob = new Date($scope.proposalInput.proposer.appointeeDob);
		}
		
		
		
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
		}
		
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			angular.forEach(insured.pedList,function(item,key){
				
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						$scope.medicalQuestions[i].exists = item.exists;
						angular.forEach(item.pedText,function(illness,key){
							var detail = angular.copy(illness);
							detail.insuredId = insuredId;
							detail.insuredName = insuredName;
							detail.questionId = code;
							$scope.medicalQuestions[i].details.push(detail);
						})
					}
				}
				
			})
		})
		//console.log($scope.medicalQuestions);
	}

	
	function ProcessMedicalQuestions(){
		
		// initialize the medical detail Array
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			angular.forEach(insured.pedList,function(item,key2){
				item.pedText = [];
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.medicalQuestions,function(question,key){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(medDetail,key){
					 var questionId = medDetail.questionId;
					 var insuredId = medDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key1){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.pedList,function(item,key2){
								 if (item.pedCode == questionId){
									 item.exists = "Y";
									 var pedDetails = {}
						    	  	 pedDetails.monthOfDiagnosis = medDetail.monthOfDiagnosis;
									 pedDetails.yearOfDiagnosis = medDetail.yearOfDiagnosis;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 pedDetails.medicationDetail = medDetail.medicationDetail;
									 pedDetails.treatmentOutCome = medDetail.treatmentOutCome;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 item.pedText.push(pedDetails);
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	
	$scope.getCity = function(state,name) {
		
		if(name=='nomadd')
		{
			$scope.nomCities = cities
			$scope.proposalInput.proposer.nomineeStateCode=state.id;
			$scope.proposalInput.proposer.nomineeState=state.value;  
		}else if(name=="policy") {
			$scope.proposalInput.proposer.corrStateCode = state.id;
			$scope.proposalInput.proposer.corrState = state.value;  
			
		}
		
		
		var cities = [];
		var input={
				url:'',
				data:{}
		}
		if (typeof(state) != "undefined"){
			input.url='/master/city/'+$scope.proposalInput.insurerId+'/'+state.id;
			var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
			tmp.$promise.then(function(data){
				cities=data['result'];
				$scope.polCities = cities
			})
		}

	}
	
	$scope.changeCity = function(city,name) {
		if(name=="policy") {
			$scope.proposalInput.proposer.corrCityCode = city.id;
			$scope.proposalInput.proposer.corrCity = city.value;
			
		}else if(name=="nomadd") {
			$scope.proposalInput.proposer.nomineeCityCode = city.id;
			$scope.proposalInput.proposer.nomineeCity =  city.value;
		} 
	}
	
	function getMaster(productId, masterId){
		$scope.insurerMaster[masterId] = [];
		var tmp=brokeredgefactory.getMaster(productId, masterId);
		tmp.$promise.then(function(data){
			//console.log(data);
			$scope.insurerMaster[masterId]= data['result'];
			if (masterId == "Relationship"){
				addGendertoRel();
			}
			//console.log($scope.insurerMaster);
		})
	}	

	
	function getQuestions(productId, type){
		var questions = [];
		 var input = {
	    			url: '/master/getQuestions/' + productId + '/' + type,
	    			data : {}
	    	};
	        
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
		var tmp=res.process(input);
		tmp.$promise.then(function(data){
			//console.log(data);
			if (data.hasOwnProperty("result")){
				if (type == "PedList"){
					$scope.medicalQuestions = data.result;
					initializeMedicalQuestions();
				}
				
			}
			//console.log($scope.insurerMaster);
		})
		
	}
	
	$scope.getMasterValue = function(masterName, masterId){
		// 
		var masterValue = masterId;
		var values = $scope.insurerMaster[masterName];
		
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value; 
					break;
				}
			}
		}
		return masterValue; 
	}
	
	$scope.getQuestionText = function(type, id){
		// 
		var questionText = "";
		var values = [];
		if (type == "PedList"){
			values = $scope.medicalQuestions;
		}

				
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == id){
					questionText = values[i].value; 
					break;
				}
			}
		}
		return questionText; 
	}
	
	function addGendertoRel (){
		var records = [];
		angular.forEach($scope.insurerMaster.Relationship,function(val,key){
			var record = val;
			record.gender = getRelGender(val.value);
			records.push(record);
		});
		$scope.insurerMaster.Relationship = records;
	}
	
	$scope.checkPolicyCond = function () {
		$scope.pageErrors.policyErrorText = "";
		$scope.pageErrors.policyError = false;
		
		if ($scope.proposalInput.policy.validPassport=='N') {
			$scope.pageErrors.policyErrorText = "Policy Can only be issued to Indian Passport Holders";
			$scope.pageErrors.policyError = true;
		}
		
		if ($scope.proposalInput.policy.travelDangerousAreas=='Y') {
			$scope.pageErrors.policyErrorText = "Policy cannot be issued to people travelling in Dangerous Areas";
			$scope.pageErrors.policyError = true;
		}
		
	}
	
	$scope.checkRelationShip=function()
     {
    	
		$scope.pageErrors.insuredErrorText = "";
		$scope.pageErrors.insuredError = false;
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
    	var selfCount = 0;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "345"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if (val.type == "C"){ // Proposer cannot be a child.
    				$scope.pageErrors.insuredError= true;
    				$scope.pageErrors.insuredErrorText = "Child cannot be a proposer";
    			}
    			selfCount++;
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
    	if (selfCount>1) {
			$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
    	}    	
    	
     }
	
	function setRelationship() {
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "345"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
		
	}
	
	$scope.AddMedicalRow = function(code){
		
		var detail = {};
		detail.insuredId = "";
		detail.details = "";
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.push(detail);
			}
		}
		
	}
	
	$scope.DeleteMedicalRow = function(code, i){
		
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.splice(i);
			}
		}
	}
	
	
	$scope.checkMedicalStatus = function(){
		
		$scope.pageErrors.medicalError = false;
		$scope.pageErrors.medicalErrorText = "";
		
		for (var i =0; i<$scope.medicalQuestions.length; i++){
			if ($scope.medicalQuestions[i].exists == "Y"){
				$scope.pageErrors.medicalError = true;
				$scope.pageErrors.medicalErrorText = "Cannot buy this policy online";
				break;
			}
			
		};
		
	}
		
	$scope.setGender=function(val)
	{
		if($scope.proposalInput.proposer.title=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	
	$scope.calulateBmi=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			
			if(key.type=='A')
				{
				var heighttm=$scope.proposalInput.insured[val].height*0.01;
				var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
				$scope.proposalInput.insured[val].bmi=parseInt(bmi)
				}
			
		})
	}

	
	function getRelGender(rel){
		var gender = "MF";
		if(rel=='Wife'  || rel=='Aunt'  || rel=='Daughter'  || rel=='Daughter in law'  || rel=='Grand Daughter'  || rel=='Grand Mother'  || rel=='Mother in law'  || rel=='Mother'   || rel=='Niece'  || rel=='Sister in law'  || rel=='Sister'){
			gender = "F";
				
		} else if (rel=='Brother in law'  || rel=='Brother'  || rel=='Father in law'  || rel=='Father'  || rel=='Grand Father'  || rel=='Grand Son'  || rel=='Husband'  || rel=='Nephew'   || rel=='Son in law'  || rel=='Son'  || rel=='Uncle'){
			gender = "M";
		}
		return gender;
	}

	//Nominee
	$scope.calculateAge=function(dob)
	{
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	
	
	$scope.checkMedicalQuition=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			angular.forEach($scope.proposalInput.insured[val].pedList,function(k,l){
				if(k.exists=='Y')
				{
					$scope.errorMessageMedical="You can't buy policy online"+$scope.proposalInput.insured[val].firstName+" has medical condition";
				}
			})
		})
	}
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "1":
			
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			break;
		case "2":
			$scope.calulateBmi();
			setRelationship();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=2;
			$scope.tabStatus.secondComplete = 'done';
			break;
		case "3":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =3;
			$scope.tabStatus.thirdComplete = 'done';
			break;
		case "4":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';			
			break;
		case "5":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =5;
			$scope.tabStatus.fifthComplete='done';
			break;
		}
		
		saveApplicationData ();
	}
	
	function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
		}
	
	 function getAreasFromPincode (type, pincode){
			
			var re = new RegExp("^[0-9]{6}$");
			if (re.test(pincode)){
				
				var input={url:'',data:{}}
		    	
				input.url="/master/getDetailsForPincode/" + $scope.proposalInput.insurerId + "/" + pincode;
				
		    	var res = $resource('./DataRequest', [], {
			          save: {
			             method: 'POST'}
			    });
				
				var tmp = res.save(input);
				tmp.$promise.then(function(data){
					if (data.hasOwnProperty("result")){
						 if (type == "P"){
							 $scope.areasPolicy = data.result;
						 } else if (type == "C")
						 {
							 $scope.areasCorr = data.result; 
						 } else if(type == "N"){
							$scope.areasNom = data.result; 
						 }
					}
					
					
				})		
			}
		}
	 
		$scope.setCityState = function(type, area){
			
			var areas = []
			if (typeof(area) != "undefined") {
				 if (type == "P"){
					 $scope.proposalInput.proposer.policyCity = area.cityName;
					 $scope.proposalInput.proposer.policyCityCode = area.cityId;
					 $scope.proposalInput.proposer.policyState = area.stateName;
					 $scope.proposalInput.proposer.policyStateCode = area.stateId;
					 $scope.proposalInput.proposer.policyDistrict = area.districtName;
					 $scope.proposalInput.proposer.policyDistrictId = area.districtId;
					 $scope.proposalInput.proposer.policyArea = area.areaName;
					 $scope.proposalInput.proposer.policyareaCode = area.areaId;
					 
				 } else if (type == "C")
				 {
					 $scope.proposalInput.proposer.corrCity = area.cityName;
					 $scope.proposalInput.proposer.corrCityCode = area.cityId;
					 $scope.proposalInput.proposer.corrState = area.stateName;
					 $scope.proposalInput.proposer.corrStateCode = area.stateId;
					 $scope.proposalInput.proposer.corrDistrict = area.districtName;
					 $scope.proposalInput.proposer.corrDistrictId = area.districtId;
					 $scope.proposalInput.proposer.corrArea = area.areaName;
					 $scope.proposalInput.proposer.corrareaCode = area.areaId;
				 } 				
			}

		}
	
			
		$scope.samePolicyCorrAddress=function( flag)
		{
			if (flag == "Y"){
				copyPolicyAddress();
			} else
			{
				$scope.proposalInput.proposer.corrAddressLine1= "";
				$scope.proposalInput.proposer.corrAddressLine2= "";
				$scope.proposalInput.proposer.corrAddressLine3= "";
				$scope.proposalInput.proposer.corrLandmark= "";
				$scope.proposalInput.proposer.corrStateCode= "";
				$scope.proposalInput.proposer.corrState = "";
				$scope.proposalInput.proposer.corrCityCode = "";
				$scope.proposalInput.proposer.corrCity = "";
				$scope.proposalInput.proposer.corrDistrictId = "";
				$scope.proposalInput.proposer.corrDistrict = "";
				$scope.proposalInput.proposer.corrareaCode ="";
				$scope.proposalInput.proposer.corrArea ="";
				$scope.proposalInput.proposer.corrPincode = "";
			}
			
			 
		}
		
		
		function copyPolicyAddress(){
			$scope.proposalInput.proposer.corrAddressLine1=$scope.proposalInput.proposer.policyAddressLine1;
			$scope.proposalInput.proposer.corrAddressLine2=$scope.proposalInput.proposer.policyAddressLine2;
			$scope.proposalInput.proposer.corrAddressLine3=$scope.proposalInput.proposer.policyArea;
			$scope.proposalInput.proposer.corrLandmark=$scope.proposalInput.proposer.policyLandmark;
			$scope.proposalInput.proposer.corrStateCode=$scope.proposalInput.proposer.policyStateCode;
			$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
			$scope.proposalInput.proposer.corrCityCode =$scope.proposalInput.proposer.policyCityCode;
			$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
			$scope.proposalInput.proposer.corrDistrictId =$scope.proposalInput.proposer.policyDistrictId;
			$scope.proposalInput.proposer.corrDistrict =$scope.proposalInput.proposer.policyDistrict;
			$scope.proposalInput.proposer.corrareaCode =$scope.proposalInput.proposer.policyareaCode;
			$scope.proposalInput.proposer.corrArea =$scope.proposalInput.proposer.policyArea;
			$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
		}
	
		function setpolicyAddress(){
	 		
			if ($scope.proposalInput.proposer.policyRegAddressSame == 'Y'){
				copyPolicyAddress();
			}
			
			var addresses = [];
			var address = {}
			address.addressType = "C";
	 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
			address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
			address.addressLine3 = $scope.proposalInput.proposer.corrArea
			address.areaCode = $scope.proposalInput.proposer.corrareaCode;
			address.district = $scope.proposalInput.proposer.corrDistrict;
			address.districtId = $scope.proposalInput.proposer.corrDistrictId;
			address.city = $scope.proposalInput.proposer.corrCity;
			address.cityCode = $scope.proposalInput.proposer.corrCityCode;
			address.state = $scope.proposalInput.proposer.corrState;
			address.stateCode = $scope.proposalInput.proposer.corrStateCode;
			address.pincode = $scope.proposalInput.proposer.corrPincode;
			address.country = "India";
			addresses.push(address);
			
			var address = {}
			address.addressType = "P";
	 		address.addressLine1 = $scope.proposalInput.proposer.policyAddressLine1;
			address.addressLine2 = $scope.proposalInput.proposer.policyAddressLine2;
			address.addressLine3 = $scope.proposalInput.proposer.policyArea;
			address.areaCode = $scope.proposalInput.proposer.policyareaCode;
			address.district = $scope.proposalInput.proposer.policyDistrict;
			address.districtId = $scope.proposalInput.proposer.policyDistrictId;
			address.city = $scope.proposalInput.proposer.policyCity;
			address.cityCode = $scope.proposalInput.proposer.policyCityCode;
			address.state = $scope.proposalInput.proposer.policyState;
			address.stateCode = $scope.proposalInput.proposer.policyStateCode;
			address.pincode = $scope.proposalInput.proposer.policyPincode;
			address.country = "India";
			addresses.push(address);
			
			$scope.proposalInput.proposer.address = angular.copy(addresses);
			
			var contacts = []
			var contact = {};
			contact.contactType = "mobile";
			contact.contactText = $scope.proposalInput.proposer.mobile;
			contacts.push(contact);
			var contact = {};
			contact.contactType = "email";
			contact.contactText = $scope.proposalInput.proposer.email;
			contacts.push(contact);
			$scope.proposalInput.proposer.contacts = angular.copy(contacts);
			
	 	}
	
	$scope.makePayment=function()
	{
		if ($scope.proposalInput.policy.floater == 'N'){
			$scope.proposalInput.policy.floaterSi = $scope.proposalInput.policy.sumInsured;
		}
		$scope.proposalError={key:"",errorPirnt:[]}
		$scope.proposalInput.proposer.nomineeName = $scope.proposalInput.proposer.nomineeFirstName + " " + $scope.proposalInput.proposer.nomineeLastName;
		
		var travelProposal = angular.copy($scope.proposalInput);
		travelProposal.policy.policyStartdate = $filter('date')(new Date(travelProposal.policy.policyStartdate),'yyyy-MM-dd');
		travelProposal.proposer.proposerDob = $filter('date')(new Date(travelProposal.proposer.proposerDob),'yyyy-MM-dd');
		angular.forEach(travelProposal.insured,function(val,key){
			val.nomineeDob = $filter('date')(new Date(val.nomineeDob),'yyyy-MM-dd');
		});
		console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
		
		var input={url:'',data:{}}
		input.url='/submitProposal/travel/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
		input.data=angular.copy(travelProposal);
		
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		$scope.loading = true;
		tmp.$promise.then(function(data){
			$scope.loading = false;
				if(data.error)
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
					if(data['error']=="Premium Mismatch")
					{	
						$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]})
					}
				}
				else if (data.hasOwnProperty("payUrl")) 
				{
					//$scope.returnUrl="http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId;
					console.log(data);
					// Parse the response
					var paymentInput = {};
					var method = 'POST';	
					paymentInput.ProposalNo = data.ProposalNo;
					paymentInput.UserID = data.UserID;
					paymentInput.PaymentType = "1";
					paymentInput.ProposalAmount = data.ProposalAmount;
					paymentInput.Responseurl = data.Responseurl;
					var url = data.payUrl;
					
					FormSubmitter.submit(url, method, paymentInput);
				} else
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
				}
			
		
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
	}
	
	getApplicationData();
	
	
	
	$scope.showTC=function(){
		
		var modalInstance=$uibModal.open({
		  animation: $scope.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      controller : 'tcCtrl',
	      templateUrl: 'templates/proposal/health/apollo/tc.html',
	      size: 'lg'
		});
		modalInstance.result.then(function (agree) {
	      console.log(agree);
	      if( typeof(agree) != 'undefined')
	      	{
	    	  $scope.proposalInput.policy.tcAgree = agree;
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	
	
	$scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
	
	
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
     
     $scope.appointeeAge=function(dob) {
 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
 	}
     
     // Watch function // Watch function
     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {

         	var date=new Date(newValue);
           	if ($scope.proposalInput.policy.productType == "AMT") {
           		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+ 1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
           	} else
           	{
           		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear(),date.getMonth(),date.getDate()+ Number($scope.proposalInput.policy.travelDays)-1),'yyyy-MM-dd');
           	}
           	console.log($scope.proposalInput.policy.policyEnddate);
         }
     	
     });
     
     $scope.$watch('proposalInput.proposer.corrPincode', function(newValue, oldValue) {
         
     	if ((typeof newValue != "undefined")&& (newValue != "")) {
     		getAreasFromPincode("C", newValue);
     	}
     	
     });
     
     $scope.$watch('proposalInput.proposer.policyPincode', function(newValue, oldValue) {
         
     	if ((typeof newValue != "undefined")&& (newValue != "")) {
     		getAreasFromPincode("P", newValue);
     	}
     	
     });
     
}])