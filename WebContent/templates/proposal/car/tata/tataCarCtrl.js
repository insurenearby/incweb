/**
*
*/
"use-strict"
brokeredgeApp.controller('tataCarCntrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','$interval','FormSubmitter',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,$interval,FormSubmitter){
	$scope.proposalList=['policy','Proposer','Vehicle','Previous policy','Contact Info','Cover','T & C']
	$scope.proposalInput={};
	$scope.drivingExps=[{'year':"1"},{'year':"2"},{'year':"3"},{'year':"4"},{'year':"5"},{'year':"6"},{'year':"7"},{'year':"8"},{'year':"9"},{'year':"10"},{'year':"11"},{'year':"12"},{'year':"13"},{'year':"14"},{'year':"15"},{'year':"16"},{'year':"17"},{'year':"18"},{'year':"19"},{'year':"20"}];
	$scope.showError='';
	$scope.insurerMaster = {};
	$scope.min=1000;
	$scope.max=100000;
	$scope.policyPremiumError="";
	$scope.premiumMilageError="";
	$scope.bank = {};
	$scope.bank.selected = [];
	$scope.polCity = {};
	$scope.corCity = {}
	$scope.nomCity={};
	$scope.polCities = [];
	$scope.corCities = []
	$scope.policyRegAddressSame = "N";
	$scope.selectCoverFilter = {isSelected : 'Y'}
	$scope.active=0;
	$scope.tabStatus={
			firstDisabled : false,
			firstComplete : 'indone',
			secondDisabled : true,
			secondComplete : 'indone',
			thirdDisabled : true,
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthDisabled:true,
			sixthComplete:'indone',
	}
	
	$scope.Months  = [
		{"id" : "1", "value" : "January"},
		{"id" : "2", "value" :  "February"},
		{"id" : "3", "value" :  "March"},
		{"id" : "4", "value" :  "April"},
		{"id" : "5", "value" :  "May"},
		{"id" : "6", "value" :  "June"},
		{"id" : "7", "value" :  "July"},
		{"id" : "8", "value" :  "August"},
		{"id" : "9", "value" :  "September"},
		{"id" : "10", "value" :  "October"},
		{"id" : "11", "value" :  "November"},
		{"id" : "12", "value" :  "December"}
	 ]
	
	
	$scope.pageErrors = {
			proposerError: false,
			vehicleError: false,
			vehicleErrorText : "",
			proposerErrorText : ""
	}
	
	// dateoptions variables
    $scope.backPreviousPage=function()
	{
		window.history.back();
	}
	
	$scope.filteredFinancier = [];
	
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    var iterations = 0;
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
       
	$scope.opened = [];
	$scope.open1 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
   $scope.open = function($event,$index) {
            $scope.opened[$index] = true;
   };
	$scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    
    // Entry Function for the Controller
	function init() {
				
		getApplicationData ();
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				$scope.proposalInput.premiumBreakup = $scope.applicationData.premiumBreakup;
				$scope.insurerMaster.Financier = []; // Added To Prevent Undefined Error
				getMaster("Financier")
				getMaster("NomineeRel");
				getMaster("Insurer");
				getMaster("Title");
				getMaster("Gender");
				getMaster("DrivingExp")
				getMaster("Occupation")
				getMaster("MaritalStatus")
				getMaster("LoanType")
				getMaster("SourceOfFund")
				formatApplication();
				if ($scope.proposalInput.policy.typeOfBusiness=="New Business"){
					$scope.proposalInput.vehicle.isOwnershipChanged = "N";
				}
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
		
		
	}
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
		
		
		
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		
		$scope.applicationData.proposalData = $scope.proposalInput;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;	
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				
				console.log($scope.proposalInput);
				formatApplication();
				
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	
	
	function formatStrToDate (){
		
		$scope.proposalInput.policy.policyStartdate = brokeredgefactory.strToDate($scope.proposalInput.policy.policyStartdate);
		$scope.proposalInput.prevpolicy.prePolicystartdate = brokeredgefactory.strToDate($scope.proposalInput.prevpolicy.prePolicystartdate);
		$scope.proposalInput.proposer.proposerDob = brokeredgefactory.strToDate($scope.proposalInput.proposer.proposerDob);	
		$scope.proposalInput.proposer.nomineeDob = brokeredgefactory.strToDate($scope.proposalInput.proposer.nomineeDob);
		$scope.proposalInput.proposer.appointeeDOB = brokeredgefactory.strToDate($scope.proposalInput.proposer.appointeeDOB);
		$scope.proposalInput.vehicle.youngestDriverDob = brokeredgefactory.strToDate($scope.proposalInput.vehicle.youngestDriverDob);
	}
	

	function formatApplication(){
		formatStrToDate();
		setSelectBoxes();
	}
	
		
	function setSelectBoxes(){
		
		
		$scope.polCity.cityId = $scope.proposalInput.proposer.policyCityCode;
		$scope.polCity.cityName = $scope.proposalInput.proposer.policyCity;
		$scope.polCity.stateId = $scope.proposalInput.proposer.policyStateCode;
		$scope.polCity.state = $scope.proposalInput.proposer.policyState;
		getCityFromPincode('P', $scope.proposalInput.proposer.policyPincode);
		
		$scope.corCity.cityId = $scope.proposalInput.proposer.corrCityCode;
		$scope.corCity.cityName = $scope.proposalInput.proposer.corrCity;
		$scope.corCity.stateId = $scope.proposalInput.proposer.corrStateCode;
		$scope.corCity.state = $scope.proposalInput.proposer.corrState;
		getCityFromPincode('C', $scope.proposalInput.proposer.corrPincode);
		$scope.proposalInput.policy.tcAgree = "N";
	}
	
	
	// Get Masters for the Insurer
	// Get Masters for the Insurer
	function getMaster (masterId){
		
		var input={url:'',data:{}}
    	input.url='/master/codes/'+ $scope.proposalInput.productId + "/" + masterId;
		
    	var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		var tmp = res.save(input);
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result"))
			$scope.insurerMaster[masterId] = data.result;
			// Set Financier if Available
			if (masterId == "Financier" && $scope.proposalInput.vehicle.bankId) {
				
				var item = {}
				item.id = $scope.proposalInput.vehicle.bankCode + "-" + $scope.proposalInput.vehicle.financierType;
				item.value = $scope.proposalInput.vehicle.bankName;
				$scope.bank.selected.push(item);
			}
			
		})		
	}
	
	$scope.resetNCB = function(){
		
		$scope.proposalInput.prevpolicy.currNcb = 0;
		var i = 0;
		for (i = 0; i<$scope.proposalInput.covers.length; i++){
			if ($scope.proposalInput.covers[i].coverId == "NCBPROTECT"){
				$scope.proposalInput.covers[i].applicable = "N";
				$scope.proposalInput.covers[i].isSelected = "N";
			}
		}
	}
	
	$scope.kitType=function()
	{
		if($scope.proposalInput.vehicle.lpgcngKit=='N')
		{
			$scope.proposalInput.vehicle.biKitType='';
		}
		
	}
	
	$scope.setCityState = function(type, city){
		
		var areas = []
		if (typeof(city) != 'undefined') {
			 if (type == "P"){
				 $scope.proposalInput.proposer.policyCity = city.cityName;
				 $scope.proposalInput.proposer.policyCityCode = city.cityId;
				 $scope.proposalInput.proposer.policyState = city.stateName;
				 $scope.proposalInput.proposer.policyStatecode = city.stateId;
				 
			 } else
			 {
				 $scope.proposalInput.proposer.corrCity = city.cityName;
				 $scope.proposalInput.proposer.corrCityCode = city.cityId;
				 $scope.proposalInput.proposer.corrState = city.stateName;
				 $scope.proposalInput.proposer.corrStatecode = city.stateId;
			 }
		}
	}		
	
	
	function getCityFromPincode (type, pincode){
		
		var re = new RegExp("^[0-9]{6}$");
		if (re.test(pincode)){
			
			var input={url:'',data:{}}
	    	
			input.url="/master/getDetailsForPincode/" + $scope.proposalInput.insurerId + "/" + pincode;
			
	    	var res = $resource('./DataRequest', [], {
		          save: {
		             method: 'POST'}
		    });
			
			var tmp = res.save(input);
			tmp.$promise.then(function(data){
				if (data.hasOwnProperty("result")){
					
					 if (type == "P"){
						 $scope.polCities = data.result;
						 if ($scope.polCities.length == 1){
							 var city = $scope.polCities[0];
							 $scope.setCityState("P", city);
						 }
					 } else
					 {
						 $scope.corCities = data.result;
						 $scope.corCities = data.result;
						 if ($scope.corCities.length == 1){
							 var city = $scope.corCities[0];
							 $scope.setCityState("C", city);
						 } 
					 }
				}
				
				
			})		
		}
	}
	

	
	$scope.ChangClaimAmount=function(previousClaim){
		if(previousClaim=='N'){
		$scope.proposalInput.prevpolicy.claimAmount=0
		}else{
			$scope.proposalInput.prevpolicy.claimAmount="";
		}
	}
	
	$scope.samePolicyCorrAddress=function(flag)
	{
		if (flag == "Y") {
			$scope.proposalInput.proposer.corrAddress1=$scope.proposalInput.proposer.policyAddress1;
			$scope.proposalInput.proposer.corrAddress2=$scope.proposalInput.proposer.policyAddress2;
			$scope.proposalInput.proposer.corrAddress3=$scope.proposalInput.proposer.policyAddress3;
			$scope.proposalInput.proposer.corrStatecode=$scope.proposalInput.proposer.policyStatecode;
			$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
			$scope.proposalInput.proposer.corrCityCode =$scope.proposalInput.proposer.policyCityCode;
			$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
			$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
		} else
		{
			$scope.proposalInput.proposer.corrAddress1 = "";
			$scope.proposalInput.proposer.corrAddress2 = "";
			$scope.proposalInput.proposer.corrAddress3 = "";
			$scope.proposalInput.proposer.corrStatecode= "";
			$scope.proposalInput.proposer.corrState = "";
			$scope.proposalInput.proposer.corrCityCode = "";
			$scope.proposalInput.proposer.corrCity = "";
			$scope.proposalInput.proposer.corrPincode = "";
		}
		
	 	 
	}
	
	
	
	$scope.premiumAdd=function(coverId,val, premiumVal)
	{
		console.log(coverId);
		var cover = {};
		cover.name = coverId;
		if(val=='Y')
		{
			$scope.proposalInput.policy.premiumPayable= Number($scope.proposalInput.policy.premiumPayable)+ Number(Math.round(premiumVal)); 
		}
		else
		{
			$scope.proposalInput.policy.premiumPayable= Number($scope.proposalInput.policy.premiumPayable) -  Number(Math.round(premiumVal));
		}
	}
	
	$scope.getcityStateFromPin = function(pincode){
		
		
		var input={url:'',data:{}}
    	input.url='/master/getStatePinStd/'+pincode;
		
    	var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		var tmp = res.save(input);
		tmp.$promise.then(function(data){
			
			
		})
	}
	
	$scope.getCity = function(state,name) {
		var cities = [];
		var input={
				url:'',
				data:{}
		}
		
		if (typeof(state.id) == "undefined"){
			return cities;
		}
		input.url='/master/city/' + $scope.proposalInput.insurerId +'/'+state.id;
		var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			cities=data['result'];
			if(name=='nominee')
			{
			$scope.cityNominee='';
			$scope.healthProposalInput.proposer.nomineeState=state.id;
			$scope.getCity(state.id,name);
		}else if(name=="Registration") {
				$scope.proposalInput.proposer.policyStatecode = state.id;
				$scope.proposalInput.proposer.policyState = state.value;
				$scope.polCities = cities
		}else if(name=="Mailing") {
				$scope.corCities = cities
				$scope.proposalInput.proposer.corrStatecode = state.id;
				$scope.proposalInput.proposer.corrState = state.value;
		} 
		})
	}
	
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	
	//next section
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "1":
			
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			$scope.tabStatus.secondDisabled = false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "2":
			$scope.active =2;
			$scope.tabStatus.secondComplete = 'done';
			$scope.tabStatus.thirdDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			console.log($scope.proposalInput)
			break;
		case "3":
				$scope.active =3;
				$scope.tabStatus.thirdComplete='done';
				$scope.tabStatus.fourthDisabled= false;
				$scope.proposalError={key:"",errorPirnt:[]}
			
			break;
		
		case "4":
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';
			$scope.tabStatus.fifthDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "5":
			$scope.showError='true';
	   		$scope.active =5;
			$scope.tabStatus.fifthOpen = false;
			$scope.tabStatus.fifthComplete = 'done';
			$scope.tabStatus.sixthOpen = true;
			$scope.tabStatus.sixthDisabled = false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "6":
			
			$scope.active=6;
			$scope.tabStatus.sixthComplete='done';
			$scope.tabStatus.fifthDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		}
		
		saveApplicationData ();
	}
	
	
	$scope.validateOldPolicy=function(value,form)
	{
		console.log(form)
		var myDate = new Date(value);
		var today=new Date();
		//add a day to the date
		myDate.setDate(value.getDate() + 1);
		if( myDate>=new Date($scope.proposalInputInput.policy.policyStartDate)|| myDate<today)
		{
			//form.OldPolicyEndDate.$setValidity(false);
			 form.OldPolicyEndDate.$setValidity("uniq", false);
			//existPolicy.OldPolicyEndDate.$setValidity("size", true);
	    	$scope.show_error=true;
	    	$scope.error_text='The Start date of New Policy must be the day after this date.';	
	    	console.log($scope.error_text)
		}
		else
		{
			form.OldPolicyEndDate.$setValidity("uniq", true);
			
		}
	}
	
	
	$scope.previousPolicyLastDate=function(prevStart)
	{
		console.log(prevStart);
		var date=new Date(prevStart);
		$scope.proposalInput.prevpolicy.prePolicyenddate=$filter('date')(new Date(date.getFullYear()+1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
	}
	
	
	
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
	//check validation policy


     function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
	}
     
 	function setpolicyAddress(){
 		
 		if ($scope.proposalInput.proposer.policyRegAddressSame =='Y'){
 			$scope.proposalInput.proposer.corrAddress1 = $scope.proposalInput.proposer.policyAddress1;
 			$scope.proposalInput.proposer.corrAddress2 = $scope.proposalInput.proposer.policyAddress2;
 			$scope.proposalInput.proposer.corrAddress3 = $scope.proposalInput.proposer.policyAddress3;
 			$scope.proposalInput.proposer.corrCity = $scope.proposalInput.proposer.policyCity;
 			$scope.proposalInput.proposer.corrCityCode = $scope.proposalInput.proposer.policyCityCode;
 			$scope.proposalInput.proposer.corrState = $scope.proposalInput.proposer.policyState;
 			$scope.proposalInput.proposer.corrStatecode = $scope.proposalInput.proposer.policyStatecode;
 			$scope.proposalInput.proposer.corrDistrict = $scope.proposalInput.proposer.policyDistrict;
 			$scope.proposalInput.proposer.corrDistrictId = $scope.proposalInput.proposer.policyDistrictId;
 			$scope.proposalInput.proposer.corrArea = $scope.proposalInput.proposer.policyArea;
 			$scope.proposalInput.proposer.corrAreaId = $scope.proposalInput.proposer.policyAreaId;
 			$scope.proposalInput.proposer.corrPincode = $scope.proposalInput.proposer.policyPincode;
 		}
 	}
 	
 	function getAccessoryValue() {
 		
 		$scope.proposalInput.vehicle.electricalAcc = 0;		
 		angular.forEach($scope.proposalInput.vehicle.electricalAccList,function(acc,key){
 			$scope.proposalInput.vehicle.electricalAcc = Number($scope.proposalInput.vehicle.electricalAcc) + Number(acc.value);
 		});
 		
 		$scope.proposalInput.vehicle.nonElectricalAcc = 0;		
 		angular.forEach($scope.proposalInput.vehicle.nonElectricalAccList,function(acc,key){
 			$scope.proposalInput.vehicle.nonElectricalAcc = Number($scope.proposalInput.vehicle.nonElectricalAcc) + Number(acc.value);
 		});
 	}
 	
 	
     // saves the Proposal and transfers to the payment gateway
    $scope.makePayment=function() {
 		//$scope.healthProposalInput.proposer.proposerDob=$(filter)
    	getAccessoryValue();
 		var input={url:'',data:{}}
 		input.url='/submitProposal/motor/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
 		
 		setpolicyAddress();
 		if ($scope.proposalInput.vehicle.isOwnershipChanged == "Y"){
 			$scope.proposalInput.prevpolicy.previousNcb = 0;
 			$scope.proposalInput.prevpolicy.currNcb = 0;
 		}
 		
 		$scope.proposalInput.vehicle.youngestDriverAge = brokeredgefactory.calculateAge($scope.proposalInput.vehicle.youngestDriverDob);
 		var carProposal=angular.copy($scope.proposalInput);
 		carProposal.policy.policyStartdate = $filter('date')(carProposal.policy.policyStartdate,'yyyy-MM-dd');
 		carProposal.prevpolicy.prePolicystartdate = $filter('date')(carProposal.prevpolicy.prePolicystartdate,'yyyy-MM-dd');
 		carProposal.proposer.proposerDob = $filter('date')(carProposal.proposer.proposerDob,'yyyy-MM-dd');	
 		carProposal.proposer.nomineeDob = $filter('date')(carProposal.proposer.nomineeDob,'yyyy-MM-dd');
 		carProposal.proposer.appointeeDOB = $filter('date')(carProposal.proposer.appointeeDOB,'yyyy-MM-dd');
 		carProposal.vehicle.youngestDriverDob = $filter('date')(carProposal.vehicle.youngestDriverDob,'yyyy-MM-dd');
		
 		console.log(carProposal);
 		
 		 		
 		input.data=carProposal;
 		$scope.loading = true;
 		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
 		console.log(JSON.stringify(carProposal))
 		tmp.$promise.then(function(data){
 			$scope.loading = false;
			if(data.hasOwnProperty("error")) {
				$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str=data['error'];
				$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
				console.log($scope.proposalError)
			}
			else
			{
				// Proposal request Submitted to BEASYNC
				$scope.proposalResuestId = data.requestId;
				$scope.loading = true;
				iterations = 0;
				resultPromise = $interval(getProposalSubmissionResults,5000, 25, false);
			}
 			
 		
 		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
 	}
    
    var getProposalSubmissionResults = function() {
    	var input = {
    			url: "",
    			data:{}
    			
    	};
		iterations++;
		
		if (iterations >24) {
			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="The insurer didn't respond within 2 minutes. Please Retry";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
			$interval.cancel(resultPromise);
			$scope.loading = false;
		}
		
		input.url = "/submitProposal/result/" + $scope.proposalResuestId;
		//console.log(JSON.stringify($scope.quoteInput))
		//console.log(JSON.stringify(input))
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
 		tmp.$promise.then(function(data){
 			if(data.hasOwnProperty("premiumBreakup")) {
 				$scope.proposalInput.premiumBreakup = angular.copy(data.premiumBreakup);
 			}
 			
			if(data.hasOwnProperty("error")) {
				$scope.loading = false;
				$interval.cancel(resultPromise);
				$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str=data['error'];
				$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
				console.log($scope.proposalError)
				
				if(data['error']=="Premium Mismatch")
				{
				 	
				 newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"],"basePremium":data["Total Premium"],"ServiceTax":data["Service Tax"]})
				}
			}
			else if(data.hasOwnProperty("payUrl"))
			{
				$scope.loading = false;
				$interval.cancel(resultPromise);
				$scope.proposalError={key:"",errorPirnt:[]}
				console.log(data);
				
				var method = 'POST';
				 
				var paymentInput = {};
				paymentInput.pgiRequest = data.pgiRequest;
				
				FormSubmitter.submit(data.payUrl, method, paymentInput);
			}
 			
 		
 		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
 	}
    
    function newPreMiumMismatch(item) {
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
    
    $scope.addEditAccessory=function(type, action, qindex)
	{
    	
    	
    	
    	if (action == "Add"){
        	var input = {};
        	input.type = type;
    		var modalInstance=$uibModal.open({
    			ariaLabelledBy:'modal-title',
    			ariaDescribedBy:'modal-body',
    			templateUrl:'templates/proposal/car/tata/modal/accessory.html',
    			controller:'accessoryCtrlAIG',
    			size:'lg',
    			resolve:{
    				input:function()
    				{
    					return input;
    				}
    			}
    		});
    		modalInstance.result.then(function(response){
    			var output = response;
    			//console.log(output);
    			if(response!=undefined)
    			{	
					if (output.type == "Elec"){
						$scope.proposalInput.vehicle.electricalAccList.push(output);
					}else
					{
						$scope.proposalInput.vehicle.nonElectricalAccList.push(output);
					}
					$scope.checkAccessories();
    			}
    		})   		
    		
    	} 
		else if(action == "Delete")
		{	
			if (type == "Elec"){
				$scope.proposalInput.vehicle.electricalAccList.splice(qindex,1);
			}else
			{
				$scope.proposalInput.vehicle.nonElectricalAccList.splice(qindex,1);
			}
			$scope.checkAccessories();
		}

	};
    
    // Validation Functions
	
	$scope.checkAccessories = function (){
		
		$scope.pageErrors.vehicleError = false;
		
		if (!$scope.proposalInput.vehicle.electricalAccList) {
    		$scope.proposalInput.vehicle.electricalAccList = [];
    	}
    	
    	if (!$scope.proposalInput.vehicle.nonElectricalAccList) {
    		$scope.proposalInput.vehicle.nonElectricalAccList = [];
    	}
    	
		if ($scope.proposalInput.vehicle.nonElectricalAccessoryFlag=='Y' && $scope.proposalInput.vehicle.nonElectricalAccList.length == 0){
			$scope.pageErrors.vehicleError = true;
			$scope.pageErrors.vehicleErrorText = "Please add Non Electrical Accesorries";
		}
		
		if ($scope.proposalInput.vehicle.nonElectricalAccessoryFlag=='N') {
			$scope.proposalInput.vehicle.nonElectricalAccList = [];
		}
		

		if ($scope.proposalInput.vehicle.electricalAccessoryFlag=='Y' && $scope.proposalInput.vehicle.electricalAccList.length == 0){
			$scope.pageErrors.vehicleError = true;
			$scope.pageErrors.vehicleErrorText = "Please add Electrical Accesorries";
		}
		
		if ($scope.proposalInput.vehicle.electricalAccessoryFlag=='N') {
			$scope.proposalInput.vehicle.electricalAccList = [];
		}
		
	}
	
	$scope.checkAnualMilage=function(form,val) {
		
			$scope.premiumMilageError="";
		if(val>=1000 && val<=50000)
		{
			form.annualMilleage.$setValidity('required',true);
			$scope.premiumMilageError=""
		}
		else
		{
			form.annualMilleage.$setValidity('required',false);
			$scope.premiumMilageError="Minimum value 1000 and Max value 50000"
		}
	}
	
	$scope.checkPolicyType=function(form,val) {
		
		$scope.policyPremiumError="";
		if(val == "C")
		{
			form.prevPolicyType.$setValidity('required',true);
			$scope.policyTypeError=""
		}
		else
		{
			form.policyPremium.$setValidity('required',false);
			$scope.policyTypeError="Renewal of Liability only policy online is not allowed"
		}
	}
	
	$scope.checkPolicyPremium=function(form,val) {
		
		$scope.policyPremiumError="";
		if(val>=2000 && val<=100000)
		{
			form.policyPremium.$setValidity('required',true);
			$scope.policyPremiumError=""
		}
		else
		{
			form.policyPremium.$setValidity('required',false);
			$scope.policyPremiumError="Minimum value 2000 and Max value 100000"
		}
	}     
	
	function validatePolicyStartDate (startData) {
	   	 if (typeof(startData) == undefined) {
	   		return ok;
	   	 }
	   	 
	   	 if($scope.proposalInput.policy.typeOfBusiness=='New Business')
			 {
	   		 
	   	
		    	 var minDate=new Date($scope.proposalInput.vehicle.vehicleRegistrationDate);
		    	 var maxDate=new Date(minDate.getFullYear(),minDate.getMonth(),minDate.getDate()+10);
		    	 if(new Date(startData)>=new Date(minDate)&& new Date(startData)<=new Date(maxDate))
	   		 {
		    		    if(new Date()<=new Date(startData))
		    			{
			    			ok=true;
				    		$scope.policyStartDateError=""
		    			}
			    		else
		    			{
			    			ok=false;
				    		$scope.policyStartDateError="Policy Start date can not be a Past Date of current date"
		    			}
	   		 }
		    	 else
	   		 {
	   		    ok=false;
		    		$scope.policyStartDateError="Policy Start date cannot be greater than the Purchase Registration Date by 10 days for New Business"
	   		 }
			 }
	   	 else
	   		 {
		    		 var minDate=new Date($scope.proposalInput.vehicle.vehicleRegistrationDate);
		    		 var date=new Date()
			    	 var maxDate=new Date(date.getFullYear(),date.getMonth(),date.getDate()+45);
		    		 console.log(maxDate);
			    	 if(new Date(startData)>new Date()&& new Date(startData)<=new Date(maxDate))
		    		 {
			    		   
			    			ok=true;
				    		$scope.policyStartDateError="";
		    		 }
			    	 else
		    		 {
		    		    ok=false;
			    		$scope.policyStartDateError="Policy Start date cannot be a Past Date and cannot be greater than 45 days from current date"
		    		 }
	   		   
	   		 }
	   	 return ok;
    }
	
	$scope.getMasterValue = function(masterName, masterId){
		
		var values = [];
		if ($scope.insurerMaster.hasOwnProperty(masterName)){
			var values = $scope.insurerMaster[masterName];
			var masterValue = "";
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value;
				}
			}
		}
		return masterValue;
		
	}
	
	$scope.financierChange = function(){
				
		if ($scope.bank.selected.length>0 ){
			$scope.proposalInput.vehicle.bankName = $scope.bank.selected[0].value;
			$scope.proposalInput.vehicle.bankCode = $scope.bank.selected[0].id.split("-")[0];
			$scope.proposalInput.vehicle.financierType = $scope.bank.selected[0].id.split("-")[1];
		}
		
	}
	
	// Function to Handle Detailed View of Product
	$scope.showPremiumBreakUp=function() {
		
		var selectedProduct= {};
		selectedProduct.premiumBreakup = angular.copy($scope.proposalInput.premiumBreakup)
		selectedProduct.totalPremium = angular.copy($scope.proposalInput.policy.premiumPayable);
		selectedProduct.netPremium = 0;
		
		// Delete AddOns that are not selected
		
		if (!selectedProduct.premiumBreakup.base){
			selectedProduct.premiumBreakup.base = $scope.proposalInput.premiumBreakup.Basic
			selectedProduct.premiumBreakup.Basic = null;
		}
		
		angular.forEach($scope.proposalInput.premiumBreakup.base,function(item, key2){
			selectedProduct.netPremium = selectedProduct.netPremium + Number(item.netPremium)
		})
		
		angular.forEach($scope.proposalInput.premiumBreakup.other,function(item, key2){
			selectedProduct.netPremium = selectedProduct.netPremium - Number(item.netPremium)
		})
		
		var selectedCovers = $filter('filter')($scope.proposalInput.covers, {isSelected : 'Y'});
		var addons = [];
		angular.forEach(selectedCovers,function(cover, key1){
			angular.forEach($scope.proposalInput.premiumBreakup.addon,function(item, key2){
				if (cover.coverId == item.coverId) {
					selectedProduct.netPremium = selectedProduct.netPremium + Number(item.netPremium);
					addons.push(item)
				}
			})
		});
		
		
		selectedProduct.serviceTax = angular.copy($scope.proposalInput.policy.serviceTax);
		selectedProduct.premiumBreakup.addon = addons;
		//
		var modalInstance = $uibModal.open({
		      animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/quote/pb.html',
		      controller: 'quotePBCtrl',
		      size: 'md',
		      resolve: {
		    	  selectedProduct: function () {
		          return selectedProduct;
		        }
		      }
		    });
		
		
		modalInstance.result.then(function (selectedProduct) {
		      //$scope.selected = selectedProduct;
		    }, function () {
		      $log.info('Modal dismissed at: ' + new Date());
		});
	}
	
	$scope.titleGenderCheck = function(){
		

		$scope.pageErrors.proposerError=  false;
		$scope.pageErrors.proposerError=  "";
		
		if (($scope.proposalInput.proposer.title == "Mr"
		 || $scope.proposalInput.proposer.title == "Mr." 
		 || $scope.proposalInput.proposer.title == "Mast" 
		 || $scope.proposalInput.proposer.title == "Shri" 
		 || $scope.proposalInput.proposer.title == "Mstr" 
		 || $scope.proposalInput.proposer.title == "Md" )
		 && $scope.proposalInput.proposer.gender == "F") {
			$scope.pageErrors.proposerError =  true;
			$scope.pageErrors.proposerErrorText =  "Title and Gender Mismatch";
		}
		
		if (($scope.proposalInput.proposer.title == "Mrs"
		 || $scope.proposalInput.proposer.title == "Mrs." 
		 || $scope.proposalInput.proposer.title == "Ms" 
		 || $scope.proposalInput.proposer.title == "Lady" 
		 || $scope.proposalInput.proposer.title == "Miss" 
		 || $scope.proposalInput.proposer.title == "Sist" )
		 && $scope.proposalInput.proposer.gender == "M") {
			$scope.pageErrors.proposerError =  true;
			$scope.pageErrors.proposerErrorText =  "Title and Gender Mismatch";
		}
	}
	
	    
    //
    
    init();
    
    
    // Watch function
    $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
        
    	
        if ((typeof newValue != "undefined")&& (newValue != "")) {
        	
        	var date=new Date(newValue);
        	if ($scope.proposalInput.policy.typeOfBusiness == "Rollover"){
        		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
        	} else
        	{
        		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+3,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
        	}
    		
        }
    	
    });
   
    // Change Pincode
    $scope.$watch('proposalInput.proposer.policyPincode', function(newValue, oldValue) {
        
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		getCityFromPincode("P", newValue);
    	}
    	
    });
    
    $scope.$watch('proposalInput.proposer.corrPincode', function(newValue, oldValue) {
        
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		getCityFromPincode("C", newValue);
    	}
    	
    });    
    
    
    // Change Nominee DOB
    $scope.$watch('proposalInput.proposer.nomineeDob', function(newValue, oldValue) {
        //console.log('insurerId changed to: ' + newValue);
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(newValue);
    	}
    	 
    });    
       
}])