/**
*
*/
"use-strict"
brokeredgeApp.controller('itgiCarCntrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','paymentService',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,paymentService){
	$scope.proposalList=['policy','Proposer','Vehicle','Previous policy','Contact Info','Cover','T & C']
	$scope.drivingExps=[{'year':"1"},{'year':"2"},{'year':"3"},{'year':"4"},{'year':"5"},{'year':"6"},{'year':"7"},{'year':"8"},{'year':"9"},{'year':"10"},{'year':"11"},{'year':"12"},{'year':"13"},{'year':"14"},{'year':"15"},{'year':"16"},{'year':"17"},{'year':"18"},{'year':"19"},{'year':"20"}];
	$scope.proposalInput={};
	$scope.drivingExpr="";
	$scope.polState={};
	$scope.polCity={};
	$scope.corState={};
	$scope.corCity={};
	$scope.showError='';
	$scope.insurerMaster = {};
	$scope.min=1000;
	$scope.max=100000;
	$scope.policyPremiumError="";
	$scope.premiumMilageError="";
	$scope.polCities = [];
	$scope.corCities = [];
	$scope.policyRegAddressSame = "N";
	
	$scope.active=0;
	$scope.tabStatus={
			firstDisabled : false,
			firstComplete : 'indone',
			secondDisabled : true,
			secondComplete : 'indone',
			thirdDisabled : true,
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthDisabled:true,
			sixthComplete:'indone',
	}
	
	
	// dateoptions variables
    $scope.backPreviousPage=function()
	{
		window.history.back();
	}
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
       
	$scope.opened = [];
	$scope.open1 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
   $scope.open = function($event,$index) {
            $scope.opened[$index] = true;
   };
	$scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    
    // Entry Function for the Controller
	function init() {
				
		getApplicationData ();
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				getMaster("Title");
				getMaster("ADDRESS_TYPE");
				getMaster("BusinessType");
				getMaster("Occupation");
				getMaster("Insurer");
				getMaster("Industry");
				getMaster("MaritalStatus");
				getMaster("Nationality");
				getMaster("Relationship");
				getMaster("PaymentMethod");
				getMaster("State");
				getMaster("NomineeRel")
				formatApplication();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
		
		
	}
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		$scope.applicationData.proposalData = $scope.proposalInput;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				
				console.log($scope.proposalInput);
				formatApplication();
				
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
		
		
	}
	
	
	function formatStrToDate (){
		
		$scope.proposalInput.policy.policyStartdate = brokeredgefactory.strToDate($scope.proposalInput.policy.policyStartdate);
		$scope.proposalInput.prevpolicy.prePolicystartdate = brokeredgefactory.strToDate($scope.proposalInput.prevpolicy.prePolicystartdate);
		$scope.proposalInput.proposer.proposerDob = brokeredgefactory.strToDate($scope.proposalInput.proposer.proposerDob);	
		$scope.proposalInput.proposer.nomineeDob = brokeredgefactory.strToDate($scope.proposalInput.proposer.nomineeDob);
		$scope.proposalInput.proposer.appointeeDOB = brokeredgefactory.strToDate($scope.proposalInput.proposer.appointeeDOB);
		$scope.proposalInput.vehicle.youngestDriverDob = brokeredgefactory.strToDate($scope.proposalInput.vehicle.youngestDriverDob);
		
		
	}
	

	function formatApplication(){
		formatStrToDate();
		setSelectBoxes();
	}
	
	function formatDateToStr (){
		
		$scope.proposalInput.policy.policyStartdate = $filter('date')($scope.proposalInput.policy.policyStartdate,'yyyy-MM-dd');
		$scope.proposalInput.prevpolicy.prePolicystartdate = $filter('date')($scope.proposalInput.prevpolicy.prePolicystartdate,'yyyy-MM-dd');
		$scope.proposalInput.proposer.proposerDob = $filter('date')($scope.proposalInput.proposer.proposerDob,'yyyy-MM-dd');	
		$scope.proposalInput.proposer.nomineeDob = $filter('date')($scope.proposalInput.proposer.nomineeDob,'yyyy-MM-dd');
		$scope.proposalInput.proposer.appointeeDOB = $filter('date')($scope.proposalInput.proposer.appointeeDOB,'yyyy-MM-dd');
		$scope.proposalInput.vehicle.youngestDriverDob = $filter('date')($scope.proposalInput.vehicle.youngestDriverDob,'yyyy-MM-dd');
		
		
	}
	
	function setSelectBoxes(){
		
		$scope.polState.id = $scope.proposalInput.proposer.policyStatecode;
		$scope.polState.value = $scope.proposalInput.proposer.policyState;
		$scope.polCity.id = $scope.proposalInput.proposer.policyCitycode;
		$scope.polCity.value = $scope.proposalInput.proposer.policyCity;
		$scope.corState.id = $scope.proposalInput.proposer.corrStatecode;
		$scope.corState.value = $scope.proposalInput.proposer.corrState;
		$scope.corCity.id = $scope.proposalInput.proposer.corrCitycode;
		$scope.corCity.value = $scope.proposalInput.proposer.corrCity;
	}
	
	
	// Get Masters for the Insurer
	function getMaster (masterId){
		
		var input={url:'',data:{}}
    	input.url='/master/codes/'+ $scope.proposalInput.productId + "/" + masterId;
		
    	var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		var tmp = res.save(input);
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result"))
			$scope.insurerMaster[masterId] = data.result;
			
		})		
	}
	
	$scope.resetNCB = function(){
		
		$scope.proposalInput.prevpolicy.currNcb = 0;
		var i = 0;
		for (i = 0; i<$scope.proposalInput.covers.length; i++){
			if ($scope.proposalInput.covers[i].coverId == "NCBPROTECT"){
				$scope.proposalInput.covers[i].applicable = "N";
				$scope.proposalInput.covers[i].isSelected = "N";
			}
		}
	}
	
	$scope.setGender=function(val)
	{
		if(val=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	$scope.kitType=function()
	{
		if($scope.proposalInput.vehicle.lpgcngKit=='N')
		{
			$scope.proposalInput.vehicle.biKitType='';
		}
		
	}
	
	$scope.ChangClaimAmount=function(previousClaim){
		if(previousClaim=='N'){
		$scope.proposalInput.prevpolicy.claimAmount=0
		}else{
			$scope.proposalInput.prevpolicy.claimAmount="";
		}
	}
	
	$scope.samePolicyCorrAddress=function()
	{
		$scope.proposalInput.proposer.corrAddress1=$scope.proposalInput.proposer.policyAddress1;
		$scope.proposalInput.proposer.corrAddress2=$scope.proposalInput.proposer.policyAddress2;
		$scope.proposalInput.proposer.corrAddress3=$scope.proposalInput.proposer.policyAddress3;
		$scope.proposalInput.proposer.corrStatecode=$scope.proposalInput.proposer.policyStatecode;
		$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
		$scope.proposalInput.proposer.corrCitycode =$scope.proposalInput.proposer.policyCitycode;
		$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
		$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
	 	 
	}
	
	
	
	$scope.premiumAdd=function(coverId,val, premiumVal)
	{
		console.log(coverId);
		var cover = {};
		cover.name = coverId;
		if(val=='Y')
		{
		  if (coverId == "PAPASS"){
			  cover.si = "100000";
			  cover.num = 5;
		  }
		  
		  $scope.proposalInput.covers.push(cover);
		  $scope.proposalInput.policy.premiumPayable= Number($scope.proposalInput.policy.premiumPayable)+ Number(Math.round(premiumVal));
		}
		else
		{
			var i = 0;
			for (i=0; i<$scope.proposalInput.covers.length; i++){
				if ($scope.proposalInput.covers[i].coverId == coverId){
					break;
				}
			}
			$scope.proposalInput.covers.splice(i, 1);
			$scope.proposalInput.policy.premiumPayable= Number($scope.proposalInput.policy.premiumPayable) -  Number(Math.round(premiumVal));
		}
	}
	
	$scope.getcityStateFromPin = function(pincode){
		
		
		var input={url:'',data:{}}
    	input.url='/master/getStatePinStd/'+pincode;
		
    	var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		var tmp = res.save(input);
		tmp.$promise.then(function(data){
			
			
		})
	}
	
	$scope.getCity = function(state,name) {
		var cities = [];
		var input={
				url:'',
				data:{}
		}
		
		if (typeof(state.id) == "undefined"){
			return cities;
		}
		input.url='/master/city/' + $scope.proposalInput.insurerId +'/'+state.id;
		var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			cities=data['result'];
			if(name=='nominee')
			{
			$scope.cityNominee='';
			$scope.healthProposalInput.proposer.nomineeState=state.id;
			$scope.getCity(state.id,name);
		}else if(name=="Registration") {
				$scope.proposalInput.proposer.policyStatecode = state.id;
				$scope.proposalInput.proposer.policyState = state.value;
				$scope.polCities = cities
		}else if(name=="Mailing") {
				$scope.corCities = cities
				$scope.proposalInput.proposer.corrStatecode = state.id;
				$scope.proposalInput.proposer.corrState = state.value;
		} 
		})
	}
	
	$scope.cityChange = function(city,name) {
		if(name=="Registration") {
			$scope.proposalInput.proposer.policyCitycode = city.id;
			$scope.proposalInput.proposer.policyCity = city.value;
			
		}else if(name=="Mailing") {
			$scope.proposalInput.proposer.corrCitycode = city.id;
			$scope.proposalInput.proposer.corrCity = city.value;
		} 
	}
	
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	
	//next section
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "1":
			
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			$scope.tabStatus.secondDisabled = false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "2":
			$scope.active =2;
			$scope.tabStatus.secondComplete = 'done';
			$scope.tabStatus.thirdDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			console.log($scope.proposalInput)
			break;
		case "3":
				$scope.active =3;
				$scope.tabStatus.thirdComplete='done';
				$scope.tabStatus.fourthDisabled= false;
				$scope.proposalError={key:"",errorPirnt:[]}
			
			break;
		
		case "4":
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';
			$scope.tabStatus.fifthDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "5":
			$scope.showError='true';
	   		$scope.active =5;
			$scope.tabStatus.fifthOpen = false;
			$scope.tabStatus.fifthComplete = 'done';
			$scope.tabStatus.sixthOpen = true;
			$scope.tabStatus.sixthDisabled = false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		case "6":
			
			$scope.active=6;
			$scope.tabStatus.sixthComplete='done';
			$scope.tabStatus.fifthDisabled= false;
			$scope.proposalError={key:"",errorPirnt:[]}
			break;
		}
		
		saveApplicationData ();
	}
	
	
	$scope.validateOldPolicy=function(value,form)
	{
		console.log(form)
		var myDate = new Date(value);
		var today=new Date();
		//add a day to the date
		myDate.setDate(value.getDate() + 1);
		if( myDate>=new Date($scope.proposalInputInput.policy.policyStartDate)|| myDate<today)
		{
			//form.OldPolicyEndDate.$setValidity(false);
			 form.OldPolicyEndDate.$setValidity("uniq", false);
			//existPolicy.OldPolicyEndDate.$setValidity("size", true);
	    	$scope.show_error=true;
	    	$scope.error_text='The Start date of New Policy must be the day after this date.';	
	    	console.log($scope.error_text)
		}
		else
		{
			form.OldPolicyEndDate.$setValidity("uniq", true);
			
		}
	}
	
	
	$scope.previousPolicyLastDate=function(prevStart)
	{
		console.log(prevStart);
		var date=new Date(prevStart);
		$scope.proposalInput.prevpolicy.prePolicyenddate=$filter('date')(new Date(date.getFullYear()+1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
	}
	
	
	
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
	//check validation policy


     function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
	}
 	
 	function setpolicyAddress(){
 		
 		if ($scope.proposalInput.proposer.policyRegAddressSame =='Y'){
 			$scope.proposalInput.proposer.corrAddress1 = $scope.proposalInput.proposer.policyAddress1;
 			$scope.proposalInput.proposer.corrAddress2 = $scope.proposalInput.proposer.policyAddress2;
 			$scope.proposalInput.proposer.corrAddress3 = $scope.proposalInput.proposer.policyAddress3;
 			$scope.proposalInput.proposer.corrCity = $scope.proposalInput.proposer.policyCity;
 			$scope.proposalInput.proposer.corrCitycode = $scope.proposalInput.proposer.policyCitycode;
 			$scope.proposalInput.proposer.corrState = $scope.proposalInput.proposer.policyState;
 			$scope.proposalInput.proposer.corrStatecode = $scope.proposalInput.proposer.policyStatecode;
 			$scope.proposalInput.proposer.corrPincode = $scope.proposalInput.proposer.policyPincode;
 		}
 	}
 	
     // saves the Proposal and transfers to the payment gateway
    $scope.makePayment=function() {
 		//$scope.healthProposalInput.proposer.proposerDob=$(filter)
 		
 		var input={url:'',data:{}}
 		input.url='/submitProposal/motor/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
 		
 		setpolicyAddress();
 		if ($scope.proposalInput.vehicle.isOwnershipChanged == "Y"){
 			$scope.proposalInput.prevpolicy.previousNcb = 0;
 			$scope.proposalInput.prevpolicy.currNcb = 0;
 		}
 		formatDateToStr ();
 		$scope.proposalInput.vehicle.youngestDriverAge = brokeredgefactory.calculateAge($scope.proposalInput.vehicle.youngestDriverDob);
 		var carProposal=$scope.proposalInput;
 		console.log($scope.proposalInput);
 		
 		 		
 		input.data=carProposal;
 		$scope.loading = true;
 		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
 		console.log(JSON.stringify(carProposal))
 		tmp.$promise.then(function(data){
 			$scope.loading = false;
			if(data.hasOwnProperty("error")) {
				$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str=data['error'];
				$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
				console.log($scope.proposalError)
				
				if(data['error']=="Premium Mismatch")
				{
				 	
				 newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"],"basePremium":data["Total Premium"],"ServiceTax":data["Service Tax"]})
				}
			}
			else
			{
				$scope.proposalError={key:"",errorPirnt:[]}
				
				// Parse the response
				var result = angular.copy(data);
				var orderNo = result.OrderNo;
				var quoteNo = result.QuoteNo;
				var channel = result.Channel;
				var product = result.Product;
				var isMobile = result.IsMobile;
				var amount = result.Amount;
				var payUrl = result.payUrl;
				
				var url = payUrl + '?OrderNo='+orderNo+'&QuoteNo='+quoteNo+'&Channel='+channel+'&Product='+product+'&IsMobile='+isMobile+'&Amount='+amount;
				console.log(url);
				$window.location.href=url;
			}
 			
 		
 		})
 	}
    
    function newPreMiumMismatch(item) {
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
    
    // Validation Functions
	
	$scope.checkAnualMilage=function(form,val) {
		
			$scope.premiumMilageError="";
		if(val>=1000 && val<=50000)
		{
			form.annualMilleage.$setValidity('required',true);
			$scope.premiumMilageError=""
		}
		else
		{
			form.annualMilleage.$setValidity('required',false);
			$scope.premiumMilageError="Minimum value 1000 and Max value 50000"
		}
	}
	
	$scope.checkPolicyType=function(form,val) {
		
		$scope.policyPremiumError="";
		if(val == "C")
		{
			form.prevPolicyType.$setValidity('required',true);
			$scope.policyTypeError=""
		}
		else
		{
			form.policyPremium.$setValidity('required',false);
			$scope.policyTypeError="Renewal of Liability only policy online is not allowed"
		}
	}
	
	$scope.checkPolicyPremium=function(form,val) {
		
		$scope.policyPremiumError="";
		if(val>=2000 && val<=100000)
		{
			form.policyPremium.$setValidity('required',true);
			$scope.policyPremiumError=""
		}
		else
		{
			form.policyPremium.$setValidity('required',false);
			$scope.policyPremiumError="Minimum value 2000 and Max value 100000"
		}
	}     
	
	function validatePolicyStartDate (startData) {
	   	 if (typeof(startData) == undefined) {
	   		return ok;
	   	 }
	   	 
	   	 if($scope.proposalInput.policy.typeOfBusiness=='New Business')
			 {
	   		 
	   	
		    	 var minDate=new Date($scope.proposalInput.vehicle.vehicleRegistrationDate);
		    	 var maxDate=new Date(minDate.getFullYear(),minDate.getMonth(),minDate.getDate()+10);
		    	 if(new Date(startData)>=new Date(minDate)&& new Date(startData)<=new Date(maxDate))
	   		 {
		    		    if(new Date()<=new Date(startData))
		    			{
			    			ok=true;
				    		$scope.policyStartDateError=""
		    			}
			    		else
		    			{
			    			ok=false;
				    		$scope.policyStartDateError="Policy Start date can not be a Past Date of current date"
		    			}
	   		 }
		    	 else
	   		 {
	   		    ok=false;
		    		$scope.policyStartDateError="Policy Start date cannot be greater than the Purchase Registration Date by 10 days for New Business"
	   		 }
			 }
	   	 else
	   		 {
		    		 var minDate=new Date($scope.proposalInput.vehicle.vehicleRegistrationDate);
		    		 var date=new Date()
			    	 var maxDate=new Date(date.getFullYear(),date.getMonth(),date.getDate()+45);
		    		 console.log(maxDate);
			    	 if(new Date(startData)>new Date()&& new Date(startData)<=new Date(maxDate))
		    		 {
			    		   
			    			ok=true;
				    		$scope.policyStartDateError="";
		    		 }
			    	 else
		    		 {
		    		    ok=false;
			    		$scope.policyStartDateError="Policy Start date cannot be a Past Date and cannot be greater than 45 days from current date"
		    		 }
	   		   
	   		 }
	   	 return ok;
    }
	
	$scope.getMasterValue = function(masterName, masterId){
		
		var values = [];
		if ($scope.insurerMaster.hasOwnProperty(masterName)){
			var values = $scope.insurerMaster[masterName];
			var masterValue = "";
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value;
				}
			}
		}
		return masterValue;
		
	}
	
	    
    //
    
    init();
    
    
    // Watch function
    $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
        
        if ((typeof newValue != "undefined")&& (newValue != "")) {
        	
        	var date=new Date(newValue);
    		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
        }
    	
    });
   
    // Change State
    $scope.$watch('proposalInput.proposer.policyStatecode', function(newValue, oldValue) {
        
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		console.log('StateCode  changed to: ' + newValue.toString());
    		$scope.getCity(newValue);
    	}
    	
    });
    
    $scope.$watch('proposalInput.proposer.corrStatecode', function(newValue, oldValue) {
        
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		console.log('StateCode  changed to: ' + newValue.toString());
    		$scope.getCity(newValue);
    	}
    	
    });
    
    // Change Nominee DOB
    $scope.$watch('proposalInput.proposer.nomineeDob', function(newValue, oldValue) {
        //console.log('insurerId changed to: ' + newValue);
    	if ((typeof newValue != "undefined")&& (newValue != "")) {
    		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(newValue);
    	}
    	 
    });
    
    // Change Title
    $scope.$watch('proposalInput.proposer.title', function(newValue, oldValue) {
        //console.log('insurerId changed to: ' + newValue);
    	if ((typeof newValue != "undefined") && (newValue != "")) {
    		$scope.setGender(newValue);
    	}
    	
    });
    
       
}])