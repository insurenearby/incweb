/**
 * 
 */
"use strict";
brokeredgeApp.controller('nomineeCtrlbsli',['$scope','$uibModalInstance','lifefactory', 'input', function($scope, $uibModalInstance, lifefactory, input){
	
	console.log(input);
	
	
	$scope.nomineeData = input;
	
	// Date stuff
	// Date picker
    var dateToday = new Date();
    var yearMin = dateToday.getFullYear()-80;
    var yearMax = dateToday.getFullYear();
    var yearAdult = dateToday.getFullYear() - 18;
    var monthToday = dateToday.getMonth();
    var dayToday = dateToday.getDate();
    $scope.dobNominee ={};
    
    $scope.dobAdult={
    	 maxDate: new Date(yearAdult, monthToday, dayToday),
    	 minDate: new Date(yearMin, monthToday, dayToday)
    }
    
    $scope.dobPast={
       	 maxDate: new Date(yearMax, monthToday, dayToday),
       	 minDate: new Date(yearMin, monthToday, dayToday)
       }
    
    $scope.opened=function($event,$index)
	{

	    $scope.opened[$index] = true;
	    
	}
    
    $scope.open = function() {
		$scope.popup1.opened = true;
	};
	$scope.popup1 = {
		opened: false
	};
	
	if ($scope.nomineeData.header == "Nominee"){
		$scope.dobNominee = $scope.dobPast;
	} else
	{
		$scope.dobNominee = $scope.dobAdult;
	}
	
	$scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    
    //
    $scope.calculateNomineeAge = function(dob){
    	
    	$scope.nomineeData.age =  lifefactory.calculateAge(dob);
    }
    
    $scope.titles = lifefactory.getMasters("Title");
    $scope.nomineeRel = lifefactory.getMasters("NomineeRel");
	//
	
	$scope.ok = function () {
		console.log($scope.nomineeData);
		$uibModalInstance.close($scope.nomineeData); // Put the result here
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	// validate 
	$scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	$scope.alphaChar=function($event)
	   {
		   	var regex = new RegExp("^[a-zA-Z ]+$");
		    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (regex.test(str)) {
		        return true;
		    }

		    event.preventDefault();
		    return false;
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
}]);