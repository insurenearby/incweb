/**
 * 
 */
"use strict";
brokeredgeApp.controller('aiaProposalCtrl', 
	['$scope', 'lifefactory','$uibModal', '$resource', '$location', '$window','$filter','$http','$routeParams', 'APIService', function($scope, lifefactory, $uibModal, $resource, $location, $window, $filter, $http, $routeParams, APIService){
		
		// Date Constants
		$scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];
		$scope.altInputFormats = ['yyyy-M!-d!'];
		$scope.dateOptions = {
				maxDate: new Date(yearMax, monthToday, dayToday),
				minDate: new Date(yearMin, monthToday, dayToday),
		};
		
		$scope.active = 0;
		$scope.futureDateOptions = {
				minDate: new Date(),
		};
		
		$scope.filesToUpload = [
			
			{"fileType": "idProof", "displayName" : "ID Proof", "fileName": ""}
			,{"fileType": "ageProof","displayName" : "Age Proof",  "fileName": ""}
			,{"fileType": "addressProof","displayName" : "Address Proof",  "fileName": ""}
			,{"fileType": "incomeProof","displayName" : "Income Proof",  "fileName": ""}
			,{"fileType": "CDF","displayName" : "CDF",  "fileName": ""}
			,{"fileType": "CCR","displayName" : "CCR",  "fileName": ""}
			,{"fileType": "FTF","displayName" : "Financial Transaction Form",  "fileName": ""}
			,{"fileType": "chequeleaf","displayName" : "Cheque Leaf",  "fileName": ""}
		];
		
		$scope.illustrationViewed = false;
		$scope.tcVisible = false;
		
		$scope.pastDateOptions = {
				maxDate: new Date(),
		};
		
		//
		$scope.percentComplete = 0;
		$scope.isMinor = false;
		$scope.errortext = "";
		$scope.menu = [];
		$scope.proposalInput={};
		// Define List of master
		$scope.titles = [];
		var insurerId = 110;
		$scope.insurerMaster = {}; 
		$scope.loading = true;
		$scope.formLoaded = false;
		$scope.masterLoaded = false;
		$scope.menuLoaded = false;
		$scope.appInput = {};
		$scope.tab=1;
		$scope.lifestyleDisplay = {};
		$scope.addFamily={"Father":'',"Mother":'','Brother':[],"Sister":[]}
		$scope.resAddressFilter = {addressType : "R"};
		$scope.perAddressFilter = {addressType : "P"};
		$scope.isProposerSameInsured = true;
		
		function init(){
			console.log($routeParams);
			
			$scope.appInput.rmId  = $routeParams.rmId;			
			$scope.appInput.customerId  = $routeParams.customerId;
			$scope.appInput.applicationId  = $routeParams.appNo;
			
			
			loadMasterData(insurerId);
			
			getMenu();
			getsavedApplicationData($scope.appInput.rmId, $scope.appInput.customerId, $scope.appInput.applicationId)
			console.log("Displaying the AIA Form");
			
			$scope.templateUrl = "./templates/proposal/life/aia/aiaLifeProposal.html";
			$scope.loading = false;
			 
		}
		
		
				
		function populatePolicyData(productId){
			
			switch (productId){
			case "109N091V01":
				
			default:
				break;
			}
		}
		

		
		function getMenu()   {
			var fl = "menu" + 109 + ".json";

			var url = "./masters/" + fl;
			var res = $resource(url, [], {
	            'query': {method: 'GET', isArray: true}
	        });
		    
		    var tmp = res.query();
		    tmp.$promise.then(function(data) {
		    	$scope.menu = data;
		    	$scope.menuLoaded = true;
		    	if ($scope.menuLoaded && $scope.masterLoaded && $scope.formLoaded){
		    		$scope.loading = false;
		    	}
		    });
	    }
		
		function loadMasterData(){
			var fl = insurerId + ".json";
			var url = "./masters/" + fl;
		    var res = $resource(url, [], {
	            'query': {method: 'GET', isArray: false}
	        });
		    
		    var tmp = res.query();
		    tmp.$promise.then(function(data) {
		    	
		    	for(var k in data)  {
		    		var masterId = k;
		    		var master = data[k];
			    	var output = [];
			    	//console.log("get Masters for :" + masterId);
					angular.forEach(master, function(value, key) {
						var item = {};
						item.id = key;
						item.value = value;
						output.push(item);
					});
					if (output.length>0){
						$scope.insurerMaster[masterId] = output;
					}
				};
		    	lifefactory.setMaster($scope.insurerMaster);
		    	$scope.states = lifefactory.getMasters("State");
				$scope.cities = lifefactory.getCity();
		    	//console.log($scope.insurerMaster);
		    	$scope.masterLoaded = true;
		    	if ($scope.menuLoaded && $scope.masterLoaded && $scope.formLoaded){
		    		
		    		$scope.loading = false;
		    	}
		    });
		    
		}
		
		
		function getsavedApplicationData(rmId, customerId, applicationNo) {
			
			$scope.loading = true;
			var appUrl = "/submitProposal/getProposal/110N129V02" 
			           + "/" + rmId
	        		   + "/" + customerId
	        		   + "/" + applicationNo;
	        console.log(appUrl);
			
	        var input = {
	    			url: '',
	    			data : {}
	    	};
			
	        input.url = appUrl;
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
    		var tmp =  res.process(input);
    		tmp.$promise.then(function(data){

    			if (data.hasOwnProperty("result")){
    				setAppInput(JSON.parse(data.result));
    				//InitializeMedicalQuestions();
    				//console.log($scope.lifestyleDisplay);
    				if ($scope.menuLoaded && $scope.masterLoaded && $scope.formLoaded){
    					//console.log($scope.proposalInput);
    					console.log("Loading Complete");
    		    	}
    			} else
    			{
    				console.log("Error Retrieving Application");
    			}
    			$scope.loading = false;
    		});
			
		}
		
		function setAppInput(data){
			
			$scope.appInput = data;
			// console.log("Data Updated at: " + new Date($scope.appInput.lastUpdateTs));
			$scope.proposalInput = $scope.appInput.proposalData;
			$scope.proposalInput.policy.modalPremium = $scope.proposalInput.policy.policyPremium
			console.log($scope.proposalInput);
			$scope.proposalInput.policy.applicationNumber = data.applicationNumber;
			$scope.proposalInput.insured.dob = new Date($scope.proposalInput.insured.dob);
			$scope.proposalInput.proposer.dob = new Date($scope.proposalInput.proposer.dob);
			$scope.formLoaded = true;

			angular.forEach($scope.proposalInput.insured.personalQuestions, function(value, key) {
				$scope.lifestyleDisplay[value.id] = [];
				getLifeStyleAnswers(value.id);
				
			});
		}
		
		$scope.showTC = function() {
			
			if ($scope.tcVisible){
				$scope.tcVisible = false;
			} else
			{
				$scope.tcVisible = true;
			}
			
		}
						
		//check completion
		
		function getCompletionPrecent(){
			
			var expr = {iscomplete : true};
			var percentComplete = 10*Number($filter('filter')($scope.menu, expr).length);
			if (Number(percentComplete) >100){
				percentComplete = 100;
			}
			return percentComplete;
		}
				
		//
		
		//get City
		$scope.city=lifefactory.getCity();
		//get State
		
		init();		
		
		$scope.bankName=lifefactory.getBankName();
		//$scope.exactNatureofDuty=lifefactory.getExactNatureofDuty();
				
		$scope.ageWords = function(amount){
			return lifefactory.numToWords(amount)
		}
		
		$scope.isProposerSameInsureds=function(value) {
			
			console.log(value);
			if(value=='Y') {
				//console.log($scope.proposalInput.proposer);
				$scope.proposalInput.proposer.isProposerInsuredSame ="Y";
				$scope.proposalInput.insured.isProposerInsuredSame ="Y";
				$scope.proposalInput.proposer.title=$scope.proposalInput.insured.title;
				$scope.proposalInput.proposer.firstName=$scope.proposalInput.insured.firstName;
				$scope.proposalInput.proposer.middleName=$scope.proposalInput.insured.middleName;
				$scope.proposalInput.proposer.lastName=$scope.proposalInput.insured.lastName;
				$scope.proposalInput.proposer.dob= $scope.proposalInput.insured.dob;
				$scope.proposalInput.proposer.age = $scope.proposalInput.insured.age;
				$scope.proposalInput.proposer.birthCity =  $scope.proposalInput.insured.birthCity;
			   	$scope.proposalInput.proposer.birthState =  $scope.proposalInput.insured.birthState;
			   	$scope.proposalInput.proposer.maritalStatus =  $scope.proposalInput.insured.maritalStatus;
			   	$scope.proposalInput.proposer.fatherSpouseFirstName =  $scope.proposalInput.insured.fatherSpouseFirstName;
			   	$scope.proposalInput.proposer.fatherSpouseLastName =  $scope.proposalInput.insured.fatherSpouseLastName;
				$scope.proposalInput.proposer.gender=$scope.proposalInput.insured.gender;
				$scope.proposalInput.proposer.otherNationality=$scope.proposalInput.insured.otherNationality;
				$scope.proposalInput.proposer.isOtherCitizenship=$scope.proposalInput.insured.isOtherCitizenship;
				$scope.proposalInput.proposer.otherCitizenshipCountry=$scope.proposalInput.insured.otherCitizenshipCountry;
				$scope.proposalInput.proposer.isOtherCountryTaxResi=$scope.proposalInput.insured.isOtherCountryTaxResi;
				$scope.proposalInput.proposer.otherCountryTaxNo=$scope.proposalInput.insured.otherCountryTaxNo;
			   	$scope.proposalInput.proposer.contacts=$scope.proposalInput.insured.contacts;
				$scope.proposalInput.proposer.mobile=$scope.proposalInput.insured.mobile;
				$scope.proposalInput.proposer.email=$scope.proposalInput.insured.email;
				$scope.proposalInput.proposer.pan=$scope.proposalInput.insured.pan;
				$scope.proposalInput.proposer.relationshipWithLi = "SAME";
				$scope.proposalInput.insured.relationshipWithLi = "SAME";
				$scope.proposalInput.proposer.isExistingPolicyHolder='N';
				$scope.proposalInput.proposer.employmentDetails = $scope.proposalInput.insured.employmentDetails;
				$scope.proposalInput.proposer.address = $scope.proposalInput.insured.address;
				$scope.proposalInput.proposer.contacts = $scope.proposalInput.insured.contacts;
				$scope.proposalInput.proposer.nationality=$scope.proposalInput.insured.nationality;
				$scope.proposalInput.proposer.pan=$scope.proposalInput.insured.pan;
				$scope.proposalInput.proposer.idProofType=$scope.proposalInput.insured.idProofType;
				$scope.proposalInput.proposer.ageProofType = $scope.proposalInput.insured.ageProofType;
				$scope.proposalInput.proposer.addressProofType = $scope.proposalInput.insured.addressProofType;
				$scope.proposalInput.proposer.incomeProof=$scope.proposalInput.insured.incomeProof;
				$scope.proposalInput.proposer.uid = $scope.proposalInput.insured.uid;
			}
			else if(value=='N')
			{
				$scope.proposalInput.proposer.title="";
				$scope.proposalInput.proposer.firstName="";
				$scope.proposalInput.proposer.middleName="";
				$scope.proposalInput.proposer.lastName="";
				$scope.proposalInput.proposer.dob="";
				$scope.proposalInput.proposer.ageProofType="";
				$scope.proposalInput.proposer.idProofType="";
				$scope.proposalInput.proposer.addressProofType="";
				$scope.proposalInput.proposer.gender="";
				$scope.proposalInput.proposer.nationality="";
				$scope.proposalInput.proposer.otherNationality="";
				$scope.proposalInput.proposer.isOtherCitizenship="";
				$scope.proposalInput.proposer.otherCitizenshipCountry="";
				$scope.proposalInput.proposer.isOtherCountryTaxResi="";
				$scope.proposalInput.proposer.otherCountryTaxNo="";
				$scope.proposalInput.proposer.contacts=[];
				$scope.proposalInput.proposer.pan="";
				$scope.proposalInput.proposer.relationshipWithLi = "";
				$scope.proposalInput.insured.relationshipWithLi = "";
				$scope.proposalInput.proposer.isExistingPolicyHolder="N";
				$scope.proposalInput.proposer.existingPolicyNo =  "N";
			   	$scope.proposalInput.proposer.age= "";
			   	$scope.proposalInput.proposer.birthCity =  "";
			   	$scope.proposalInput.proposer.birthState =  "";
			   	$scope.proposalInput.proposer.fatherSpouseFirstName =  "";
			   	$scope.proposalInput.proposer.fatherSpouseLastName =  "";
			   	$scope.proposalInput.proposer.maritalStatus =  "";			    
			   	$scope.proposalInput.proposer.pan = "";
			   	$scope.proposalInput.proposer.isOtherCountryTaxResi =  "";
			   	$scope.proposalInput.proposer.isReqForPromoOffers =  "";
			   	$scope.proposalInput.proposer.isReqForEDocument =  "";
			   	$scope.proposalInput.proposer.isReqForEPolicyInfo =  "";
			   	$scope.proposalInput.proposer.incomeProofType =  "";
			   	$scope.proposalInput.proposer.insurancePurpose =  "";
			   	$scope.proposalInput.proposer.isPEP= "";
			   	$scope.proposalInput.proposer.isAadhaarAuthenticated =  "";
			   	$scope.proposalInput.proposer.isAadhaarAddressChanged =  "";
			   	$scope.proposalInput.insured.isProposerSameInsured = "N";
				$scope.proposalInput.proposer.employmentDetails = {};
				$scope.proposalInput.proposer.address = [];
				$scope.proposalInput.proposer.contacts = [];
				$scope.proposalInput.proposer.nationality="";
				$scope.proposalInput.proposer.pan="";
				$scope.proposalInput.proposer.ageProofType = "";
				$scope.proposalInput.proposer.addressProofType = "";
				$scope.proposalInput.proposer.incomeProof="";
				$scope.proposalInput.proposer.aadhar = "";
				$scope.proposalInput.proposer.isProposerInsuredSame ="N";
				$scope.proposalInput.insured.isProposerInsuredSame ="N";
			}
			console.log($scope.proposalInput.proposer);
			console.log($scope.proposalInput.insured);
		}

		// contact all mail document
		$scope.getAdrressLine = function (address){
			var addressline = address.addressLine1;
			if (typeof(address.addressLine2) != "undefined"){
				if (address.addressLine2.length>0 ) addressline = addressline + ",";
				addressline = addressline + address.addressLine2;
				
			}
				
			if (typeof(address.addressLine3) != "undefined"){
				if (address.addressLine3.length>0 ) addressline = addressline + ",";
				addressline = addressline + address.addressLine3;
				
			}
			
			return addressline;
		}
		
		function populateConcat(){
			
			$scope.proposalInput.proposer.contacts = [];
			if ($scope.proposalInput.proposer.email != "" && $scope.proposalInput.proposer.email != null){
				var contact = {}
				contact.contactType = "email";
				contact.contactText = $scope.proposalInput.proposer.email;
				$scope.proposalInput.proposer.contacts.push(contact);
			}
			if ($scope.proposalInput.proposer.mobile != "" && $scope.proposalInput.proposer.mobile != null){
				var contact = {}
				contact.contactType = "mobile";
				contact.contactText = $scope.proposalInput.proposer.mobile;
				$scope.proposalInput.proposer.contacts.push(contact);
			}
			
			$scope.proposalInput.insured.contacts = [];
			if ($scope.proposalInput.insured.email != "" && $scope.proposalInput.insured.email != null){
				var contact = {}
				console.log("Adding email")
				contact.contactType = "email";
				contact.contactText = $scope.proposalInput.insured.email;
				console.log("Adding email");
				console.log(contact);
				$scope.proposalInput.insured.contacts.push(contact);
			}
			if ($scope.proposalInput.insured.mobile != "" && $scope.proposalInput.insured.mobile != null){
				var contact = {}
				contact.contactType = "mobile";
				contact.contactText = $scope.proposalInput.insured.mobile;
				$scope.proposalInput.insured.contacts.push(contact);
			}
		}
		
		$scope.contactComplete = function(){
			
			if ($scope.loading){
				return false;
			}
			var contactComplete = false;
			var resAddressFilter = {addressType : "R"};
			var perAddressFilter = {addressType : "P"};
			//console.log($filter('filter')($scope.proposalInput.proposer.address, resAddressFilter));
			//console.log($filter('filter')($scope.proposalInput.proposer.address, perAddressFilter));
			if (typeof($scope.proposalInput.insured) != "undefined"){
				if ($filter('filter')($scope.proposalInput.insured.address, resAddressFilter).length == 1
					&& $filter('filter')($scope.proposalInput.insured.address, perAddressFilter).length == 1
						
				){
						
						contactComplete =  true;
					
					}
				
			}
			//console.log(contactComplete);
			return contactComplete;
		}
		
		$scope.healthComplete = function(){
			
			if ($scope.loading){
				return false;
			}
			var fatherCount = 0;
			var motherCount = 0;
			var healthComplete = false;
			var hasFather = false;
			var hasMother = false;
			angular.forEach($scope.proposalInput.familyHealth,function(k,l){
				if($scope.proposalInput.familyHealth[l].member=="Father") {
					hasFather = true;
					fatherCount++;
				}
				if($scope.proposalInput.familyHealth[l].member=="Mother") {
					hasMother = true;
					motherCount++;
				}
			})
			
			if (hasFather && hasMother){
				healthComplete = true;
			}
			
			if(motherCount !=1 || fatherCount != 1){
				healthComplete = false;
			}
			return healthComplete;
		}
		
		$scope.checkEmployment = function(){
			var valid = true;
			if (typeof($scope.proposalInput.insured) != "undefined" && $scope.proposalInput.insured.employmentDetails.occupation == "Student/Juvenile" && $scope.proposalInput.insured.age > 25){
				$scope.errorText = "Age of insured cannot be more than 25";
				valid = false;
			}
			return valid;
		}

		$scope.AllMailsDocument=function(j)
		{
			angular.forEach($scope.proposalInput.proposer.address,function(k,l){
				if($scope.proposalInput.insured.address[l].addressType==j)
				{
					$scope.proposalInput.insured.communicationAllMailDocument=$scope.proposalInput.insured.address[l];
					
				}
			})
		}
		
				
		// Final submit button
		$scope.submitApplication=function()
		{
			console.log(JSON.stringify($scope.proposalInput));
			// Change Status of proposal and save?
			$scope.getRequirements();
			// $scope.getBi(); // Commented out the server call for now.
			// Call method to Submit proposal and Process Response
			
			//
			
		}
		
		$scope.getPayMode = function(mode){
			var payMode = "";
			
			switch (mode){
			case "1":
				payMode = "Annual";
				break;
			case "12":
				payMode = "Monthly";
				break;
			case "2":
				payMode = "Semi-Annual";
				break;
			case "4":
				payMode = "Monthly";
				break;
			}
		}
		// Set tab
		
		$scope.prev=function(tabNo)
		{
			$scope.active = tabNo;
		}
		$scope.nextTab=function(newTab)
		{
			// Async call to Save Data 
			$scope.saveApplication();	
			
			// 
			var oldtab = Number(newTab) - 1;
			$scope.menu[oldtab].inActive=true;
			$scope.active=newTab;
			// console.log($scope.menu[oldtab-1]);
		}
		
		
		$scope.setTab = function(newTab){
		 
		 
		  var oldtab = Number(newTab) - 1;
		  //console.log( $scope.menu[oldtab-1])
		  $scope.menu[oldtab].iscomplete = true;
		 
		  $scope.percentComplete = getCompletionPrecent();
		  $scope.tab = newTab;
	      if($scope.tab==2)
	      {
	      	$scope.contactComplete();
	      }
	      $scope.active=newTab;
	      window.scrollTo(0,0);
	    };

	    $scope.isSet = function(tabNum){
	      return $scope.tab === tabNum;
	    };
	    //file upload
	    $scope.IDProof=function(files)
	    {

	    	$scope.IDProof=lifefactory.getUploadFile(files);
	    	//console.log(lifefactory.getUploadFile(files))
	    }
	    $scope.AgeProof=function(files)
	    {
	    	lifefactory.getUploadFile(files);
	    	
	    }
	    $scope.AddProof=function(files)
	    {
	    	lifefactory.getUploadFile(files);
	    	
	    }
	   
	    $scope.getMasterValue = function(masterName, masterId){
			
			var masterValue = "";
			var values = $scope.insurerMaster[masterName];
			
			if (typeof(values) != "undefined"){
				for (var i = 0; i<values.length; i++){
					
					if (values[i].id == masterId){
						masterValue = values[i].value;
						//console.log(masterValue)
						break;
					}
				}
				
			}
					
			
			return masterValue; 
		}
	    
	    //Minimal slider config
	    $scope.proposalInput.suAssured = {
	        value: 100000,
	        options: {
	            floor: 100000,
	            ceil: 5000000,
	            step: 100000,
	            translate: function(value) {
      			return value+'L';
    			}
	        }
	    };
	    $scope.proposalInput.policyTerm={
	    	value:10,
	    	options:{
	    		floor:10,
	    		ceil:30,
	    		step:1,
	    		showTicksValues: true,
	    		
	    	}
	    };

	    
	    //validate of number and char
	    $scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	   $scope.alphaChar=function($event)
	   {
		   	var inputValue = event.which;
	        // allow letters and whitespaces only.
	        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
	            event.preventDefault(); 
	        }
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
	    // Date picker
	    var dateToday = new Date();
        var yearMin = dateToday.getFullYear()-60;
        var yearMax = dateToday.getFullYear()-18;
        var monthToday = dateToday.getMonth();
        var dayToday = dateToday.getDate();
        var yearMaxs = dateToday.getFullYear();
        $scope.medicalOption={
        	 maxDate: new Date(yearMaxs, monthToday, dayToday)
        }
        $scope.dateOptions = {
            maxDate: new Date(yearMax, monthToday, dayToday),
            minDate: new Date(yearMin, monthToday, dayToday)
          };
          $scope.dateOptionss = {
            maxDate: new Date(yearMaxs, monthToday, dayToday),
            minDate: new Date(yearMin, monthToday, dayToday)
          };
          $scope.nomineeDateOption={
          	maxDate:new Date()
          }
        $scope.opened=function($event,$index)
		{

		    $scope.opened[$index] = true;
		    
		}
		
        $scope.open = function() {
			$scope.popup1.opened = true;
		};
		
		$scope.openDI = function() {
			$scope.popup3.opened = true;
		};
		
        $scope.openDE = function() {
			$scope.popup2.opened = true;
		};
		
		$scope.popup1 = {
			opened: false
		};
		$scope.appointmentDateTime=function()
		{
			$scope.popup2.opened = true;
		}
		
		$scope.popup2 = {
			opened: false
		}
		

		
		$scope.popup3 = {
			opened: false
		}
		
		
        $scope.setDate = function(year, month, day) {
              $scope.selectedOption.targetym= new Date(year, month, day);
        };
        $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];
 
	    
	    // LifeStyle section 
		
		function formNeeded(questionId){
			
			//console.log(questionId);
			var needed = false;
			if (questionId == "IsAlcoholic"
			|| questionId == "IsNarcotic"
			|| questionId == "IsTobaccoConsumed"){
				needed = true;
			}
			//console.log(needed);
			return needed;
		}
		
				
		// get the Headers for life Style Questions
		$scope.lifeStyleHeaders = function(questionId){
			//console.log(questionId);
			var rows = $scope.insurerMaster[questionId + "Hdr"];
			var headers = [];
			if (typeof(rows) != 'undefined'){
				for (var i=0; i<rows.length; i++){
					headers.push(rows[i]);
				}
			}
			
			//console.log(headers);
			return headers;
		}
		
		
		// The This functions will get the answers for the lifestyle questions and 
		// convert them into required format
		 function getLifeStyleAnswers(questionId){
			
			var rows = [];
			
			for (var i=0; i<$scope.proposalInput.insured.personalQuestions.length; i++){
				var item = $scope.proposalInput.insured.personalQuestions[i];
				if (item.id == questionId){
					//item.formNeeded = formNeeded(questionId);
					rows = item.details;
					break;
				}
			}
			
			var answers = [];
			var headers = $scope.lifeStyleHeaders(questionId);
			for (var i=0; i<rows.length; i++){
				var tags = [];
				for (var j = 0; j<headers.length; j++){
					var item = rows[i][headers[j].id];
					if (typeof(item) =='undefined'){
						item = "";
					}
					tags.push(item);
				}
				answers.push(tags);
			}
			//console.log(questionId);
			//console.log(answers);
			$scope.lifestyleDisplay[questionId] = answers;
		}
		
		//Medical Questions 
		function InitializeMedicalQuestions(){
			
			var rows = $scope.insurerMaster.MedicalQuestions;
			var i=0;
			var questions = [];
			angular.forEach(rows, function(value, key) {
				  var question = {};
				  var parts = value.id.split("~");
				  question.seq = Number(parts[0]);
				  question.id = parts[1];
				  question.isYesNo = "";
				  question.details = [];
				  questions.push(question);
			});
			$scope.proposalInput.insured.medicalQuestions = questions;

		}
		
		$scope.showIllustration = function(){
			$scope.illustrationViewed = true;
			$window.open($scope.bi.illusUrl);
			
		}
		
		// Get Requirements
		$scope.getRequirements = function(){
			
			$scope.loading = true;
	    	
	    						
			var biInput = {};
			biInput.type = "P";
			biInput.productType = $scope.proposalInput.policy.productType;
			biInput.productId = $scope.proposalInput.policy.productId;
			biInput.planId = $scope.proposalInput.policy.planId;
			biInput.pt = $scope.proposalInput.policy.pt;
			biInput.ppt = $scope.proposalInput.policy.ppt;
			biInput.mode = "1" //$scope.proposalInput.policy.mode;
			biInput.ap = $scope.proposalInput.policy.policyPremium;
			biInput.sa = $scope.proposalInput.policy.basicSumAssured;
			biInput.ownage = $scope.proposalInput.proposer.age;
			biInput.insage = $scope.proposalInput.insured.age;
			biInput.DOB = $scope.proposalInput.insured.dob;
			biInput.owngender = $scope.proposalInput.proposer.gender;
			biInput.insgender = $scope.proposalInput.insured.gender;
			biInput.ownname = $scope.proposalInput.proposer.firstName + " " + $scope.proposalInput.proposer.lastName;
			biInput.insname = $scope.proposalInput.insured.firstName + " " + $scope.proposalInput.insured.lastName;;
			biInput.nrv = $scope.proposalInput.insured.nrv;
			biInput.relPeriod = $scope.proposalInput.insured.relPeriod;
			biInput.rating = "NS";
			if ( $scope.proposalInput.insured.employmentDetails.occupation == "Salaried"
			|| $scope.proposalInput.insured.employmentDetails.occupation == "Self Employed"){
				biInput.occupationCode = "1";
			} else
			{
				biInput.occupationCode = "3";
			}
			console.log(biInput);
			
			var input = {
	    			url: "",
	    			data : biInput
	    	};
			
	    	input.url = "/quote/getBI";
	    	
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
    		var tmp =  res.process(input);
	    	$scope.loading = true;
	    	
			tmp.$promise.then(function(data){
				
	    		if (!data.hasOwnProperty("error")){
	    			$scope.bi = data;
	    			// Check the premium and populate.
	    			$scope.proposalInput.policy.modalPremium = $scope.bi.PremiumInstallmentPaybale;
	    			//
	    			if ($scope.bi.medicalReq.length > 0) {
	    				$scope.bi.isMedical = true
	    			} else
	    			{
	    				$scope.bi.isMedical = false
	    			}
	    			$scope.active = 6;
	    		}
	    		$scope.loading = false;
			}, function (error) {
	    	    console.error(data);
	    	    $scope.loading = false;
	    	});
		}
		
		function createIllusLink() {
			
			var obj = {};
		    
			var url = "https://italic.co.in/SISEngine/PerformAction?";
			obj.InsAge = $scope.proposalInput.insured.age;
			obj.OwnAge = $scope.proposalInput.proposer.age;
			obj.InsName = $scope.proposalInput.insured.firstName + " " + $scope.proposalInput.insured.lastName;
			obj.OwnName = $scope.proposalInput.proposer.firstName + " " + $scope.proposalInput.proposer.lastName;
			obj.InsOcc= "M24";
			obj.OwnOcc= "M24";
			obj.PaymentMode= "A";
			obj.InsSex= $scope.proposalInput.insured.gender;
			obj.OwnSex= $scope.proposalInput.proposer.gender;
			obj.PolicyTerm= $scope.proposalInput.policy.pt;
			obj.PPTerm= $scope.proposalInput.policy.ppt;
			obj.PropNo= $scope.proposalInput.policy.applicationNumber;
			obj.BasePlan = "SRL5V2N1";
			obj.SAgeProofFlg= "Y";
			// Convert dob to dd-MMM-YYYY
			obj.PropDOB= formatDate($scope.proposalInput.proposer.dob);

			obj.insDOB= formatDate($scope.proposalInput.insured.dob);
			obj.SumAssured= $scope.proposalInput.policy.basicSumAssured;
			obj.commision= "1";
			obj.opType= "2";
			obj.InsSmoker= "1";
			obj.tataDis= "";
			let ret = [];
		   for (let d in obj)
		     ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(obj[d]));
		   return url + ret.join('&');
		}
		
		
		function formatDate(date) {
			  var monthNames = [
			    "JAN", "FEB", "MAR",
			    "APR", "MAY", "JUN", "JUL",
			    "AUG", "SEP", "OCT",
			    "NOV", "DEC"
			  ];

			  var day = date.getDate();
			  var monthIndex = date.getMonth();
			  var year = date.getFullYear();

			  return day + '-' + monthNames[monthIndex] + '-' + year;
			}
	
		// Create Illustration Link
		$scope.getBi = function(){
			
			
			
			var biInput = {};
			biInput.type = "P";
			biInput.productId = $scope.proposalInput.policy.productId;
			biInput.planId = $scope.proposalInput.policy.planId;
			biInput.pt = $scope.proposalInput.policy.pt;
			biInput.ppt = $scope.proposalInput.policy.ppt;
			biInput.mode = "1" //$scope.proposalInput.policy.mode;
			biInput.ap = $scope.proposalInput.policy.policyPremium;
			biInput.sa = $scope.proposalInput.policy.basicSumAssured;
			biInput.ownage = $scope.proposalInput.proposer.age;
			biInput.insage = $scope.proposalInput.insured.age;
			biInput.owngender = $scope.proposalInput.proposer.gender;
			biInput.insgender = $scope.proposalInput.insured.gender;
			biInput.rating = "NS";
			console.log(biInput);
			
			var input = {
	    			url: "/quote/getBI",
	    			data : biInput
	    	};
			
	    	input.url = "/quote/getBI";
	    	
	    	var tmp = APIService.processRequest({}, JSON.stringify(input));
	    	$scope.loading = true;
	    	
			tmp.$promise.then(function(data){
				
	    		if (!data.hasOwnProperty("error")){
	    			$scope.bi = data;
	    			$scope.setTab(12);
	    			$scope.loading = false;
	    		}
			}, function (error) {
	    	    console.error(data);
	    	    $scope.loading = false;
	    	});
		}
		
		// Save the Application after very section update and proceed to tabNo.
		$scope.saveandProceed = function (tabNo){
			
			if (tabNo < 3){
				populateConcat();
			}
			if ($scope.proposalInput.insured.isProposerSameInsured == "Y"){
				$scope.isProposerSameInsureds("Y");
			}
			
			if (typeof($scope.proposalInput.proposer.dob) != undefined){
				$scope.proposalInput.proposer.age = lifefactory.calculateAge($scope.proposalInput.proposer.dob);
			}
			
			if (typeof($scope.proposalInput.insured.dob) != undefined){
				$scope.proposalInput.insured.age = lifefactory.calculateAge($scope.proposalInput.insured.dob);
			}
			
			$scope.appInput.proposalData = $scope.proposalInput;
			
			var appUrl = "/submitProposal/saveProposal/" + $scope.appInput.productId
	           + "/" + $scope.appInput.rmId
	           + "/" + $scope.appInput.customerId
	           + "/" + $scope.appInput.applicationNumber;
			 console.log(appUrl);
				var input = {
						url: appUrl,
						data : $scope.appInput
				};
				
				var res = $resource('./DataRequest', [], {
			       save: {
			          method: 'POST'}
			 });
			$scope.loading = true;
    		var tmp =  res.save(JSON.stringify(input));
    		
    		
    		tmp.$promise.then(function(data){
    			//console.log(data);
    			if (data.hasOwnProperty("result")){
    				setAppInput(JSON.parse(data.result));
    				$scope.setTab(tabNo);
    			}
    			$scope.loading = false;
    		}, function (error) {
	    	    console.error(data);
	    	    $scope.loading = false;
	    	});
    		
		}
		
		
		// Upload file
		$scope.uploadDocs = function(){
		 
			angular.forEach($scope.filesToUpload, function(value, key) {
				var item = value;
				if (typeof(item.fileName) != "undefined" && item.fileName != ""){
					var fd = new FormData();
					fd.append('file', item.fileName);
			        fd.append('docType', item.fileType);
			        fd.append('appId', $scope.proposalInput.policy.applicationNumber);
			        fd.append('agentId', "DBS");
			        uploadUrl(key, fd);
				}
			});
		        
		};
		
		var uploadStarted = false;
		
		$scope.disableUpload = function(){
			
			 var disable = false;
			 if (!uploadStarted) {
				 angular.forEach($scope.filesToUpload, function(value, key) {
						
						var item = value;
						item.errorText = "";
						item.status = 0;
						if (typeof(item.fileName) != "undefined" && item.fileName != ""){
							var fd = new FormData();
							if (item.fileName.size > 1024 * 1024 * 2){
								item.status = 3;
								item.errorText = "File too large";
								disable = true;
							}
							var start = item.fileName.name.indexOf(".");
							var ext = item.fileName.name.substr(Number(start)+1);
							if (ext != "pdf" && ext != "png" && ext != "jpg" && ext != "jpeg"){
								item.status = 3;
								item.errorText = "Invalid File Type";
								disable = true;
							}
					       
						} else
						{
							disable = true;
						}
				 }); 
			 } else
			 {
				 disable = true;
			 }
				
			

			 
			 if (!$scope.illustrationViewed){
				 disable = true; 
			 }
			 
			 return disable;
		}	
	    		    
	    function uploadUrl(i, fd){
	    	
	    	uploadStarted = true;
	    	
	    	$http.post("./fileUpload", fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined},
	            uploadEventHandlers: {
	                progress: function (e) {
	                    if (e.lengthComputable) {
	                    	$scope.filesToUpload[i].status = 1;
	                    	$scope.filesToUpload[i].progressBar = (Number(e.loaded)/Number(e.total))* 100;
	                    	$scope.filesToUpload[i].progressCounter = $scope.filesToUpload[i].progressBar;
	                    }
	                }
	            }
	        }).then(function successCallback(response) {
	        	$scope.filesToUpload[i].status = 2;
	        	checkUploadCompletion();
            }, function errorCallback(response) {
            	$scope.filesToUpload[i].errorText = "Error Uploading file";
            	$scope.filesToUpload[i].status = 3;
            	checkUploadCompletion()
            });
	    	
	    }
	    

		
		function checkUploadCompletion() {
			
			var complete =true;
			angular.forEach($scope.filesToUpload, function(value, key) {
				
				if (value.status != 2){
					complete =false;
				}
			})
			
			if (complete) {
				// display final confirmation page
				$scope.active=7
				$scope.proposalInput.policy.insurerPolicyNo = "U988316434";
			}
			
		}
		
		//*********************************** Modal Forms Section ************************
	    
	        
	    
        $scope.addEditAddress=function(action, i, address)
		{
			
        	var addressList = angular.copy($scope.proposalInput.insured.address);
			//console.log(address);
			if (action == "Add") {
				var modalInstance=$uibModal.open({
					ariaLabelledBy:'modal-title',
					ariaDescribedBy:'modal-body',
					templateUrl:'./templates/proposal/life/aia/modal/Address.html',
					controller:'addressCtrlAia',
					size:'lg',
					resolve:{
						input:function()
						{
							return addressList;
						}
					}
				});
				modalInstance.result.then(function(response){
					var output = response;
					//console.log(output);
					if(response!=undefined)
					{
						if (action == "Add"){
							$scope.proposalInput.insured.address.push(output);
						}else
						{
							// find out the address index and then substitute
							$scope.proposalInput.insured.address[i]= output;
						}
						//console.log($scope.proposalInput.proposer.address);
					}
				})				
			} else
			{
				$scope.proposalInput.insured.address.splice(i,1);
			}

		}
 
        $scope.addEditNominee=function(action, i, nominee)
		{
			
			//console.log(nominee);
        	if (action == "Add") {
            	if ($scope.proposalInput.insured.isProposerSameInsured=='Y'){
            		nominee.isNominee = true;
            		nominee.header = "Nominee";
            	} else
            	{
            		nominee.isNominee = false;
            		nominee.header = "Contingent Policy Holder";
            	}
            	
            	
    			var modalInstance=$uibModal.open({
    				ariaLabelledBy:'modal-title',
    				ariaDescribedBy:'modal-body',
    				templateUrl:'./templates/proposal/life/aia/modal/nominee.html',
    				controller:'nomineeCtrlAia',
    				size:'lg',
    				resolve:{
    					input:function()
    					{
    						return nominee;
    					}
    				}
    			});
    			modalInstance.result.then(function(response){
    				var output = response;
    				//console.log(output);
    				if(response!=undefined)
    				{
    					if (action == "Add"){
    						$scope.proposalInput.nominee.push(output);
    					}
    					
    				}
    			})        		
        		
        	}else
        	{
        		$scope.proposalInput.nominee.splice(i,1);
        	}
        	
        	

		}
        
        $scope.addEditFamilyMember=function(action, i, member)
		{
			
        	if (action == "Add"){
        		var modalInstance=$uibModal.open({
    				ariaLabelledBy:'modal-title',
    				ariaDescribedBy:'modal-body',
    				templateUrl:'./templates/proposal/life/aia/modal/familyMember.html',
    				controller:'familyHealthCtrlAia',
    				size:'lg',
    				resolve:{
    					input:function()
    					{
    						return member;
    					}
    				}
    			});
    			modalInstance.result.then(function(response){
    				var output = response;
    				console.log(output);
    				if(response!=undefined)
    				{
    					$scope.proposalInput.familyHealth.push(output);
    					
    				}
    			})
        	} else (action == "delete")
        	{
        		$scope.proposalInput.familyHealth.splice(i,1);
        	}
			
		}
        
        $scope.addEditMinorInsurance=function(action, i, member)
		{
			
        	if (action == "Add"){
        		var modalInstance=$uibModal.open({
    				ariaLabelledBy:'modal-title',
    				ariaDescribedBy:'modal-body',
    				templateUrl:'./templates/proposal/life/aia/modal/minorFamilyInsurance.html',
    				controller:'minorInsuranceCtrlAia',
    				size:'lg',
    				resolve:{
    					input:function()
    					{
    						return member;
    					}
    				}
    			});
    			modalInstance.result.then(function(response){
    				var output = response;
    				//console.log(output);
    				if(response!=undefined)
    				{
    					$scope.proposalInput.minorInsurance.push(output);
    				}
    			})
        	} else if (action == "delete")
        	{
        		$scope.proposalInput.minorInsurance.splice(i,1);
        	}
			
		}
        
        $scope.addEditExistingInsurance=function(action, i, item) {
			
			//console.log(member);
        	if (action == "Add"){
        		var modalInstance=$uibModal.open({
    				ariaLabelledBy:'modal-title',
    				ariaDescribedBy:'modal-body',
    				templateUrl:'./templates/proposal/life/aia/modal/concurrentInsurance.html',
    				controller:'concurrentInsuranceCtrlAia',
    				size:'lg',
    				resolve:{
    					input:function()
    					{
    						return item;
    					}
    				}
    			});
    			modalInstance.result.then(function(response){
    				var output = response;
    				console.log(output);
    				if(response!=undefined)
    				{
    					$scope.proposalInput.existingInsurance.push(output);
    					
    				}
    			})
        		
        	}else if (action == "delete")
        	{
        		$scope.proposalInput.existingInsurance.splice(i, 1);
        	}
			
		}
		
		// Get the modal forms
		$scope.addEditLifeStyleMember = function (questionId, action ,i, data){
			//console.log(questionId)
			var qIndex = 0;
			for (var k=0; k<$scope.proposalInput.insured.personalQuestions.length; k++){
				var item = $scope.proposalInput.insured.personalQuestions[k];
				if (item.id == questionId){
					qIndex = k;
				}
			}	
			//console.log(qIndex);
			
			var currTemplate = "";
			var currCtrl = "";
			if (action == "Add") {
				switch (questionId){
				case "IsNarcotic":
					currTemplate = "./templates/proposal/life/aia/modal/Narcotic.html";
					currCtrl = "narcoticCtrlAia";
					break;
				case "IsAlcoholic":
					currTemplate = "./templates/proposal/life/aia/modal/Alcoholic.html";
					currCtrl = "alcoholicCtrlAia";
					break;
				case "IsTobaccoConsumed":
					currTemplate = "./templates/proposal/life/aia/modal/Tobacco.html";
					currCtrl = "tobaccoCtrlAia";
					break;
				}
				
				if (currCtrl.length >0) {

					var modalInstance=$uibModal.open({
						ariaLabelledBy:'modal-title',
						ariaDescribedBy:'modal-body',
						templateUrl:currTemplate,
						controller:currCtrl,
						size:'lg',
						resolve:{
							input:function()
							{
								return data;
							}
						}
					});
					modalInstance.result.then(function(response){
						var output = response;
						
						console.log(output);
						if(typeof(output)!=undefined) {
							if (action == "Add")
							{
								$scope.proposalInput.insured.personalQuestions[qIndex].details.push(output);
							} 
							else if(action == "Edit")
							{
								//console.log(output)
								$scope.proposalInput.insured.personalQuestions[qIndex].details[i] = output;
							}
							getLifeStyleAnswers(questionId);
						}
					})				
				} 
			}else
	        {
				$scope.proposalInput.insured.personalQuestions[qIndex].details.splice(i, 1);
				getLifeStyleAnswers(questionId);
        	

			}
		}
		
		
		        
        $scope.checkMedical = function(i, value){
        	if (value == "Y"){
        		$scope.proposalInput.insured.medicalQuestions[i].details = [];
        		//console.log()
        	}
        	else
        	{
        		$scope.proposalInput.insured.medicalQuestions[i].details = "";
        	}
        	
        	
        }
        
        
 
        $scope.addEditMedical=function(questionId, action, qindex, qdetails)
		{

			var modalInstance=$uibModal.open({
				ariaLabelledBy:'modal-title',
				ariaDescribedBy:'modal-body',
				templateUrl:'proposal/life/aia/modal/medicalQuestionDetail.html',
				controller:'medicalQuestionDetailCtrlAia',
				size:'lg',
				resolve:{
					input:function()
					{
						return qdetails;
					}
				}
			});
			modalInstance.result.then(function(response){
				var output = response;
				//console.log(output);
				if(response!=undefined)
				{

					if (action == "Add"){
						$scope.proposalInput.insured.medicalQuestions[questionId].details.push(output);
					} 
					else if(action == "Edit")
					{
						
						$scope.proposalInput.insured.medicalQuestions[questionId].details[qindex] = output;
					}
				}
			})
		};
	}
	]);

