/**
 * 
 */
"use strict";
brokeredgeApp.controller('narcoticCtrlAia',['$scope','$uibModalInstance','lifefactory','input', function($scope,$uibModalInstance, lifefactory, input){
	
	
	$scope.isNarcoticDetails = {
			"NarcoticTypeId": "",
			"years": "",
			"Frequency" : "",
			"Qty": "",
			"stopped": "N"
				
		}
	
	$scope.months = lifefactory.getMasters("Month");
	$scope.years = [];
	var currYear = (new Date()).getFullYear();
	
	for (var i=0; i<10; i++){
		$scope.years.push(currYear-i);
	}
	
	$scope.types = lifefactory.getMasters("NarcoticType");
	console.log(input);
	
	$scope.ok = function () {
		$scope.isNarcoticDetails.NarcoticType = lifefactory.getMasterValue("NarcoticType", $scope.isNarcoticDetails.NarcoticTypeId);
		$uibModalInstance.close($scope.isNarcoticDetails); // Put the result here
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	// validate 
	$scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	$scope.alphaChar=function($event)
	   {
		   		var regex = new RegExp("^[a-zA-Z ]+$");
		    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (regex.test(str)) {
		        return true;
		    }

		    event.preventDefault();
		    return false;
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
}]);