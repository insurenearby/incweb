/**
 * 
 */
"use strict";
brokeredgeApp.controller('addressCtrlAia',['$scope','$uibModalInstance', 'lifefactory','input', function($scope,$uibModalInstance, lifefactory, input){
	
	
	console.log(input);
	
	$scope.addressList = input;
	
	$scope.address = {};
	$scope.selectedAddress = {};
	
	function init(){
		
		$scope.states = lifefactory.getMasters("State");
		$scope.cities = lifefactory.getCity();
		$scope.types = lifefactory.getMasters("AddressType");
		
		for (var i = 0; i<$scope.addressList.length; i++){
			$scope.addressList[i].typeValue = getAddressTypeValue($scope.addressList[i].addressType);
		};
		
		resetAddressType();
		
	}
    
	function resetAddressType(){
		
		var output = [];
		
		angular.forEach($scope.types, function(value, key) {
			if (!typeExists (value.id)){
				output.push(value);
			}
		});
		$scope.types = angular.copy(output);
	}
	
	function typeExists (type){
		
		var exists = false;
		
		for (var i = 0; i<$scope.addressList.length; i++){
			if( $scope.addressList[i].addressType == type){
				exists = true;
				break;
			};
		};
		
		return exists;
	}
	
	function getAddressTypeValue(type){
		var typeValue = "";
		angular.forEach($scope.types, function(value, key) {
			if (value.id == type){
				typeValue = value.value;
			}
		});
		
		return typeValue;
	}
	
	$scope.setAddress = function(address){
		
		console.log($scope.selectedAddress);
		var type = $scope.address.addressType;
		$scope.address = address;
		$scope.address.addressType = type;
	}
	
	init();
	
	$scope.ok = function () {
		var output = angular.copy($scope.address);
		delete output.$$hashKey;
		$uibModalInstance.close(output); // Put the result here
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	// validate 
	$scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	$scope.alphaChar=function($event)
	   {
		   	var regex = new RegExp("^[a-zA-Z ]+$");
		    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (regex.test(str)) {
		        return true;
		    }

		    event.preventDefault();
		    return false;
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
	   
	   // Change Title
	    $scope.$watch('selectedAddress1', function(newValue, oldValue) {
	        //console.log('insurerId changed to: ' + newValue);
	    	if ((typeof newValue != "undefined") && (newValue != "")) {
	    		console.log(newValue);
	    		var type = newValue.addressType;
	    		$scope.address = newValue;
	    		$scope.address.addressType = type;
	    	}
	    	
	    });
}]);