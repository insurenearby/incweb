/**
 * 
 */
"use strict";
brokeredgeApp.controller('familyHealthCtrlAia',['$scope','$uibModalInstance','lifefactory','input', function($scope,$uibModalInstance, lifefactory,input){
	
	$scope.family = input;
	$scope.family.alive = "Y";
    
	$scope.healthstates = lifefactory.getMasters("HealthCode");
	$scope.deathCause=lifefactory.getMasters("DeathCause");
	
	$scope.ok = function () {
		var output = {}
		output.member = $scope.family.member;
		output.alive = $scope.family.alive;
		output.age = $scope.family.age;
		output.healthState = $scope.family.healthState;
		output.deathCause = $scope.family.deathCause;
		$uibModalInstance.close(output); // Put the result here
	};

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	// validate 
	$scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	$scope.alphaChar=function($event)
	   {
		   	var inputValue = event.which;
	        // allow letters and whitespaces only.
	        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
	            event.preventDefault(); 
	        }
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
	
}]);