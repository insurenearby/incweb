/**
 * 
 */
brokeredgeApp.component('brokeredgePostPayment',{
	templateUrl:'templates/proposal/PostPayment.html',
	controller:['$scope','$routeParams','$resource',function($scope,$routeParams,$resource){
		
		$scope.paymentResults = {};
		function init()
		{
			
			$scope.insurerId=$routeParams.insurerId;
			$scope.referenceId=$routeParams.referenceId;
			// get Payment Parameters
			getPaymentDetails($scope.referenceId)
			// 
		}
		
		
		function getPaymentDetails(referenceId) {
			var appUrl = "/payment/getPaymentDetails/" + referenceId;
			 console.log(appUrl);
				var input = {
						url: appUrl,
						data : {}
				};
				
				var res = $resource('./DataRequest', [], {
			       save: {
			          method: 'POST'}
			 });
	
			$scope.loading = true;
			var tmp=res.save(JSON.stringify(input));
			tmp.$promise.then(function(data){
				if (data.hasOwnProperty("result")){
					$scope.paymentResults = data.result;
				} else
				{
					$scope.paymentResults.status = "Error";
					$scope.paymentResults.msg = "Error getting payment details";
					
				}
				$scope.loading = false;			
			})
			
		}
		
		
		init()
	}]
})
