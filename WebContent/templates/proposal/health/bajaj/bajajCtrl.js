/**
 * 
 */
"use-strict"
brokeredgeApp.controller('bajajCntrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource',function($scope,brokeredgefactory,$routeParams,$filter,$resource){
	$scope.proposalList=['policy','proposer','insured member','nominee','contact info','medical histery','Life Style','T & C']
	$scope.healthProposalInput='';
	$scope.lifeStyelQuitions=[{
			    "q": "Do You consume alcohol?"},
			    {"q": "Do you consume tobacco?"},
			    {"q":""},
			    { "q": "Do you consume or have you ever consumed any narcotic substance?"}
			  ]
	
	$scope.medicalQuitions=[
	                        {"q":"Within the last 2 years have you consulted a doctor or healthcare professional? (other than Preventive or Pre-Employment Health Check-up)?","ans":"N","status":[]},
	                        {"q":"Within the last 2 years have you undergone any detailed investigation other than Preventive or Pre-Employment Health Check-up (e.g. X-ray, CT Scan, biopsy, MRI, Sonography, etc)?","ans":"N","status":[]},
	                        {"q":"Within the last 5 years have you been to a hospital for an operation/medical treatment?","ans":"N","status":[]},
	                        {"q":"Do you take tablets, medicines or drugs on a regular basis?","ans":"N","status":[]},
	                        {"q":"Within the last 3 months have you experienced any health problems or medical conditions which you/proposed insured person have/has not seen a doctor for?","ans":"N","status":[]},
	                        {"q":"Have any of the person(s) proposed to be insured ever suffered from or taken treatment, or been hospitalized for or have been recommended to undergo / take investigations / medication / surgery or undergone a surgery for any of the following - Diabetes; Hypertension; Ulcer / Cyst / Cancer; Cardiac Disorder; Kidney or Urinary Tract Disorder; Disorder of muscle / bone / joint; Respiratory disorder; Digestive tract or gastrointestinal disorder; Nervous System disorder; Mental Illness or disorder, HIV or AIDS?","ans":"N","status":[]},
	                  ];
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.occupations='';
	$scope.qualifications='';
	$scope.nationalitys="";
	$scope.ocupationError="";
	$scope.relationships=""
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}		
	
	
	$scope.init=function()
	{
		var tmp=brokeredgefactory.getAmhi('royal');
		tmp.$promise.then(function(data){
			$scope.healthProposalInput=data;
			console.log($scope.healthProposalInput);
			 $scope.healthProposalInput.productId=$scope.quoteInput.quotes.productId;
			 $scope.policy();
			 $scope.insured();
		})
		$scope.input.lob = $routeParams.lob;
		$scope.input.productType = $routeParams.productType;
		 $scope.quoteInput=brokeredgefactory.base64Decode($routeParams.base64Input);
		 console.log( $scope.quoteInput)
		
		 
		 $scope.getOccupation();
		 $scope.getQualification();
		 $scope.getNationality();
		 $scope.getRelation();
		 
		 $scope.getState();
		 
	}
	$scope.getState=function()
	{
		var input={
				url:'',
				data:{}
		}
		input.url='/master/codes/'+$scope.quoteInput.quotes.productId+'/State';
		var tmp=	brokeredgefactory.getState().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			$scope.states=data['result'];
			console.log($scope.states)
			angular.forEach(data['result'],function(val,key){
					if(val.value==$scope.policyState)
					{
						 $scope.polState=val;
					 	 
					    // $scope.getCity(val.id)
					}
			});
		})
	}
	$scope.changeState=function(state,name)
	{
		if(name=='nominee')
			{
			$scope.cityNominee='';
			$scope.healthProposalInput.proposer.nomineeState=state.id;
			$scope.getCity(state.id,name);
		}else if(name="parmanent")
			{
			$scope.city='';
			$scope.polCity='';
			$scope.healthProposalInput.proposer.address[1].state=state.id;
		 	$scope.getCity(state.id);
			}
		console.log($scope.healthProposalInput.proposer)
	}
	$scope.getCity=function(id,name)
	{
		var input={
				url:'',
				data:{}
		}
		input.url='/master/city/'+$scope.quoteInput.quotes.insurerId+'/'+id;
		var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			if(name=='nominee')
			{
				$scope.cityNominee=data['result'];
				
			}
			else {
				$scope.city=data['result'];
			}
			angular.forEach(data['result'],function(val,key){
					if(val.value==$scope.policyCity)
					{
						    if(name=='nominee')
							{
							$scope.healthProposalInput.proposer.nomineeCity=val.value;
							}else
								{
								 //$scope.polCity=val;
								 
								 $scope.healthProposalInput.proposer.address[1].city=val.value;
								}
						console.log($scope.healthProposalInput.proposer)
					}
					
			});
			
			
		})
	}
	$scope.changeCity=function(city,name)
	{
		//console.log(city);
		if(name=='nominee'){
			$scope.healthProposalInput.proposer.nomineeCity=city.value;
		}
		else
			{
			$scope.healthProposalInput.proposer.address[1].city=city.value;
			}
		
	}
	$scope.getOccupation=function()
	{
		var input={url:'',data:{}}
		input.url='/master/codes/'+$scope.quoteInput.quotes.productId+'/Occupation';
		var tmp=brokeredgefactory.getMasterRsa().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			console.log(data);
			$scope.occupations=data['result'];
		
		})
	}
	$scope.getRelation=function()
	{
		var input={url:'',data:{}}
		input.url='/master/codes/'+$scope.quoteInput.quotes.productId+'/Relationship';
		var tmp=brokeredgefactory.getMasterRsa().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			console.log(data);
			$scope.relationships=data['result'];
		
		})
	}
	
	$scope.getQualification=function()
	{
		
		var input={url:'',data:{}}
		input.url='/master/codes/'+$scope.quoteInput.quotes.productId+'/Qualification';
		var tmp=brokeredgefactory.getMasterRsa().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			console.log(data);
			$scope.qualifications=data['result'];
		
		})
	}
	
	$scope.getNationality=function()
	{
		//Nationality
		var input={url:'',data:{}}
		input.url='/master/codes/'+$scope.quoteInput.quotes.productId+'/Nationality';
		var tmp=brokeredgefactory.getMasterRsa().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
			console.log(data);
			$scope.nationalitys=data['result'];
		
		})
	}
	//validate of 
	$scope.validateOcupation=function()
	{
		if($scope.healthProposalInput.proposer.occupationType=='POLITICIAN')
		{
			$scope.ocupationError="Please contact our nearest branch or kindly chat with our online representative or call us at 1860 425 0000 for more information.";
		}
		else
			{
			$scope.ocupationError="";
			}
	}
	$scope.policy=function(){
		var dateToday=new Date();
		var yearMax=dateToday.getFullYear();
		var monthToday=dateToday.getMonth();
		
		var dayToday=dateToday.getDate()+1;
		$scope.healthProposalInput.policy.policyStartdate=new Date(yearMax,monthToday,dayToday);
		$scope.policyStart();
		var k=$scope.quoteInput.quotes.productName.split('-');
		$scope.healthProposalInput.policy.productType=$scope.input.productType
		$scope.healthProposalInput.policy.serviceTax=$scope.quoteInput.quotes.serviceTax;
		$scope.healthProposalInput.policy.basePremium=parseInt($scope.quoteInput.quotes.totalPremium)-parseInt($scope.quoteInput.quotes.serviceTax)
		$scope.healthProposalInput.policy.premiumPayable=$scope.quoteInput.quotes.totalPremium;
		$scope.healthProposalInput.policy.floaterSi=$scope.quoteInput.quotes.sumInsured;
		$scope.healthProposalInput.policy.numAdults=$scope.quoteInput.healthInput.adult;	
		$scope.healthProposalInput.policy.numChildren=$scope.quoteInput.healthInput.children;
		$scope.healthProposalInput.policy.tenure=$scope.quoteInput.healthInput.tenure;
		$scope.healthProposalInput.policy.planType=k[1];
		if($scope.input.productType)
		{
			$scope.healthProposalInput.policy.floater='Y';
		}
		else{
			$scope.healthProposalInput.policy.floater='N';
		}
		console.log($scope.healthProposalInput.policy)
	}
	$scope.insured=function()
	{
		
		$scope.healthProposalInput.insured=[];
		angular.forEach($scope.quoteInput.member,function(key,val){
			console.log(key.adultChild=='Adult')
			if(key.adultChild=='Adult')
			{
				console.log($filter('date')(new Date(key.dob),'yyyy-MM-dd'))
				$scope.healthProposalInput.insured.push({
				      "type": "A",
				      "title": "",
				      "firstName": "",
				      "lastName": "",
				      "gender":key.gender,
				      "dob": $filter('date')(new Date(key.dob),'yyyy-MM-dd'),
				      "relationshipId": "",
				      "occupationType": "",
				      "designation": "",
				      "height": "",
				      "weight": "",
				      "bmi": "",
				      "pedList": [
				        {
				          "pedName": "medicalQuestionOne",
				          "pedCode": "",
				          "exists": "N"
				        },
				        {
				          "pedName": "medicalQuestionTwo",
				          "pedCode": "",
				          "exists": "N"
				        },
				        {
				          "pedName": "medicalQuestionThree",
				          "pedCode": "",
				          "exists": "N"
				        },
				        {
				          "pedName": "medicalQuestionFour",
				          "pedCode": "",
				          "exists": "N"
				        },
				        {
				          "pedName": "medicalQuestionFive",
				          "pedCode": "",
				          "exists": "N"
				        },
				        {
				          "pedName": "medicalQuestionSix",
				          "pedCode": "",
				          "exists": "N"
				        }
				      ],
				      "lifestyleInfo": [
				        {
				          "name": "alcohol",
				          "value": "N",
				          "quantity": "",
				          "noOfYears": ""
				        },
				        {
				          "name": "smoking",
				          "value": "N",
				          "quantity": "",
				          "noOfYears": "N"
				        },
				        {
				          "name": "anyOtherSubstance",
				          "value": "N",
				          "quantity": "",
				          "noOfYears": ""
				        },
				        {
				          "name": "narcotics",
				          "value": "N",
				          "quantity": "",
				          "noOfYears": ""
				        }
				      ]
				    })
				
			}
			else if(key.adultChild=='Child')
			{
				$scope.healthProposalInput.insured.push( {"type": "C",
					      "title": "",
					      "firstName": "",
					      "lastName": "",
					      "gender": key.gender,
					      "dob": $filter('date')(new Date(key.dob),'yyyy-MM-dd'),
					      "relationshipId": "",
					      "occupationType": "",
					      "height": "",
					      "weight": "",
					      "pedList": [
					        {
					          "pedName": "medicalQuestionOne",
					          "pedCode": "",
					          "exists": "N"
					        },
					        {
					          "pedName": "medicalQuestionTwo",
					          "pedCode": "",
					          "exists": "N"
					        },
					        {
					          "pedName": "medicalQuestionThree",
					          "pedCode": "",
					          "exists": "N"
					        },
					        {
					          "pedName": "medicalQuestionFour",
					          "pedCode": "",
					          "exists": "N"
					        },
					        {
					          "pedName": "medicalQuestionFive",
					          "pedCode": "",
					          "exists": "N"
					        },
					        {
					          "pedName": "medicalQuestionSix",
					          "pedCode": "",
					          "exists": "N"
					        }
					      ]
					    })
			}
		});
		console.log($scope.healthProposalInput.insured);
	}
	$scope.calulateBmi=function()
	{
		angular.forEach($scope.healthProposalInput.insured,function(key,val){
			
			if(key.type=='A')
				{
				var heighttm=$scope.healthProposalInput.insured[val].height*0.01;
				var bmi=$scope.healthProposalInput.insured[val].weight/(heighttm*heighttm);
				$scope.healthProposalInput.insured[val].bmi=parseInt(bmi)
				}
			
		})
	}
	$scope.WeightKg=function(form,wight,height,index)
	{
	
		
		
		var heighttm=height*0.01;
		var bmi=wight/(heighttm*heighttm);
		if( (bmi<=17.5 || bmi>=24.9))
		{
			form.weightKg.$setValidity('required',false);
		}
		else
		{
			form.weightKg.$setValidity('required',true);
		}
	}
	//Nominee
	$scope.nomineeDobCalculateAge=function(dob)
	{
		$scope.healthProposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	//contact
	$scope.contact=function()
	{
		//$scope.healthProposalInput.proposer.contacts[0].contactText=
		$scope.healthProposalInput.proposer.address[0].addressLine1=$scope.healthProposalInput.proposer.address[1].addressLine1;
		$scope.healthProposalInput.proposer.address[0].addressLine2=$scope.healthProposalInput.proposer.address[1].addressLine2;
		$scope.healthProposalInput.proposer.address[0].addressLine3=$scope.healthProposalInput.proposer.address[1].addressLine3;
		$scope.healthProposalInput.proposer.address[0].city=$scope.healthProposalInput.proposer.address[1].city;
		$scope.healthProposalInput.proposer.address[0].state=$scope.healthProposalInput.proposer.address[1].state;
		$scope.healthProposalInput.proposer.address[0].pincode=$scope.healthProposalInput.proposer.address[1].pincode;
		
	}
	$scope.checkMedicalQuition=function()
	{
		angular.forEach($scope.healthProposalInput.insured,function(key,val){
			angular.forEach($scope.healthProposalInput.insured[val].pedList,function(k,l){
				if(k.exists=='Y')
				{
					$scope.errorMessageMedical="You can't buy policy online"+$scope.healthProposalInput.insured[val].firstName+" has medical condition";
				}
			})
		})
	}
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "second":
			
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			
			break;
		case "third":
			$scope.active =2;
			$scope.tabStatus.secondComplete = 'done';
			
			break;
		case "fourth":
			$scope.calulateBmi();
			$scope.active =3;
			$scope.tabStatus.thirdComplete='done';
			break;
		
		case "fifth":
			
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';
			
			break;
		case "sixth":
			
					$scope.active =5;
					$scope.tabStatus.fifthComplete = 'done';
					$scope.contact();
			         break;
		case "seventh":
			
			$scope.active =6;
			$scope.tabStatus.sixthComplete='done';
			break;
		case "eight":
			$scope.healthProposalInput.policy.policyStartdate=$filter('date')(new Date($scope.healthProposalInput.policy.policyStartdate),'yyyy-MM-dd');
			console.log(JSON.stringify($scope.healthProposalInput))
			$scope.active=7;
			$scope.tabStatus.seventhComplete='done';
		}
		
		
	}
	$scope.makePayment=function()
	{
		var input={url:'',data:{}}
		input.url='/submitProposal/health/'+$scope.quoteInput.quotes.insurerId+'/'+$scope.healthProposalInput.productId;
		input.data=$scope.healthProposalInput;
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
				if(data.error)
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=str.split(";");
					console.log($scope.proposalError)
					//console.log(data['error'])
				}
				else
					{
					console.log(data)
					}
			
		
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
	}
	$scope.init();
	
	$scope.policyStart=function()
	{
		var date=new Date($scope.healthProposalInput.policy.policyStartdate);
		$scope.healthProposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+1,date.getMonth(),date.getDate()-1),'yyyy-MM-dd')
		
	}
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
	//date picker
	// Date picker
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    $scope.nomineeDob={
    		maxDate: new Date(newYearMax, newMaxMonthToday-3, newdayToday),
        	minDate: new Date(newYearMin-97, monthToday, newdayToday)
    }
    $scope.proposeDob={
    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
    	minDate: new Date(newYearMin-60, monthToday, newdayToday)
    }
	$scope.policyStartDate={
    	maxDate: new Date(newYearMax, newMaxMonthToday+1, newdayToday),
    	minDate: new Date(newYearMin, monthToday, newdayToday+1)
    }
	
	$scope.opened = [];
   $scope.open = function($event,$index) {
            $scope.opened[$index] = true;
        };
	 $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
}])