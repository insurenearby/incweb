/**
 * 
 */
"use-strict"
brokeredgeApp.controller('religareHealthCtrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','paymentService',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,paymentService){
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polCity = {};
	$scope.corCity = {}
	$scope.nomCity={};
	$scope.polCities = [];
	$scope.corCities = []
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}

	$scope.pageErrors = {
			insuredError: false,
			medicalError: false,
			insuredErrorText : "",
			medicalErrorText : ""
	}
	
	$scope.insureds = [];
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    var ffRel = ["SELF", "SPSE", "SONM", "MOTH", "FATH","UDTR" ];
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-60, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax -18, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
    $scope.illnessDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-60, monthToday, dayToday),
	    	minMode : "month",
	    	mode : "month"	
    }
    
    $scope.opened= {
        	proposerDob : false,
        	nomineeDob : false,
        	appointeeDob : false,
        	policyStartDate : false
    };
	$scope.openQ = function(seq , i) {
		
		var key = seq + "_" + i;
		$scope.opened[key] = true;
	};
   $scope.open = function(name) {
            $scope.opened[name] = true;
   };
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.formatPedDate = "yyyy-MM";
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
	
   function init() {
		$scope.input.lob = "Health";
		$scope.input.productType = $scope.proposalInput.productType;
		
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			switch (addressType){
			case "C":
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCityCode = value.cityCode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStateCode = value.stateCode;
				$scope.proposalInput.proposer.corrPincode = value.pincode;
				break;
			case "P":
				$scope.proposalInput.proposer.policyAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.policyAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.policyCity = value.city;
				$scope.proposalInput.proposer.policyCityCode = value.cityCode;
				$scope.proposalInput.proposer.policyState = value.state;
				$scope.proposalInput.proposer.policyStateCode = value.stateCode;
				$scope.proposalInput.proposer.policyPincode = value.pincode;
				break;
			}
		});
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		getMaster($scope.proposalInput.productId, "Relationship");
		getMaster($scope.proposalInput.productId, "NomineeRel");
		getQuestions($scope.proposalInput.productId, "PedList")	;	
		
		getCityFromPincode('P', $scope.proposalInput.proposer.policyPincode);
		$scope.polCity.cityCode = $scope.proposalInput.proposer.policyCityCode;
		$scope.polCity.cityName = $scope.proposalInput.proposer.policyCity;
		$scope.polCity.stateCode = $scope.proposalInput.proposer.policyStateCode;
		$scope.polCity.state = $scope.proposalInput.proposer.policyState;
		$scope.setCityState('P', $scope.polCity);
		getCityFromPincode('C', $scope.proposalInput.proposer.corrPincode);
		$scope.corCity.cityCode = $scope.proposalInput.proposer.corrCityCode;
		$scope.corCity.cityName = $scope.proposalInput.proposer.corrCity;
		$scope.corCity.stateCode = $scope.proposalInput.proposer.corrStateCode;
		$scope.corCity.state = $scope.proposalInput.proposer.corrState;
		$scope.setCityState('C', $scope.corCity);
		$scope.proposalInput.policy.tcAgree = "N";
		
		checkRelationship();
				
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalQuestions();
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;	
		
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates(){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob.substr(0, 10));	
		}
		
		if (typeof($scope.proposalInput.proposer.nomineeDob) != "undefined" && $scope.proposalInput.proposer.nomineeDob != ""){
			$scope.proposalInput.proposer.nomineeDob = new Date($scope.proposalInput.proposer.nomineeDob.substr(0, 10));
		}
		
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeDob = new Date($scope.proposalInput.proposer.appointeeDob.substr(0, 10));
		}
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
			$scope.medicalQuestions[i].exists = "";
			if ($scope.medicalQuestions[i].reponsetype == "YESNOText"){
				$scope.medicalQuestions[i].dateNeeded = "Y";
			} else
			{
				$scope.medicalQuestions[i].dateNeeded = "N";
			}
			$scope.medicalQuestions[i].detailNeeded = "N";
			if ($scope.medicalQuestions[i].code == "504" || $scope.medicalQuestions[i].code == "210" || $scope.medicalQuestions[i].code == "201"){
				$scope.medicalQuestions[i].detailNeeded = "Y";
			}
		}
		
		$scope.insureds = [];
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			
			angular.forEach(insured.pedList,function(item,key){
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						if ($scope.medicalQuestions[i].exists == ""){
							$scope.medicalQuestions[i].exists = item.exists;
						}
						if (item.exists == 'Y'){
							$scope.medicalQuestions[i].exists = "Y";
							angular.forEach(item.pedData,function(illness,key){
								var detail = {};
								detail.insuredId = insuredId.toString();
								detail.insuredDob = new Date(insured.dob)
								detail.insuredName = insuredName;
								detail.code = code;
								detail.conditionSince = new Date(illness.conditionSince + "-01")
								detail.details = illness.details;
								$scope.medicalQuestions[i].details.push(detail);
							});
							
						}
					}
				}
				
			})
		})
		console.log($scope.medicalQuestions);
	}
	
	$scope.AddMedicalRow = function(code) {
		
		var detail = {};
		detail.insuredId = "";
		detail.conditionSince = "";
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.push(detail);
				break;
			}
		}
		
		$scope.medicalPageComplete();
		
	}
	
	$scope.DeleteMedicalRow = function(code, j){
		
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.splice(j);
				break;
			}
		}
		
		$scope.medicalPageComplete();
	}
	
	function ProcessMedicalQuestions(){
		
		// initialize the medical detail Array for the insured
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			angular.forEach(insured.pedList,function(item,key2){
				item.pedData = []; // Use an array even if there is only one item
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.medicalQuestions,function(question,key){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(medDetail,key){
					 var code = question.code;
					 var insuredId = medDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key1){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.pedList,function(item,key2){
								 if (item.pedCode == code){
									 item.exists = "Y";
									 var pedDetails = {}
									 if (medDetail.conditionSince) {
										 pedDetails.conditionSince = $filter('date')(new Date(medDetail.conditionSince),'yyyy-MM'); 
									 }
									 if (medDetail.details) {
										 pedDetails.details = medDetail.details; 
									 }
						    	  	 
									 item.pedData.push(pedDetails);								 
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	

	
		function getCityFromPincode (type, pincode){
			
			var re = new RegExp("^[0-9]{6}$");
			if (re.test(pincode)){
				
				var input={url:'',data:{}}
		    	
				input.url="/master/getDetailsForPincode/" + $scope.proposalInput.insurerId + "/" + pincode;
				
		    	var res = $resource('./DataRequest', [], {
			          save: {
			             method: 'POST'}
			    });
				
				var tmp = res.save(input);
				tmp.$promise.then(function(data){
					if (data.hasOwnProperty("result")){
						
						 if (type == "P"){
							 $scope.polCities = data.result;
						 } else
						 {
							 $scope.corCities = data.result; 
						 }
					}
					
					
				})		
			}
		}
		
		$scope.setCityState = function(type, area){
			
			console.log(area);
			if (typeof(area) != "undefined") {

				 if (type == "P"){
					 $scope.proposalInput.proposer.policyCity = area.cityName;
					 $scope.proposalInput.proposer.policyCityCode = area.cityCode;
					 $scope.proposalInput.proposer.policyState = area.stateCode;
					 $scope.proposalInput.proposer.policyStateCode = area.stateCode;
					 
				 } else
				 {
					 $scope.proposalInput.proposer.corrCity = area.cityName;
					 $scope.proposalInput.proposer.corrCityCode = area.cityCode;
					 $scope.proposalInput.proposer.corrState = area.stateCode;
					 $scope.proposalInput.proposer.corrStateCode = area.stateCode;
				 }
			}
		}
		
		function getMaster(productId, masterId){
			$scope.insurerMaster[masterId] = [];
			var tmp=brokeredgefactory.getMaster(productId, masterId);
			tmp.$promise.then(function(data){
				//console.log(data);
				$scope.insurerMaster[masterId]= data['result'];
				if (masterId == "Relationship" || masterId == "NomineeRel"){
					addGendertoRel(masterId);					
				}
				//console.log($scope.insurerMaster);
			})
		}

		
		function getQuestions(productId, type){
			var questions = [];
			 var input = {
		    			url: '/master/getQuestions/' + productId + "/" + type,
		    			data : {}
		    	};
		        
				var res = $resource('./DataRequest', [], {
			          process: {
			             method: 'POST'}
			    });
			var tmp=res.process(input);
			tmp.$promise.then(function(data){
				//console.log(data);
				if (data.hasOwnProperty("result")){
					if (type == "PedList"){
						$scope.medicalQuestions = data.result;
						initializeMedicalQuestions();
					}
					
				}
				//console.log($scope.insurerMaster);
			})
			
		}
	
		$scope.getMasterValue = function(masterName, masterId){
			// 
			var masterValue = masterId;
			var values = $scope.insurerMaster[masterName];
			
			if (typeof(values) != 'undefined'){
				for (var i = 0; i<values.length; i++){
					if (values[i].id == masterId){
						masterValue = values[i].value; 
						break;
					}
				}
			}
			return masterValue; 
		}
	
		$scope.getQuestionText = function(type, id){
			// 
			var questionText = "";
			var values = [];
			if (type == "PedList"){
				values = $scope.medicalQuestions;
			}
	
					
			if (typeof(values) != 'undefined'){
				for (var i = 0; i<values.length; i++){
					if (values[i].id == id){
						questionText = values[i].value; 
						break;
					}
				}
			}
			return questionText; 
		}
	
		function addGendertoRel (masterId){
			var records = [];
			var items = angular.copy($scope.insurerMaster[masterId]);
			angular.forEach(items,function(val,key){
				var record = val;
				record.gender = getRelGender(val.value);
				var addRecord = true;
				if ($scope.proposalInput.policy.productType == "FF" ) {
					if (!isvalidFFRel(val.id) && masterId == 'Relationship') {
						addRecord = false;
					}
				}
				if (addRecord) {
					records.push(record);
				}
				
			});
			$scope.insurerMaster[masterId] = angular.copy(records);
		}
		
		
	
		function isvalidFFRel(id){
			
			var valid = false;
			for (var i=0; i<ffRel.length; i++) {
				if (ffRel[i] == id){
					valid = true;
					break;
				}
			}
			
			return valid;
		}
	
		function checkRelationship ()
	     {
			$scope.pageErrors.insuredError= false;
			$scope.pageErrors.insuredErrorText = "";
			$scope.proposalInput.proposer.title= "";
			$scope.proposalInput.proposer.firstName = "";
			$scope.proposalInput.proposer.middleName = "";
			$scope.proposalInput.proposer.lastName= "";
			$scope.proposalInput.proposer.gender = "";
			$scope.proposalInput.proposer.proposerDob =  "";
			$scope.proposalInput.proposer.occupationType= "";
			$scope.proposalInput.proposer.maritalStatus= "";
			$scope.proposalInput.proposer.designation = "";
			$scope.proposalInput.proposer.business = "";
			$scope.isProposerInsured = false;
			var adultRels = [];
			$scope.proposalInput.proposer.isPrimaryInsured = "N";
			$scope.proposalInput.proposer.IsProposerInsured = "N";
	    	var selfCount = 0;
	    	angular.forEach($scope.proposalInput.insured, function(val,key){
	    		
				var rel = val.relationshipId;
				
				if (val.type == "A"){
					if (rel == "GDAU" || rel == "GSON" || rel == "NEPH" || rel == "NIEC"|| rel == "SONM"|| rel == "UDTR" || rel == "NBON"){
						$scope.pageErrors.insuredError= true;
		   				$scope.pageErrors.insuredErrorText = "Invalid Adult RelationShip";
					}
					
					if ($scope.proposalInput.policy.numAdults == 2){
						if (val.maritalStatus == "Single"){
							$scope.pageErrors.insuredError= true;
							$scope.pageErrors.insuredErrorText = "Adults Must be Married";
						}
					}
					
					adultRels.push(rel);
				} else
					
				{
					if (rel != "GDAU" && rel != "GSON" && rel != "NEPH" && rel != "NIEC" && rel != "SONM" && rel != "UDTR" && rel != "NBON"){
						$scope.pageErrors.insuredError= true;
		   				$scope.pageErrors.insuredErrorText = "Child can only be Daughter, Grand Daughter, Grand Son & Son";
					}
					if (val.maritalStatus != "Single"){
						$scope.pageErrors.insuredErrorText = "Children must be Unmarried";
					}
				}
				
				
		   		switch (rel) {
		   		case "SELF":
		   			$scope.proposalInput.proposer.title= val.title;
	    			$scope.proposalInput.proposer.firstName = val.firstName;
	    			$scope.proposalInput.proposer.middleName = val.middleName;
	    			$scope.proposalInput.proposer.lastName= val.lastName;
	    			$scope.proposalInput.proposer.gender = val.gender;
	    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
	    			$scope.proposalInput.proposer.occupationType=val.occupationType;
	    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
	    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
	    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
	    			if (val.idType == "PAN"){
	    				$scope.proposalInput.proposer.pan = val.idNumber
	    			}
	    			if (val.type == "C"){ // Proposer cannot be a child.
	    				$scope.pageErrors.insuredError= true;
	    				$scope.pageErrors.insuredErrorText = "Child cannot be a proposer";
	    			}
	    			selfCount++;
	    			$scope.isProposerInsured = true;
	    			console.log($scope.proposalInput.proposer);
		   			break;
		   		case "FATH":
		   		case "MOTH":
		   			parentIncluded = true;	   			
		   			break;
		   		}	   
	    	});
	    	if (selfCount>1) {
				$scope.pageErrors.insuredError= true;
				$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
	    	}
	    	
		   	adultRels = $filter('orderBy')(adultRels);
		   	
		   	
		   	if (!$scope.pageErrors.insuredError && $scope.proposalInput.policy.productType == "FF" && $scope.proposalInput.policy.numAdults == 2){
		   		
		   		
		   		$scope.pageErrors.insuredError= true;
				$scope.pageErrors.insuredErrorText = "Adults Must be Husband/Wife";
				
				
				if (adultRels[0] == "SELF" && (adultRels[1] == "SPSE")) {
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}
				
				if (adultRels[1] == "SELF" && (adultRels[0] == "SPSE")) {
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

				if (adultRels[0] == "FATH"  && adultRels[1] == "MOTH" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}

				if (adultRels[0] == "FLAW"  && adultRels[1] == "MLAW" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}

				if (adultRels[0] == "GFAT"  && adultRels[1] == "GMOT" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}

				if (adultRels[0] == "DLAW"  && adultRels[1] == "SONM" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}

				if (adultRels[0] == "UDTR"  && adultRels[1] == "SLAW" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}

				if (adultRels[0] == "MDTR"  && adultRels[1] == "SIST" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}

				if (adultRels[0] == "BOTH"  && adultRels[1] == "MMBR" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}

				if (adultRels[0] == "MANT"  && adultRels[1] == "MUNC" )
					{
						$scope.pageErrors.insuredError= false;
						$scope.pageErrors.insuredErrorText = "";
					}
				
				if (adultRels[1] == "FATH"  && adultRels[0] == "MOTH" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

			if (adultRels[1] == "FLAW"  && adultRels[0] == "MLAW" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

			if (adultRels[1] == "GFAT"  && adultRels[0] == "GMOT" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

			if (adultRels[1] == "DLAW"  && adultRels[0] == "SONM" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

			if (adultRels[1] == "UDTR"  && adultRels[0] == "SLAW" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

			if (adultRels[1] == "MDTR"  && adultRels[0] == "SIST" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

			if (adultRels[1] == "BOTH"  && adultRels[0] == "MMBR" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}

			if (adultRels[1] == "MANT"  && adultRels[0] == "MUNC" )
				{
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}
		   	}	    	
	    	
	     }
		
		
		function setRelationship(){

	    	angular.forEach($scope.proposalInput.insured, function(val,key){
	    		
	    		if (val.relationshipId == "SELF"){
	    			
	    			$scope.proposalInput.proposer.title= val.title;
	    			$scope.proposalInput.proposer.firstName = val.firstName;
	    			$scope.proposalInput.proposer.middleName = val.middleName;
	    			$scope.proposalInput.proposer.lastName= val.lastName;
	    			$scope.proposalInput.proposer.gender = val.gender;
	    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
	    			$scope.proposalInput.proposer.occupationType=val.occupationType;
	    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
	    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
	    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
	    			$scope.isProposerInsured = true;
	    			console.log($scope.proposalInput.proposer);
	    		}
	    	});
		}
		
		$scope.medicalPageComplete = function(){
			
			$scope.pageErrors.medicalError = false;
			$scope.pageErrors.medicalErrorText = "";
			var questionId = 0;
			angular.forEach($scope.medicalQuestions, function(question,key){
				question.errorText = "";
				if (question.exists == "Y" && question.dateNeeded == "Y"){
					if (question.details.length == 0 ){
						$scope.pageErrors.medicalError = true;
						$scope.pageErrors.medicalErrorText = "Please Provide the details of the Medical condition";
						question.errorText = "Please Provide the details of the Medical condition";
					} else
					{
						angular.forEach(question.details,function(illness,key){
							var illnessYear = illness.conditionSince.getFullYear();
							var illnessMonth = illness.conditionSince.getMonth();
							var dobYear = illness.insuredDob.getFullYear();
							var dobMonth = illness.insuredDob.getMonth();
							if (illnessYear<dobYear || (illnessYear==dobYear && illnessMonth<dobMonth)){
								$scope.pageErrors.medicalError = true;
								$scope.pageErrors.medicalErrorText = "Date of illness cannot be before Date of Birth";
								question.errorText = "Date of illness cannot be before Date of Birth";
							}
						})
					}
				}
			});
			

		}
		
		$scope.setGender=function(val)
		{
			if(val=='MR') {
				$scope.proposalInput.proposer.gender='M';
			}
			else if(val=='MS')
			{
				$scope.proposalInput.proposer.gender = "F";
			}
		}
		
		function setCover (covers){
			angular.forEach(covers,function(val,key){
				var cover = {};
				cover.name = val;
				$scope.proposalInput.covers.push(cover);			
			})
		}
	
	
		$scope.calulateBmi=function()
		{
			angular.forEach($scope.proposalInput.insured,function(key,val){
				
				if(key.type=='A')
					{
					var heighttm=$scope.proposalInput.insured[val].height*0.01;
					var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
					$scope.proposalInput.insured[val].bmi=parseInt(bmi)
					}
				
			})
		}
	
		
		function getRelGender(rel){
			var gender = "MF";
			
			rel = rel.toLowerCase();
			
			if(rel=='wife'  || rel=='aunt'  || rel=='daughter'  || rel=='daughter in law'  || rel=='grand daughter'  || rel=='grand mother'  || rel=='mother in law'  || rel=='mother'   || rel=='niece'  || rel=='sister in law'  || rel=='sister'){
				gender = "F";
					
			} else if (rel=='brother in law'  || rel=='brother'  || rel=='father in law'  || rel=='father'  || rel=='grand father'  || rel=='grand son'  || rel=='husband'  || rel=='nephew'   || rel=='son in law'  || rel=='son'  || rel=='uncle'){
				gender = "M";
			}
			return gender;
		}
	
		//Nominee
		$scope.calculateAge=function(dob)
		{
			$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
		}
		
		
		
		//next tab 
		//next section
		$scope.back=function(index)
		{
			$scope.active=index;
		}
		$scope.nextSection = function(section)
		{
			
			switch (section){
			case "0":
				$scope.calulateBmi();
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active=0;
				$scope.tabStatus.firstComplete = 'done';
				break;
			case "1":
				setRelationship();
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =1;
				$scope.tabStatus.secondComplete = 'done';
				break;
			case "2":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =2;
				$scope.tabStatus.thirdComplete='done';			
				break;
			case "3":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =3;
				$scope.tabStatus.fourthComplete='done';
				break;
			case "4":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =4;
				$scope.tabStatus.fifthComplete = 'done';
				
				break;
			}
			
			saveApplicationData ();
		}
		
		function cleanArray(actual) {
			  var newArray = new Array();
			  for (var i = 0; i < actual.length; i++) {
			    if (actual[i]) {
			      newArray.push(actual[i]);
			    }
			  }
			  return newArray;
			}
		
		function populateCoverData(){
			
			angular.forEach($scope.covers,function(value,key){
				
				if(value.isSelected == "Y")
					{
					 var cover = {};
					 cover.name = value.displayName;
					 $scope.proposalInput.covers.push(cover);
					}
				
			})
			
		}
		
		$scope.samePolicyCorrAddress=function( flag)
		{
			
			if (flag == "Y"){
				CopyPolicyAddress();
			} else
			{
				$scope.proposalInput.proposer.corrAddressLine1= "";
				$scope.proposalInput.proposer.corrAddressLine2= "";
				$scope.proposalInput.proposer.corrAddressLine3= "";
				$scope.proposalInput.proposer.corrLandmark= "";
				$scope.proposalInput.proposer.corrStateCode= "";
				$scope.proposalInput.proposer.corrState = "";
				$scope.proposalInput.proposer.corrCityCode = "";
				$scope.proposalInput.proposer.corrCity = "";
				$scope.proposalInput.proposer.corrPincode = "";
			}
		}
		
		function CopyPolicyAddress(){

			$scope.proposalInput.proposer.corrAddressLine1=$scope.proposalInput.proposer.policyAddressLine1;
			$scope.proposalInput.proposer.corrAddressLine2=$scope.proposalInput.proposer.policyAddressLine2;
			$scope.proposalInput.proposer.corrLandMark=$scope.proposalInput.proposer.policyLandMark;
			$scope.proposalInput.proposer.corrStateCode=$scope.proposalInput.proposer.policyStateCode;
			$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
			$scope.proposalInput.proposer.corrCityCode =$scope.proposalInput.proposer.policyCityCode;
			$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
			$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
		}

	
		function setpolicyAddress(){
	 		
			if ($scope.proposalInput.proposer.policyRegAddressSame == 'Y'){
				CopyPolicyAddress();
			}
			var addresses = [];
			var address = {}
			address.addressType = "C";
	 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
			address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
			address.addressLine3 = $scope.proposalInput.proposer.corrArea
			address.city = $scope.proposalInput.proposer.corrCity;
			address.cityCode = $scope.proposalInput.proposer.corrCityCode;
			address.state = $scope.proposalInput.proposer.corrState;
			address.stateCode = $scope.proposalInput.proposer.corrStateCode;
			address.pincode = $scope.proposalInput.proposer.corrPincode;
			addresses.push(address);
			
			var address = {}
			address.addressType = "P";
	 		address.addressLine1 = $scope.proposalInput.proposer.policyAddressLine1;
			address.addressLine2 = $scope.proposalInput.proposer.policyAddressLine2;
			address.addressLine3 = $scope.proposalInput.proposer.policyArea;
			address.city = $scope.proposalInput.proposer.policyCity;
			address.cityCode = $scope.proposalInput.proposer.policyCityCode;
			address.state = $scope.proposalInput.proposer.policyState;
			address.stateCode = $scope.proposalInput.proposer.policyStateCode;
			address.pincode = $scope.proposalInput.proposer.policyPincode;
			addresses.push(address);
			$scope.proposalInput.proposer.address = angular.copy(addresses);
			
			var contacts = []
			var contact = {};
			contact.contactType = "mobile";
			contact.contactText = $scope.proposalInput.proposer.mobile;
			contacts.push(contact);
			var contact = {};
			contact.contactType = "email";
			contact.contactText = $scope.proposalInput.proposer.email;
			contacts.push(contact);
			$scope.proposalInput.proposer.contacts = angular.copy(contacts);
			
	 	}
	
		$scope.makePayment=function() {
			//Populate Start Date of Policy
			$scope.proposalInput.policy.policyStartdate = $filter('date')(new Date(),'yyyy-MM-dd');
			$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(new Date().getFullYear()+Number($scope.proposalInput.policy.tenure),new Date().getMonth(),new Date().getDate()-1),'yyyy-MM-dd');
			
			if ($scope.proposalInput.policy.floater == 'N'){
				$scope.proposalInput.policy.floaterSi = $scope.proposalInput.policy.sumInsured;
			}
			// Populate Medical and LifeStyle data
			populateCoverData();
			$scope.proposalInput.proposer.nomineeName = $scope.proposalInput.proposer.nomineeFirstName + " " + $scope.proposalInput.proposer.nomineeLastName;
			console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
			
			var healthProposal = angular.copy($scope.proposalInput);
			healthProposal.policy.policyStartdate = $filter('date')(new Date(healthProposal.policy.policyStartdate),'yyyy-MM-dd');
			healthProposal.policy.policyEnddate = $filter('date')(new Date(healthProposal.policy.policyEnddate),'yyyy-MM-dd');
			healthProposal.proposer.proposerDob = $filter('date')(new Date(healthProposal.proposer.proposerDob),'yyyy-MM-dd');
			healthProposal.proposer.nomineeDob = $filter('date')(new Date(healthProposal.proposer.nomineeDob),'yyyy-MM-dd');
			
			var input={url:'',data:{}}
			input.url='/submitProposal/health/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
			input.data=angular.copy(healthProposal);
			var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
			$scope.loading = true;
			tmp.$promise.then(function(data){
				
					if(data.hasOwnProperty("error"))
					{
						$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
						var str=data['error'];
						$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
						console.log($scope.proposalError)
						//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
						if(data['error']=="Premium Mismatch")
						{
						 	
						 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]});
						}
					} else if (data.hasOwnProperty("online_purchase_link")) {
					
						var url = data.online_purchase_link
						+ "?proposalNum=" + data.proposalNum
						+ "&returnURL=" + data.returnURL;
						$window.location.href = url;
						
					} else
					{
						$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
						var str="Unexpected error while submitting proposal";
						$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
						console.log($scope.proposalError)
					}
					$scope.loading = false;
			},
	 		  function(error) {
	 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str="Unexpected error from Insurer while processing the policy";
				$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
				$scope.loading = false;
	 		  })
		}
		
		getApplicationData();
		
		
		$scope.showTC=function(){
			
			var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      controller : 'tcCtrl',
		      templateUrl: 'templates/proposal/health/religare/tc.html',
		      size: 'lg'
			});
			modalInstance.result.then(function (agree) {
		      console.log(agree);
		      if( typeof(agree) != 'undefined')
		      	{
		    	  $scope.proposalInput.policy.tcAgree = agree;
		      	}
	      }, function () {
	          //console.log('Modal dismissed at: ' + new Date());
	      });
			
		}
		
		
		$scope.newPreMiumMismatch=function(item)
		{
			$scope.items=item;
			var modalInstance=$uibModal.open({
				  animation: $scope.animationsEnabled,
			      ariaLabelledBy: 'modal-title',
			      ariaDescribedBy: 'modal-body',
			      templateUrl: 'templates/proposal/premiumModal.html',
			      controller: 'premiumModalCtrl',
			      size: 'md',
			      resolve: {
			    	  'premiums': function () {
			          return $scope.items;
			        }
			      }
			});
			modalInstance.result.then(function (selectedProduct) {
	        $scope.selected = selectedProduct;
	        if($scope.selected!=undefined)
	        	{
	        	//basePremium.
	        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
	        	$scope.makePayment();
	        	
	        	}
	        }, function () {
	            //console.log('Modal dismissed at: ' + new Date());
	        });
		}
		
		
		$scope.backPreviousPage=function()
		{
			window.history.back();
		}
		
		$scope.rowWidth=function(a)
		{
			return 100/parseInt(a);
		}
		
		$scope.filterValue = function($event){
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	     };
	     
	     $scope.appointeeAge=function(dob) {
	 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
	 	 }
	     
	     // Change State
	     $scope.$watch('proposalInput.proposer.policyPincode', function(newValue, oldValue) {
	         
	     	if ((typeof newValue != "undefined")&& (newValue != "")) {
	     		getCityFromPincode("P", newValue);
	     	}
	     	
	     });
	     
	     $scope.$watch('proposalInput.proposer.corrPincode', function(newValue, oldValue) {
	         
	     	if ((typeof newValue != "undefined")&& (newValue != "")) {
	     		getCityFromPincode("C", newValue);
	     	}
	     	
	     });
	     
	  // Watch function
	     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
	         
	         if ((typeof newValue != "undefined")&& (newValue != "")) {
	         	
	         	var date=new Date(newValue);
	     		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+Number($scope.proposalInput.policy.tenure),date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
	         }
	     	
	     });
     
}])