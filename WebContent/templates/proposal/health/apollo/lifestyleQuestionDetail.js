/**
 * 
 */
"use strict";
brokeredgeApp.controller('rsaLSQuestionDetailCtrl',['$scope','$uibModalInstance', '$filter', 'input', function($scope,$uibModalInstance, $filter, input){
	

    console.log(input);
	$scope.question = {};
	$scope.question.questionId = input.questionId;
    $scope.insureds = input.insureds;
    $scope.question.text1 = "Quantity";

	switch (input.questionId){
	case "alcohol":
		$scope.question.text1 = "Bottle/ml per Week";
		break;
	case "smoking":
		$scope.question.text1 = "Number per Day";
		break;
	default:
		$scope.question.text1 = "Quantity";
		break;
	}
		
	$scope.ok = function () {
		
		var output = {};
		
		output.questionId = $scope.question.questionId;
		console.log($scope.question.text1);
		output.insuredId = $scope.question.insuredId;
		output.insuredName = $scope.insureds[$scope.question.insuredId-1].name;
		output.quantity = $scope.question.quantity;
		output.noOfYears = $scope.question.noOfYears;
		output.action = "add";
		$uibModalInstance.close(output); // Put the result here
	};

	$scope.cancel = function () {
		output.action = "cancel";
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	// validate 
	$scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	$scope.alphaChar=function($event)
	   {
		   		var regex = new RegExp("^[a-zA-Z ]+$");
		    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (regex.test(str)) {
		        return true;
		    }

		    event.preventDefault();
		    return false;
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
}]);