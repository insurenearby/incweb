/**
 * 
 */
"use-strict"
brokeredgeApp.controller('hdfcHealthCtrl',['$scope','brokeredgefactory','FormSubmitter','$routeParams','$filter','$resource','$uibModal','$http','$window','paymentService',function($scope,brokeredgefactory,FormSubmitter,$routeParams,$filter,$resource,$uibModal,$http,$window,paymentService){
	
	$scope.isPPC = false;
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polState={};
	$scope.polCity={};
	$scope.nomState={};
	$scope.nomCity={};
	$scope.polCities = [];
	$scope.nomCities = [];
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	
	$scope.pageErrors = {
			
			insuredError: false,
			medicalError: false,
			insuredErrorText : "",
			medicalErrorText : ""
	}
	
	
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}
	
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
       
    $scope.opened= {
        	proposerDob : false,
        	nomineeDob : false,
        	appointeeDob : false,
        	policyStartDate : false
    };
	$scope.open1 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
   $scope.open = function(name) {
            $scope.opened[name] = true;
   };
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
    

	
	function init() {
		$scope.input.lob = "Health";
		$scope.input.productType = $scope.proposalInput.productType;
		initializeMedicalQuestions();
		$scope.checkMedicalStatus();
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			switch (addressType){
			case "C":
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				value.corrAddressLine3 = $scope.proposalInput.proposer.corrArea = value.addressLine3;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCityCode = value.citycode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStatecode = value.statecode;
				$scope.proposalInput.proposer.corrPincode = value.pincode;
				break;
			}
		});
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		getMaster($scope.proposalInput.productId, "State");
		getMaster($scope.proposalInput.productId, "Realtionship"); // Keep the misspelling
		getMaster($scope.proposalInput.productId, "NomineeRel");
		getQuestions($scope.proposalInput.productId, "PedList")	;
		
		$scope.polState.id = $scope.proposalInput.proposer.corrStateCode;
		$scope.polState.value = $scope.proposalInput.proposer.corrState;
		$scope.getCity($scope.polState,'policy');
		$scope.polCity.id = $scope.proposalInput.proposer.corrCityCode;
		$scope.polCity.value = $scope.proposalInput.proposer.corrCity;
		$scope.proposalInput.policy.tcAgree = "N";
		
	}
	

	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalQuestions();
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates (){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob);	
		}
		
		if (typeof($scope.proposalInput.proposer.nomineeDob) != "undefined" && $scope.proposalInput.proposer.nomineeDob != ""){
			$scope.proposalInput.proposer.nomineeDob = new Date($scope.proposalInput.proposer.nomineeDob);
		}
		
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeDob = new Date($scope.proposalInput.proposer.appointeeDob);
		}
		
		
		
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
		}
		
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			insured.ped = "";
			angular.forEach(insured.pedList,function(item,key){
				
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						$scope.medicalQuestions[i].exists = item.exists;
						angular.forEach(item.pedText,function(illness,key){
							var detail = angular.copy(illness);
							detail.insuredId = insuredId;
							detail.insuredName = insuredName;
							detail.questionId = code;
							$scope.medicalQuestions[i].details.push(detail);
						})
					}
				}
			})
		})
		//console.log($scope.medicalQuestions);
	}

	
	function ProcessMedicalQuestions(){
		
		// initialize the medical detail Array
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			angular.forEach(insured.pedList,function(item,key2){
				item.pedText = [];
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.medicalQuestions,function(question,key){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(medDetail,key){
					 var questionId = medDetail.questionId;
					 var insuredId = medDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key1){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.pedList,function(item,key2){
								 if (item.pedCode == questionId){
									 item.exists = "Y";
									 var pedDetails = {}
						    	  	 pedDetails.monthOfDiagnosis = medDetail.monthOfDiagnosis;
									 pedDetails.yearOfDiagnosis = medDetail.yearOfDiagnosis;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 pedDetails.medicationDetail = medDetail.medicationDetail;
									 pedDetails.treatmentOutCome = medDetail.treatmentOutCome;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 item.pedText.push(pedDetails);
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	
	$scope.getCity = function(state,name) {
		
		if(name=='nomadd')
		{
			$scope.proposalInput.proposer.nomineeStateCode=state.id;
			$scope.proposalInput.proposer.nomineeState=state.value;  
		}else if(name=="policy") {
			$scope.proposalInput.proposer.corrStateCode = state.id;
			$scope.proposalInput.proposer.corrState = state.value;
		}
		
		var cities = [];
		var input={
				url:'',
				data:{}
		}
		
	
		if (typeof(state) != "undefined"){
			input.url='/master/city/'+$scope.proposalInput.insurerId+'/'+state.id;
			var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
			tmp.$promise.then(function(data){
				cities=data['result'];
				if(name=='nomadd')
				{
					$scope.nomCities = cities
				}else if(name=="policy") { 
					$scope.polCities = cities
				}
				
			})
		}

	}
	
	$scope.changeCity = function(city,name) {
		if(name=="policy") {
			$scope.proposalInput.proposer.corrCityCode = city.id;
			$scope.proposalInput.proposer.corrCity = city.value;
			
		}else if(name=="nomadd") {
			$scope.proposalInput.proposer.nomineeCityCode = city.id;
			$scope.proposalInput.proposer.nomineeCity =  city.value;
		} 
	}
	
	function getMaster(productId, masterId){
		$scope.insurerMaster[masterId] = [];
		var tmp=brokeredgefactory.getMaster(productId, masterId);
		tmp.$promise.then(function(data){
			//console.log(data);
			$scope.insurerMaster[masterId]= data['result'];
			if (masterId == "Realtionship"){
				addGendertoRel();
			}
			//console.log($scope.insurerMaster);
		})
	}
	

	
	function getQuestions(productId, type){
		var questions = [];
		 var input = {
	    			url: '/master/getQuestions/' + productId + '/' + type,
	    			data : {}
	    	};
	        
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
		var tmp=res.process(input);
		tmp.$promise.then(function(data){
			//console.log(data);
			if (data.hasOwnProperty("result")){
				if (type == "PedList"){
					$scope.medicalQuestions = data.result;
					initializeMedicalQuestions();
				}
				
			}
			//console.log($scope.insurerMaster);
		})
		
	}
	
	$scope.getMasterValue = function(masterName, masterId){
		// 
		var masterValue = masterId;
		var values = $scope.insurerMaster[masterName];
		
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value; 
					break;
				}
			}
		}
		return masterValue; 
	}
	
	$scope.getQuestionText = function(type, id){
		// 
		var questionText = "";
		var values = [];
		if (type == "PedList"){
			values = $scope.medicalQuestions;
		}

				
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == id){
					questionText = values[i].value; 
					break;
				}
			}
		}
		return questionText; 
	}
	
	function addGendertoRel (){
		var records = [];
		angular.forEach($scope.insurerMaster.Realtionship,function(val,key){
			var record = val;
			record.gender = getRelGender(val.value);
			record.type = getRelType(val.value);
			records.push(record);
		});
		$scope.insurerMaster.Realtionship = records;
	}
	
	
	//validate of 
	$scope.validateOcupation=function()
	{
		if($scope.proposalInput.proposer.occupationType=='POLITICIAN')
		{
			$scope.ocupationError="Please contact our nearest branch or kindly chat with our online representative or call us at 1860 425 0000 for more information.";
		}
		else
			{
			$scope.ocupationError="";
			}
	}
	
	$scope.checkRelationShip=function(form)
     {
    	

		$scope.pageErrors.insuredErrorText = "";
		$scope.pageErrors.insuredError = false;
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.maritalStatus= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
    	var selfCount = 0;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "I"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if (val.type == "C"){ // Proposer cannot be a child.
    				$scope.pageErrors.insuredError= true;
    				$scope.pageErrors.insuredErrorText = "Proposer Cannot be child";
    			}
    			selfCount++;
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
    	if (selfCount>1) {
    		$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
    	}
     }
	
	function setRelationship() {
		
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "I"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
	}
	
	
	$scope.checkMedicalStatus = function(){
		
		$scope.pageErrors.medicalError = false;
		$scope.pageErrors.medicalErrorText = "";
		$scope.isPPC = false;
		for (var i =0; i<$scope.proposalInput.insured.length; i++){
			var age = brokeredgefactory.calculateAge($scope.proposalInput.insured[i].dob);
			if (age + Number($scope.proposalInput.policy.tenure) > 46){
				$scope.isPPC = true;
				break;
			}
			if ($scope.proposalInput.insured[i].isPED == "Y"){
				$scope.isPPC = true;
				break;
			} else
			{
				$scope.proposalInput.insured[i].ped = "";
			}
		}
		
		if ($scope.isPPC && $scope.proposalInput.policy.ppcPedAccepted == "N"){
			$scope.pageErrors.medicalError = true;
			$scope.pageErrors.medicalErrorText = "Cannot buy this policy online";
		}
		
	}
	
	$scope.setGender=function(val)
	{
		if($scope.proposalInput.proposer.title=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	$scope.calulateBmi=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			
			if(key.type=='A')
				{
				var heighttm=$scope.proposalInput.insured[val].height*0.01;
				var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
				$scope.proposalInput.insured[val].bmi=parseInt(bmi)
				}
			
		})
	}

	
	function getRelGender(rel){
		var gender = "MF";
		if(rel=='Wife'  || rel=='Aunt'  || rel=='Dependent Daughter'  || rel=='Daughter in law'  || rel=='Grand Daughter'  || rel=='Grand Mother'  || rel=='Mother in law'  || rel=='Mother'   || rel=='Niece'  || rel=='Sister in law'  || rel=='Sister'){
			gender = "F";
				
		} else if (rel=='Brother in law'  || rel=='Brother'  || rel=='Father in law'  || rel=='Father'  || rel=='Grand Father'  || rel=='Grand Son'  || rel=='Husband'  || rel=='Nephew'   || rel=='Son in law'  || rel=='Dependent Son'  || rel=='Uncle'){
			gender = "M";
		}
		return gender;
	}
	
	function getRelType(rel){
		var type = "A";
		if(rel=='Dependent Son'  || rel=='Dependent Daughter' ){
			type = "C";				
		} 
		
		return type;
	}

	//Nominee
	$scope.calculateAge=function(dob)
	{
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	
	
	$scope.checkMedicalQuition=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			angular.forEach($scope.proposalInput.insured[val].pedList,function(k,l){
				if(k.exists=='Y')
				{
					$scope.errorMessageMedical="You can't buy policy online"+$scope.proposalInput.insured[val].firstName+" has medical condition";
				}
			})
		})
	}
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
		if (index == 2) {

			if ($scope.isPPC) {
				$scope.active=2;
			} else
			{
				$scope.active=1;
			}
		}
	}
	
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "1":
			$scope.calulateBmi();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			break;
		case "2":
			setRelationship();
			if ($scope.isPPC) {
				$scope.active=2;
			} else
			{
				$scope.active=3;
			}
			$scope.proposalError={key:"",errorPirnt:[]}			
			$scope.tabStatus.secondComplete = 'done';
			break;
		case "3":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =3;
			$scope.tabStatus.thirdComplete = 'done';
			break;
		case "4":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';			
			break;
		case "5":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =5;
			$scope.tabStatus.fifthComplete='done';
			break;
		}
		
		saveApplicationData ();
	}
	
	function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
	}
	

	
	function setpolicyAddress(){
 		
		var addresses = [];
		var address = {}
		address.addressType = "C";
 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
		address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
		address.addressLine3 = $scope.proposalInput.proposer.corrArea;
		address.city = $scope.proposalInput.proposer.corrCity;
		address.citycode = $scope.proposalInput.proposer.corrCityCode;
		address.state = $scope.proposalInput.proposer.corrState;
		address.statecode = $scope.proposalInput.proposer.corrStateCode;
		address.pincode = $scope.proposalInput.proposer.corrPincode;
		addresses.push(address);
		$scope.proposalInput.proposer.address = angular.copy(addresses);
		
		var contacts = []
		var contact = {};
		contact.contactType = "mobile";
		contact.contactText = $scope.proposalInput.proposer.mobile;
		contacts.push(contact);
		var contact = {};
		contact.contactType = "email";
		contact.contactText = $scope.proposalInput.proposer.email;
		contacts.push(contact);
		$scope.proposalInput.proposer.contacts = angular.copy(contacts);
		
 	}
	
	$scope.makePayment=function()
	{
		//
		$scope.proposalInput.proposer.nomineeFirstName = $scope.proposalInput.insured[0].nomineeName;
		$scope.proposalInput.proposer.nomineeLastName = $scope.proposalInput.insured[0].nomineeName;
		// Populate Medical and LifeStyle data
		setpolicyAddress();
		console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
		var input={url:'',data:{}}
		input.url='/submitProposal/health/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
		
		var healthProposal = angular.copy($scope.proposalInput);
		healthProposal.proposer.proposerDob = $filter('date')(new Date(healthProposal.proposer.proposerDob),'yyyy-MM-dd');
		healthProposal.proposer.nomineeDob = $filter('date')(new Date(healthProposal.proposer.nomineeDob),'yyyy-MM-dd');
		input.data=angular.copy(healthProposal);
		
		$scope.loading = true;
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
				if(data.hasOwnProperty("error"))
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
					//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
					if(data['error']=="Premium Mismatch")
					{
					 	
					 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"],"basePremium":data["basePremium"],"serviceTax":data["serviceTax"] })
					}
				}
				else
				{
					$scope.proposalError={key:"",errorPirnt:[]}
					console.log(data);

					var url = data.payUrl;
					var paymentInput = {};
					returnUrl = "http://Heartbeatuat.us-east-1.elasticbeanstalk.com/Postpayment/125"
					var method = 'POST';
					
					paymentInput.CustomerID = data.CustomerID
					paymentInput.TxnAmount= data.TxnAmount;
					paymentInput.AdditionalInfo1= data.AdditionalInfo1;
					paymentInput.AdditionalInfo2= data.AdditionalInfo2;
					paymentInput.AdditionalInfo3= data.AdditionalInfo3;
					paymentInput.hdnPayMode= data.hdnPayMode;
					paymentInput.UserName= data.UserName;
					paymentInput.UserMailId= data.UserMailId;
					paymentInput.ProductCd= data.ProductCd;
					paymentInput.ProducerCd= data.ProducerCd;
					FormSubmitter.submit(url, method, paymentInput);
					
				}
			
				$scope.loading = false;
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
	}
	
	getApplicationData();
	
	$scope.AddMedicalRow = function(){
		var questionDetail = {
			"insuredId" : "",
			"details" : ""
		}
		$scope.proposalInput.medicalDetails.push(questionDetail);
	}
	

	$scope.DeleteMedicalRow = function(i){
		$scope.proposalInput.medicalDetails.splice(i);
	}
	
	$scope.AddMedicalDetails = function(questionId){
		
		var input = {};
		input.questionId = questionId;
		var insureds = [];
		
		angular.forEach($scope.proposalInput.insured,function(val,key){
			var insured = {};
			insured.id = key;
			insured.name = val.firstName + " " + val.lastName;
			insureds.push(insured);
		});
		
		input.insureds = insureds;
	
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/health/royalSundaram/medicalQuestionDetail.html',
		      controller: 'rsaMedicalQuestionDetailCtrl',
		      size: 'md',
		      resolve: {
		    	  'input': function () {
		          return input;
		        }
		      }
		});
		modalInstance.result.then(function (questionDetail) {
	      console.log(questionDetail);
	      if( typeof(questionDetail) != 'undefined' && questionDetail.action == "add")
	      	{
	    	  //Add Question details
	      	  $scope.proposalInput.medicalDetails.push(questionDetail);
	      	  console.log($scope.proposalInput.medicalDetails);
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	$scope.showTC=function(){
		
		var modalInstance=$uibModal.open({
		  animation: $scope.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      controller : 'tcCtrl',
	      templateUrl: 'templates/proposal/health/hdfc/tc.html',
	      size: 'lg'
		});
		modalInstance.result.then(function (agree) {
	      console.log(agree);
	      if( typeof(agree) != 'undefined')
	      	{
	    	  $scope.proposalInput.policy.tcAgree = agree;
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	
	
	$scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.proposalInput.policy.basePremium= $scope.selected.basePremium;
        	$scope.proposalInput.policy.serviceTax= $scope.selected.serviceTax;
        	$scope.makePayment();
        	
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
	
	
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
     
     $scope.appointeeAge=function(dob) {
 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
 	}
     
     // Watch function // Watch function
     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {
         	
         	var date=new Date(newValue);
     		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+ Number($scope.proposalInput.policy.tenure),date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
         }
     	
     });
     
}])