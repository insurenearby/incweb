/**
 * 
 */
"use-strict"
brokeredgeApp.controller('itgiHealthCtrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','FormSubmitter',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,FormSubmitter){
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.corState = {};
	$scope.corCity={};
	$scope.nomState = {};
	$scope.nomCity={};
	$scope.corCities = [];
	$scope.nomCities = []
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}

	$scope.pageErrors = {
			insuredError: false,
			medicalError: false,
			insuredErrorText : "",
			medicalErrorText : ""
	}
	
	$scope.insureds = [];
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    var ffRel = ["Self", "Spouse", "Father", "Mother", "Son","Daughter","Father in law", "Mother in Law" ];
    $scope.lifeStyleQuestions=[];
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-60, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
    $scope.illnessDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-60, monthToday, dayToday),
	    	minMode : "month",
	    	mode : "month"	
    }
    
    $scope.opened= {
        	proposerDob : false,
        	nomineeDob : false,
        	appointeeDob : false,
        	policyStartDate : false
    };
	$scope.openQ = function(seq , i) {
		
		var key = seq + "_" + i;
		$scope.opened[key] = true;
	};
   $scope.open = function(name) {
            $scope.opened[name] = true;
   };
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.formatPedDate = "yyyy-MM";
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
	
   function init() {
		$scope.input.lob = "Health";
		$scope.input.productType = $scope.proposalInput.productType;
		
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			switch (addressType){
			case "C":
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.corrAddressLine3 = value.addressLine3 ;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCityCode = value.cityCode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStateCode = value.stateCode;
				$scope.proposalInput.proposer.corrPincode = value.pincode;
				break;
			}
		});
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		getMaster($scope.proposalInput.productId, "Relationship");
		getMaster($scope.proposalInput.productId, "Title");	
		getMaster($scope.proposalInput.productId, "Occupation");	
		getMaster($scope.proposalInput.productId, "MaritalStatus");	
		getMaster($scope.proposalInput.productId, "State");	
		
		$scope.corState.id = $scope.proposalInput.proposer.corrStateCode;
		$scope.corState.value = $scope.proposalInput.proposer.corrState;
		$scope.getCities('C', $scope.corState);
		$scope.corCity.id = $scope.proposalInput.proposer.corrCityCode;
		$scope.corCity.value = $scope.proposalInput.proposer.corrCity;
		
		$scope.nomState.id = $scope.proposalInput.proposer.nomineeStateCode;
		$scope.nomState.value = $scope.proposalInput.proposer.nomineeState;
		$scope.getCities('N', $scope.nomState);
		$scope.nomCity.id = $scope.proposalInput.proposer.nomineeCityCode;
		$scope.nomCity.value = $scope.proposalInput.proposer.nomineeCity;
		
		$scope.proposalInput.policy.tcAgree = "N";
		$scope.checkInsuredData();		
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;	
		
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates(){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob.substr(0, 10));	
		}
		
		if (typeof($scope.proposalInput.proposer.nomineeDob) != "undefined" && $scope.proposalInput.proposer.nomineeDob != ""){
			$scope.proposalInput.proposer.nomineeDob = new Date($scope.proposalInput.proposer.nomineeDob.substr(0, 10));
		}
		
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeDob = new Date($scope.proposalInput.proposer.appointeeDob.substr(0, 10));
		}
	}
	

	
	
	$scope.getCities = function(type,state) {
		var cities = [];
		var input={
				url:'',
				data:{}
		}
		if (typeof(state) != "undefined"){
			input.url='/master/city/'+$scope.proposalInput.insurerId+'/'+state.id;
			var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
			tmp.$promise.then(function(data){
				cities=data['result'];
				cities= $filter('orderBy')(cities, 'value')
				if(type=='N') {
					$scope.nomCities = cities;
					 $scope.proposalInput.proposer.nomineeState = state.value;
					 $scope.proposalInput.proposer.nomineeStateCode = state.id;
					
				}else if(type=="C") {
					$scope.corCities = cities;
					$scope.proposalInput.proposer.corrState = state.value
					$scope.proposalInput.proposer.corrStateCode = state.id;
				}
			})
		}

	}
	
	function getCityFromPincode (type, pincode){
		
		var re = new RegExp("^[0-9]{6}$");
		if (re.test(pincode)){
			
			var input={url:'',data:{}}
	    	
			input.url="/master/getDetailsForPincode/" + $scope.proposalInput.insurerId + "/" + pincode;
			
	    	var res = $resource('./DataRequest', [], {
		          save: {
		             method: 'POST'}
		    });
			
			var tmp = res.save(input);
			tmp.$promise.then(function(data){
				if (data.hasOwnProperty("result")){
					
					 if (type == "C"){
						 $scope.corCities = data.result; 
					 } 
				}
				
				
			})		
		}
	}
		
	$scope.setCityState = function(type, city){
		
		console.log(city);
		if (typeof(city) != "undefined") {

			 if (type == "C"){
				 $scope.proposalInput.proposer.corrCity = city.value;
				 $scope.proposalInput.proposer.corrCityCode = city.id;
				 
			 } else
			 {
				 $scope.proposalInput.proposer.nomineeCity = city.value;
				 $scope.proposalInput.proposer.nomineeCityCode = city.id;
			 }
		}
	}
	

	
	$scope.premiumAdd=function(coverId,val, premiumVal) {
		if(val=='Y')
		{
		  $scope.proposalInput.policy.premiumPayable= Number($scope.proposalInput.policy.premiumPayable)+ Number(Math.round(premiumVal));
		}
		else
		{
			$scope.proposalInput.policy.premiumPayable= Number($scope.proposalInput.policy.premiumPayable) -  Number(Math.round(premiumVal));
		}
	}
		
	function getMaster(productId, masterId){
		$scope.insurerMaster[masterId] = [];
		var tmp=brokeredgefactory.getMaster(productId, masterId);
		tmp.$promise.then(function(data){
			//console.log(data);
			var items = data['result'];
			items = $filter('orderBy')(items, 'value')
			$scope.insurerMaster[masterId]= items;
			if (masterId == "Relationship"){
				addGendertoRel();					
			}
			//console.log($scope.insurerMaster);
		})
	}

		
	function getQuestions(productId, type){
		var questions = [];
		 var input = {
	    			url: '/master/getQuestions/' + productId + "/" + type,
	    			data : {}
	    	};
	        
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
		var tmp=res.process(input);
		tmp.$promise.then(function(data){
			//console.log(data);
			//console.log($scope.insurerMaster);
		})
		
	}
	
	$scope.getMasterValue = function(masterName, masterId){
		// 
		var masterValue = masterId;
		var values = $scope.insurerMaster[masterName];
		
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value; 
					break;
				}
			}
		}
		return masterValue; 
	}
	
	$scope.getQuestionText = function(type, id){
		// 
		var questionText = "";
		var values = [];
		if (type == "PedList"){
			values = $scope.medicalQuestions;
		}

				
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == id){
					questionText = values[i].value; 
					break;
				}
			}
		}
		return questionText; 
	}
	
	function addGendertoRel (){
		var records = [];
		angular.forEach($scope.insurerMaster.Relationship,function(val,key){
			var record = val;
			record.gender = getRelGender(val.value);
			var addRecord = true;
			if ($scope.proposalInput.policy.productType == "FF" ) {
				if (!isvalidFFRel(val.id)) {
					addRecord = false;
				}
			}
			if (addRecord) {
				records.push(record);
			}
			
		});
		$scope.insurerMaster.Relationship = records;
	}

	function isvalidFFRel(id){
		
		var valid = false;
		for (var i=0; i<ffRel.length; i++) {
			if (ffRel[i] == id){
				valid = true;
				break;
			}
		}
		
		return valid;
	}

	$scope.checkRelationShip=function() {
		$scope.pageErrors.insuredError= false;
		$scope.pageErrors.insuredErrorText = "";
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.maritalStatus= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		var adultRels = [];
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
    	var selfCount = 0;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
			var rel = val.relationshipId;
			
			if (val.type == "A"){
				if (rel == "Son" || rel == "Daughter" || rel == "Son in law" || rel == "Daughter in law"){
					$scope.pageErrors.insuredError= true;
	   				$scope.pageErrors.insuredErrorText = "Invalid Adult RelationShip";
				}
				
				if ($scope.proposalInput.policy.numAdults == 2){
					if (val.maritalStatus == "Single"){
						$scope.pageErrors.insuredError= true;
						$scope.pageErrors.insuredErrorText = "Adults Must be Married";
					}
				}
				
				adultRels.push(rel);
			} else
				
			{
				if (rel != "Son" && rel != "Daughter" && rel != "Son in law" && rel != "Daughter in law"){
					$scope.pageErrors.insuredError= true;
	   				$scope.pageErrors.insuredErrorText = "Not a Valid Child Relationship";
				}
				if (val.maritalStatus != "Single"){
					$scope.pageErrors.insuredErrorText = "Children must be Unmarried";
				}
			}
			
	   		switch (rel) {
	   		case "Self":
	   			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if (val.type == "C"){ // Proposer cannot be a child.
    				$scope.pageErrors.insuredError= true;
    				$scope.pageErrors.insuredErrorText = "Child cannot be a proposer";
    			}
    			selfCount++;
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
	   			break;
	   		case "Father":
	   		case "Mother":
	   		case "Father in law":
	   		case "Mother in law":
	   			parentIncluded = true;	   			
	   			break;
	   		}	   
    	});
    	
    	if (selfCount>1) {
			$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
    	}
    	
    	if (selfCount == 0) {
			$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Proposer Must be Insured";
    	}
    	
    	
	   	adultRels = $filter('orderBy')(adultRels);
	   	
	   	
	   	if (!$scope.pageErrors.insuredError && $scope.proposalInput.policy.productType == "FF" && $scope.proposalInput.policy.numAdults == 2){
	   		
	   		
	   		$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Adults Must be Husband/Wife";
			
			
			if (adultRels[0] == "Self" && (adultRels[1] == "Spouse")) {
				$scope.pageErrors.insuredError= false;
				$scope.pageErrors.insuredErrorText = "";
			}
			
			if (adultRels[0] == "Father" && (adultRels[1] == "Mother")) {
				$scope.pageErrors.insuredError= false;
				$scope.pageErrors.insuredErrorText = "";
			}
			
			if (adultRels[0] == "Brother" && (adultRels[1] == "Sister in law")) {
				$scope.pageErrors.insuredError= false;
				$scope.pageErrors.insuredErrorText = "";
			}

			if (adultRels[0] == "Brother in law"  && adultRels[1] == "Sister" ) {
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
			}

			if (adultRels[0] == "Father in law"  && adultRels[1] == "Mother in law" ) {
					$scope.pageErrors.insuredError= false;
					$scope.pageErrors.insuredErrorText = "";
				}
			if (adultRels[1] == "Grand Father"  && adultRels[0] == "Grand Mother" ) {
				$scope.pageErrors.insuredError= false;
				$scope.pageErrors.insuredErrorText = "";
			}
	   	}
	}
	
	$scope.checkInsuredData = function() {
		$scope.pageErrors.insuredError= false;
		$scope.pageErrors.insuredErrorText = "";
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.maritalStatus= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
	   	var selfCount = 0;
	   	var paCount = 0;
	   	var childCount = 0;
	   	var hasDependentParent = false;
	   	var hasNonParent = false;
	   	
	   	angular.forEach($scope.proposalInput.insured, function(val,key){
   			if (val.preExistingCond == "Y"){ // Pre-Existing Condition not allowed.
   				$scope.pageErrors.insuredError= true;
   				$scope.pageErrors.insuredErrorText = "The policy cannot be bought Online";
   			}
   			
	   		if (val.relationshipId == "1"){
	   			
	   			$scope.proposalInput.proposer.title= val.title;
	   			$scope.proposalInput.proposer.firstName = val.firstName;
	   			$scope.proposalInput.proposer.middleName = val.middleName;
	   			$scope.proposalInput.proposer.lastName= val.lastName;
	   			$scope.proposalInput.proposer.gender = val.gender;
	   			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
	   			$scope.proposalInput.proposer.occupationType=val.occupationType;
	   			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
	   			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
	   			$scope.proposalInput.proposer.IsProposerInsured = "Y";
	   			if (val.type == "C"){ // Proposer cannot be a child.
	   				$scope.pageErrors.insuredError= true;
	   				$scope.pageErrors.insuredErrorText = "Child cannot be a proposer";
	   			}
	   			selfCount++;
	   			$scope.isProposerInsured = true;
	   			console.log($scope.proposalInput.proposer);
	   		}
	   		if (val.type == "C") {
	   			childCount++;
	   		}
	   		if (val.isPersonalAccidentApplicable=='Y') {
	   			paCount++;
	   		}
	   		if (val.relationshipId == "4"){
	   			hasDependentParent = true;
	   		} else
	   		{
	   			hasNonParent = true;
	   		}
	   	});
   	
   	// PA Cover for Comprehensive Plan
   	
   	if (selfCount>1) {
			$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
   	}
   	if (childCount> 0 && selfCount==0) {
   		$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Children can only be insured with Self";
   	}
   	
   	if (hasDependentParent && hasNonParent) {
   		$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Dependent parents cannot be insured with anyone else";
   	}
   	
    }
	
	function setRelationship(){
		
		$scope.proposalInput.proposer.IsProposerInsured = "N";
		$scope.isProposerInsured = false;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "Self"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
	}
	
	
	function reArrangeWithSelfFirst(){
		
		var insureds = angular.copy($scope.proposalInput.insured);
		
		var idxSelf = 0;
		for (var i=0; i<insureds.length; i++){
			if (insureds[i].relationshipId == "Self"){
				idxSelf = i;
			}
		}
		
		// Swap the array Elements to make First Member Self.
		var member = angular.copy(insureds[0]);
		if (idxSelf !=0){
			insureds[0] = angular.copy($scope.proposalInput.insured[idxSelf]);
			insureds[idxSelf] = angular.copy(member);
		}
		
		return insureds;
	}
	
	$scope.setGender=function(val)
	{
		if($scope.proposalInput.proposer.title=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	function setCover (covers){
		angular.forEach(covers,function(val,key){
			var cover = {};
			cover.name = val;
			$scope.proposalInput.covers.push(cover);			
		})
	}
	
	
	$scope.calulateBmi=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			
			if(key.type=='A')
				{
				var heighttm=$scope.proposalInput.insured[val].height*0.01;
				var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
				$scope.proposalInput.insured[val].bmi=parseInt(bmi)
				}
			
		})
	}

	
	function getRelGender(rel){
		var gender = "MF";
		if(rel=='Wife' || rel=='Daughter'   || rel=='Mother in Law'  || rel=='Mother'  || rel=='Sister'){
			gender = "F";
				
		} else if (rel=='Father'  || rel=='Brother' || rel=='Father in law' || rel=='Son' ){
			gender = "M";
		}
		return gender;
	}

	//Nominee
	$scope.calculateAge=function(dob)
	{
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	
	
	
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "0":
			$scope.calulateBmi();			
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=0;
			break;
		case "1":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =1;
			$scope.tabStatus.firstComplete = 'done';
			break;
		case "2":
			setRelationship();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =2;
			$scope.tabStatus.secondComplete = 'done';		
			break;
		case "3":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =3;
			$scope.tabStatus.thirdComplete='done';
			break;
		case "4":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =4;	
			$scope.tabStatus.fourthComplete='done';
			
			break;
		case "5":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =5;
			$scope.tabStatus.fifthComplete = 'done';
			
			break;
		case "6":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =6;
			$scope.tabStatus.sixthComplete = 'done';
			
			break;
		}
		
		saveApplicationData ();
	}
	
	function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
	}	


	function setpolicyAddress(){
 		
		
		var addresses = [];
		var address = {}
		address.addressType = "C";
 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
		address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
		address.addressLine3 = $scope.proposalInput.proposer.corrAddressLine3
		address.city = $scope.proposalInput.proposer.corrCity;
		address.cityCode = $scope.proposalInput.proposer.corrCityCode;
		address.state = $scope.proposalInput.proposer.corrState;
		address.stateCode = $scope.proposalInput.proposer.corrStateCode;
		address.pincode = $scope.proposalInput.proposer.corrPincode;
		addresses.push(address);
		
		$scope.proposalInput.proposer.address = angular.copy(addresses);
		
		var contacts = []
		var contact = {};
		contact.contactType = "mobile";
		contact.contactText = $scope.proposalInput.proposer.mobile;
		contacts.push(contact);
		var contact = {};
		contact.contactType = "email";
		contact.contactText = $scope.proposalInput.proposer.email;
		contacts.push(contact);
		$scope.proposalInput.proposer.contacts = angular.copy(contacts);
		
 	}

	$scope.makePayment=function() {
		//
		if ($scope.proposalInput.policy.floater == 'N'){
			$scope.proposalInput.policy.floaterSi = $scope.proposalInput.policy.sumInsured;
		}
		// Populate Medical and LifeStyle data
		
		$scope.proposalInput.proposer.nomineeName = $scope.proposalInput.proposer.nomineeFirstName + " " + $scope.proposalInput.proposer.nomineeLastName;
		console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
		// Always set Policy StartDate to Today's Date for Religare
		var date=new Date();
		$scope.proposalInput.policy.policyStartdate = date;
		$scope.proposalInput.policy.policyEnddate = new Date(date.getFullYear()+Number($scope.proposalInput.policy.tenure),date.getMonth(),date.getDate()-1);
		var healthProposal = angular.copy($scope.proposalInput);
		healthProposal.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
		healthProposal.policy.policyEnddate = $filter('date')(new Date(healthProposal.policy.policyEnddate),'yyyy-MM-dd');
		healthProposal.proposer.proposerDob = $filter('date')(new Date(healthProposal.proposer.proposerDob),'yyyy-MM-dd');
		healthProposal.proposer.nomineeDob = $filter('date')(new Date(healthProposal.proposer.nomineeDob),'yyyy-MM-dd');
		healthProposal.insured = reArrangeWithSelfFirst();
		
		var input={url:'',data:{}}
		input.url='/submitProposal/health/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
		input.data=angular.copy(healthProposal);
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		$scope.loading = true;
		tmp.$promise.then(function(data){
			
				if(data.hasOwnProperty("error"))
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
					//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
					if(data['error']=="Premium Mismatch")
					{
					 	
					 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]});
					}
				} else if (data.hasOwnProperty("proposalUrl")) {
				
					var paymentInput = {};
					console.log(data.XML_DATA);
					var method = 'POST';	
					paymentInput.RESPONSE_URL = data.RESPONSE_URL;
					paymentInput.PARTNER_CODE = data.PARTNER_CODE;
					paymentInput.UNIQUE_QUOTEID = data.UNIQUE_QUOTEID;
					paymentInput.XML_DATA = data.XML_DATA;
					paymentInput.premiumPayable = data.premiumPayable;
					var url = data.proposalUrl;
					
					FormSubmitter.submit(url, method, paymentInput);
					
				} else
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str="Unexpected error while submitting proposal";
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
				}
				$scope.loading = false;
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
	}
	
	getApplicationData();
	
	
	$scope.showTC=function(){
		
		var modalInstance=$uibModal.open({
		  animation: $scope.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      controller : 'tcCtrl',
	      templateUrl: 'templates/proposal/health/religare/tc.html',
	      size: 'lg'
		});
		modalInstance.result.then(function (agree) {
	      console.log(agree);
	      if( typeof(agree) != 'undefined')
	      	{
	    	  $scope.proposalInput.policy.tcAgree = agree;
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	
	$scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
	
	
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
    };
     
     

	     
  // Watch function
     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {
         	
         	var date=new Date(newValue);
     		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+Number($scope.proposalInput.policy.tenure),date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
         }
     	
     });
     
}])