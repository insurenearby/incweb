/**
 * 
 */
"use strict";
brokeredgeApp.controller('rsaMedicalQuestionDetailCtrl',['$scope','$uibModalInstance', '$filter', 'input', function($scope,$uibModalInstance, $filter, input){
	

    console.log(input);
	$scope.question = {};
	$scope.question.questionId = input.questionId;
    $scope.insureds = input.insureds;
    
	// Date picker
    var dateToday = new Date();
    var yearMin = dateToday.getFullYear()-5;
    var yearMax = dateToday.getFullYear();
    var monthToday = dateToday.getMonth();
    var dayToday = dateToday.getDate();
    $scope.medicalOption={minMode: 'month'};
    
    
    $scope.opened=function($event,$index)
	{

	    $scope.opened[$index] = true;
	    
	}
    
    $scope.open = function() {
		$scope.popup1.opened = true;
	};
	$scope.popup1 = {
		opened: false
	};
	
	$scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    
    //
	
	$scope.ok = function () {
		
		 var error = $scope.medicalDetails.$error;
		 console.log(error);
		 angular.forEach(error.pattern, function(field){
	        if(field.$invalid){
	            var fieldName = field.$name;
	            console.log(fieldName);
	        }
		 });
		    
		var output = {};
		output.monthOfDiagnosis = $filter('date')(new Date($scope.question.DOTreatment), "MM");
		output.yearOfDiagnosis = $filter('date')(new Date($scope.question.DOTreatment), "yyyy");
		output.questionId = $scope.question.questionId;
		output.insuredId = $scope.question.insuredId;
		output.insuredName = $scope.insureds[$scope.question.insuredId-1].name;
		output.nameOfIllness = $scope.question.nameOfIllness;
		output.medicationDetail = $scope.question.medicationDetail;
		output.treatmentOutCome = $scope.question.treatmentOutCome;
		output.action = "add";
		$uibModalInstance.close(output); // Put the result here
	};

	$scope.cancel = function () {
		output.action = "cancel";
	    $uibModalInstance.dismiss('cancel'); // Put the dismiss reason here
	};
	// validate 
	$scope.filterValue = function($event){
	    	
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	       
		};
	$scope.alphaChar=function($event)
	   {
		   		var regex = new RegExp("^[a-zA-Z ]+$");
		    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (regex.test(str)) {
		        return true;
		    }

		    event.preventDefault();
		    return false;
	   }
	   
	   $scope.alphaNumeric=function($event)
	   {

	   	var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    event.preventDefault();
	    return false;

	   }
}]);