/**
 * 
 */
"use-strict"
brokeredgeApp.controller('rsaHealthCtrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','paymentService',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,paymentService){
	
	$scope.lifeStyleQuestions=[
				{"id" : "alcohol", "q": "Do You consume alcohol?","exists":"N",},
			    {"id" : "smoking", "q": "Do you consume tobacco?","exists":"N"},
			    {"id" : "narcotics", "q": "Do you consume or have you ever consumed any narcotic substance?","exists":"N"},
			    {"id" : "anyOtherSubstance", "q": "Do you consume or have you ever consumed any other substance?","exists":"N"}
			  ]
	
	$scope.medicalQuestions=[];
	
	
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polState={};
	$scope.polCity={};
	$scope.nomState={};
	$scope.nomCity={};
	$scope.polCities = [];
	$scope.nomCities = [];
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}
	
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
       
    $scope.opened= {
        	proposerDob : false,
        	nomineeDob : false,
        	appointeeDob : false,
        	policyStartDate : false
    };
    
	$scope.open1 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
   $scope.open = function(name) {
            $scope.opened[name] = true;
   };
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
    

   //*********************************Functions Start Here *******************************
	
	function init() {
		$scope.input.lob = "Health";
		$scope.input.productType = $scope.proposalInput.productType;
		//var insuredData = $scope.proposalInput.insured[0];
		//setPolicy();
		//$scope.proposalInput.insured = setInsured(insuredData); 
		//setCover($scope.quoteInput.quotes.addc) 
		initializeLSQuestions();
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		formatDates ();
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge($scope.proposalInput.proposer.nomineeDob)
		getMaster($scope.proposalInput.productId, "Occupation");
		getMaster($scope.proposalInput.productId, "State");
		getMaster($scope.proposalInput.productId, "AppointeeRel");
		getMaster($scope.proposalInput.productId, "Relationship");
		getMaster($scope.proposalInput.productId, "Qualification");
		getMaster($scope.proposalInput.productId, "Nationality");
		getQuestions($scope.proposalInput.productId, "PedList")	;
		$scope.polState.id = $scope.proposalInput.proposer.address[0].stateCode;
		$scope.polState.value = $scope.proposalInput.proposer.address[0].state;
		$scope.getCity($scope.polState,'policy');
		$scope.nomState.id = $scope.proposalInput.proposer.nomineeStateCode;
		$scope.nomState.value = $scope.proposalInput.proposer.nomineeState;
		$scope.getCity($scope.nomState,'nomadd');
		$scope.polCity.id = $scope.proposalInput.proposer.address[0].cityCode;
		$scope.polCity.value = $scope.proposalInput.proposer.address[0].city;
		$scope.nomCity.id = $scope.proposalInput.proposer.nomineeCityCode;
		$scope.nomCity.value = $scope.proposalInput.proposer.nomineeCity;
		$scope.proposalInput.policy.tcAgree = "N";
				 		 
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal/102HL03F01"
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
		
		
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalList();
		ProcessLSList();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;	
		
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates ();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates (){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = (new Date($scope.proposalInput.proposer.proposerDob));
		}
		
		if (typeof($scope.proposalInput.proposer.nomineeDob) != "undefined" && $scope.proposalInput.proposer.nomineeDob != ""){
			$scope.proposalInput.proposer.nomineeDob = new Date($scope.proposalInput.proposer.nomineeDob);
		}
		
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeDob = new Date($scope.proposalInput.proposer.appointeeDob);
		}
		
		
		
	}
	
	$scope.getCity = function(state,name) {
		var cities = [];
		var input={
				url:'',
				data:{}
		}
		if (typeof(state) != "undefined"){
			input.url='/master/city/'+$scope.proposalInput.insurerId+'/'+state.id;
			var tmp=brokeredgefactory.getCity().processRequest({}, JSON.stringify(input));
			tmp.$promise.then(function(data){
				cities=data['result'];
				if(name=='nomadd')
				{
					$scope.nomCities = cities
					$scope.proposalInput.proposer.nomineeStateCode=state.id;
					$scope.proposalInput.proposer.nomineeState=state.value;  //state.value;
				}else if(name=="policy") {
					$scope.proposalInput.proposer.address[0].stateCode = state.id;
					$scope.proposalInput.proposer.address[0].state = state.value;  //state.value;
					$scope.polCities = cities
				}
			})
		}

	}

	$scope.changeCity = function(city,name) {
		if(name=="policy") {
			$scope.proposalInput.proposer.address[0].cityCode = city.id;
			$scope.proposalInput.proposer.address[0].city = city.value; //city.value;
			
		}else if(name=="nomadd") {
			$scope.proposalInput.proposer.nomineeCityCode = city.id;
			$scope.proposalInput.proposer.nomineeCity =  city.value;
		} 
	}
	
	function getMaster(productId, masterId){
		$scope.insurerMaster[masterId] = [];
		var tmp=brokeredgefactory.getMaster(productId, masterId);
		tmp.$promise.then(function(data){
			//console.log(data);
			$scope.insurerMaster[masterId]= data['result'];
			if (masterId == "Relationship"){
				addGendertoRel();
			}
			//console.log($scope.insurerMaster);
		})
	}
	
	function getQuestions(productId, type){
		var questions = [];
		 var input = {
	    			url: '/master/getQuestions/' + productId + '/' + type,
	    			data : {}
	    	};
	        
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
		var tmp=res.process(input);
		tmp.$promise.then(function(data){
			
			if (data.hasOwnProperty("result")){
				if (type == "PedList"){
					$scope.medicalQuestions = data.result;
					initializeMedicalQuestions();
				}
				
			}
			//console.log($scope.insurerMaster);
		})
		
	}
	

	
	$scope.getMasterValue = function(masterName, masterId){
		// 
		var masterValue = masterId;
		var values = $scope.insurerMaster[masterName];
		
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value; 
					break;
				}
			}
		}
		return masterValue; 
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
		}
		
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			angular.forEach(insured.pedList,function(item,key){
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						$scope.medicalQuestions[i].exists = item.exists;
						angular.forEach(item.pedData,function(illness,key){
							var detail = angular.copy(illness);
							detail.insuredId = insuredId;
							detail.insuredName = insuredName;
							detail.questionId = code;
							$scope.medicalQuestions[i].details.push(detail);
						})
					}
				}
				
			})
		})
		//console.log($scope.medicalQuestions);
	}
	
	function initializeLSQuestions(){
		
		for(var i=0; i<$scope.lifeStyleQuestions.length; i++){
			$scope.lifeStyleQuestions[i].details = [];
		}
		
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			angular.forEach(insured.lifestyleInfo,function(item,key){
				var id = item.name;
				for(var i=0; i<$scope.lifeStyleQuestions.length; i++){
					
					if ($scope.lifeStyleQuestions[i].id == id){
						if (item.exists == "Y"){
							$scope.lifeStyleQuestions[i].exists = item.exists;
							angular.forEach(item.lsData,function(ls,key){
								var detail = angular.copy(ls);
								detail.insuredId = insuredId;
								detail.insuredName = insuredName;
								detail.questionId = id;
								$scope.lifeStyleQuestions[i].details.push(detail);
							})
						}
					}
				}
				
			})
		})
		
		
		console.log($scope.lifeStyleQuestions);
	}
	
	function addGendertoRel (){
		var records = [];
		angular.forEach($scope.insurerMaster.Relationship,function(val,key){
			var record = val;
			record.gender = getRelGender(val.value);
			records.push(record);
		});
		$scope.insurerMaster.Relationship = records;
	}
	
	
	//validate of 
	$scope.validateOcupation=function()
	{
		if($scope.proposalInput.proposer.occupationType=='POLITICIAN')
		{
			$scope.ocupationError="Please contact our nearest branch or kindly chat with our online representative or call us at 1860 425 0000 for more information.";
		}
		else
			{
			$scope.ocupationError="";
			}
	}
	
	$scope.checkRelationShip=function(form)
     {
    	
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.maritalStatus= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
    	var selfCount = 0;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "Self"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if(val.occupationType=='SALARIED'||val.occupationType=='OTHERS')
    			{
    				$scope.proposalInput.proposer.designation =val.designation;
    			
    			}
    			else if(val.occupationType=='SELF EMPLOYED')
    			{
    		
    				$scope.proposalInput.proposer.business =val.business;
    			}
    			if (val.type == "C"){ // Proposer cannot be a child.
    				form.relaInsured.$setValidity('required',false);
    			}
    			selfCount++;
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
    	if (selfCount>1) {
    		form.relaInsured.$setValidity('required',false);
    	}
    	
     }
	
	function setRelationship() {
		
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "Self"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if(val.occupationType=='SALARIED'||val.occupationType=='OTHERS')
    			{
    				$scope.proposalInput.proposer.designation =val.designation;
    			
    			}
    			else if(val.occupationType=='SELF EMPLOYED')
    			{
    		
    				$scope.proposalInput.proposer.business =val.business;
    			}
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    	});
	}
	
	$scope.medicalPageComplete = function(){
		
		var complete = true;
		var questionId = 0;
		
		for (var i =0; i<$scope.medicalQuestions.length; i++){
			if ($scope.medicalQuestions[i].exists == "Y"){
				if ($scope.medicalQuestions[i].details.length == 0){
					complete = false;
				}
			}
			if (!complete){
				break;
			}
			questionId++;
		};
		return complete;
	}	
	
	$scope.lsPageComplete = function(){
		
		var complete = true;
			
		for (var i =0; i<$scope.lifeStyleQuestions.length; i++){
			if ($scope.lifeStyleQuestions[i].exists == "Y"){
				if ($scope.lifeStyleQuestions[i].details.length == 0){
					complete = false;
				}
			}
			if (!complete){
				break;
			}
		};
		return complete;
	}
	
	
	
	$scope.setGender=function(val)
	{
		if($scope.proposalInput.proposer.title=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	$scope.calulateBmi=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			
			if(key.type=='A')
				{
				var heighttm=$scope.proposalInput.insured[val].height*0.01;
				var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
				$scope.proposalInput.insured[val].bmi=parseInt(bmi)
				}
			
		})
	}

	
	function getRelGender(rel){
		var gender = "MF";
		if(rel=='Wife'  || rel=='Aunt'  || rel=='Daughter'  || rel=='Daughter in law'  || rel=='Grand Daughter'  || rel=='Grand Mother'  || rel=='Mother in law'  || rel=='Mother'   || rel=='Niece'  || rel=='Sister in law'  || rel=='Sister'){
			gender = "F";
				
		} else if (rel=='Brother in law'  || rel=='Brother'  || rel=='Father in law'  || rel=='Father'  || rel=='Grand Father'  || rel=='Grand Son'  || rel=='Husband'  || rel=='Nephew'   || rel=='Son in law'  || rel=='Son'  || rel=='Uncle'){
			gender = "M";
		}
		return gender;
	}

	//Nominee
	$scope.calculateAge=function(dob)
	{
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	
	
	$scope.checkMedicalQuition=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			angular.forEach($scope.proposalInput.insured[val].pedList,function(k,l){
				if(k.exists=='Y')
				{
					$scope.errorMessageMedical="You can't buy policy online"+$scope.proposalInput.insured[val].firstName+" has medical condition";
				}
			})
		})
	}
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	$scope.nextSection = function(section)
	{
		
		
		switch (section){
		case "1":
			$scope.calulateBmi();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.tabStatus.firstComplete = 'done';
			break;
		case "2":
			setRelationship();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =2;
			$scope.tabStatus.secondComplete = 'done';
			break;
		case "3":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.tabStatus.thirdComplete='done';			
			break;
		case "4":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.tabStatus.fourthComplete='done';
			break;
		case "5":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.tabStatus.fifthComplete = 'done';
			
			break;
		case "6":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.proposalInput.policy.policyStartdate=$filter('date')(new Date($scope.proposalInput.policy.policyStartdate),'yyyy-MM-dd');
			$scope.tabStatus.sixthComplete='done';
			break;
		case "7":
			$scope.proposalError={key:"",errorPirnt:[]}
			console.log(JSON.stringify($scope.proposalInput))
			$scope.tabStatus.seventhComplete='done';
		}
		
		saveApplicationData ();
		$scope.active=Number(section);
	}
	
	function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
		}
	
	function populateCoverData(){
		
		angular.forEach($scope.covers,function(value,key){
			
			if(value.isSelected == "Y")
				{
				 var cover = {};
				 cover.name = value.displayName;
				 $scope.proposalInput.covers.push(cover);
				}
			
		})
		
	}

	function ProcessMedicalList(){
		
		// initialize the medical detail Array
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			angular.forEach(insured.pedList,function(item,key2){
				item.pedData = [];
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.medicalQuestions,function(question,key){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(medDetail,key){
					 var questionId = medDetail.questionId;
					 var insuredId = medDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key1){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.pedList,function(item,key2){
								 if (item.pedCode == questionId){
									 item.exists = "Y";
									 var pedDetails = {}
						    	  	 pedDetails.monthOfDiagnosis = medDetail.monthOfDiagnosis;
									 pedDetails.yearOfDiagnosis = medDetail.yearOfDiagnosis;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 pedDetails.medicationDetail = medDetail.medicationDetail;
									 pedDetails.treatmentOutCome = medDetail.treatmentOutCome;
									 pedDetails.nameOfIllness = medDetail.nameOfIllness;
									 item.pedData.push(pedDetails);
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	
	function ProcessLSList(){
		
		// initialize the medical detail Array
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			angular.forEach(insured.lifestyleInfo,function(item,key2){
				item.lsData = [];
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.lifeStyleQuestions,function(question,key1){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(lsDetail,key2){
					 var questionId = lsDetail.questionId;
					 var insuredId = lsDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key3){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.lifestyleInfo,function(item,key4){
								 if (item.name == questionId){
									 item.exists = "Y";
									 var detail = {}
						    	  	 detail.quantity = lsDetail.quantity;
									 detail.noOfYears = lsDetail.noOfYears;
									 item.lsData.push(detail);
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	
	$scope.makePayment=function()
	{
		//$scope.proposalInput.proposer.proposerDob=$(filter);
		
		// Populate Medical and LifeStyle data
		$scope.loading = true;
		populateCoverData();
		
		var healthProposal = angular.copy($scope.proposalInput);		
		healthProposal.proposer.proposerDob = $filter('date')(new Date(healthProposal.proposer.proposerDob),'yyyy-MM-dd');
		healthProposal.proposer.nomineeDob = $filter('date')(new Date(healthProposal.proposer.nomineeDob),'yyyy-MM-dd');
		console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
		var input={url:'',data:{}}
		input.url='/submitProposal/health/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
		input.data=angular.copy(healthProposal);
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		tmp.$promise.then(function(data){
				if(data.error)
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
					//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
					if(data['error']=="Premium Mismatch")
					{
					 	
					 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]})
					}
				}
				else
				{
					$scope.proposalError={key:"",errorPirnt:[]}
					console.log(data);
					var payUrl = data.payUrl 
							   + '?agentId='+data.agentId
							   + '&premium='+data.premium
							   + '&apikey='+data.apikey
							   + '&quoteId='+data.quoteId
							   + '&version_no='+data.version_no
							   + '&strFirstName='+data.strFirstName
							   + '&strEmail='+data.strEmail
							   + '&isQuickRenew='+data.isQuickRenew
							   + '&crossSellProduct='+data.crossSellProduct
							   + '&crossSellQuoteid='+data.crossSellQuoteid
							   + '&returnUrl='+data.returnUrl
							   + '&vehicleSubLine='+data.vehicleSubLine
							   + '&elc_value='+data.elc_value
							   + '&paymentType='+data.paymentType;
					$window.location.href=payUrl;
				
				}
				
				$scope.loading = false;
		
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
			$scope.loading = false;
 		  })
	}
	
	getApplicationData();
	
	$scope.AddMedicalDetails = function(i){
		
		
		var input = {};
		input.questionId = $scope.medicalQuestions[i].code;
		var insureds = [];
		
		angular.forEach($scope.proposalInput.insured,function(val,key){
			var insured = {};
			insured.id = val.insuredId;
			insured.name = val.firstName + " " + val.lastName;
			insureds.push(insured);
		});
		
		input.insureds = insureds;
	
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/health/royalSundaram/medicalQuestionDetail.html',
		      controller: 'rsaMedicalQuestionDetailCtrl',
		      size: 'md',
		      resolve: {
		    	  'input': function () {
		          return input;
		        }
		      }
		});
		modalInstance.result.then(function (questionDetail) {
	      console.log(questionDetail);
	      if( typeof(questionDetail) != 'undefined')
	      	{
	    	  //Add Question details
	    	     $scope.medicalQuestions[i].details.push(questionDetail);
				 console.log($scope.medicalQuestions);
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	$scope.AddLSDetails = function(i){
		
		var input = {};
		input.questionId = $scope.lifeStyleQuestions[i].id;
		var insureds = [];
		
		angular.forEach($scope.proposalInput.insured,function(val,key){
			var insured = {};
			insured.id = val.insuredId;
			insured.name = val.firstName + " " + val.lastName;
			insureds.push(insured);
		});
		
		input.insureds = insureds;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/health/royalSundaram/lifestyleQuestionDetail.html',
		      controller: 'rsaLSQuestionDetailCtrl',
		      size: 'md',
		      resolve: {
		    	  'input': function () {
		          return input;
		        }
		      }
		});
		modalInstance.result.then(function (questionDetail) {
	      console.log(questionDetail);
	      if( typeof(questionDetail) != 'undefined' && questionDetail.action == "add")
	      	{
	    	  //Add Question details
	    	  $scope.lifeStyleQuestions[i].details.push(questionDetail);
	      	  console.log($scope.lifeStyleQuestions);
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	};	

	
	$scope.showTC=function(){
		
		var modalInstance=$uibModal.open({
		  animation: $scope.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      controller : 'tcCtrl',
	      templateUrl: 'templates/proposal/health/royalSundaram/tc.html'
		});
		

		modalInstance.result.then(function (agree) {
	      console.log(agree);
	      if( typeof(agree) != 'undefined')
	      	{
	    	  $scope.proposalInput.policy.tcAgree = agree;
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	
	$scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
	
	
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
     
     $scope.appointeeAge=function(dob) {
 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
 	}
     
     // Watch function // Watch function
     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {
         	
         	var date=new Date(newValue);
     		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+ Number($scope.proposalInput.policy.tenure),date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
         }
     	
     });
     
}])