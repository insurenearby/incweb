/**
 * 
 */
"use-strict"
brokeredgeApp.controller('rgiHealthCtrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','FormSubmitter',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,FormSubmitter){
		
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polArea = {};
	$scope.corArea = {}
	$scope.nomArea={};
	$scope.areasPolicy = []; 
	$scope.areasCorr = []; 
	$scope.areasNom = [];
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.paymentProcess = false;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}
	


	$scope.pageErrors = {
			insuredError: false,
			medicalError: false,
			insuredErrorText : "",
			medicalErrorText : ""
	}
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday)
    }
    
       
    $scope.opened= {
        	proposerDob : false,
        	nomineeDob : false,
        	appointeeDob : false,
        	policyStartDate : false
    };
	$scope.open1 = function() {
		
		$scope.popup1.opened = true;
	};
	$scope.open2 = function() {
		
		$scope.popup1.opened = true;
	};
   $scope.open = function(name) {
            $scope.opened[name] = true;
   };
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
	
   function init() {
		$scope.input.lob = "Health";
		$scope.input.productType = $scope.proposalInput.productType;
		$scope.proposalInput.medicalDetails = [];
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			
			switch (addressType){
			case "C":
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCityCode = value.cityCode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStateCode = value.stateCode;
				$scope.proposalInput.proposer.corrDistrict = value.district;
				$scope.proposalInput.proposer.corrDistrictId = value.districtId;
				$scope.proposalInput.proposer.corrPincode = value.pincode;
				$scope.corArea.areaId = value.areaCode;
				$scope.corArea.areaName = value.addressLine3;
				$scope.corArea.stateId = value.stateCode;
				$scope.corArea.stateName = value.state;
				$scope.corArea.cityId = value.cityCode;
				$scope.corArea.cityName = value.city;
				$scope.corArea.districtId = value.districtId;
				$scope.corArea.districtName = value.district;
				
				break;
			case "P":
				$scope.proposalInput.proposer.policyAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.policyAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.policyCity = value.city;
				$scope.proposalInput.proposer.policyCityCode = value.cityCode;
				$scope.proposalInput.proposer.policyState = value.state;
				$scope.proposalInput.proposer.policyStateCode = value.stateCode;
				$scope.proposalInput.proposer.policyDistrict = value.district;
				$scope.proposalInput.proposer.policyDistrictId = value.districtId;
				$scope.proposalInput.proposer.policyPincode = value.pincode;
				$scope.polArea.areaId = value.areaCode;
				$scope.polArea.areaName = value.addressLine3;
				$scope.polArea.stateId = value.stateCode;
				$scope.polArea.stateName = value.state;
				$scope.polArea.cityId = value.cityCode;
				$scope.polArea.cityName = value.city;
				$scope.polArea.districtId = value.districtId;
				$scope.polArea.districtName = value.district;
				break;
			case "N":
				$scope.proposalInput.proposer.nomineeAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.nomineeAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.nomineeCity = value.city;
				$scope.proposalInput.proposer.nomineeCityCode = value.cityCode;
				$scope.proposalInput.proposer.nomineeState = value.state;
				$scope.proposalInput.proposer.nomineeStateCode = value.stateCode;
				$scope.proposalInput.proposer.nomineeDistrict = value.district;
				$scope.proposalInput.proposer.nomineeDistrictId = value.districtId;
				$scope.proposalInput.proposer.nomineePincode = value.pincode;
				$scope.nomArea.areaId = value.areaCode;
				$scope.nomArea.areaName = value.addressLine3;
				$scope.nomArea.stateId = value.stateCode;
				$scope.nomArea.stateName = value.state;
				$scope.nomArea.cityId = value.cityCode;
				$scope.nomArea.cityName = value.city;
				$scope.nomArea.districtId = value.districtId;
				$scope.nomArea.districtName = value.district;
				break;
			}
		});
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		getMaster($scope.proposalInput.productId, "Occupation");
		getMaster($scope.proposalInput.productId, "Relationship");
		getMaster($scope.proposalInput.productId, "MaritalStatus");
		getMaster($scope.proposalInput.productId, "Nationality");
		getMaster($scope.proposalInput.productId, "NomineeRel");
		getQuestions($scope.proposalInput.productId, "PedList")	;	
		getMaster($scope.proposalInput.productId, "Disease")	;
		
		getAreasFromPincode('P', $scope.proposalInput.proposer.policyPincode);
		getAreasFromPincode('C', $scope.proposalInput.proposer.corrPincode);
		getAreasFromPincode('N', $scope.proposalInput.proposer.nomineePincode);
		$scope.proposalInput.policy.tcAgree = "N";
				
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalQuestions();
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;
		
		/*Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates();
				console.log($scope.proposalInput);
				
				
			} else
			{
				
				// Show Error page;
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates(){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob.substr(0, 10));	
		}
		
		if (typeof($scope.proposalInput.proposer.nomineeDob) != "undefined" && $scope.proposalInput.proposer.nomineeDob != ""){
			$scope.proposalInput.proposer.nomineeDob = new Date($scope.proposalInput.proposer.nomineeDob.substr(0, 10));
		}
		
		if (typeof($scope.proposalInput.proposer.appointeeDOB) != "undefined" && $scope.proposalInput.proposer.appointeeDOB != ""){
			$scope.proposalInput.proposer.appointeeDOB = new Date($scope.proposalInput.proposer.appointeeDOB.substr(0, 10));
		}
		
		
		
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
			$scope.medicalQuestions[i].exists = "";
		}
		$scope.insureds = [];
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			
			angular.forEach(insured.pedList,function(item,key){
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						if ($scope.medicalQuestions[i].exists == ""){
							$scope.medicalQuestions[i].exists = item.exists;
						}
						if (item.exists == 'Y'){
							$scope.medicalQuestions[i].exists = "Y";
							angular.forEach(item.pedData,function(illness,key){
								var detail = {};
								detail.insuredId = insuredId.toString();
								detail.insuredName = insuredName;
								detail.diseaseId = illness.diseaseId;
								detail.code = code;
								detail.conditionSince = new Date(illness.conditionSince + "-01")
								$scope.medicalQuestions[i].details.push(detail);
							});
							
						}
					}
				}
				
			})
		})
		console.log($scope.medicalQuestions);
	}
	
	$scope.AddMedicalRow = function(code) {
		
		var detail = {};
		detail.insuredId = "";
		detail.conditionSince = "";
		detail.diseaseId = "";
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.push(detail);
				break;
			}
		}
		
	}
	
	$scope.DeleteMedicalRow = function(code, j){
		
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.splice(j);
				break;
			}
		}
	}
	
	function ProcessMedicalQuestions(){
		
		// initialize the medical detail Array
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			angular.forEach(insured.pedList,function(item,key2){
				item.pedData = []; // Use an array even if there is only one item
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.medicalQuestions,function(question,key){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(medDetail,key){
					 var code = question.code;
					 var insuredId = medDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key1){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.pedList,function(item,key2){
								 if (item.pedCode == code){
									 item.exists = "Y";
									 var pedDetails = {}
						    	  	 pedDetails.conditionSince = $filter('date')(new Date(medDetail.conditionSince),'yyyy-MM');
									 pedDetails.diseaseId = medDetail.diseaseId;
									 item.pedData.push(pedDetails);								 
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	
	 function getAreasFromPincode (type, pincode){
			
			var re = new RegExp("^[0-9]{6}$");
			if (re.test(pincode)){
				
				var input={url:'',data:{}}
		    	
				input.url="/master/getDetailsForPincode/" + $scope.proposalInput.insurerId + "/" + pincode;
				
		    	var res = $resource('./DataRequest', [], {
			          save: {
			             method: 'POST'}
			    });
				
				var tmp = res.save(input);
				tmp.$promise.then(function(data){
					if (data.hasOwnProperty("result")){
						 if (type == "P"){
							 $scope.areasPolicy = data.result;
						 } else if (type == "C")
						 {
							 $scope.areasCorr = data.result; 
						 } else if(type == "N"){
							$scope.areasNom = data.result; 
						 }
					}
					
					
				})		
			}
		}
	 
		$scope.setCityState = function(type, area){
			
			var areas = []
			if (typeof(area) != "undefined") {
				 if (type == "P"){
					 $scope.proposalInput.proposer.policyCity = area.cityName;
					 $scope.proposalInput.proposer.policyCityCode = area.cityId;
					 $scope.proposalInput.proposer.policyState = area.stateName;
					 $scope.proposalInput.proposer.policyStateCode = area.stateId;
					 $scope.proposalInput.proposer.policyDistrict = area.districtName;
					 $scope.proposalInput.proposer.policyDistrictId = area.districtId;
					 $scope.proposalInput.proposer.policyArea = area.areaName;
					 $scope.proposalInput.proposer.policyareaCode = area.areaId;
					 
				 } else if (type == "C")
				 {
					 $scope.proposalInput.proposer.corrCity = area.cityName;
					 $scope.proposalInput.proposer.corrCityCode = area.cityId;
					 $scope.proposalInput.proposer.corrState = area.stateName;
					 $scope.proposalInput.proposer.corrStateCode = area.stateId;
					 $scope.proposalInput.proposer.corrDistrict = area.districtName;
					 $scope.proposalInput.proposer.corrDistrictId = area.districtId;
					 $scope.proposalInput.proposer.corrArea = area.areaName;
					 $scope.proposalInput.proposer.corrareaCode = area.areaId;
				 } else if (type == "N"){
					 $scope.proposalInput.proposer.nomineeCity = area.cityName;
					 $scope.proposalInput.proposer.nomineeCityCode = area.cityId;
					 $scope.proposalInput.proposer.nomineeState = area.stateName;
					 $scope.proposalInput.proposer.nomineeStateCode = area.stateId;
					 $scope.proposalInput.proposer.nomineeDistrict = area.districtName;
					 $scope.proposalInput.proposer.nomineeDistrictId = area.districtId;
					 $scope.proposalInput.proposer.nomineeArea = area.areaName;
					 $scope.proposalInput.proposer.nomineeareaCode = area.areaId;
				 }				
			}

		}
	
		
		function getMaster(productId, masterId){
			$scope.insurerMaster[masterId] = [];
			var tmp=brokeredgefactory.getMaster(productId, masterId);
			tmp.$promise.then(function(data){
				//console.log(data);
				$scope.insurerMaster[masterId]= data['result'];
				if (masterId == "Relationship"){
					addGendertoRel();
				}
				//console.log($scope.insurerMaster);
			})
		}

	
		function getQuestions(productId, type){
			var questions = [];
			 var input = {
		    			url: '/master/getQuestions/' + productId + "/" + type,
		    			data : {}
		    	};
		        
				var res = $resource('./DataRequest', [], {
			          process: {
			             method: 'POST'}
			    });
			var tmp=res.process(input);
			tmp.$promise.then(function(data){
				//console.log(data);
				if (data.hasOwnProperty("result")){
					if (type == "PedList"){
						$scope.medicalQuestions = data.result;
						initializeMedicalQuestions();
					}
					
				}
				//console.log($scope.insurerMaster);
			})
			
		}
	
		$scope.getMasterValue = function(masterName, masterId){
			// 
			var masterValue = masterId;
			var values = $scope.insurerMaster[masterName];
			
			if (typeof(values) != 'undefined'){
				for (var i = 0; i<values.length; i++){
					if (values[i].id == masterId){
						masterValue = values[i].value; 
						break;
					}
				}
			}
			return masterValue; 
		}
	
		$scope.getQuestionText = function(type, id){
			// 
			var questionText = "";
			var values = [];
			if (type == "PedList"){
				values = $scope.medicalQuestions;
			}
	
					
			if (typeof(values) != 'undefined'){
				for (var i = 0; i<values.length; i++){
					if (values[i].id == id){
						questionText = values[i].value; 
						break;
					}
				}
			}
			return questionText; 
		}
	
		function addGendertoRel (){
			var records = [];
			angular.forEach($scope.insurerMaster.Relationship,function(val,key){
				var record = val;
				record.gender = getRelGender(val.value);
				records.push(record);
			});
			$scope.insurerMaster.Relationship = records;
		}
	
	
	//validate of 
		$scope.validateOcupation=function()
		{
			if($scope.proposalInput.proposer.occupationType=='POLITICIAN')
			{
				$scope.ocupationError="Please contact our nearest branch or kindly chat with our online representative or call us at 1860 425 0000 for more information.";
			}
			else
				{
				$scope.ocupationError="";
				}
		}
	
		$scope.checkRelationShip=function(form)
	     {
			$scope.pageErrors.insuredError= false;
			$scope.pageErrors.insuredErrorText = "";
			$scope.proposalInput.proposer.title= "";
			$scope.proposalInput.proposer.firstName = "";
			$scope.proposalInput.proposer.middleName = "";
			$scope.proposalInput.proposer.lastName= "";
			$scope.proposalInput.proposer.gender = "";
			$scope.proposalInput.proposer.proposerDob =  "";
			$scope.proposalInput.proposer.occupationType= "";
			$scope.proposalInput.proposer.maritalStatus= "";
			$scope.proposalInput.proposer.designation = "";
			$scope.proposalInput.proposer.business = "";
			$scope.isProposerInsured = false;
			$scope.proposalInput.proposer.isPrimaryInsured = "N";
			$scope.proposalInput.proposer.IsProposerInsured = "N";
	    	var selfCount = 0;
	    	angular.forEach($scope.proposalInput.insured, function(val,key){
	    		
	    		if (val.relationshipId == "345"){
	    			
	    			$scope.proposalInput.proposer.title= val.title;
	    			$scope.proposalInput.proposer.firstName = val.firstName;
	    			$scope.proposalInput.proposer.middleName = val.middleName;
	    			$scope.proposalInput.proposer.lastName= val.lastName;
	    			$scope.proposalInput.proposer.gender = val.gender;
	    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
	    			$scope.proposalInput.proposer.occupationType=val.occupationType;
	    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
	    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
	    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
	    			if (val.type == "C"){ // Proposer cannot be a child.
	    				$scope.pageErrors.insuredError= true;
	    				$scope.pageErrors.insuredErrorText = "Child cannot be a proposer";
	    			}
	    			selfCount++;
	    			$scope.isProposerInsured = true;
	    			console.log($scope.proposalInput.proposer);
	    		}
	    	});
	    	if (selfCount>1) {
				$scope.pageErrors.insuredError= true;
				$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
	    	}
	    	if (selfCount==0) {
	    		$scope.pageErrors.insuredError= true;
				$scope.pageErrors.insuredErrorText = "Proposer Must be insured";
	    	}
	    	
	     }
		
		function setRelationship(){

	    	angular.forEach($scope.proposalInput.insured, function(val,key){
	    		
	    		if (val.relationshipId == "345"){
	    			
	    			$scope.proposalInput.proposer.title= val.title;
	    			$scope.proposalInput.proposer.firstName = val.firstName;
	    			$scope.proposalInput.proposer.middleName = val.middleName;
	    			$scope.proposalInput.proposer.lastName= val.lastName;
	    			$scope.proposalInput.proposer.gender = val.gender;
	    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
	    			$scope.proposalInput.proposer.occupationType=val.occupationType;
	    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
	    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
	    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
	    			$scope.isProposerInsured = true;
	    			console.log($scope.proposalInput.proposer);
	    		}
	    	});
			
		}
		
		$scope.medicalPageComplete = function(){
			
			var complete = true;
			var questionId = 0;
			
			for (var i =0; i<$scope.medicalQuestions.length; i++){
				if ($scope.medicalQuestions[i].exists == "Y"){
					complete = false;
					for (var j =0; j<$scope.proposalInput.medicalDetails.length; j++){
						if ($scope.proposalInput.medicalDetails[j].questionId == questionId){
							complete = true;
							break;
						}
					}
				}
				if (!complete){
					break;
				}
				questionId++;
			};
			return complete;
		}
		
		$scope.setGender=function(val)
		{
			if($scope.proposalInput.proposer.title=='Mr') {
				$scope.proposalInput.proposer.gender='M';
			}
			else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
			{
				$scope.proposalInput.proposer.gender = "F";
			}
		}
		
		function setCover (covers){
			angular.forEach(covers,function(val,key){
				var cover = {};
				cover.name = val;
				$scope.proposalInput.covers.push(cover);			
			})
		}
	
	
		$scope.calulateBmi=function()
		{
			angular.forEach($scope.proposalInput.insured,function(key,val){
				
				if(key.type=='A')
					{
					var heighttm=$scope.proposalInput.insured[val].height*0.01;
					var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
					$scope.proposalInput.insured[val].bmi=parseInt(bmi)
					}
				
			})
		}
	
		
		function getRelGender(rel){
			var gender = "MF";
			if(rel=='Wife'  || rel=='Aunt'  || rel=='Daughter'  || rel=='Daughter in law'  || rel=='Grand Daughter'  || rel=='Grand Mother'  || rel=='Mother in law'  || rel=='Mother'   || rel=='Niece'  || rel=='Sister in law'  || rel=='Sister'){
				gender = "F";
					
			} else if (rel=='Brother in law'  || rel=='Brother'  || rel=='Father in law'  || rel=='Father'  || rel=='Grand Father'  || rel=='Grand Son'  || rel=='Husband'  || rel=='Nephew'   || rel=='Son in law'  || rel=='Son'  || rel=='Uncle'){
				gender = "M";
			}
			return gender;
		}
	
		//Nominee
		$scope.calculateAge=function(dob)
		{
			$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
		}
		
		
		$scope.checkMedicalQuition=function()
		{
			angular.forEach($scope.proposalInput.insured,function(key,val){
				angular.forEach($scope.proposalInput.insured[val].pedList,function(k,l){
					if(k.exists=='Y')
					{
						$scope.errorMessageMedical="You can't buy policy online"+$scope.proposalInput.insured[val].firstName+" has medical condition";
					}
				})
			})
		}
		//next tab 
		//next section
		$scope.back=function(index)
		{
			$scope.active=index;
		}
		$scope.nextSection = function(section)
		{
			
			switch (section){
			case "1":
				
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active=1;
				$scope.tabStatus.firstComplete = 'done';
				break;
			case "2":
				$scope.calulateBmi();
				setRelationship();
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =2;
				$scope.tabStatus.secondComplete = 'done';
				break;
			case "3":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =3;
				$scope.tabStatus.thirdComplete='done';			
				break;
			case "4":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =4;
				$scope.tabStatus.fourthComplete='done';
				break;
			case "5":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.active =5;
				$scope.tabStatus.fifthComplete = 'done';
				
				break;
			case "6":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.proposalInput.policy.policyStartdate=$filter('date')(new Date($scope.proposalInput.policy.policyStartdate),'yyyy-MM-dd');
				$scope.active =6;
				$scope.tabStatus.sixthComplete='done';
				break;
			case "7":
				$scope.proposalError={key:"",errorPirnt:[]}
				$scope.proposalInput.policy.policyStartdate=$filter('date')(new Date($scope.proposalInput.policy.policyStartdate),'yyyy-MM-dd');
				$scope.proposalInput.proposer.proposerDob=$filter('date')(new Date($scope.proposalInput.proposer.proposerDob),'yyyy-MM-dd');
				console.log(JSON.stringify($scope.proposalInput))
				$scope.active=7;
				$scope.tabStatus.seventhComplete='done';
			}
			
			saveApplicationData ();
		}
		
		function cleanArray(actual) {
			  var newArray = new Array();
			  for (var i = 0; i < actual.length; i++) {
			    if (actual[i]) {
			      newArray.push(actual[i]);
			    }
			  }
			  return newArray;
			}
		
		function populateCoverData(){
			
			angular.forEach($scope.covers,function(value,key){
				
				if(value.isSelected == "Y")
					{
					 var cover = {};
					 cover.name = value.displayName;
					 $scope.proposalInput.covers.push(cover);
					}
				
			})
			
		}
		
		$scope.samePolicyCorrAddress=function( flag)
		{
			if (flag == "Y"){
				copyPolicyAddress();
			} else
			{
				$scope.proposalInput.proposer.corrAddressLine1= "";
				$scope.proposalInput.proposer.corrAddressLine2= "";
				$scope.proposalInput.proposer.corrAddressLine3= "";
				$scope.proposalInput.proposer.corrLandmark= "";
				$scope.proposalInput.proposer.corrStateCode= "";
				$scope.proposalInput.proposer.corrState = "";
				$scope.proposalInput.proposer.corrCityCode = "";
				$scope.proposalInput.proposer.corrCity = "";
				$scope.proposalInput.proposer.corrDistrictId = "";
				$scope.proposalInput.proposer.corrDistrict = "";
				$scope.proposalInput.proposer.corrareaCode ="";
				$scope.proposalInput.proposer.corrArea ="";
				$scope.proposalInput.proposer.corrPincode = "";
			}
			
			 
		}
	

		
		function copyPolicyAddress(){
			$scope.proposalInput.proposer.corrAddressLine1=$scope.proposalInput.proposer.policyAddressLine1;
			$scope.proposalInput.proposer.corrAddressLine2=$scope.proposalInput.proposer.policyAddressLine2;
			$scope.proposalInput.proposer.corrAddressLine3=$scope.proposalInput.proposer.policyArea;
			$scope.proposalInput.proposer.corrLandmark=$scope.proposalInput.proposer.policyLandmark;
			$scope.proposalInput.proposer.corrStateCode=$scope.proposalInput.proposer.policyStateCode;
			$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
			$scope.proposalInput.proposer.corrCityCode =$scope.proposalInput.proposer.policyCityCode;
			$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
			$scope.proposalInput.proposer.corrDistrictId =$scope.proposalInput.proposer.policyDistrictId;
			$scope.proposalInput.proposer.corrDistrict =$scope.proposalInput.proposer.policyDistrict;
			$scope.proposalInput.proposer.corrareaCode =$scope.proposalInput.proposer.policyareaCode;
			$scope.proposalInput.proposer.corrArea =$scope.proposalInput.proposer.policyArea;
			$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
		}
	
		function setpolicyAddress(){
	 		
			if ($scope.proposalInput.proposer.policyRegAddressSame == 'Y'){
				copyPolicyAddress();
			}
			
			var addresses = [];
			var address = {}
			address.addressType = "C";
	 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
			address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
			address.addressLine3 = $scope.proposalInput.proposer.corrArea
			address.areaCode = $scope.proposalInput.proposer.corrareaCode;
			address.district = $scope.proposalInput.proposer.corrDistrict;
			address.districtId = $scope.proposalInput.proposer.corrDistrictId;
			address.city = $scope.proposalInput.proposer.corrCity;
			address.cityCode = $scope.proposalInput.proposer.corrCityCode;
			address.state = $scope.proposalInput.proposer.corrState;
			address.stateCode = $scope.proposalInput.proposer.corrStateCode;
			address.pincode = $scope.proposalInput.proposer.corrPincode;
			addresses.push(address);
			
			var address = {}
			address.addressType = "P";
	 		address.addressLine1 = $scope.proposalInput.proposer.policyAddressLine1;
			address.addressLine2 = $scope.proposalInput.proposer.policyAddressLine2;
			address.addressLine3 = $scope.proposalInput.proposer.policyArea;
			address.areaCode = $scope.proposalInput.proposer.policyareaCode;
			address.district = $scope.proposalInput.proposer.policyDistrict;
			address.districtId = $scope.proposalInput.proposer.policyDistrictId;
			address.city = $scope.proposalInput.proposer.policyCity;
			address.cityCode = $scope.proposalInput.proposer.policyCityCode;
			address.state = $scope.proposalInput.proposer.policyState;
			address.stateCode = $scope.proposalInput.proposer.policyStateCode;
			address.pincode = $scope.proposalInput.proposer.policyPincode;
			addresses.push(address);
			
			
	
			var address = {}
			address.addressType = "N";
	 		address.addressLine1 = $scope.proposalInput.proposer.nomineeAddressLine1;
			address.addressLine2 = $scope.proposalInput.proposer.nomineeAddressLine2;
			address.addressLine3 = $scope.proposalInput.proposer.nomineeArea;
			address.areaCode = $scope.proposalInput.proposer.nomineeareaCode;
			address.district = $scope.proposalInput.proposer.nomineeDistrict;
			address.districtId = $scope.proposalInput.proposer.nomineeDistrictId;
			address.city = $scope.proposalInput.proposer.nomineeCity;
			address.cityCode = $scope.proposalInput.proposer.nomineeCityCode;
			address.state = $scope.proposalInput.proposer.nomineeState;
			address.stateCode = $scope.proposalInput.proposer.nomineeStateCode;
			address.pincode = $scope.proposalInput.proposer.nomineePincode;
			addresses.push(address);
			$scope.proposalInput.proposer.address = angular.copy(addresses);
			
			var contacts = []
			var contact = {};
			contact.contactType = "mobile";
			contact.contactText = $scope.proposalInput.proposer.mobile;
			contacts.push(contact);
			var contact = {};
			contact.contactType = "email";
			contact.contactText = $scope.proposalInput.proposer.email;
			contacts.push(contact);
			$scope.proposalInput.proposer.contacts = angular.copy(contacts);
			
	 	}
	
		$scope.makePayment=function()
		{
			//$scope.proposalInput.proposer.proposerDob=$(filter);
			
			// Populate Medical and LifeStyle data
			$scope.paymentProcess = true;
			populateCoverData();
			$scope.proposalInput.proposer.nomineeName = $scope.proposalInput.proposer.nomineeFirstName + " " + $scope.proposalInput.proposer.nomineeLastName;
			console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
			if ($scope.proposalInput.policy.floater == 'N'){
				$scope.proposalInput.policy.floaterSi = $scope.proposalInput.policy.sumInsured;
			}
			
			var healthProposal = angular.copy($scope.proposalInput);
			healthProposal.policy.policyStartdate = $filter('date')(new Date(healthProposal.policy.policyStartdate),'yyyy-MM-dd');
			healthProposal.proposer.proposerDob = $filter('date')(new Date(healthProposal.proposer.proposerDob),'yyyy-MM-dd');
			healthProposal.proposer.nomineeDOB = $filter('date')(new Date(healthProposal.proposer.nomineeDOB),'yyyy-MM-dd');
			
			var input={url:'',data:{}}
			input.url='/submitProposal/health/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
			input.data=angular.copy(healthProposal);
			var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
			$scope.loading = true;
			tmp.$promise.then(function(data){
				$scope.loading = false;
					if(data.hasOwnProperty("error"))
					{
						$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
						var str=data['error'];
						$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
						console.log($scope.proposalError)
						//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
						if(data['error']=="Premium Mismatch")
						{
						 	
						 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]})
						}
						$scope.paymentProcess = false;
					}
					else
					{
						$scope.proposalError={key:"",errorPirnt:[]}
						
						//$scope.returnUrl="http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId;
						console.log(data);
						// Parse the response
						var paymentInput = {};
						
						var method = 'POST';	
						paymentInput.ProposalNo = data.ProposalNo;
						paymentInput.UserID = data.UserID;
						paymentInput.PaymentType = "1";
						paymentInput.ProposalAmount = data.ProposalAmount;
						paymentInput.Responseurl = data.Responseurl;
						var url = data.payUrl;
						
						FormSubmitter.submit(url, method, paymentInput);
					} 
				
			
			},
	 		  function(error) {
	 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
				var str="Unexpected error from Insurer while processing the policy";
				$scope.proposalError.errorPirnt=cleanArray(str.split(";"));
				$scope.loading = false;
	 		  })

		}
		
		/*function submitPayment(url, method, params) {
            url = $sce.trustAsResourceUrl(url);

            $rootScope.$broadcast('form.submit', {
                url: url,
                method: method,
                params: params
            });
        }*/
		
		getApplicationData();
		
		$scope.AddMedicalDetails = function(questionId){
			
			var input = {};
			input.questionId = questionId;
			var insureds = [];
			
			angular.forEach($scope.proposalInput.insured,function(val,key){
				var insured = {};
				insured.id = key;
				insured.name = val.firstName + " " + val.lastName;
				insureds.push(insured);
			});
			
			input.insureds = insureds;
		
			var modalInstance=$uibModal.open({
				  animation: $scope.animationsEnabled,
			      ariaLabelledBy: 'modal-title',
			      ariaDescribedBy: 'modal-body',
			      templateUrl: 'templates/proposal/health/royalSundaram/medicalQuestionDetail.html',
			      controller: 'rsaMedicalQuestionDetailCtrl',
			      size: 'md',
			      resolve: {
			    	  'input': function () {
			          return input;
			        }
			      }
			});
			modalInstance.result.then(function (questionDetail) {
		      console.log(questionDetail);
		      if( typeof(questionDetail) != 'undefined' && questionDetail.action == "add")
		      	{
		    	  //Add Question details
		      	  $scope.proposalInput.medicalDetails.push(questionDetail);
		      	  console.log($scope.proposalInput.medicalDetails);
		      	}
	      }, function () {
	          //console.log('Modal dismissed at: ' + new Date());
	      });
			
		}
		
		$scope.showTC=function(){
			
			var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      controller : 'tcCtrl',
		      templateUrl: 'templates/proposal/health/rgi/tc.html',
		      size: 'lg'
			});
			modalInstance.result.then(function (agree) {
		      console.log(agree);
		      if( typeof(agree) != 'undefined')
		      	{
		    	  $scope.proposalInput.policy.tcAgree = agree;
		      	}
	      }, function () {
	          //console.log('Modal dismissed at: ' + new Date());
	      });
			
		}
		
		
		$scope.newPreMiumMismatch=function(item)
		{
			$scope.items=item;
			var modalInstance=$uibModal.open({
				  animation: $scope.animationsEnabled,
			      ariaLabelledBy: 'modal-title',
			      ariaDescribedBy: 'modal-body',
			      templateUrl: 'templates/proposal/premiumModal.html',
			      controller: 'premiumModalCtrl',
			      size: 'md',
			      resolve: {
			    	  'premiums': function () {
			          return $scope.items;
			        }
			      }
			});
			modalInstance.result.then(function (selectedProduct) {
	        $scope.selected = selectedProduct;
	        if($scope.selected!=undefined)
	        	{
	        	//basePremium.
	        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
	        	$scope.makePayment();
	        	
	        	}
	        }, function () {
	            //console.log('Modal dismissed at: ' + new Date());
	        });
		}
		
		
		$scope.backPreviousPage=function()
		{
			window.history.back();
		}
		
		$scope.rowWidth=function(a)
		{
			return 100/parseInt(a);
		}
		
		$scope.filterValue = function($event){
	        if(isNaN(String.fromCharCode($event.keyCode))){
	            $event.preventDefault();
	        }
	     };
	     
	     $scope.appointeeAge=function(dob) {
	 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
	 	 }
	     
	     // Change State
	     $scope.$watch('proposalInput.proposer.policyPincode', function(newValue, oldValue) {
	         
	     	if ((typeof newValue != "undefined")&& (newValue != "")) {
	     		getAreasFromPincode("P", newValue);
	     	}
	     	
	     });
	     
	     $scope.$watch('proposalInput.proposer.corrPincode', function(newValue, oldValue) {
	         
	     	if ((typeof newValue != "undefined")&& (newValue != "")) {
	     		getAreasFromPincode("C", newValue);
	     	}
	     	
	     });
	     
	     $scope.$watch('proposalInput.proposer.nomineePincode', function(newValue, oldValue) {
	         
		     	if ((typeof newValue != "undefined")&& (newValue != "")) {
		     		getAreasFromPincode("N", newValue);
		     	}
		     	
		  }); 
	     
	     // Watch function
	     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
	         
	         if ((typeof newValue != "undefined")&& (newValue != "")) {
	         	
	         	var date=new Date(newValue);
	     		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+ Number($scope.proposalInput.policy.tenure),date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
	         }
	     	
	     });
}])