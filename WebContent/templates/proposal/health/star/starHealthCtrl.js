/**
 * 
 */
"use-strict"
brokeredgeApp.controller('starHealthCtrl',['$scope','brokeredgefactory','$routeParams','$filter','$resource','$uibModal','$http','$window','paymentService',function($scope,brokeredgefactory,$routeParams,$filter,$resource,$uibModal,$http,$window,paymentService){
	$scope.proposalList=['insured member','medical history','Life Style','proposer','nominee','contact info','T & C']
	
	$scope.medicalQuestions=[];
	$scope.loading = true;
	$scope.proposalInput={};
	$scope.applicationData={};
	$scope.polArea = {};
	$scope.corArea = {}
	$scope.polState={};
	$scope.polCity={};
	$scope.nomState={};
	$scope.nomCity={};
	$scope.polCities = [];
	$scope.nomCities = [];
	$scope.areasPolicy = []; 
	$scope.areasCorr = []; 
	$scope.isProposerInsured = false;
	$scope.active=0;
	$scope.iMainTabIndex =0;
	$scope.iTab1Index = 0;
	$scope.iTabLifeIndex=0;
	$scope.iTab1Indexs=function(val)
	{
		$scope.iTab1Index=val;
	}
	$scope.iTabLifeIndexs=function(val)
	{
		$scope.iTabLifeIndex=val;
	}
	$scope.input={lob:"",productType:""}
	$scope.insurerMaster = {};
		//$scope.active=4;
	$scope.tabStatus={
			firstComplete : 'indone',
			secondComplete : 'indone',
			thirdComplete : 'indone',
			fourthOpen :[],
			fourthDisabled :[],
			fourthComplete:[],
			fifthDisabled : true,
			fifthComplete : 'indone',
			sixthComplete:'indone',
			seventhComplete:"indone"
	}
	$scope.medicalTab=0;
	$scope.medicalQuitionTab=function(index)
	{
		$scope.medicalTab=index;
	}

	$scope.pageErrors = {
			insuredError: false,
			medicalError: false,
			insuredErrorText : "",
			medicalErrorText : "",
			proposerError: false,
			proposerErrorText: "",
	}
	
	
	var dateToday=new Date();
	var yearMax=dateToday.getFullYear();
	var monthToday=dateToday.getMonth();
	var dayToday=dateToday.getDate();
	var yearMin=dateToday.getFullYear();
	var newYearMin=dateToday.getFullYear();
    var newYearMax=dateToday.getFullYear();
    var newMaxMonthToday=dateToday.getMonth();
    var newdayToday = dateToday.getDate();
    
    
    
    // oldPolicyEndDateOption
    $scope.pastDateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }
    
    $scope.nomineeDobOptions = {
    		maxDate: new Date(yearMax, monthToday-3, dayToday),
    		minDate: new Date(yearMin-100, monthToday, dayToday)	
    }

	$scope.adultDobOptions={
	    	maxDate: new Date(newYearMax-18, newMaxMonthToday, newdayToday),
	    	minDate: new Date(newYearMin-100, monthToday, newdayToday)
	}
    
    
    $scope.policyStartDateOptions={
			maxDate: new Date(newYearMax, newMaxMonthToday, newdayToday+44),
	    	minDate: new Date(newYearMin, monthToday, newdayToday+1)
    }
    
       
    
    $scope.opened= {
        	proposerDob : false,
        	nomineeDob : false,
        	appointeeDob : false,
        	policyStartDate : false
    };
   $scope.open = function(name) {
        $scope.opened[name] = true;
   };
   $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
   $scope.format = $scope.formats[0];
   $scope.altInputFormats = ['yyyy-MM-dd', 'M!/d!/yyyy'];
    

	
	function init() {
		$scope.input.lob = "Health";
		$scope.input.productType = $scope.proposalInput.productType;
		$scope.proposalInput.medicalDetails = [];
		$scope.covers = [];
		angular.forEach($scope.proposalInput.addons,function(value,key){
			var cover = angular.copy(value);
			$scope.covers.push(cover);				
		})
		
		angular.forEach($scope.proposalInput.proposer.address,function(value,key){
			var addressType = value.addressType;
			switch (addressType){
			case "C":
				$scope.corArea.cityName = value.city;
				$scope.corArea.cityId = value.citycode;
				$scope.corArea.stateName = value.state ;
				$scope.corArea.stateId =  value.statecode;
				$scope.corArea.areaName = value.areaName;
				$scope.corArea.areaId = value.areaId ;
				$scope.proposalInput.proposer.corrAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.corrAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.corrCity = value.city;
				$scope.proposalInput.proposer.corrCitycode = value.citycode;
				$scope.proposalInput.proposer.corrState = value.state;
				$scope.proposalInput.proposer.corrStatecode = value.statecode;
				$scope.proposalInput.proposer.corrArea = value.areaName;
				$scope.proposalInput.proposer.corrAreaCode = value.areaId;
				$scope.proposalInput.proposer.corrPincode = value.pincode;
				break;
			case "P":
				$scope.polArea.cityName = value.city;
				$scope.polArea.cityId = value.citycode;
				$scope.polArea.stateName = value.state ;
				$scope.polArea.stateId =  value.statecode;
				$scope.polArea.areaName = value.areaName;
				$scope.polArea.areaId = value.areaId ;
				$scope.proposalInput.proposer.policyAddressLine1 = value.addressLine1;
				$scope.proposalInput.proposer.policyAddressLine2 = value.addressLine2 ;
				$scope.proposalInput.proposer.policyCity = value.city;
				$scope.proposalInput.proposer.policyCitycode = value.citycode;
				$scope.proposalInput.proposer.policyState = value.state;
				$scope.proposalInput.proposer.policyStatecode = value.statecode;
				$scope.proposalInput.proposer.policyArea = value.areaName;
				$scope.proposalInput.proposer.policyAreaCode = value.areaId;
				$scope.proposalInput.proposer.policyPincode = value.pincode;
				break;
			}
		});
		
		
		
		angular.forEach($scope.proposalInput.proposer.contacts,function(value,key){
			var contactType = value.contactType;
			switch(contactType){
			case "mobile":
				$scope.proposalInput.proposer.mobile = value.contactText;
				break;
			case "email":
				$scope.proposalInput.proposer.email = value.contactText;
				break;
			}
		});
		
		formatDates ();
		getMaster($scope.proposalInput.productId, "Occupation");
		getMaster($scope.proposalInput.productId, "Relationship");
		getMaster($scope.proposalInput.productId, "NomineeRel");
		getMaster($scope.proposalInput.productId, "AppointeeRel");
		getQuestions($scope.proposalInput.productId, "PedList");
		
		getAreasFromPincode('P', $scope.proposalInput.proposer.policyPincode);
		$scope.setCityState('P',$scope.polArea);
		getAreasFromPincode('C', $scope.proposalInput.proposer.corrPincode);
		$scope.setCityState('C',$scope.corArea);
		$scope.proposalInput.policy.tcAgree = "N";
		checkInsuredData();
	}
	
	
	function getApplicationData (){
		
		$scope.loading = true;
		var appUrl = "/submitProposal/getProposal"
	           	   + "/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		
        var input = {
    			url: '',
    			data : {}
    	};
		
        input.url = appUrl;
		var res = $resource('./DataRequest', [], {
	          process: {
	             method: 'POST'}
	    });
		var tmp=res.process(JSON.stringify(input));
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				init();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})
	}
	
	
	function saveApplicationData (){
		
		var appUrl = "/submitProposal/saveProposal/" + $routeParams.productId
		           + "/" + $routeParams.rmId
        		   + "/" + $routeParams.customerId
        		   + "/" + $routeParams.appNo;
        console.log(appUrl);
		var input = {
    			url: '',
    			data : {}
    	};
		ProcessMedicalQuestions();
		setpolicyAddress();
		$scope.applicationData.proposalData = $scope.proposalInput;
		var res = $resource('./DataRequest', [], {
	          save: {
	             method: 'POST'}
	    });
		
		input.url = appUrl;
		input.data = $scope.applicationData;
		$scope.loading = true;
		var tmp=res.save(JSON.stringify(input));
		$scope.loading = false;
		
		/* Don't wait for return of save
		tmp.$promise.then(function(data){
			if (data.hasOwnProperty("result")){
				$scope.applicationData = JSON.parse(data.result);
				$scope.proposalInput=$scope.applicationData.proposalData;
				formatDates();
				console.log($scope.proposalInput);
			} else
			{
				// Show an error page
				
				
			}
			$scope.loading = false;			
		})*/
		
		
	}
	

	function formatDates(){
		
		$scope.proposalInput.policy.policyStartdate = new Date($scope.proposalInput.policy.policyStartdate);
			
		if (typeof($scope.proposalInput.proposer.proposerDob) != "undefined" && $scope.proposalInput.proposer.proposerDob != ""){
			$scope.proposalInput.proposer.proposerDob = new Date($scope.proposalInput.proposer.proposerDob.substr(0, 10));	
		}
		
		if (typeof($scope.proposalInput.proposer.nomineeDob) != "undefined" && $scope.proposalInput.proposer.nomineeDob != ""){
			$scope.proposalInput.proposer.nomineeDob = new Date($scope.proposalInput.proposer.nomineeDob.substr(0, 10));
		}
		
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeDob = new Date($scope.proposalInput.proposer.appointeeDob.substr(0, 10));
		}
		
		
		
	}
	
	function initializeMedicalQuestions(){
		
		for(var i=0; i<$scope.medicalQuestions.length; i++){
			$scope.medicalQuestions[i].details = [];
			$scope.medicalQuestions[i].exists = "";
		}
		$scope.insureds = [];
		angular.forEach($scope.proposalInput.insured,function(insured,key){
			
			var insuredId = insured.insuredId;
			var insuredName = insured.firstName + " " + insured.lastName;
			
			angular.forEach(insured.pedList,function(item,key){
				var code = item.pedCode;
				for(var i=0; i<$scope.medicalQuestions.length; i++){
					if ($scope.medicalQuestions[i].code == code){
						if ($scope.medicalQuestions[i].exists == ""){
							$scope.medicalQuestions[i].exists = item.exists;
						}
						if (item.exists == 'Y'){
							$scope.medicalQuestions[i].exists = "Y";
							angular.forEach(item.pedData,function(illness,key){
								var detail = {};
								detail.insuredId = insuredId.toString();
								detail.insuredName = insuredName;
								detail.code = code;
								detail.desc = illness.desc;
								$scope.medicalQuestions[i].details.push(detail);
							});
							
						}
					}
				}
				
			})
		})
		console.log($scope.medicalQuestions);
	}
	
	$scope.AddMedicalRow = function(code) {
		
		var detail = {};
		detail.insuredId = "";
		detail.conditionSince = "";
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.push(detail);
				break;
			}
		}
		$scope.medicalPageComplete();
	}
	
	$scope.DeleteMedicalRow = function(code, j){
		
		for (var i = 0; i<$scope.medicalQuestions.length;i++){
			if ($scope.medicalQuestions[i].code == code){
				$scope.medicalQuestions[i].details.splice(j);
				break;
			}
		}
		$scope.medicalPageComplete();
	}
	
	function ProcessMedicalQuestions(){
		
		// initialize the medical detail Array
		
		angular.forEach($scope.proposalInput.insured,function(insured,key1){
			angular.forEach(insured.pedList,function(item,key2){
				item.pedData = []; // Use an array even if there is only one item
				item.exists = "N";
			})
		});
		
		// Populate details into each insurer.
		angular.forEach($scope.medicalQuestions,function(question,key){
			if (question.exists == "Y"){
				angular.forEach(question.details,function(medDetail,key){
					 var code = question.code;
					 var insuredId = medDetail.insuredId;
					 // Check the insured and the question to which the detail belongs
					 angular.forEach($scope.proposalInput.insured,function(insured,key1){
						 if (insured.insuredId == insuredId) {
							 // check for question
							 angular.forEach(insured.pedList,function(item,key2){
								 if (item.pedCode == code){
									 item.exists = "Y";
									 var pedDetails = {}
						    	  	 pedDetails.desc = medDetail.desc;
									 item.pedData.push(pedDetails);								 
								 }
							 })
						 }
					 })			
				})
				console.log($scope.proposalInput.insured)
			}
		})
		
	}
	

	
	 function getAreasFromPincode (type, pincode){
			
			var re = new RegExp("^[0-9]{6}$");
			if (re.test(pincode)){
				
				var input={url:'',data:{}}
		    	
				input.url="/master/getDetailsForPincode/" + $scope.proposalInput.insurerId + "/" + pincode;
				
		    	var res = $resource('./DataRequest', [], {
			          save: {
			             method: 'POST'}
			    });
				
				var tmp = res.save(input);
				tmp.$promise.then(function(data){
					if (data.hasOwnProperty("result")){
						 if (type == "P"){
							 $scope.areasPolicy = data.result;
							 $scope.setCityState("P", $scope.polArea)
						 } else
						 {
							 $scope.areasCorr = data.result; 
							 $scope.setCityState("C", $scope.corArea)
						 }
					}
					
					
				})		
			}
			
			
			
		}
		
		$scope.setCityState = function(type, area){
			
			var areas = []
			if (typeof(area.areaId) != "undefined"){
				 if (type == "P"){
					 $scope.proposalInput.proposer.policyCity = area.cityName;
					 $scope.proposalInput.proposer.policyCityCode = area.cityId;
					 $scope.proposalInput.proposer.policyState = area.stateName;
					 $scope.proposalInput.proposer.policyStateCode = area.stateId;
					 $scope.proposalInput.proposer.policyArea = area.areaName;
					 $scope.proposalInput.proposer.policyAreaCode = area.areaId;
					 
				 } else
				 {
					 $scope.proposalInput.proposer.corrCity = area.cityName;
					 $scope.proposalInput.proposer.corrCityCode = area.cityId;
					 $scope.proposalInput.proposer.corrState = area.stateName;
					 $scope.proposalInput.proposer.corrStateCode = area.stateId;
					 $scope.proposalInput.proposer.corrArea = area.areaName;
					 $scope.proposalInput.proposer.corrAreaCode = area.areaId;
				 }
			}
		}
		
	function getMaster(productId, masterId){
		$scope.insurerMaster[masterId] = [];
		var tmp=brokeredgefactory.getMaster(productId, masterId);
		tmp.$promise.then(function(data){
			//console.log(data);
			$scope.insurerMaster[masterId]= data['result'];
			if (masterId == "Relationship" || masterId == "NomineeRel" || masterId == "AppointeeRel"){
				addGendertoRel(masterId);
			}
		})
	}

	
	function getQuestions(productId, type){
		var questions = [];
		 var input = {
	    			url: '/master/getQuestions/' + productId + "/" + type,
	    			data : {}
	    	};
	        
			var res = $resource('./DataRequest', [], {
		          process: {
		             method: 'POST'}
		    });
		var tmp=res.process(input);
		tmp.$promise.then(function(data){
			//console.log(data);
			if (data.hasOwnProperty("result")){
				if (type == "PedList"){
					$scope.medicalQuestions = data.result;
					initializeMedicalQuestions();
				}
				
			}
			//console.log($scope.insurerMaster);
		})
		
	}
	
	$scope.getMasterValue = function(masterName, masterId){
		// 
		var masterValue = masterId;
		var values = $scope.insurerMaster[masterName];
		
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == masterId){
					masterValue = values[i].value; 
					break;
				}
			}
		}
		return masterValue; 
	}
	
	$scope.getQuestionText = function(type, id){
		// 
		var questionText = "";
		var values = [];
		if (type == "PedList"){
			values = $scope.medicalQuestions;
		}

				
		if (typeof(values) != 'undefined'){
			for (var i = 0; i<values.length; i++){
				if (values[i].id == id){
					questionText = values[i].value; 
					break;
				}
			}
		}
		return questionText; 
	}
	
	function addGendertoRel (masterId){
		var records = [];
		angular.forEach($scope.insurerMaster[masterId],function(val,key){
			var record = val;
			record.gender = getRelGender(val.value);
			records.push(record);
		});
		$scope.insurerMaster[masterId] = records;
	}
	
	function checkInsuredData()
     {
		$scope.pageErrors.insuredError= false;
		$scope.pageErrors.insuredErrorText = "";
		$scope.proposalInput.proposer.title= "";
		$scope.proposalInput.proposer.firstName = "";
		$scope.proposalInput.proposer.middleName = "";
		$scope.proposalInput.proposer.lastName= "";
		$scope.proposalInput.proposer.gender = "";
		$scope.proposalInput.proposer.proposerDob =  "";
		$scope.proposalInput.proposer.occupationType= "";
		$scope.proposalInput.proposer.maritalStatus= "";
		$scope.proposalInput.proposer.designation = "";
		$scope.proposalInput.proposer.business = "";
		$scope.isProposerInsured = false;
		$scope.proposalInput.proposer.isPrimaryInsured = "N";
		$scope.proposalInput.proposer.IsProposerInsured = "N";
    	var selfCount = 0;
    	var paCount = 0;
    	var childCount = 0;
    	var hasDependentParent = false;
    	var hasNonParent = false;
    	angular.forEach($scope.proposalInput.insured, function(val,key){
    		
    		if (val.relationshipId == "1"){
    			
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			if (val.type == "C"){ // Proposer cannot be a child.
    				$scope.pageErrors.insuredError= true;
    				$scope.pageErrors.insuredErrorText = "Child cannot be a proposer";
    			}
    			selfCount++;
    			$scope.isProposerInsured = true;
    			console.log($scope.proposalInput.proposer);
    		}
    		if (val.type == "C") {
    			childCount++;
    		}
    		if (val.isPersonalAccidentApplicable=='Y') {
    			paCount++;
    		}
    		if (val.relationshipId == "4"){
    			hasDependentParent = true;
    		} else
    		{
    			hasNonParent = true;
    		}
    	});
    	
    	// PA Cover for Comprehensive Plan
    	if ($scope.proposalInput.productId == "129HL01F01" && paCount != 1) {
			$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Must select exactly one member with PA Cover";
    	}
    	if (selfCount>1) {
			$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Multiple Self Relations not allowed";
    	}
    	if (childCount> 0 && selfCount==0) {
    		$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Children can only be insured with Self";
    	}
    	
    	if (hasDependentParent && hasNonParent) {
    		$scope.pageErrors.insuredError= true;
			$scope.pageErrors.insuredErrorText = "Dependent parents cannot be insured with anyone else";
    	}
    	
     }
	
	$scope.validateProposer=function() {
		
		$scope.pageErrors.proposerError= false;
		$scope.pageErrors.proposerErrorText = "";
		if ($scope.proposalInput.proposer.socialStatus == '1'
		&& $scope.proposalInput.proposer.socialStatusBpl == '0'
		&& $scope.proposalInput.proposer.socialStatusUnorganized == '0'
		&& $scope.proposalInput.proposer.socialStatusDisabled == '0'
		&& $scope.proposalInput.proposer.socialStatusInformal == '0'
		) {
			$scope.pageErrors.proposerError= true;
			$scope.pageErrors.proposerErrorText = "At least one of the social indicators must be selected";
		}
		
		if ($scope.proposalInput.proposer.gstNo && $scope.proposalInput.proposer.pan != "" && $scope.proposalInput.proposer.gstNo != "" ){
			var str = $scope.proposalInput.proposer.gstNo.substr(2, 10);
			if ($scope.proposalInput.proposer.pan != str){
				$scope.pageErrors.proposerError= true;
				$scope.pageErrors.proposerErrorText = "Pan No and GST Number must Match";
			}
		}
    }
	
	function populateProposer() {
		
		angular.forEach($scope.proposalInput.insured,function(val,key){
    		
    		if (val.relationshipId == "Self"){

    			$scope.isProposerInsured = true;
        		$scope.proposalInput.proposer.isPrimaryInsured = "Y";
    			$scope.proposalInput.proposer.IsProposerInsured = "Y";
    			$scope.proposalInput.proposer.title= val.title;
    			$scope.proposalInput.proposer.firstName = val.firstName;
    			$scope.proposalInput.proposer.middleName = val.middleName;
    			$scope.proposalInput.proposer.lastName= val.lastName;
    			$scope.proposalInput.proposer.gender = val.gender;
    			$scope.proposalInput.proposer.proposerDob = new Date(val.dob);
    			$scope.proposalInput.proposer.occupationType=val.occupationType;
    			$scope.proposalInput.proposer.maritalStatus=val.maritalStatus;
    			if(val.occupationType=='SALARIED'||val.occupationType=='OTHERS')
    			{
    				$scope.proposalInput.proposer.designation =val.designation;
    			
    			}
    			else if(val.occupationType=='SELF EMPLOYED')
    			{
    		
    				$scope.proposalInput.proposer.business =val.business;
    			}
    		}
		})
	}
	
	$scope.medicalPageComplete = function(){
		
		var complete = true;
		
		$scope.pageErrors.medicalError = false;
		$scope.pageErrors.medicalErrorText = "";
		
		for (var i =0; i<$scope.medicalQuestions.length; i++){
			if ($scope.medicalQuestions[i].exists == "Y"){
				complete = false;
				if ($scope.medicalQuestions[i].details && $scope.medicalQuestions[i].details.length> 0){
						complete = true;					
				}
				if( $scope.medicalQuestions[i].id == "criticalIllness") {
					$scope.pageErrors.medicalError = true;
					$scope.pageErrors.medicalErrorText = "Cannot buy policy online if insured has critical Illness";
					break;
				}
			} else
			{
				$scope.medicalQuestions[i].details = [];
			}
			if (!complete){
				$scope.pageErrors.medicalError = true;
				$scope.pageErrors.medicalErrorText = "Please complete the details of the medical condition.";
				break;
			}
			
			
		};
		return complete;
	}	
	
	$scope.lsPageComplete = function(){
		
		var complete = true;
			
		for (var i =0; i<$scope.lifeStyleQuestions.length; i++){
			if ($scope.lifeStyleQuestions[i].exists == "Y"){
				complete = false;
				for (var j =0; j<$scope.proposalInput.lsDetails.length; j++){
					if ($scope.proposalInput.lsDetails[j].questionId == $scope.lifeStyleQuestions[i].id){
						complete = true;
						break;
					}
				}
			}
			if (!complete){
				break;
			}
		};
		return complete;
	}
	
	$scope.setGender=function(val)
	{
		if($scope.proposalInput.proposer.title=='Mr') {
			$scope.proposalInput.proposer.gender='M';
		}
		else if($scope.proposalInput.proposer.title=='Mrs' || $scope.proposalInput.proposer.title=='Ms')
		{
			$scope.proposalInput.proposer.gender = "F";
		}
	}
	
	function setCover (covers){
		angular.forEach(covers,function(val,key){
			var cover = {};
			cover.name = val;
			$scope.proposalInput.covers.push(cover);			
		})
	}
	
	
	$scope.calulateBmi=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			
			if(key.type=='A')
				{
				var heighttm=$scope.proposalInput.insured[val].height*0.01;
				var bmi=$scope.proposalInput.insured[val].weight/(heighttm*heighttm);
				$scope.proposalInput.insured[val].bmi=parseInt(bmi)
				}
			
		})
	}

	
	function getRelGender(rel){
		var gender = "MF";
		if(rel=='Wife'  || rel=='Aunt'  || rel=='Daughter'  || rel=='Daughter in law'  || rel=='Grand Daughter'  || rel=='Grand Mother'  || rel=='Mother in law'  || rel=='Mother'   || rel=='Niece'  || rel=='Sister in law'  || rel=='Sister'){
			gender = "F";
				
		} else if (rel=='Brother in law'  || rel=='Brother'  || rel=='Father in law'  || rel=='Father'  || rel=='Grand Father'  || rel=='Grand Son'  || rel=='Husband'  || rel=='Nephew'   || rel=='Son in law'  || rel=='Son'  || rel=='Uncle'){
			gender = "M";
		}
		return gender;
	}

	//Nominee
	$scope.calculateAge=function(dob)
	{
		$scope.proposalInput.proposer.nomineeAge=brokeredgefactory.calculateAge(dob)
	}
	
	
	$scope.checkMedicalQuition=function()
	{
		angular.forEach($scope.proposalInput.insured,function(key,val){
			angular.forEach($scope.proposalInput.insured[val].pedList,function(k,l){
				if(k.exists=='Y')
				{
					$scope.errorMessageMedical="You can't buy policy online"+$scope.proposalInput.insured[val].firstName+" has medical condition";
				}
			})
		})
	}
	//next tab 
	//next section
	$scope.back=function(index)
	{
		$scope.active=index;
	}
	$scope.nextSection = function(section)
	{
		
		switch (section){
		case "1":
			$scope.calulateBmi();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active=1;
			$scope.tabStatus.firstComplete = 'done';
			break;
		case "2":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =2;
			$scope.tabStatus.secondComplete = 'done';
			break;
		case "3":
			populateProposer();
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =3;
			$scope.tabStatus.thirdComplete='done';			
			break;
		case "4":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =4;
			$scope.tabStatus.fourthComplete='done';
			break;
		case "5":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.active =5;
			$scope.tabStatus.fifthComplete = 'done';
			
			break;
		case "6":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.proposalInput.policy.policyStartdate=$filter('date')(new Date($scope.proposalInput.policy.policyStartdate),'yyyy-MM-dd');
			$scope.active =6;
			$scope.tabStatus.sixthComplete='done';
			break;
		case "7":
			$scope.proposalError={key:"",errorPirnt:[]}
			$scope.proposalInput.policy.policyStartdate=$filter('date')(new Date($scope.proposalInput.policy.policyStartdate),'yyyy-MM-dd');
			$scope.proposalInput.proposer.proposerDob=$filter('date')(new Date($scope.proposalInput.proposer.proposerDob),'yyyy-MM-dd');
			console.log(JSON.stringify($scope.proposalInput))
			$scope.active=7;
			$scope.tabStatus.seventhComplete='done';
		}
		
		saveApplicationData ();
	}
	
	function cleanArray(actual) {
		  var newArray = new Array();
		  for (var i = 0; i < actual.length; i++) {
		    if (actual[i]) {
		      newArray.push(actual[i]);
		    }
		  }
		  return newArray;
		}
	
	function populateCoverData(){
		
		angular.forEach($scope.proposalInput.addons,function(value,key){
			
			if(value.isSelected == "Y") {
				var cover = {};
				cover.coverId = value.coverId;
				cover.isSelected = "Y";
				$scope.proposalInput.covers.push(cover);
			}
			
		})
		
		// if Product is comprehensive force a PA cover
		if ($scope.proposalInput.policy.productId == "129HL01F01"){
			$scope.proposalInput.covers = [];
			var cover = {};
			cover.coverId = "PA";
			cover.isSelected = "Y";
			$scope.proposalInput.covers.push(cover);
			
		}
		
		
	}
	
	$scope.samePolicyCorrAddress=function( flag)
	{
		if (flag == "Y"){
			$scope.proposalInput.proposer.corrAddressLine1=$scope.proposalInput.proposer.policyAddressLine1;
			$scope.proposalInput.proposer.corrAddressLine2=$scope.proposalInput.proposer.policyAddressLine2;
			$scope.proposalInput.proposer.corrAddressLine3=$scope.proposalInput.proposer.policyArea;
			$scope.proposalInput.proposer.corrLandmark=$scope.proposalInput.proposer.policyLandmark;
			$scope.proposalInput.proposer.corrStatecode=$scope.proposalInput.proposer.policyStatecode;
			$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
			$scope.proposalInput.proposer.corrCitycode =$scope.proposalInput.proposer.policyCitycode;
			$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
			$scope.proposalInput.proposer.corrAreaCode =$scope.proposalInput.proposer.policyAreaCode;
			$scope.proposalInput.proposer.corrArea =$scope.proposalInput.proposer.policyArea;
			$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
		} else
		{
			$scope.proposalInput.proposer.corrAddressLine1= "";
			$scope.proposalInput.proposer.corrAddressLine2= "";
			$scope.proposalInput.proposer.corrAddressLine3= "";
			$scope.proposalInput.proposer.corrLandmark= "";
			$scope.proposalInput.proposer.corrStatecode= "";
			$scope.proposalInput.proposer.corrState = "";
			$scope.proposalInput.proposer.corrCitycode = "";
			$scope.proposalInput.proposer.corrCity = "";
			$scope.proposalInput.proposer.corrAreaCode = "";
			$scope.proposalInput.proposer.corrArea = "";
			$scope.proposalInput.proposer.corrPincode = "";
		}
		
		 
	}
	
	function CopyPolicyAddress(){

		$scope.proposalInput.proposer.corrAddressLine1=$scope.proposalInput.proposer.policyAddressLine1;
		$scope.proposalInput.proposer.corrAddressLine2=$scope.proposalInput.proposer.policyAddressLine2;
		$scope.proposalInput.proposer.corrAddressLine3=$scope.proposalInput.proposer.policyArea;
		$scope.proposalInput.proposer.corrLandmark=$scope.proposalInput.proposer.policyLandmark;
		$scope.proposalInput.proposer.corrStateCode=$scope.proposalInput.proposer.policyStateCode;
		$scope.proposalInput.proposer.corrState =$scope.proposalInput.proposer.policyState;
		$scope.proposalInput.proposer.corrCityCode =$scope.proposalInput.proposer.policyCityCode;
		$scope.proposalInput.proposer.corrCity =$scope.proposalInput.proposer.policyCity;
		$scope.proposalInput.proposer.corrAreaCode =$scope.proposalInput.proposer.policyAreaCode;
		$scope.proposalInput.proposer.corrArea =$scope.proposalInput.proposer.policyArea;
		$scope.proposalInput.proposer.corrPincode =$scope.proposalInput.proposer.policyPincode;
	}

	
	function setpolicyAddress(){
 		
		if ($scope.proposalInput.proposer.policyRegAddressSame == 'Y'){
			CopyPolicyAddress();
		}
		var addresses = [];
		var address = {}
		address.addressType = "C";
 		address.addressLine1 = $scope.proposalInput.proposer.corrAddressLine1;
		address.addressLine2 = $scope.proposalInput.proposer.corrAddressLine2;
		address.addressLine3 = $scope.proposalInput.proposer.corrAddressLine3;
		address.areaId = $scope.proposalInput.proposer.corrAreaCode;
		address.areaName = $scope.proposalInput.proposer.corrArea;
		address.city = $scope.proposalInput.proposer.corrCity;
		address.cityCode = $scope.proposalInput.proposer.corrCitycode;
		address.state = $scope.proposalInput.proposer.corrState;
		address.stateCode = $scope.proposalInput.proposer.corrStatecode;
		address.pincode = $scope.proposalInput.proposer.corrPincode;
		addresses.push(address);
		
		var address = {}
		address.addressType = "P";
 		address.addressLine1 = $scope.proposalInput.proposer.policyAddressLine1;
		address.addressLine2 = $scope.proposalInput.proposer.policyAddressLine2;
		address.addressLine3 = $scope.proposalInput.proposer.policyAddressLine3;
		address.areaId = $scope.proposalInput.proposer.policyAreaCode;
		address.areaName = $scope.proposalInput.proposer.policyArea;
		address.city = $scope.proposalInput.proposer.policyCity;
		address.cityCode = $scope.proposalInput.proposer.policyCitycode;
		address.state = $scope.proposalInput.proposer.policyState;
		address.stateCode = $scope.proposalInput.proposer.policyStatecode;
		address.pincode = $scope.proposalInput.proposer.policyPincode;
		addresses.push(address);
		
		$scope.proposalInput.proposer.address = angular.copy(addresses);
		
		var contacts = []
		var contact = {};
		contact.contactType = "mobile";
		contact.contactText = $scope.proposalInput.proposer.mobile;
		contacts.push(contact);
		var contact = {};
		contact.contactType = "email";
		contact.contactText = $scope.proposalInput.proposer.email;
		contacts.push(contact);
		$scope.proposalInput.proposer.contacts = angular.copy(contacts);
		
 	}
	
	$scope.makePayment=function()
	{
		//$scope.proposalInput.proposer.proposerDob=$(filter);
		
		// Populate Medical and LifeStyle data
		populateCoverData();
		$scope.proposalInput.proposer.nomineeName = $scope.proposalInput.proposer.nomineeFirstName + " " + $scope.proposalInput.proposer.nomineeLastName;
		if (typeof($scope.proposalInput.proposer.appointeeDob) != "undefined" && $scope.proposalInput.proposer.appointeeDob != ""){
			$scope.proposalInput.proposer.appointeeAge = brokeredgefactory.calculateAge($scope.proposalInput.proposer.appointeeDob);
		}
		console.log("http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId);
		if ($scope.proposalInput.policy.floater == 'N'){
			$scope.proposalInput.policy.floaterSi = $scope.proposalInput.policy.sumInsured;
		}
		
		var healthProposal = angular.copy($scope.proposalInput);
		healthProposal.proposer.proposerDob = $filter('date')(new Date(healthProposal.proposer.proposerDob),'yyyy-MM-dd');
		healthProposal.proposer.nomineeDob = $filter('date')(new Date(healthProposal.proposer.nomineeDob),'yyyy-MM-dd');
		healthProposal.policy.policyStartdate = $filter('date')(new Date(healthProposal.policy.policyStartdate),'yyyy-MM-dd');
		
		var input={url:'',data:{}}
		input.url='/submitProposal/health/'+$scope.proposalInput.insurerId+'/'+$scope.proposalInput.productId;
		input.data=angular.copy(healthProposal);
		var tmp=brokeredgefactory.getQuoteProposal().processRequest({}, JSON.stringify(input));
		$scope.loading = true;
		tmp.$promise.then(function(data){
			$scope.loading = false;
				if(data.hasOwnProperty("error"))
				{
					$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
					var str=data['error'];
					$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
					console.log($scope.proposalError)
					//$scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"passedPremium":data["passedPremium"]})
					if(data['error']=="Premium Mismatch")
					{
					 	
					 $scope.newPreMiumMismatch({"premiumPayable":data["premiumPayable"],"premiumPassed":data["premiumPassed"]})
					}
				}
				else
				{
				$scope.proposalError={key:"",errorPirnt:[]}
				
				//$scope.returnUrl="http://dbstest.us-east-1.elasticbeanstalk.com/#!/PostPayment/"+$routeParams.productId;
				console.log(data);
				var url = data.online_purchase_link;
				$window.location.href = url;
				//$window.location.href =( 'https://www.royalsundaram.net/web/dtctest/paymentgateway?agentId='+data.agentId+'&premium='+data.premium+'&apikey='+data.apikey+'&quoteId='+data.quoteId+'&version_no='+data.version_no+'&strFirstName='+data.strFirstName+'&strEmail='+data.strEmail+"&isQuickRenew="+data.isQuickRenew+"&crossSellProduct="+data.crossSellProduct+"&crossSellQuoteid="+data.crossSellQuoteid+"&returnUrl="+data.returnUrl+"&vehicleSubLine="+data.vehicleSubLine+"&elc_value="+data.elc_value+"&paymentType="+data.paymentType);
				
				}
			
		
		},
 		  function(error) {
 			$scope.proposalError={key:"Proposal Submission couldn't be completed due to the following errors",errorPirnt:[]}
			var str="Unexpected error from Insurer while processing the policy";
			$scope.proposalError.errorPirnt=cleanArray(str.split(";"))
			$scope.loading = false;
 		  })
	}
	
	getApplicationData();
	
	$scope.AddMedicalDetails = function(questionId){
		
		var input = {};
		input.questionId = questionId;
		var insureds = [];
		
		angular.forEach($scope.proposalInput.insured,function(val,key){
			var insured = {};
			insured.id = key;
			insured.name = val.firstName + " " + val.lastName;
			insureds.push(insured);
		});
		
		input.insureds = insureds;
	
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/health/royalSundaram/medicalQuestionDetail.html',
		      controller: 'rsaMedicalQuestionDetailCtrl',
		      size: 'md',
		      resolve: {
		    	  'input': function () {
		          return input;
		        }
		      }
		});
		modalInstance.result.then(function (questionDetail) {
	      console.log(questionDetail);
	      if( typeof(questionDetail) != 'undefined' && questionDetail.action == "add")
	      	{
	    	  //Add Question details
	      	  $scope.proposalInput.medicalDetails.push(questionDetail);
	      	  console.log($scope.proposalInput.medicalDetails);
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	$scope.AddLSDetails = function(questionId){
		
		var input = {};
		input.questionId = questionId;
		var insureds = [];
		
		angular.forEach($scope.proposalInput.insured,function(val,key){
			var insured = {};
			insured.id = key;
			insured.name = val.firstName + " " + val.lastName;
			insureds.push(insured);
		});
		
		input.insureds = insureds;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/health/royalSundaram/lifestyleQuestionDetail.html',
		      controller: 'rsaLSQuestionDetailCtrl',
		      size: 'md',
		      resolve: {
		    	  'input': function () {
		          return input;
		        }
		      }
		});
		modalInstance.result.then(function (questionDetail) {
	      console.log(questionDetail);
	      if( typeof(questionDetail) != 'undefined' && questionDetail.action == "add")
	      	{
	    	  //Add Question details
	      	  $scope.proposalInput.lsDetails.push(questionDetail);
	      	  console.log($scope.proposalInput.lsDetails);
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	$scope.showTC=function(){
		
		var modalInstance=$uibModal.open({
		  animation: $scope.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      controller : 'tcCtrl',
	      templateUrl: 'templates/proposal/health/apollo/tc.html',
	      size: 'lg'
		});
		modalInstance.result.then(function (agree) {
	      console.log(agree);
	      if( typeof(agree) != 'undefined')
	      	{
	    	  $scope.proposalInput.policy.tcAgree = agree;
	      	}
      }, function () {
          //console.log('Modal dismissed at: ' + new Date());
      });
		
	}
	
	
	$scope.newPreMiumMismatch=function(item)
	{
		$scope.items=item;
		var modalInstance=$uibModal.open({
			  animation: $scope.animationsEnabled,
		      ariaLabelledBy: 'modal-title',
		      ariaDescribedBy: 'modal-body',
		      templateUrl: 'templates/proposal/premiumModal.html',
		      controller: 'premiumModalCtrl',
		      size: 'md',
		      resolve: {
		    	  'premiums': function () {
		          return $scope.items;
		        }
		      }
		});
		modalInstance.result.then(function (selectedProduct) {
        $scope.selected = selectedProduct;
        if($scope.selected!=undefined)
        	{
        	//basePremium.
        	$scope.proposalInput.policy.premiumPayable= $scope.selected.premiumPayable;
        	$scope.makePayment();
        	
        	}
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
	}
	
	
	$scope.backPreviousPage=function()
	{
		window.history.back();
	}
	$scope.rowWidth=function(a)
	{
		return 100/parseInt(a);
	}
	$scope.filterValue = function($event){
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
        }
     };
     
     $scope.appointeeAge=function(dob) {
 		$scope.proposalInput.proposer.appointeeAge=brokeredgefactory.calculateAge(dob)
 	}
     

     
     // Change State
     $scope.$watch('proposalInput.proposer.policyPincode', function(newValue, oldValue) {
         
     	if ((typeof newValue != "undefined")&& (newValue != "")) {
     		getAreasFromPincode("P", newValue);
     	}
     	
     });
     
     $scope.$watch('proposalInput.proposer.corrPincode', function(newValue, oldValue) {
         
     	if ((typeof newValue != "undefined")&& (newValue != "")) {
     		getAreasFromPincode("C", newValue);
     	}
     	
     });
     
     // Watch function
     $scope.$watch('proposalInput.policy.policyStartdate', function(newValue, oldValue) {
         
         if ((typeof newValue != "undefined")&& (newValue != "")) {
         	
         	var date=new Date(newValue);
     		$scope.proposalInput.policy.policyEnddate=$filter('date')(new Date(date.getFullYear()+ Number($scope.proposalInput.policy.tenure),date.getMonth(),date.getDate()-1),'yyyy-MM-dd');
         }
     	
     });
     
}])