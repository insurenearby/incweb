/**
 * 
 */
"use strict";
brokeredgeApp.controller('brokeredgeLifeCtrl',['$scope','$location','brokeredgefactory','$filter','$resource',function($scope,$location,brokeredgefactory,$filter, $resource){
	$scope.anularIncome=brokeredgefactory.getAnualIncome();
	$scope.lifeInput = {
    		age : "18",
    		mma : "60",
    		rating : "NS",
    		pt : 0,
    		ppt : 0,
    		sa : 10000000,
    		mode : "1",
    		lob: "Life",
    		productType: "Term"
    		
    };
$scope.idvValid = false;

	var lifeInputObj = {
    		lob: "Life",
    		productType: "Term"
    };
	
	//submit form
	$scope.lifeSubmit=function()
	{
		var input={
				url:'',
				data:{},
			}
		   input.url="/master/getStatePinStd/"+$scope.lifeInput.pincode;
		   var tmp=brokeredgefactory.getPincode().processRequest({}, JSON.stringify(input));
		   tmp.$promise.then(function(data){
			   if(data.pincode==undefined){
					$scope.error="Invalid Pin code";
				
				}
				else
				{
					$scope.error="";
					$scope.lifeInput.age=brokeredgefactory.calculateAge($scope.lifeInput.lifeDob);
			    				    	
			    	$scope.lifeInput.dob=$filter('date')(new Date($scope.lifeInput.lifeDob), "yyyy-MM-dd");
			    	lifeInputObj.age = $scope.lifeInput.age;
			    	lifeInputObj.gender = $scope.lifeInput.gender;
			    	lifeInputObj.pt = $scope.lifeInput.pt;
			    	lifeInputObj.ppt = $scope.lifeInput.ppt;
			    	lifeInputObj.mode = $scope.lifeInput.mode;
			    	lifeInputObj.rating = $scope.lifeInput.rating;
			    	if ($scope.lifeInput.rating == "S"){
			    		lifeInputObj.smoker = "Yes";
			    	}else
			    	{
			    		lifeInputObj.smoker = "No";
			    	}
			    	$scope.lifeInput.mode = "LP";
			    	if ($scope.lifeInput.ppt == 1) {
			    		$scope.lifeInput.mode = "SP";
			    	}
			    	if ($scope.lifeInput.ppt == $scope.lifeInput.pt) {
			    		$scope.lifeInput.mode = "RP";
			    	}
			    	
			    	lifeInputObj.sa = $scope.lifeInput.sa;
			    	lifeInputObj.mma = $scope.lifeInput.mma;
			    	lifeInputObj.pincode = $scope.lifeInput.pincode;
			    	lifeInputObj.dob = $scope.lifeInput.dob;
			    	
			    	// Submit Quote Request
					submitQuoteRequest(lifeInputObj);
				
				}
		   })
	}
	
	
	$scope.filterValue = function($event){
		$scope.error="";
        if(isNaN(String.fromCharCode($event.keyCode))){
            $event.preventDefault();
            
        }
    };
    
    
	// SubmitQuoteRequest
	function submitQuoteRequest(quoteInput){
		var input = {
    			url: "",
    			data:quoteInput
    			
    	};
		
		var quotes = $resource('./DataRequest', {}, {
	          processRequest: {
	             method: 'POST'}
	    });
		
		input.url = "/quote/submitAllQuotesRequest/" + quoteInput.lob + "/" + quoteInput.productType;
		//console.log(JSON.stringify($scope.quoteInput))
		//console.log(JSON.stringify(input));
    	var tmp = quotes.processRequest({}, JSON.stringify(input));
    	$scope.loading = true;
    	tmp.$promise.then(function(data){
    		//console.log(data)
    		$scope.loading = false;
    		if (!data.hasOwnProperty("error")){
    			if (data.numResultsExpected == 0){
    				$scope.error = "No Quotes available for this selection";
    			} else
    			{
    				// Transfer Control to Quotes page
    				var url = "/quotes/Life/"+ quoteInput.productType +'/' + data.requestId +'/'+ data.numResultsExpected ;
    				console.log(url);
			    	$location.path(url);
    			}
    		} else
    		{
    			$scope.loading = false;
    			console.error(data);
	    	    var str=data.error;
				$scope.quoteError.errors=str.split(";");
				
			}
    		
    	}, function (error) {
    	    console.error(data);
    	    $scope.error ="Unexpected error processing quote Request";
			
    	});
	}
	// Date Picker format
	
    var dateToday = new Date();
	var yearMin = dateToday.getFullYear() - 60;
	var yearMax = dateToday.getFullYear() - 18;
    var monthToday = dateToday.getMonth();
    var dayToday = dateToday.getDate();
    $scope.dateOptions = {
    		maxDate: new Date(yearMax, monthToday, dayToday),
    		minDate: new Date(yearMin, monthToday, dayToday)
    };
    
    $scope.open1 = function() {
		$scope.popup1.opened = true;
	};

  
	$scope.setDate = function(year, month, day) {
		$scope.selectedOption.targetym = new Date(year, month, day);
	};

	$scope.formats = ['yyyy-MM-dd','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];
	$scope.altInputFormats = ['M!/d!/yyyy'];

	$scope.popup1 = {
		opened: false
	};
}])